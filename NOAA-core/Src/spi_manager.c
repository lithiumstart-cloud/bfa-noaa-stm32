/**
 * @file spi_manager.c
 * @brief Manages SPI transmissions and SPI ports 
 * @copyright 2020 EaglePicher Technologies
 * @author Miroslav Grubic
 * @date   October 25, 2020
**/

//############################## INCLUDES ######################################
#include "spi_manager.h"
#include "spi_hw.h"
#include "stdio.h"
#include "gpio.h"

//############################## DEFINES #######################################
#define SIZE_OF_BUFFER  (128)
#define MAX_NUM_SPI_PORTS (4)

//############################# TYPEDEFS #######################################
typedef enum
{
    ST_IDLE = 0,
    ST_TRANSFER_DONE,
} T_State;

typedef struct
{
    uint8_t portInx;
    uint8_t* dataBuffer;
    int nBytes;
    bool* isMsgSent;
} portList_t;

//######################### MODULE VARIABLES ###################################
static uint16_t bufferLength;
static uint16_t readIndex;
static uint16_t writeIndex;
static T_State state;
static portList_t circularBuffer[SIZE_OF_BUFFER];
static uint8_t numActivePorts;
static uint8_t portList[MAX_NUM_SPI_PORTS];

//####################### PUBLIC CODE ##########################################
void SPI_Init(void)
{
    bufferLength = 0;
    readIndex = 0;
    writeIndex = 0;
    numActivePorts = 0;
    state = ST_IDLE;
}

bool SPI_Update(void)
{
    switch(state)
    {
        case ST_IDLE:
            if(bufferLength > 0)
            {
                SPI_HW_StartMasterTxRx(circularBuffer[readIndex].portInx, circularBuffer[readIndex].dataBuffer, circularBuffer[readIndex].nBytes);
                state = ST_TRANSFER_DONE;
            }
        break;
        
        case ST_TRANSFER_DONE:
            if(SPI_HW_IsReceiveDone(circularBuffer[readIndex].portInx, NULL))
            {
                *circularBuffer[readIndex].isMsgSent = true;
                bufferLength--;
                readIndex++;
                
                if (readIndex == SIZE_OF_BUFFER) 
                {
                    readIndex = 0;
                }
                
                state = ST_IDLE;
            }
        break;
    }
    
    return false;
}

bool SPI_StartMasterTxRx(uint8_t portInx, uint8_t* data, int count, bool* isMsgSent)
{
    if (bufferLength == SIZE_OF_BUFFER)
    {
        printf("SPI: Buffer is full!\n");
        return false;
    }
    else
    {
        circularBuffer[writeIndex].portInx = portInx;
        circularBuffer[writeIndex].dataBuffer = data;
        circularBuffer[writeIndex].nBytes = count;
        circularBuffer[writeIndex].isMsgSent = isMsgSent;
        bufferLength++;
        writeIndex++;
    }
    
    if (writeIndex == SIZE_OF_BUFFER) 
    {
        writeIndex = 0;
    }
    
    return true;
}

bool SPI_Init_Master(uint8_t portInx, unsigned int spiClockHz, bool cpol, bool cpha, bool mssen)
{
    bool result = false;
    
    if(numActivePorts <= MAX_NUM_SPI_PORTS)
    {
        portList[numActivePorts++] = portInx;
        return SPI_HW_Init_Master(portInx, spiClockHz, cpol, cpha, mssen);
    }
    
    return result;
}