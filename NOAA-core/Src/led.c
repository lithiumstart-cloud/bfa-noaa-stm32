/**
 * @file led.c
 * @brief Manages the board LEDs
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-06
 *
**/

//########################## MODULE INCLUDES #################################

#include "led.h"
#include "bluflex_core.h"
#include "core_config.h"

#include <stdio.h>
//########################### MODULE DEFINES #################################

#define LED_PTRN_START_MASK         0x8000000000000000ULL

#define LED_OFF_PTRN                0x0000000000000000ULL //0b0000000000000000000000000000000000000000000000000000000000000000ULL ///< Always off
#define LED_SOLID_ON_PTRN           0xFFFFFFFFFFFFFFFFULL //0b1111111111111111111111111111111111111111111111111111111111111111ULL ///< Always on

#define LED_BLINK_SLOW_PTRN         0xFFFFFFFF00000000ULL //0b1111111111111111111111111111111100000000000000000000000000000000ULL ///< Blink Slow
#define LED_BLINK_FAST_PTRN         0xF800F800F800F800ULL //0b1111100000000000111110000000000011111000000000001111100000000000ULL ///< Blink Fast
#define LED_BLINK_ULTRA_FAST_PTRN   0x3333333333333333ULL //0b0011001100110011001100110011001100110011001100110011001100110011ULL ///< Blink Ultra Fast

#define LED_BLINK_DOUBLE_PTRN       0xF000F00000000000ULL //0b1111000000000000111100000000000000000000000000000000000000000000ULL ///< Blink Double
#define LED_BLINK_TRIPLE_PTRN       0xE003801C00000000ULL //0b1110000000000011100000000001110000000000000000000000000000000000ULL ///< Blink Triple
#define LED_BLINK_QUADRUPLE_PTRN    0xC006003001800000ULL //0b1100000000000110000000000011000000000001100000000000000000000000ULL ///< Blink Quadruple

#define LED_BLIP_SLOW_PTRN          0x8000000000000000ULL //0b1000000000000000000000000000000000000000000000000000000000000000ULL ///< Blip Slow
#define LED_BLIP_PTRN               0x8000000080000000ULL //0b1000000000000000000000000000000010000000000000000000000000000000ULL ///< Blip Normal
#define LED_BLIP_FAST_PTRN          0x8000800080008000ULL //0b1000000000000000100000000000000010000000000000001000000000000000ULL ///< Blip Fast
#define LED_BLIP_ULTRA_FAST_PTRN    0x8080808080808080ULL //0b1000000010000000100000001000000010000000100000001000000010000000ULL ///< Blip Ultra Fast

#define LED_WINK_PTRN               0xFFE7FFFFFFE7FFFFULL //0b1111111111100111111111111111111111111111111001111111111111111111ULL ///< Wink
#define LED_WINK_FAST_PTRN          0x7FFF7FFF7FFF7FFFULL //0b0111111111111111011111111111111101111111111111110111111111111111ULL ///< Wink Fast

#define LED_SLOW_WINK_PTRN          0x3FFFFFFFFFFFFFFCULL //0b0011111111111111111111111111111111111111111111111111111111111100ULL ///< Extra zeros are on the end here to accomodate pauses between version numbers

#define LED_CONNIPTION_PTRN         0x612E868C1C2B1709ULL //0b0110000100101110100001101000110000011100001010110001011100001001ULL ///< CONNIPTION

//########################## CONFIG VALUES #####################################

static struct
{
       uint32_t versionBitPeriod_ms; ///< Period of one 'bit' when in version mode
       uint32_t normalBitPeriod_ms; ///< Period of one 'bit' in normal mode
       uint32_t allOnAfterVersionPeriod_ms;

} configValues =
{
    .versionBitPeriod_ms = 10,
    .normalBitPeriod_ms = 20,
    .allOnAfterVersionPeriod_ms = 600,
};

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "versionBitPeriod_ms",
    "normalBitPeriod_ms",
    "allOnAfterVersion_ms",
};



//################################ LOOKUP TABLES #############################


static const uint64_t mode2Pattern[LMODE_NUMBER_MODES] =
{
   LED_OFF_PTRN,
   LED_SOLID_ON_PTRN,

   LED_BLINK_SLOW_PTRN, 
   LED_BLINK_FAST_PTRN,
   LED_BLINK_ULTRA_FAST_PTRN, 

   LED_BLINK_DOUBLE_PTRN,
   LED_BLINK_TRIPLE_PTRN, 
   LED_BLINK_QUADRUPLE_PTRN,

   LED_BLIP_SLOW_PTRN,
   LED_BLIP_PTRN,
   LED_BLIP_FAST_PTRN, 
   LED_BLIP_ULTRA_FAST_PTRN,

   LED_WINK_PTRN, 
   LED_WINK_FAST_PTRN, 
   LED_SLOW_WINK_PTRN,

   LED_CONNIPTION_PTRN,

};


//########################### MODULE VARS/TYPES ##############################


typedef enum
{
    NORMAL = 0,
    SHOWING_GENERIC_VERSION_NUM,
    DONE_SHOWING_ALL_OFF_PAUSE_A,
    DONE_SHOWING_ALL_ON,
    DONE_SHOWING_ALL_OFF_PAUSE_B,

} T_State;


struct sLed
{
    const char* name;
    T_Pin sPin;
    T_LMode modeNow;
    T_LMode stashedMode;
    struct
    {
        unsigned  isLocked : 1;
        unsigned  isActiveLo : 1;
        unsigned            : 6;
    };
    
};

static struct
{
    T_LED leds[MAX_NUM_LEDS];
    uint32_t length;
} ledList;


static uint64_t patternMask;
static T_Timer tmrLedBit;

static T_State currentState;


static uint8_t v_led_ind;
static uint8_t nVersionCycles;
static uint8_t versionValue_ind;
static T_LMode vLedPattern;

static T_Version appVersion;

//############################## PRIVATE FUNCTIONS #############################
static void setAllLeds(bool on);
static uint8_t getVersionValue(uint8_t ind);
static const char* mode2str(T_LMode mode);

//############################## MODULE CODE ###################################

void LED_Init()
{
    CORE_GetParsedAppVersion(&appVersion);
    ledList.length = 0;
    patternMask = LED_PTRN_START_MASK;
    TMR_StartRepeating(&tmrLedBit, configValues.normalBitPeriod_ms);
    currentState = NORMAL;
}

bool LED_Update(void)
{
    
    uint32_t ii;
    switch (currentState)
    {
        case NORMAL:
            if (TMR_HasTripped_ms(&tmrLedBit))
            {
                /*Set all of the LEDs forward one 'bit'. */
                for (ii=0; ii < ledList.length; ii++)
                {
                    T_LED* this = &ledList.leds[ii];
                    GPIO_WriteOutput(&this->sPin, (patternMask & mode2Pattern[this->modeNow]) ^ this->isActiveLo);
                }

                patternMask  >>= 1;
                if (patternMask == 0)
                {
                    patternMask = LED_PTRN_START_MASK;
                }
            }
        break;

        case SHOWING_GENERIC_VERSION_NUM:
            if (TMR_HasTripped_ms(&tmrLedBit))
            {
                  /*Set all of the LEDs to zero, except the current version LED. */
                uint32_t ii;
                for (ii=0; ii < ledList.length; ii++)
                {
                    T_LED* this = &ledList.leds[ii];
                    if (ii == v_led_ind)
                    {
                        GPIO_WriteOutput(&this->sPin, (patternMask & vLedPattern) ^ this->isActiveLo);
                    }
                    else
                    {
                       GPIO_WriteOutput(&this->sPin, this->isActiveLo); //To turn off, we do [0 ^ this->isActiveLo] === [this->isActiveLo]
                    }
                }

                patternMask >>= 1;

                if (patternMask == 0)
                {
                    /* We completed a cycle! */
                    patternMask = LED_PTRN_START_MASK;

                    nVersionCycles++;

                    if (nVersionCycles >= getVersionValue(versionValue_ind))
                    {
                        //move on to next state, with different LED

                        v_led_ind++;
                        if (v_led_ind >= ledList.length)
                        {
                            v_led_ind = 0;
                        }

                        versionValue_ind++;
                        /*Are there more version values to do? */
                        if (versionValue_ind < NUM_VERSION_VALUES)
                        {
                            //More to do
                            nVersionCycles = 0;
                            //We need to not blink anything if this version number is zero.
                            vLedPattern = getVersionValue(versionValue_ind) > 0 ? LED_SLOW_WINK_PTRN : LED_OFF_PTRN;

                        }
                        else
                        {
                          //Nope! All done. Turn all LEDs on to indicate we're done with version number.
                            currentState = DONE_SHOWING_ALL_OFF_PAUSE_A;
                            setAllLeds(false);
                            TMR_StartRepeating(&tmrLedBit, configValues.allOnAfterVersionPeriod_ms);
                        }
                    }
                    else
                    {
                        //We're still blnking out this version number. Stay
                        //in this state...
                    }
                }

            }
        break;
        
        case DONE_SHOWING_ALL_OFF_PAUSE_A:
            if (TMR_HasTripped_ms(&tmrLedBit))
            {
                currentState = DONE_SHOWING_ALL_ON;
                setAllLeds(true);
                TMR_StartRepeating(&tmrLedBit, configValues.allOnAfterVersionPeriod_ms);
            }
        break;
        
        case DONE_SHOWING_ALL_ON:
            if (TMR_HasTripped_ms(&tmrLedBit))
            {
                currentState = DONE_SHOWING_ALL_OFF_PAUSE_B;
                setAllLeds(false);
                TMR_StartRepeating(&tmrLedBit, configValues.allOnAfterVersionPeriod_ms);
            }
        break;
        
        
        case DONE_SHOWING_ALL_OFF_PAUSE_B:
            if (TMR_HasTripped_ms(&tmrLedBit))
            {
                //Reset the timer to the normal period.
                currentState = NORMAL;
                TMR_StartRepeating(&tmrLedBit, configValues.normalBitPeriod_ms);
            }
        break;
    }

    return true; //Always ready to sleep
}


T_LED* LED_CreatePin(const char* name, T_Pin xPin)
{
    return LED_Create(name, xPin, false);
}

T_LED* LED_Create(const char* name, T_Pin xPin, bool isActiveLo)
{
    if (ledList.length < MAX_NUM_LEDS)
    {
        T_LED* this     = &ledList.leds[ledList.length];
        this->name      = name;
        this->sPin.port = xPin.port;
        this->sPin.pin  = xPin.pin;
        if (!GPIO_IsValidPin(&this->sPin))
        {
            puts("LED: Invalid pin assignment!");
            return NULL;
        }

        this->modeNow     = LMODE_OFF;
        this->stashedMode = LMODE_OFF;
        this->isLocked    = 0;
        this->isActiveLo  = isActiveLo;
        //Set the pin as an output
        GPIO_SetDirection(&this->sPin, GPIO_IS_OUTPUT);
        GPIO_WriteOutput(&this->sPin, isActiveLo);  //To turn off, we do [0 ^ this->isActiveLo] === [this->isActiveLo]

        ledList.length++;
        return this;
    }

    puts("LED: Out of room.");
    return NULL;
}

bool LED_SetMode(T_LED* pLed, T_LMode mode)
{
    if (pLed)
    {
        if (mode < LMODE_NUMBER_MODES)
        {
            if (pLed->isLocked)
            {
                pLed->stashedMode = mode;
            }
            else
            {
                pLed->modeNow = mode;
            }
            
            return true;
        }
        else
        {
          puts("LED: Bad mode selection.");
        }
    }
    
    return false;
}

bool LED_SetNow(T_LED* pLed, bool isOn)
{
     if (pLed)
    {
          if (pLed->isLocked)
        {
            pLed->stashedMode = isOn ? LMODE_SOLID_ON : LMODE_OFF;
        }
        else
        {
            pLed->modeNow = isOn ? LMODE_SOLID_ON : LMODE_OFF;
        }
          
        GPIO_WriteOutput(&pLed->sPin, isOn ^ pLed->isActiveLo);
        
        return true;
    }
     
     return false;
}

void LED_DisplayVersion(uint32_t vInd)
{
    if (ledList.length == 0)
    {
        puts("LED: Cannot blink version - no LEDs!");
        return;
    }


    //Load the version pattern and the timer period for LED version blinks
    currentState = SHOWING_GENERIC_VERSION_NUM;
    TMR_StartRepeating(&tmrLedBit, configValues.versionBitPeriod_ms);

    vLedPattern = getVersionValue(0) ? LED_SLOW_WINK_PTRN : LED_OFF_PTRN;
    patternMask = LED_PTRN_START_MASK;

    nVersionCycles = 0;
    versionValue_ind = 0; //start with the first version valie

    v_led_ind = vInd < ledList.length ? vInd : 0;
}

void LED_StashAndLockOthers(T_LED* pLed)
{

    //Now we turn off all of the LEDs we just locked.
    uint32_t ii;
    for(ii = 0; ii < ledList.length; ii++)
    {
        T_LED* this = &ledList.leds[ii];
        if (this != pLed)
        {
            this->stashedMode = this->modeNow;
            this->isLocked = true;
            this->modeNow = LMODE_OFF;
        }
    }
    
}

void LED_UnstashAndUnlockAll(void)
{
    //reload the stashed states
    uint32_t ii;
    for(ii = 0; ii < ledList.length; ii++)
    {
        T_LED* this = &ledList.leds[ii];
        if (this->isLocked)
        {
            this->modeNow = this->stashedMode;
            this->stashedMode = LMODE_OFF;
            this->isLocked = false;
        }
    }
}

void LED_SetVersionBitPeriod_ms(uint32_t t_ms, bool announce)
{
    configValues.versionBitPeriod_ms = t_ms;
    
    if (announce)
    {
        printf("LED: Version bit period <-- %u ms.\r\n", configValues.versionBitPeriod_ms);
    }
}

void LED_SetNormalBitPeriod_ms(uint32_t t_ms, bool announce)
{
    configValues.normalBitPeriod_ms = t_ms;
    //Restart the timer..
    TMR_StartRepeating(&tmrLedBit, configValues.normalBitPeriod_ms);
    
    if (announce)
    {
        printf("LED: Normal bit period <-- %u ms.\r\n", configValues.normalBitPeriod_ms);
    }
}

//Handle command line inputs that had first token 'bal'
void LED_ProcessInput(const char* tokens[], uint8_t nTokens)
{
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("config\tView/adjust module configuration");
        puts("list\tList LED pins/modes.");
        puts("version [inx]\tBlink the application version starting at given index.");
        puts("inc-mode [inx]\tIncrement mode of LED[inx].");

    }
    else if (stricmp(tokens[0], "list") == 0)
    {
        uint32_t ii;
        puts("Inx\tName\tPin\tActvLo\tMode");
        for (ii =0; ii < ledList.length; ii++)
        {
            T_LED* this = &ledList.leds[ii];
            
            printf("[%02d]\t%s\t", ii, this->name);
            GPIO_PrintPin(&this->sPin);
            printf("\t%u\t%s\r\n",  this->isActiveLo, mode2str(this->modeNow));
            
        }
    }
    else if (stricmp(tokens[0], "version") == 0)
    {
        if (nTokens == 2)
        {
            int32_t ind = atoi(tokens[1]);
            if (ind >= 0 && ind < ledList.length)
            {
                LED_DisplayVersion(ind);
            }
            else
            {
                printf("Bad LED index choice: %d\r\n", ind);
            }
        }
        else
        {
              LED_DisplayVersion(0);
        }
    }
    else if (stricmp(tokens[0], "inc-mode") == 0)
    {
        if (nTokens == 2)
        {
            int32_t ind = atoi(tokens[1]);
            if (ind >= 0 && ind < ledList.length)
            {
                T_LED* this = &ledList.leds[(uint8_t) ind];
                
                this->modeNow++;
                if (this->modeNow >= LMODE_NUMBER_MODES)
                {
                    this->modeNow = (T_LMode) 0;
                }
                printf("\"%s\" Mode <-- %s\r\n", this->name, mode2str(this->modeNow));
            }
            else
            {
                printf("Bad LED index choice: %d\r\n", ind);
            }
        }
        else
        {
            puts("inc-mode [inx]");
        }
        
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
          CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, tokens, nTokens);
    }
    else
    {
        //Print error string.
    	Console_WriteByte('?');
    }
}

static const char* mode2str(T_LMode mode)
{
    switch (mode)
    {
        case LMODE_OFF: return "Off";
        case LMODE_SOLID_ON: return "On";

        case LMODE_BLINK_SLOW: return "BlinkSlow";
        case LMODE_BLINK_FAST: return "BlinkFast";
        case LMODE_BLINK_ULTRA_FAST: return "BlinkUltraFast";

        case LMODE_BLINK_DOUBLE: return "BlinkDouble";
        case LMODE_BLINK_TRIPLE: return "BlinkTriple";
        case LMODE_BLINK_QUADRUPLE: return "BlinkQuadruple";

        case LMODE_BLIP_SLOW: return "BlipSlow";
        case LMODE_BLIP: return "Blip";
        case LMODE_BLIP_FAST: return "BlipFast";
        case LMODE_BLIP_ULTRA_FAST: return "BlipUltraFast";

        case LMODE_WINK: return "Wink";
        case LMODE_WINK_FAST: return "WinkFast";
        case LMODE_WINK_SLOWER: return "WinkSlow";

        case  LMODE_CONNIPTION: return "Conniption";
    }

    return "???";
}

static void setAllLeds(bool on)
{
    uint32_t ii;
    for (ii=0; ii < ledList.length; ii++)
    {
        T_LED* this = &(ledList.leds[ii]);
        GPIO_WriteOutput(&(this->sPin), on ^ this->isActiveLo);
    }
}



static uint8_t getVersionValue(uint8_t ind)
{
    switch (ind)
    {
        case 0: return appVersion.x;
        case 1: return appVersion.y;
        case 2: return appVersion.z;
    }

    LOG_Report(LOG_MOD_LED, LOG_LVL_FATAL,  LOG_MSG_INDEX_OUT_OF_RANGE, __LINE__);
    return 0;
}
