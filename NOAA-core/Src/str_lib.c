/**
 * @file str_lib.c
 * @brief One place to keep strings together, to avoid duplication.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-09
 *
**/


#include "str_lib.h"

 const char STR_NewLine[] = "\r\n";
 const char STR_dC[] = "dC";

 const char STR_CommandPrompt[] = ">>"; 

 const char STR_cd[] = "[c,d]";
 const char STR_EscOpen[] = "\x1b[";
 const char STR_NewLineTab[] = "\r\n\t";



 const char STR_CurHome[] = "\x1b[[H";

 
 //String literals
 
 //A
 const char STR_a2d[] = "A2D"; 
 const char STR_acc[] = "Acc";
 const char STR_afe[] = "Afe";
 const char STR_amb[] = "Amb";
 const char STR_amps[] = "Amps";
 const char STR_alarm[]  = "Alarm";
 const char STR_auto[]  = "auto";
 const char STR_ave[] = "Ave";

 //B
 const char STR_bal[]   = "Bal";
 const char STR_bms[]   = "bms";
 const char STR_board[] = "Board";

 //C
 const char STR_can[] = "Can";
 const char STR_cell[]  = "Cell";
 const char STR_close[] = "close";
 const char STR_count[] = "Count";
 const char STR_clear[] = "clear";
 const char STR_cli[] = "CLI";
 const char STR_ccu[] = "ccu";

 //D
 const char STR_daq[]   = "daq";

 //E
 const char STR_error[]   = "error";
 const char STR_empty[]   = "empty";

 //F
 const char STR_full[] = "full";
 //G
 //H
 //I
 //J
 //K
 //L
 const char STR_led[]   = "led";
 const char STR_log[]   = "log";
 const char STR_level[]= "Level";

 const char STR_lpf[] = "Lpf";
 //M
 const char STR_mA[] = "mA";
 const char STR_mV[] = " mV";
 const char STR_mode[] = "Mode";

 //N
 const char STR_name[] = "Name";
 const char STR_now[] = "Now";

 //O
 const char STR_open[]  = "open";
 const char STR_ovrd[]  = "ovrd";

 //P
 const char STR_pack[]  = "Pack";

 //Q
 //R
 const char STR_relay[] = "Relay";
 const char STR_restart[] = "Restart";
 const char STR_reset[] = "Reset";

 //S
 const char STR_sim[] = "Sim";
 const char STR_sig[] = "Sig";
 const char STR_snap[] = "Snap";
 const char STR_soc[]   = "SoC";
 const char STR_stat[]  = "State";
 const char STR_status[]  = "Status";
 const char STR_stick[] = "Stick";
 const char STR_stuck[] = "Stuck";
 const char STR_sbs[] = "Sbs";
 const char STR_show[] = "Show";
 const char STR_send[] = "Send";
 const char STR_smb[] = "Smb";
 const char STR_sys[] = "Sys";

 //T
 const char STR_time[]  = "Time";
 const char STR_temp[]  = "Temp";

 //U
 const char STR_uart[] ="Uart";
 const char STR_unstick[] = "Unstick";

 //V
 const char STR_volts[] = "Volts";

 //W
 //X
 //Y
 //Z
 
 
 
 
 
 
 
 
 
 
