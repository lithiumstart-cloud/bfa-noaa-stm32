/**
 * @file sys.c
 * @brief Implements the system setup for hardware, timers, reset.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-04-17
 *
**/

//############################# INCLUDES ########################################
#include "sys.h"
#include "mcu_hw.h"
#include "bluflex_core.h"

//############################# DEFINES ########################################
#define SECONDS_PER_MINUTE  (60)
#define MINUTES_PER_HOUR    (60)
#define HOURS_PER_DAY       (24)


//############################## TYPEDEFS ######################################


//######################## MODULE VARIABLES ####################################
static T_MCU_ResetType lastReset;

#if STM32
static T_TimeDelta uptime __attribute__((section(".noinit")));	// this is GCC notation
#else
static T_TimeDelta uptime __attribute__((persistent));
#endif


static struct
{
    T_MemAddr serialNumber_vAddr; ///< This is where serial number is stored in NV RAM
    uint32_t serialNumber;
} serialData;


static T_Timer tmrOneSec;
static bool killFlag = 0;

//######################### FUNCTION PROTOTYPES#################################

//These should be defined in a device specific driver file.

extern void readSerialNumberFromFlash(uint8_t* serialNumberBytes);

//############################## MODULE CODE ####################################

void SYS_Init()
{
    //Init hardware control...

    lastReset = MCU_HW_GetLastReset();
    
	switch (lastReset)
	{
		case RST_FIREWALL: 				puts("RST_FIREWALL"); break;
		case RST_OPTION_BYTE_LOADER: 	puts("RST_OPTION_BYTE_LOADER"); break;
		case RST_WDT: 					puts("RST_WDT"); break;
		case RST_BROWN_OUT: 			puts("RST_BROWN_OUT"); break;
		case RST_SOFTWARE: 				puts("RST_SOFTWARE"); break;
		case RST_WINDOW_WD: 			puts("RST_WINDOW_WD"); break;
		case RST_LOW_POWER: 			puts("RST_LOW_POWER"); break;
		case RST_POWER_ON: 				puts("RST_POWER_ON"); break;
		case RST_UNKNOWN: 				puts("RST_UNKNOWN"); break;
		default: 			            puts("INVALID RESET REASON"); break;
	}

    if (lastReset == RST_WDT)
    {
        VT100_PrintBold("Warning! Last reset was due to internal WDT!\r\n");
    }
    
    puts("System uptime on last reset: ");
    SYS_PrintUptime();
    printf("\r\nLast module executed: %lu\r\n", SCH_GetLastModuleBeforeReset());
    
    uptime.days = 0;
    uptime.hours = 0;
    uptime.mins = 0;
    uptime.secs = 0;

    TMR_StartRepeating(&tmrOneSec, 1000); 


    serialData.serialNumber_vAddr = MEM_AllocateWord();

    if (serialData.serialNumber_vAddr)
    {
        if (MEM_WasJustProgrammed())
        {
            serialData.serialNumber = 0;
            MEM_WriteWordFromRamToNvm(serialData.serialNumber_vAddr, serialData.serialNumber);
        }
        else
        {
            MEM_ReadWordFromNvmToRam(serialData.serialNumber_vAddr, &serialData.serialNumber);
        }
    }
    else
    {
        puts("Unable to allocate NVR space for SN.");
    }

}



bool SYS_Update(void)
{
    
    if (killFlag)
     {
         volatile int a = 0;
         while (1)
         {
             a++;
         }
     }


    if (TMR_HasTripped_ms(&tmrOneSec))
    {
        uptime.secs++;
        if (uptime.secs >= SECONDS_PER_MINUTE)
        {
            uptime.secs = 0;
            uptime.mins++;
            if (uptime.mins >= MINUTES_PER_HOUR)
            {
                uptime.mins = 0;
                uptime.hours++;
                if (uptime.hours >= HOURS_PER_DAY)
                {
                    uptime.hours = 0;
                    uptime.days++;
                }
            }

        }
        
    }


    //This module is always ready to go to SLEEP
    return true;
}

void SYS_PrintSerialNumber()
{
    printf("0x%l08X", serialData.serialNumber);
}


char const * SYS_LastResetString()
{
    switch (lastReset)
    {
        case RST_CONFIG_MISMATCH: return "CONFG";
        case RST_SOFTWARE: return  "SW";
        case RST_WDT: return "WDT";
        case RST_POWER_ON: return "POR";
        case RST_BROWN_OUT: return "BOR";
        case RST_UNKNOWN: return "PRG";
        case RST_SLEEP: return "SLEEP";
        case RST_IDLE: return "IDLE";
        case RST_VREGS: return "VREGS";

        // STM32 specific reset type strings
        case RST_FIREWALL: return "FRWL";
        case RST_OPTION_BYTE_LOADER: return "OBL";
        case RST_WINDOW_WD: return "WWD";
        case RST_LOW_POWER: return "LPW";
    }

    return "???";
}


bool SYS_GetUptimeAsString(char* buffer, size_t bufferLength)
{
    //when this returned value is non-negative and less than n, the string has been completely written
    int retVal = snprintf(buffer, bufferLength, "%lu:%02u:%02u:%02u", uptime.days, uptime.hours, uptime.mins, uptime.secs);
    return (retVal >=0 && retVal < bufferLength);
}


void SYS_PrintUptime()
{
    
    printf("%lu:%02u:%02u:%02u", uptime.days, uptime.hours, uptime.mins, uptime.secs);
}



//Just return LSW
uint16_t SYS_GetSerialNumber_LSW()
{
    return (uint16_t) (serialData.serialNumber & 0xFFFF);
}

//This reverses the order of the bytes to comply with little-endian formatting.
uint32_t SYS_GetSerialNumber()
{
    return serialData.serialNumber;
}

//Handle command line inputs that had first token 'sys'
static const char* str = "test string!";
void SYS_ProcessInput(const char* tokens[], uint8_t nTokens)
{
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("uptime\t\tPrint current system uptime (independent from RTC).");
        puts("reset\t\tSoft-reset MCU.");
        puts("snset <SN>\tSet serial number");
        puts("kill-now\tImmediately enter infinite loop.");
        puts("kill-next\tEnter infinite loop on next run of SYS_Update().");
        puts("kill-div0\tAttempt to divide by zero. I think it'll work this time!");
        puts("vt100-test\tTest VT100 functionality.");
        puts("cli-test\tTest printf, puts, puts, etc.");
    }
    else if (stricmp(tokens[0], "uptime") == 0)
    {
        SYS_PrintUptime();
    }
    else if (stricmp(tokens[0], STR_reset) == 0)
    {
        SYS_Restart();
    }
    else if (stricmp(tokens[0], "snset") == 0)
    {
        if (nTokens == 2)
        {
            uint32_t newSN = atoi(tokens[1]);
            printf("Got: %lu (0x%lX).\r\n", newSN, newSN);

            uint32_t tempBuffer;
            //Write SN to NVRAM
            if (!MEM_WriteWordFromRamToNvm(serialData.serialNumber_vAddr, newSN))
            {
                puts("Error occurred writing new serial number to NVRAM.");
            }
            else if (!MEM_ReadWordFromNvmToRam(serialData.serialNumber_vAddr, &tempBuffer))
            {
                puts("Error occurred reading back new serial number from NVRAM.");
            }
            else if (tempBuffer == newSN)
            {
                printf("Serial number successfully set to 0x%lX.\r\n", tempBuffer);
                serialData.serialNumber = tempBuffer;
            }
            else
            {
                puts("Failed to write new serial number to flash.");
                printf("Read back from NVRAM: 0x%lX.\r\n", tempBuffer);
            }
        }
        else
        {
            puts("Bad input.");
        }
    }
    else if (stricmp(tokens[0], "kill-next") == 0)
    {
        killFlag  = 1;
    }
    else if (stricmp(tokens[0], "kill-now") == 0)
    {
        int a;
        while (1)
        {
            a++;
        }
    }
    else if (stricmp(tokens[0], "kill-div0") == 0)
    {
        int a = 0;
        int b = 1/a;
        (void) b;
        
    }
    else if (stricmp(tokens[0], "cli-test")==0)
    {
    	putstr("Testing putstr(). There should be NO newline for next string: ");
    	putstr("Am I on the same line?");


    	puts("\r\nTesting puts(). The next string SHOULD be the next line...");
    	puts("Am I on the next line?");

    	printf("Testing printf()... 2+2=%d, hex(32)=0x%02X, string=%s \r\n", 4, 32, str);

    	putstr("Testing putchar('*'): ");
    	putchar('*');
    	puts("\r\n---END TEST---");
    }
    else if (stricmp(tokens[0], "vt100-test")==0)
    {

    	VT100_PrintBold("Testing bold\r\n");
    	VT100_PrintUnderline("Testing underline\r\n");
    	puts("****************************");
    	VT100_RelativeMoveCursor(VT100_CD__MOVE_UP, 1);
    	VT100_PrintClearLinePrint("\t", "\n\rThere should be only 1 tab's worth of stars above.");
    }
    else
    {
        //Print error string.
    	Console_WriteByte('?');
    }
}

void SYS_GetUpTime(T_TimeDelta* dest)
{
    dest->secs  = uptime.secs; ///< 0,59
    dest->mins  = uptime.mins; ///< 0,59
    dest->hours = uptime.hours; ///< 0,23
    dest->days  = uptime.days; ///< 0,65k = 179 years.
}



void SYS_FillUpTime(uint8_t* dest, uint8_t nBytes)
{
    if (nBytes >= 3)
    {
        dest[0] = uptime.hours;
        dest[1] = uptime.mins;
        dest[2] = uptime.secs;
    }
    else
    {
        LOG_Event(LOG_LVL_FATAL, "Not enough bytes to fill Sys time!");   
    }
}

void SYS_Restart(void)
{
    MCU_HW_Restart();
}
