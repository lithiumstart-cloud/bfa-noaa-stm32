/**
 * @file tmr.c
 * @brief Timer Handler
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-06-26
 *
 * 
 * IMPLEMENTATION NOTES:
 * 
 * Internally, Timers are implemented by storing the start time and
 * period, and checking if the difference between the current time and start
 * time is greater than the period. This is opposed to the naive method of just
 * storing the end time (as the start time plus period), and checking if that is 
 * greater than the current. This would introduce a bug when the free running
 * counter is near rollover. 
 * 
 *  Consider the following example, where we assume the free running counter is
 *  stored as an uint8_t, for simplicty: In user code, a timer is started with a
 *  period of 15 ticks and the free running counter = 250 ticks.
 * 
 * Using the "Naive" method, at timer start, the "end time" is calculated as 
 * (250 + 15)%256  = 9 ticks. The user calls TMR_HasTripped() eight ticks later, 
 * when the free running counter has rolled over to 2. The code checks:
 *          (timer->endTimer >= now)
 *          9 >= 2
 *          TRUE
 * The test evalulates to true, even though only eight ticks have elapsed!
 * 
 * Using the method implemented here, at timer start, the "start time" and
 * period are recorded as 250 ticks and 15 ticks, respecitvley. Then, when the
 * user calls TMR_HasTripped() eight ticks later, the code checks:
 * 
 *     (2 - timer->startTime) >= timer->period 
 *     (2 - 250) >= 15 
 *      8 >= 15 //Note that the timer substraction is mod 256 by design (all uint8_ts)
 *      FALSE
 * 
 * Just to carry the example through, when the timer is suppsosed to expire (15 
 * ticks from the start), the free running counter reads 9 ticks.
 * TMR_HasTripped() is called:
 *  
 *     (now - timer->startTime) >= timer->period 
 *     (9 - 250) >= 15
 *     15 >= 15  
 *     TRUE
 * 
**/
//############################## INCLUDES ######################################


#include "bluflex_core.h"
#include "tmr.h"
#include "tmr_hw.h"

//############################## DEFINES ######################################

#define MAIN_TICK_ISR_FREQ (8000) //8KHz <--> 125us

#define MS_TO_TICKS(x) ((x) << 3) //multiply by 8
#define TICKS_TO_MS(x) ((x) >> 3) //divide by 8

//########################### PRIVATE VARIABLES ################################


//############################## PROTOTYPES ####################################


//############################## PUBLIC CODE ###################################
void TMR_Init()
{
    //Start the timer in hardware.
    TMR_HW_Init(MAIN_TICK_ISR_FREQ);   
}


///Stopwatch Functions
void TMR_Stopwatch_Start(T_Stopwatch* this)
{
    if (this)
    {
        this->isRunning = true;
        this->startTime_ticks = TMR_HW_GetClock_ticks();
    }
    else
    {
        LOG_Report(LOG_MOD_TMR, LOG_LVL_ERROR, LOG_MSG_NULL_PTR_REF, __LINE__);
    }
}

uint32_t TMR_Stopwatch_GetElapsed_ms(const T_Stopwatch* this)
{
    if (this)
    {
        if (this->isRunning)
        {
            return TICKS_TO_MS(TMR_HW_GetClock_ticks() - this->startTime_ticks);
        }
        else
        {
            printf("TMR: Warning! Stopwatch @ 0x%08X checked before started.\r\n", (uint32_t) this);
        }
    }
    else
    {
        LOG_Report(LOG_MOD_TMR, LOG_LVL_ERROR, LOG_MSG_NULL_PTR_REF, __LINE__);
    }
    
    return 0;
}


uint32_t TMR_Stopwatch_Split_ms(T_Stopwatch* this)
{
    if (this)
    {
        if (this->isRunning)
        {
            uint32_t now = TMR_HW_GetClock_ticks(); //its volatile - grab it once!
            uint32_t elapsed = TICKS_TO_MS(now - this->startTime_ticks);
            this->startTime_ticks = now; //reset the start time to now
            return elapsed;    
        }
        else
        {
            printf("TMR: Warning! Stopwatch @ 0x%08X checked before started.\r\n", (uint32_t) this);
        }
    }
    else
    {
        LOG_Report(LOG_MOD_TMR, LOG_LVL_ERROR, LOG_MSG_NULL_PTR_REF, __LINE__);
    }   
    
    return 0;
}


///Timer Functions

void TMR_StartOneshot(T_Timer* this, uint32_t time_ms)
{
    if (this)
    {   
        if (time_ms)
        {
            this->isRunning = true;
            this->isRepeating = false;
        
            this->startTime_ticks = TMR_HW_GetClock_ticks();
            this->timerLength_ticks = MS_TO_TICKS(time_ms);
        }
        else
        {
            LOG_Event(LOG_LVL_ERROR, "TMR: Attempted to start 0ms one-shot timer!");
        }
              
        
    }
    else
    {
        LOG_Report(LOG_MOD_TMR, LOG_LVL_ERROR, LOG_MSG_NULL_PTR_REF, __LINE__);
    }
}


void TMR_StartRepeating(T_Timer* this, uint32_t time_ms)
{
    if (this)
    {   
        if (time_ms)
        {
            this->isRunning = true;
            this->isRepeating = true;

            this->startTime_ticks = TMR_HW_GetClock_ticks();
            this->timerLength_ticks = MS_TO_TICKS(time_ms);
        }
        else
        {
            LOG_Event(LOG_LVL_ERROR, "TMR: Attempted to start 0ms repeating timer!");
        }
    }
    else
    {
        LOG_Report(LOG_MOD_TMR, LOG_LVL_ERROR, LOG_MSG_NULL_PTR_REF, __LINE__);
    }    
}

/*
 * Note that this needs to subtract from the start time, not add to it - adding
 * would introduce erros when the start time is greater than the current time,
 * leading the timer to think it has expired early.
 */
void TMR_SetStagger(T_Timer* this, uint32_t stagger_ms)
{
    if (this)
    {
        this->startTime_ticks-= MS_TO_TICKS(stagger_ms);
    }
    else
    {
        LOG_Report(LOG_MOD_TMR, LOG_LVL_ERROR, LOG_MSG_NULL_PTR_REF, __LINE__);
    }
}


uint32_t TMR_HasTripped_ms(T_Timer* this)
{
    if (this)
    {
        if (this->isRunning)
        {
            volatile uint32_t now = TMR_HW_GetClock_ticks(); //volatile - grab once!
            volatile uint32_t elapsed = now - this->startTime_ticks;
            if (elapsed >= this->timerLength_ticks)
            {
                //If this is a repeating timer, we need to have it start agian.
                if (this->isRepeating)
                {
                    this->startTime_ticks += this->timerLength_ticks; //keep the period consistent
                }
                else
                {
                    //If its a one-shot timer, stop it from running.
                    this->isRunning = 0;
                }
                      
                
                //Check to see if timer has gone way too long without checking
                //(skip if the timer length was 0)
                if (this->timerLength_ticks && elapsed >= (this->timerLength_ticks << 1))
                {
                    printf("TMR: Warning! Timer 0x%08X trigger(s) missed. Check after %ums; period = %ums.\r\n",
                           (uint32_t) this,
                           TICKS_TO_MS(elapsed),
                           TICKS_TO_MS(this->timerLength_ticks) 
                            );
                 
                    //To avoid cascading errors, set the next time relative to 
                    //now when recovering from such an event.
                    if (this->isRepeating)
                    {
                        this->startTime_ticks = now;
                    }
                }              
                
                return TICKS_TO_MS(elapsed); //return the actual elapsed time
            }
            else
            {
                return 0;
            }
        }
        else
        {
            printf("TMR: Warning! Timer @ 0x%08X checked before started.\r\n", (uint32_t) this);
        }
            
    }
    else
    {
        LOG_Report(LOG_MOD_TMR, LOG_LVL_ERROR, LOG_MSG_NULL_PTR_REF, __LINE__);
    }
    
    return 0;
      
}


/**
 * Stop a timer from running.
 * @param Timer to stop
 */
void TMR_Stop(T_Timer* this)
{
    if (this)
    {
        if (this->isRunning)
        {
            this->isRunning = 0;
        }
        else
        {
            printf("TMR: Warning! Timer @ 0x%08X checked before started.\r\n", (uint32_t) this);
        }
    }
    else
    {
        LOG_Report(LOG_MOD_TMR, LOG_LVL_ERROR, LOG_MSG_NULL_PTR_REF, __LINE__);
    }
    
}


bool TMR_IsRunning(const T_Timer* this)
{
    if (this)
    {
        return this->isRunning;
    }
    else
    {
        LOG_Report(LOG_MOD_TMR, LOG_LVL_ERROR, LOG_MSG_NULL_PTR_REF, __LINE__);
    }
    return 0;
}


/// "Class" Functions

uint32_t TMR_GetClock_ticks(void)
{
    return TMR_HW_GetClock_ticks();
}

void TMR_FastForwardFreeRunningClock_ms(uint32_t amount_ms)
{
    uint32_t ticks_to_add = MS_TO_TICKS(amount_ms);
    TMR_HW_SetClock_ticks( TMR_HW_GetClock_ticks() + ticks_to_add ) ;
}




