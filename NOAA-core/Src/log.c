/**
 * @file log.c
 * @brief Log errors, messages, and warnings reported by other modules.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-26
 *
**/

#include "log.h"
#include "bluflex_core.h"
#include "core_config.h"
#include <stdio.h>
//############################## DEFINES ######################################

//############################## CONFIG ######################################
static struct
{
       uint32_t logLevel; ///< Min level to print
       uint32_t useRtc;///< Use RTC for events?
} configValues =
{
    .logLevel = LOG_LVL_INFO, 
    .useRtc = 0,
    
};
#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "LogLevel",
    "UseRtc?",
};


//############################## GLOBAL VARS ###################################

char logBuffer[LOG_BUFFER_LENGTH];
//############################# STATIC VARS ####################################

//##############################################################################
static const char * lvl2Str(T_Level lvl)
{
    switch (lvl)
    {
        case LOG_LVL_DBG:       return "DEBUG";
        case LOG_LVL_INFO:      return "INFO";
        case LOG_LVL_WARN:      return "WARN";
        case LOG_LVL_ERROR:     return "FAULT";
        case LOG_LVL_FATAL:     return "FATAL";
        default:                return "???";

    }
}

static const char * ModRef2Str(T_ModRef m)
{
    switch (m)
    {
        case LOG_MOD_MAIN:      return "MAIN";
        case LOG_MOD_SMB:       return "SMB";
        case LOG_MOD_BMS:       return STR_bms;
        case LOG_MOD_UART:      return "URT";
        case LOG_MOD_SYS:       return STR_sys;
        case LOG_MOD_UTIL:      return "UTIL";
        case LOG_MOD_SIG:       return "SIG";
        case LOG_MOD_SBS:       return "SBS";
        case LOG_MOD_ALARM:     return STR_alarm;
        case LOG_MOD_BAL:       return STR_bal;
        case LOG_MOD_SOC:       return STR_soc;
        case LOG_MOD_LED:       return STR_led;
        case LOG_MOD_RLY:       return "RLY";
        case LOG_MOD_TMR:       return "TMR";
        case LOG_MOD_LOG:       return STR_log;
        case LOG_MOD_CLI:       return STR_cli;
        case LOG_MOD_SCH:       return "SCH";
        case LOG_MOD_ACC:       return STR_acc;
        case LOG_MOD_A2D:       return STR_a2d;
        case LOG_MOD_AFE:       return "AFE";
        case LOG_MOD_RSC:       return "RSC";
        case LOG_MOD_COM:       return "COM";
        case LOG_MOD_CAN:       return "CAN";

        default:            return "???";
    }
}
static const char * MsgRef2Str(T_MsgRef e)
{
    switch (e)
    {
        case LOG_MSG_INDEX_OUT_OF_RANGE:    return "inxOoR";
        case LOG_MSG_DIVIDE_BY_ZERO:        return "x/0";
        case LOG_MSG_NULL_PTR_REF:          return "nullRef";
        case LOG_MSG_EARLY_NACK:            return "earlyNack";
        case LOG_MSG_TIMEOUT:               return "timeout";
        case LOG_MSG_MESSAGE_TOO_LONG:      return "msg2Long";
        case LOG_MSG_NO_REPLY:              return "noReply";
        case LOG_MSG_UNHANDLED_STATE:       return "badState";
        case LOG_MSG_NOT_SUPPORTED:         return "noSupport";
        case LOG_MSG_COMM_COLLISION:       return "commCollision";
        case LOG_MSG_EXTRA_ACK:             return "extraACK";
        case LOG_MSG_QUEUE_OVERFLOW:        return "qOvrflw";
        case LOG_MSG_TICK:                  return "tick";
        case LOG_MSG_CALC_OVERFLOW:         return "calcOflow";
        case LOG_MSG_CALC_UNDERFLOW:        return "calcUflow";
        case LOG_MSG_REJECTED_BAD_DATA:     return "rejectData";
        case LOG_MSG_EARLY_STOP:            return "earlyStop";
        case LOG_MSG_RUNNING_SLOW:          return "slow";
        case LOG_MSG_ASSERTION_FAILED:      return "assertFail";
        case LOG_MSG_FRAMING_ERROR:         return "frameErr";
        case LOG_MSG_UNINITIALIZED:         return "unInit'd";
        case LOG_MSG_SIZE_MISMATCH:         return "sizeMismatch";
        case LOG_MSG_SENSOR_FAIL:           return "sensorFail";
        case LOG_MSG_SENSOR_DISCONNECTED:   return "sensorDiscon";
        case LOG_MSG_SENSOR_BAD_DATA:       return "sensorBadData";
        case LOG_MSG_RLY_STUCK_OPENED:      return "rlyStuckOpened";
        case LOG_MSG_RLY_STUCK_CLOSED:      return "rlyStuckClosed";
        case LOG_MSG_RLY_PRECHARGE_FAIL:    return "rlyPrechargeFail";
        default:                            return "???";
    }
}

//############################# MODULE CODE ####################################

void LOG_Init()
{
   //nothing to here... 
   
}

void LOG_SetUseRealtime(bool x)
{
    configValues.useRtc = x;
}

void LOG_Report(T_ModRef mod, T_Level lvl, T_MsgRef msg, T_Param param)
{
    //Throw this onto the error log.
    if (lvl >= configValues.logLevel)
    {
        snprintf(logBuffer, LOG_BUFFER_LENGTH,  "%s: %s (%d)", ModRef2Str(mod), MsgRef2Str(msg), param);
        LOG_Event(lvl, logBuffer);
    }
}



//Handle command line inputs that had first token 'err'
void LOG_ProcessInput(const char* tokens[], uint8_t nTokens)
{ uint8_t ii;
    
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("config\tView/adjust module configuration");
        puts("show [x]\tSet minimum logging level (0 = all, 3 = err)");
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
      CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, tokens, nTokens);
    }
    else if (stricmp(tokens[0], STR_show) == 0)
    {
        if (nTokens >= 2)
        {
            ii = tokens[1][0] - '0';
            if (ii < LOG_LVL_NUM_LEVELS)
            { 
                configValues.logLevel = ii;
            }
            else
            {
              puts("?");
            }
        }
        else
        {
            puts("log show [x]");
            for (ii =0; ii < LOG_LVL_NUM_LEVELS; ii++)
            {
                printf("%d = %s \r\n", ii, lvl2Str((T_Level) ii));
            }
        }


    }
    else
    {
        //Print error string.
    	Console_WriteByte('?');
    }
}


#define BUF_LENGTH (32)
static char buf[BUF_LENGTH];
void LOG_Event(T_Level lvl, const char* str)
{
    
    if (lvl >= configValues.logLevel)
    {
        if (configValues.useRtc)
        {
            RTC_GetDatetimeNow_str(buf, BUF_LENGTH);
        }
        else
        {
            SYS_GetUptimeAsString(buf, BUF_LENGTH);
        }
        printf("%s %s ", buf, lvl2Str(lvl));
        puts(str);
        
    } 
    
}

/**
 * This works, but I don't know how. Just use sprintf for now.
 */
#if 0
void LOG_Eventf(T_Level lvl, const char* format, ...)
{
    
    if (lvl > minLevel2Print)
    {
        SYS_PrintTimeNow();
        SEND_SPACE();
        puts(lvl2Str(lvl));
        SEND_SPACE();

        va_list argptr;
        va_start(argptr, format);
        vprintf(format, argptr);
        va_end(argptr);

        NEW_LINE();
    }
}
#endif
