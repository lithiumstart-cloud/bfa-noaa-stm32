/**
 * @file mem.h
 * @brief Non-Volatile Memory Interface
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2012-01-16
 *
**/


#include "mem.h"
#include "bluflex_core.h"

#include "mem_hw.h"
#include "mcu_hw.h"

//####################### DEFINES ##############################################

/*
 * To map from an address in "virtual" memory space to offset in the giant array
 * that is allocated NV memory, we just set the most significant bit. This lets
 * us use 0 as a valid offset, but invalid virtual memory address.
 *
 */
#define VADDR_MASK  (1 << 31)
#define VADDR_TO_EEO(v) ( v & ~VADDR_MASK)
#define EEO_TO_VADDR(e) ( e | VADDR_MASK)



//############################# VARIABLES ######################################

static uint32_t maxNumWords;
static uint32_t nAllocatedWords; ///< Number of 32bit words that have been allocated

static bool justProgrammed;
//############################# FUNCTIONS ######################################
const char* MEM_ResultToStr(T_MEM_HW_Result result);

//############################## CODE ##########################################

void MEM_Init()
{
   /* This routine finds an unexpired page to become an active page. It then
    * counts the number of active pages. If no active pages are found, the
    * first unexpired page is initialized for emulation. If one or two active pages
    * found, it assumes a reset occurred and the function does nothing.
    *
    * If three active pages are found, it is assumes a reset occurred during a pack.
    * The page after current is erased and a pack is called. This
    * function must be called prior to any other operation.
    */
    
    maxNumWords = MEM_HW_GetDiskSize_Words();
    if (MEM_HW_Init())
    {
        puts("Init of MEM HW failed!");
        nAllocatedWords = maxNumWords; //this stops the system from allocating any real data
        justProgrammed = true;
    }
    else
    {
#if STM32
        nAllocatedWords = 1;	// In STM32, virtual address 0x0000 is not allowed
#else
        nAllocatedWords = 0;
#endif
        //The first word allocated is always the WAS_PROGRAMMED flag.
        T_MemAddr vpProgFlag = MEM_AllocateData(1);

        puts("Was just programmed? ");
        //Attempt to read it.
        uint32_t dummy;
        T_MEM_HW_Result res = MEM_HW_Read(&dummy, VADDR_TO_EEO(vpProgFlag));
        if (res == MEM_SUCCESS)
        {
            //If success, then the system is not coming of a programming event.
            justProgrammed = false;
            puts("No!");
        }
        else if (res == ADDR_NOT_FOUND)
        {
            //The system was just programmed!
            justProgrammed = true;
            puts("Yes!");
            //Now write to this space to next time, we know we were NOT programmed.
            T_MEM_HW_Result wRes = MEM_HW_Write(0xDEAD, VADDR_TO_EEO(vpProgFlag));
            if (wRes != MEM_SUCCESS)
            {
                puts("Failed writing JustProgrammed flag to NVRM!\t");
                puts(MEM_ResultToStr(wRes));
                NEW_LINE();
            }
        }
        else
        {
            puts("\r\nFailed checking if system was coming off reboot!\r\n");
            puts("??? \r\n");
            justProgrammed = true; //assume we did?
        }



    }
    
    

}


T_MemAddr MEM_AllocateWord()
{
    return MEM_AllocateData(1);

}

T_MemAddr MEM_AllocateData(uint8_t nWords)
{
    if ((nAllocatedWords + nWords) <= maxNumWords)
    {
        //There's room!
        T_MemAddr newAddr = EEO_TO_VADDR(nAllocatedWords);
        nAllocatedWords+= nWords;
        return newAddr;
    }

    puts("MEM: Out of space!");
    return (T_MemAddr) 0;

}

bool MEM_ReadDataFromNvmToRam(T_MemAddr vAddr, uint32_t* pDest, uint8_t nWords)
{
    bool success = true;
    while (nWords)
    {

        success &= MEM_ReadWordFromNvmToRam(vAddr, pDest);

        vAddr++;
        pDest++;
        nWords--;
    }

    return success;
}


bool MEM_WriteDataFromRamToNvm(T_MemAddr vAddr, uint32_t* pSrc, uint8_t nWords)
{
    bool success = true;
    while (nWords)
    {

        success &= MEM_WriteWordFromRamToNvm(vAddr, *pSrc);

        vAddr++;
        pSrc++;
        nWords--;
    }

    return success;
}

bool MEM_ReadWordFromNvmToRam(T_MemAddr vAddr, uint32_t* pDest)
{
    if (vAddr  && VADDR_TO_EEO(vAddr) < maxNumWords)
    {
        T_MEM_HW_Result result = MEM_HW_Read(pDest, VADDR_TO_EEO(vAddr));
        if (result)
        {
            //Error!
            printf("DEE Error reading data: %s (0x%08X)\r\n", 
                   MEM_ResultToStr(result),  
                   VADDR_TO_EEO(vAddr));
            return false;
        }

        return true;
    }
    else
    {
        puts("Uninit'd MEM ptr!");
        return false;
    }
}


bool MEM_WriteWordFromRamToNvm(T_MemAddr vAddr, uint32_t data)
{

    if (vAddr && VADDR_TO_EEO(vAddr) < maxNumWords)
    {
#if 0
        puts("MEM: Writing ");
        PrintAs_Hex_U32(data);
        puts(" to ");
        PrintAs_Hex_U32(vAddr);
        NEW_LINE();
#endif

        
        MCU_HW_ClearWDT();
        T_MEM_HW_Result result = MEM_HW_Write(data, VADDR_TO_EEO(vAddr));
        

        if (result)
        {
            //Error!
            puts("DEE Error writing data...");
            return false;
        }

        return true;
    }
    else
    {
        puts("Uninit'd MEM ptr!");
        return false;
    }
}

bool MEM_WasJustProgrammed()
{
    return justProgrammed;
}

bool MEM_LoadDefaultData(const char* name, T_MemAddr memAddr, uint32_t* data, uint8_t nWords)
{
    if (MEM_WasJustProgrammed())
    {
        //If we were just programmed, we take default topology and write it
        //to NVRM, so next time it will load by default.
        if(!MEM_WriteDataFromRamToNvm(memAddr, data, nWords))
        {
            printf("Failed to write default %s to NVRAM on boot.\r\n", name);
            return false;
        }
        else
        {
            printf("Wrote default %s to NVRAM on boot.\r\n", name);
        }
    }
    else
    {
        //We did not just get programmed, so its ok to load from NVRM!
        if(!MEM_ReadDataFromNvmToRam(memAddr, data, nWords))
        {
           printf("Failed to read %s from NVRAM on boot!", name);
           return false;
        }
   }
    
    return true;
}

const char* MEM_ResultToStr(T_MEM_HW_Result result)
{
    switch (result)
    {
        case MEM_SUCCESS:       return "memSuccess";
        case ADDR_NOT_FOUND:    return "addrNotFound";
        case EXPIRED_PAGE:      return "expiredPage";
        case PACK_SKIPPED:      return "packSkipped";
        case ILLEGAL_ADDR:      return "illegalAddr";
        case PAGE_CORRUPT_STATUS: return "pageCorrupt";
        case WRITE_ERROR:       return "writeError";
        case LOW_VOLTAGE_OP:    return "lowVltgOp";
        default: return "???";
    }
}

void MEM_ProcessInput(const char* tokens[], uint8_t nArgs)
{
    if (nArgs == 0 || tokens[0][0] == '?')
    {
        //Print list of available commands
        puts("info\tPrint module information.");
    }
    else if (stricmp(tokens[0], "info") == 0)
    {
        printf("There are %u allocated 32-bit words.\r\n", nAllocatedWords);
    }
}
