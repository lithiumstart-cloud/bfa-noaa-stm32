
/**
 * @file print_as.c
 * @brief "Print As" library - helpful printing functions
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-11-03
 *
 *
**/

#include "print_as.h"
#include <bluflex_core.h>
#include <stdio.h>
#include <ctype.h>

void (*PrintAs_OutputByte) (uint8_t byte) = Console_WriteByte;



//Enqueue a null-terminated string for transmission.
void PrintAs_str(const char* data)
{
    uint8_t inx = 0;

    //Iterate over string until we hit null
    while (data[inx] != '\0')
    {
        PrintAs_OutputByte(data[inx]);
        inx++;
    }
}

//Enqueue a null-terminated string for transmission, but make it lower case
//before sending.
void PrintAs_strl(const char* data)
{
    uint8_t inx = 0;

    //Iterate over string until we hit null
    while (data[inx] != '\0')
    {
        PrintAs_OutputByte(tolower(data[inx]));
        inx++;
    }
}

//Enqueue a null-terminated string for transmission, but make it UPPER case
//before sending.
void PrintAs_stru(const char* data)
{
    uint8_t inx = 0;

    //Iterate over string until we hit null
    while (data[inx] != '\0')
    {
        PrintAs_OutputByte(toupper(data[inx]));
        inx++;
    }
}


// Queue a single word for transmission.
// Send MSB first and LSB second.
// Author: S. Trujillo
void PrintAs_U16(uint16_t data)
{
    PrintAs_OutputByte(data & 0xFF);
    PrintAs_OutputByte((data >> 8) & 0xFF);
}


//Queue a byte for transmission, in hex.
//Author: S. Trujillo
void PrintAs_Hex_U8(uint8_t number)
{
    PrintAs_OutputByte('0');
    PrintAs_OutputByte('x');

         uint8_t inx;
     uint8_t nibble;

    for (inx = 0; inx < 2; inx++)
    {
        nibble = (number & 0xF0) >> 4;
        if (nibble < 10)
        {
            PrintAs_OutputByte('0' + nibble);
        }
        else
        {
            PrintAs_OutputByte('A' + nibble - 10);
        }

        number <<= 4;
    }

}


//Queue a word for transmission, in hex.
//Author: S. Trujillo
void PrintAs_Hex_U16(uint16_t number)
{
     PrintAs_OutputByte('0');
     PrintAs_OutputByte('x');


     uint8_t inx;
     uint8_t nibble;

    for (inx = 0; inx < 4; inx++)
    {
        nibble = (number & 0xF000) >> 12;
        if (nibble < 10)
        {
            PrintAs_OutputByte('0' + nibble);
        }
        else
        {
            PrintAs_OutputByte('A' + nibble - 10);
        }

        number <<= 4;
    }
}

//Queue a U32 for transmission, in hex.
//Author: S. Trujillo
void PrintAs_Hex_U32(uint32_t number)
{
     uint8_t inx;
     uint8_t nibble;

     PrintAs_OutputByte('0');
     PrintAs_OutputByte('x');

    for (inx = 0; inx < 8; inx++)
    {
        nibble = (number & 0xF0000000) >> 28;
        if (nibble < 10)
        {
            PrintAs_OutputByte('0' + nibble);
        }
        else
        {
            PrintAs_OutputByte('A' + nibble - 10);
        }

        number <<= 4;
    }
}


#define MAX_DIGITS_U16 5
#define MAX_DIGITS_U8  3

static const uint16_t pwrsTen_U16[] = {1, 10, 100, 1000, 10000};
static const uint8_t  pwrsTen_U8[] = {1, 10, 100};



#define MAX_DIGITS_U32 10
static const uint32_t pwrsTen_U32[] = {1, 10, 100, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9};

void PrintAs_Dec_U32(uint32_t x, uint8_t d_nDigits)
{
    uint8_t nDig;
    for (nDig = 1; nDig < MAX_DIGITS_U32; nDig++)
    {
        if (x < pwrsTen_U32[nDig])
        {
            break;
        }
    }


    //Take whichever one is bigger.
    d_nDigits = (d_nDigits > MAX_DIGITS_U32 ? MAX_DIGITS_U32 : d_nDigits); //max at 5 digits
    nDig = (nDig > d_nDigits ? nDig : d_nDigits);

    uint32_t p10;
    uint8_t d;
    while (nDig)
    {
        p10 = pwrsTen_U32[nDig - 1];
        d = x/p10;
        PrintAs_OutputByte(d + '0');
        x -= d*p10;
        nDig--;
    }

}

void PrintAs_Dec_I32(int32_t y, uint8_t d_nDigits)
{

    uint32_t x;

    if (y < 0)
    {
        x = (uint32_t)(-y);
        PrintAs_OutputByte('-');
    }
    else
    {
        x = (uint32_t) y;
        PrintAs_OutputByte('+');
    }

    PrintAs_Dec_U32(x,d_nDigits);
}


//Print a U16 in decimal with desired # digits. For min # digits, use 0. 
//Non-recursive implementation of itoa
void PrintAs_Dec_U16(uint16_t x, uint8_t d_nDigits)
{
    uint8_t nDig;
    if (x < 10)
    {
        nDig = 1;
    }
    else if (x < 100)
    {
        nDig = 2;
    }
    else if (x < 1000)
    {
        nDig = 3;
    }
    else if (x < 10000)
    {
        nDig = 4;
    }
    else
    {
        nDig = 5;
    }

    //Take whichever one is bigger.
    d_nDigits = (d_nDigits > MAX_DIGITS_U16 ? MAX_DIGITS_U16 : d_nDigits); //max at 5 digits
    nDig = (nDig > d_nDigits ? nDig : d_nDigits);

    uint16_t p10;
    uint8_t d;
    while (nDig)
    {
        p10 = pwrsTen_U16[nDig - 1];
        d = x/p10;
        PrintAs_OutputByte(d + '0');
        x -= d*p10;
        nDig--;
    }
}


//Print a U8 in decimal with desired # digits. For min # digits, use 0.
//Non-recursive implementation of itoa
void PrintAs_Dec_U8(uint8_t x, uint8_t d_nDigits)
{
    uint8_t nDig;
    if (x < 10)
    {
        nDig = 1;
    }
    else if (x < 100)
    {
        nDig = 2;
    }
    else
    {
        nDig = 3;
    }


    //Take whichever one is bigger.
    d_nDigits = (d_nDigits > MAX_DIGITS_U8 ? MAX_DIGITS_U8 : d_nDigits); //max the user input at 3 digits
    nDig = (nDig > d_nDigits ? nDig : d_nDigits); //take the bigger of user input and number sig figs

    uint8_t d;
    uint8_t p10;
    while (nDig)
    {
        p10 = pwrsTen_U8[nDig - 1];
        d = x/p10;
        PrintAs_OutputByte(d + '0');
        x -= d*p10; //get remainder without using the evil % operator
        nDig--;
    }
}

//Print a signed int16 in decimal with desired # digits. For min # digits, use 0.
//Non-recursive implementation of itoa
void PrintAs_Dec_I16(int16_t y, uint8_t d_nDigits)
{

    uint16_t x;

    if (y < 0)
    {
        x = (uint16_t)(-y);
        PrintAs_OutputByte('-');
    }
    else
    {
        x = (uint16_t) y;
        PrintAs_OutputByte('+');
    }

    PrintAs_Dec_U16(x,d_nDigits);
}

//Print a signed int16 in decimal with desired # digits. For min # digits, use 0.
//Non-recursive implementation of itoa
void PrintAs_Dec_I8(int8_t y, uint8_t d_nDigits)
{

    uint8_t x;

    if (y < 0)
    {
        x = (uint8_t)(-y);
        PrintAs_OutputByte('-');
    }
    else
    {
        x = (uint8_t) y;
        PrintAs_OutputByte('+');
    }

    PrintAs_Dec_U8(x,d_nDigits);
}


void PrintAs_Millis_U16(uint16_t mU)
{
    uint8_t d1 = mU/1000;
    PrintAs_OutputByte(d1 + '0');
    PrintAs_OutputByte('.');
    PrintAs_Dec_U16(mU - d1*1000,3);

}

void PrintAs_Millis_U16_f2(uint16_t mU)
{
    mU +=5;
    uint8_t d1 = mU/1000;
    PrintAs_Dec_U8(d1,0);
    PrintAs_OutputByte('.');
    mU -= d1*1000;
    PrintAs_Dec_U16(mU/10,2);

}
void PrintAs_Millis_U32_f2(uint32_t mU)
{
    mU +=5;
    uint32_t wholeNumber = mU/1000;
    PrintAs_Dec_U32(wholeNumber,0);
    PrintAs_OutputByte('.');
    mU -= wholeNumber*1000;
    PrintAs_Dec_U32(mU/10,2);

}

void PrintAs_Millis_U64_f2(uint64_t mU)
{
    mU +=5;
    uint64_t wholeNumber = mU/1000;
    mU -= wholeNumber*1000;
    printf("%llu.%02llu", wholeNumber, mU/10);
    //TODO: write in assembly to avoid unnecessary extra divisions; use only PrintAs

}

void PrintAs_Scaled_U32(uint32_t raw, uint8_t scaleExp)
{
    uint32_t scale = 1;
    if (scaleExp < MAX_DIGITS_U32)
    {
        scale = pwrsTen_U32[scaleExp];
    }
    uint32_t wholeNumber = raw/scale;
    PrintAs_Dec_U32(wholeNumber, 0);
    PrintAs_OutputByte('.');
    raw -= wholeNumber*scale;
    PrintAs_Dec_U32(raw, scaleExp);
}





void PrintAs_Millis_I16(int16_t mU)
{
    uint16_t uVal;
    if (mU < 0)
    {
        PrintAs_OutputByte('-');
        uVal = (uint16_t) (-mU);
    }
    else
    {
        PrintAs_OutputByte('+');
        uVal = (uint16_t) mU;
    }
    
    PrintAs_Millis_U16(uVal);


}

void PrintAs_Millis_I16_f2(int16_t mU)
{
    uint16_t uVal;
    if (mU < 0)
    {
        PrintAs_OutputByte('-');
        uVal = (uint16_t) (-mU);
    }
    else
    {
        PrintAs_OutputByte('+');
        uVal = (uint16_t) mU;
    }

    PrintAs_Millis_U16_f2(uVal);


}
void PrintAs_Millis_I32_f2(int32_t mU)
{
    uint32_t uVal;
    if (mU < 0)
    {
        PrintAs_OutputByte('-');
        uVal = (uint32_t) (-mU);
    }
    else
    {
        PrintAs_OutputByte('+');
        uVal = (uint32_t) mU;
    }

    PrintAs_Millis_U32_f2(uVal);
}


void PrintAs_Millis_I64_f2(int64_t mU)
{
    uint64_t uVal;
    if (mU < 0)
    {
        PrintAs_OutputByte('-');
        uVal = (uint64_t) (-mU);
    }
    else
    {
        PrintAs_OutputByte('+');
        uVal = (uint64_t) mU;
    }

    PrintAs_Millis_U64_f2(uVal);
}

uint8_t GetNumberDigits_U8(uint8_t x)
{
    if (x < 10)
    {
        return 1;
    }
    else if (x < 100)
    {
        return 2;
    }
     
   return 3;
    
}

