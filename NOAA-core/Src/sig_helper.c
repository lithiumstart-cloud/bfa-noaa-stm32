
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "console.h"
#include "vt100.h"
#include "sig.h"
#include "log.h"
#include "sig_helper.h"
#include "print_as.h"

static void throwBadOptionError(uint8_t x, const char* funcName)
{
    snprintf(logBuffer, LOG_BUFFER_LENGTH, "SIG: Unknown option %u in %s()", x, funcName);
    LOG_Event(LOG_LVL_FATAL, logBuffer);
    
}

void sigh_DetailedPrintNode(const T_SignalNode* node)
{
    bool isLeaf = node->firstChild ? false : true ;

    putstr("\r\nName\t\t"); SIG_PrintName(node);
    
    puts(node->name ? " (Local)" : " (Derived)");
    
    
    putstr("Parent\t\t");
    if (node->parentNode)
    {
        SIG_PrintName(node->parentNode);
        NEW_LINE();
    }
    else
    {
        puts("n/a");

    }
    
    printf("BirthIndex\t%u\r\n", node->birthOrderInx);
    printf("NodeAddr\t0x%08X\r\n", (uint32_t) node);
    printf("Children?\t%u\r\n", node->numChildren);
   
    if (isLeaf)
    {
    	putstr("Value\t\t");
        SIG_PrintSignalDatum(node->avg, node->type);
        if (node->unit)
        {
        	putstr(sigh_Unit2Str(node->unit));
        }
    }
    else //Not leaf!
    {
        if (node->childTypesConsistent)
        {
        	putstr("\r\nAvg,Min,Max,Sum\t(");
            SIG_PrintSignalDatum(node->avg, node->type);
            Console_WriteByte(',');
            SIG_PrintSignalDatum(node->min, node->type);
            Console_WriteByte(',');
            SIG_PrintSignalDatum(node->max, node->type);
            Console_WriteByte(',');
            SIG_PrintSignalDatum(node->sum, node->type);
            putstr(") ");
            putstr(sigh_Unit2Str(node->unit));
        }
    }

    putstr("\r\nAlarm Type\t");

    if (node->pAlarmTemplate || node->anyDescendentsHaveAlarms)
    {
      printf("%s\r\n", node->pAlarmTemplate ? node->pAlarmTemplate->name : "(Inherit)");  
      

      puts("Alarm Lvls...");
      printf("\tNow...%s\r\n", Alarm_Level2Str(node->alarm.now));
      printf("\tSinceRst...%s\r\n", Alarm_Level2Str(node->alarm.sinceBoot));
      printf("\tAllTime...%s\r\n", Alarm_Level2Str(node->alarm.worstEver));
    }
    else
    {
        puts("None");
    }

    printf("Init'd?\t%c\r\n", node->isInitialized ? 'Y' : 'N');
   
}

void sigh_PrintIndentedNameUnits(T_SignalNode* node, uint8_t depth)
{
    while (depth)
    {
        SEND_TAB();
        depth--;
    }

    Console_WriteByte('[');
    SIG_PrintName(node);

    if (node->unit != UNIT_NONE)
    {
        uint32_t ii;
        putstr(": ");
        //add extra space for gap between number and unit
        for (ii = 0; ii <= sigh_Type2CharLength(node->type); ii++)
        {
            SEND_SPACE();
        }
        putstr(sigh_Unit2Str(node->unit));
    }

    NEW_LINE();

}

T_SignalDatum sigh_Type2Max(T_TypeId type)
{
    T_SignalDatum toRet;

    switch (type)
    {
        case TYPE_U32:   toRet.u32 = UINT32_MAX; break;
        case TYPE_I32:   toRet.i32 = INT32_MAX;  break;
       
        default:
            throwBadOptionError(type,  __FUNCTION__);
        break;
        
    }

    return toRet;

}

T_SignalDatum sigh_Type2Min(T_TypeId type)
{
    T_SignalDatum toRet;

    switch (type)
    {

        case TYPE_U32:   toRet.u32 = 0; break;
        case TYPE_I32:   toRet.i32 = INT32_MIN;  break;

        default:
            throwBadOptionError(type,  __FUNCTION__);
        break;
    }

    return toRet;

}

uint8_t sigh_Type2CharLength(T_TypeId t)
{
    switch(t)
    {
        case TYPE_U32:      return 7; // we asssume we won't got longer than 9,999,999
        case TYPE_I32:      return 7; // we asssume we won't got longer than +999,999
        default:
            throwBadOptionError(t,  __FUNCTION__);
        break;
    }
    
    return 0;
}

const char* sigh_Type2Str(T_TypeId t)
{
    switch(t)
    {
        case TYPE_U32:      return "u32";
        case TYPE_I32:      return "i32";
        default:            return "???";
    }
}

 T_TypeId sigh_Unit2Type(T_UnitId uid)
{
    switch(uid)
    {

        case UNIT_CURRENT_MA:   return TYPE_I32;
        case UNIT_VOLTAGE_MV:   return TYPE_U32;
        case UNIT_TEMP_DC:      return TYPE_I32;
        case UNIT_NONE:         return TYPE_U32;
        case UNIT_PERCENTAGE:   return TYPE_U32;
        default:                return TYPE_U32;
    }
}

const char* sigh_Unit2Str(T_UnitId t)
{

    switch(t)
    {
        case UNIT_NONE:         return "none";
        case UNIT_CURRENT_MA:   return STR_mA;
        case UNIT_TEMP_DC:      return STR_dC;
        case UNIT_VOLTAGE_MV:   return STR_mV;
        case UNIT_PERCENTAGE:   return "%";
        default:                return "???";
    }
}

uint8_t sigh_Unit2Width(T_UnitId u)
{
    switch (u)
    {
        case UNIT_NONE:       return 0;
        case UNIT_PERCENTAGE: return 3;
        case UNIT_TEMP_DC:    return 3;
        case UNIT_VOLTAGE_MV: return 4;
        case UNIT_CURRENT_MA: return 5;
        
        default: return 0;
    }
}

void sigh_PrintDatumWidth(T_SignalDatum sigDat, T_TypeId type, uint8_t width)
{
    switch (type)
    {
       case TYPE_U32:
           PrintAs_Dec_U32(sigDat.u32, width);
       break;

       case TYPE_I32:
           PrintAs_Dec_I32(sigDat.i32, width);
       break;
    }
}


// <editor-fold defaultstate="collapsed" desc="Datum Operations">
 T_SignalDatum sigh_SmartAdd(T_TypeId type, T_SignalDatum a, T_SignalDatum b)
{

    T_SignalDatum r = {.u32 = 0};
    int64_t i64;
    switch (type)
    {
        case TYPE_U32:

            r.u32 = a.u32 + b.u32;
            //Check for overflow
            if (r.u32 < a.u32)
            {
                r.u32 = UALL_MAX;
            }
        break;

        case TYPE_I32:
            i64 = (int64_t) a.i32  + (int64_t) b.i32;
            
            if (i64 > INT32_MAX)
            {
                r.i32  = INT32_MAX;
                LOG_Event(LOG_LVL_FATAL,"SIG: INT32 calculation + overflow");    
            }
            else if (i64 < INT32_MIN)
            {
                r.i32 = INT32_MIN;
                LOG_Event(LOG_LVL_FATAL,"SIG: INT32 calculation - overflow!");    

            }
            else
            {
                r.i32 = (int32_t) i64;
            }
        break;
    }

    return r;
}

T_SignalDatum sigh_SmartDivide(T_TypeId type, T_SignalDatum x, uint32_t n)
{

    if (n == 0)
    {
        //Throw error
        return x;
    }

    switch (type)
    {
        case TYPE_U32:
           x.u_all = x.u_all / ((uint32_t) n);
        break;

        case TYPE_I32:
            x.i_all = x.i_all / ((int32_t) n);
        break;
    }
    return x;
}

bool sigh_SmartIsGreaterThan(T_TypeId type, T_SignalDatum x, T_SignalDatum y)
{
    switch (type)
    {
        case TYPE_U32:      return (x.u32 > y.u32);
        case TYPE_I32:      return (x.i32 > y.i32);
    }
    
    return (false);
}

// </editor-fold>


void sigh_PrintStopAlarmValues(const T_SignalNode* node)
{
	putstr("Signal ");
    SIG_PrintQuickName(node);
    putstr(" has a STOP alarm! (");
    SIG_PrintData(node, SIG_RQST_MIN);
    putstr(", "); SIG_PrintData(node, SIG_RQST_MAX);
    puts(")");
}

const T_SignalNode* sigh_ChildStringToNode(const T_SignalNode* parent, const char* str)
{
    uint32_t a,b,c;
    int found = sscanf(str, "%u.%u.%u", &a, &b, &c);
    const T_SignalNode* goalNode = parent;
    //printf("\r\na = %u, b =%u, c=%u\r\n", a, b, c);
    
    //turn these into indices (rollover from 0 is ok)
    a--;
    b--;
    c--;
    
    if (found > 0 && (a < SIG_GetNumChildren(goalNode)))
    {
        goalNode = SIG_GetChild(goalNode, a);
    }
    
    if (found > 1 && (b < SIG_GetNumChildren(goalNode)))
    {
        goalNode = SIG_GetChild(goalNode, b);
    }
        
    if (found > 2 && (c < SIG_GetNumChildren(goalNode)))
    {
        goalNode = SIG_GetChild(goalNode, c);
    }        
    return goalNode;
}

