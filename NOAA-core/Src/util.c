/**
 * @file util.c
 * @brief Implements the misc utility functions
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-04-17
 *
**/

#include <bluflex_core.h>

#include "util.h"


#define NUM_BITS_IN_AVE_U32 (32)

void UTIL_InitLowPassFilter_U32(T_LowPassFilter_U32 * lpf, uint8_t nExp)
{
    if (lpf != NULL)
    {
        lpf->theAve = 0;
        lpf->theErr = 0;
        lpf->nExp = nExp;
    }
}

void UTIL_InitLowPassFilter_I32(T_LowPassFilter_I32 * lpf, uint8_t nExp)
{
    if (lpf != NULL)
    {
        lpf->theAve = 0;
        lpf->theErr = 0;
        lpf->nExp = nExp;
    }
}


uint32_t UTIL_UpdateLowPassFilter_U32(T_LowPassFilter_U32 * lpf, uint32_t newRead)
{
    if (!lpf)
    {
        return 0;
    }
    
    uint32_t oldAve = lpf->theAve;
    uint32_t oldErr = lpf->theErr;
    uint32_t change;

    if (newRead > lpf->theAve)
    {
        change= newRead - lpf->theAve;

        lpf->theAve += (change >> lpf->nExp);
        lpf->theErr += (change << (NUM_BITS_IN_AVE_U32 - lpf->nExp));
        
        // See if error overflowed (did adding a value make it smaller?)
        if (lpf->theErr < oldErr)
        {
            //Yes!
            lpf->theAve++; //increment the average by one
        }

        //Now check the average for overflow...
        //If this somehow DECREASED the running average, we must have overflowed.
        if (lpf->theAve < oldAve)
        {
            lpf->theAve = UINT32_MAX; //max this out
        }
    }
    else
    {
        change = lpf->theAve - newRead;
        lpf->theAve -= (change >> lpf->nExp);
        lpf->theErr -= (change << (NUM_BITS_IN_AVE_U32 - lpf->nExp));

        //See if error underflowed (did subtracting make it bigger?)
        if (lpf->theErr > oldErr)
        {
            //Yes!
            lpf->theAve--;
        }

        //Now check ave for underflow...
        //If this somehow INCREASED the running average, we must have underflowed.
        if (lpf->theAve > oldAve)
        {
            lpf->theAve = 0; //min this out
        }
    }
    
    return lpf->theAve;
}

int32_t UTIL_UpdateLowPassFilter_I32(T_LowPassFilter_I32 *lpf, int32_t newRead)
{
    
    if (!lpf)
    {
        return 0;
    }
    
    //XC8 Compiler sign-extends signed values, so we don't need to explicitly do that.
    int32_t oldAve = lpf->theAve;
    uint32_t oldErr = lpf->theErr;
    uint32_t change;
    if (newRead > oldAve)
    {
        change = (uint32_t) (newRead - oldAve);

        lpf->theAve += change >> lpf->nExp;
        lpf->theErr += (change << (NUM_BITS_IN_AVE_U32 - lpf->nExp));

        //See if error overflowed (did adding make it smaller?)
        if (lpf->theErr < oldErr)
        {
            //Yes!
            lpf->theAve++;
        }

        //TODO: not entirely true; signed int overflow is undefined behavior
        //If this somehow DECREASED the running average, we must have overflowed.
        if (lpf->theAve < oldAve)
        {
            lpf->theAve = INT32_MAX; //max this out
        }
    }
    else
    {
        change = (uint32_t) (oldAve - newRead);

        lpf->theAve -= change >> lpf->nExp;
        lpf->theErr -= (change << (NUM_BITS_IN_AVE_U32 - lpf->nExp));

        //See if error underflowed (did subtracting make it bigger?)
        if (lpf->theErr > oldErr)
        {
            //Yes!
            lpf->theAve--;
        }

        //TODO: not entirely true; signed int underflow is undefined behavior
        //If this somehow INCREASED the running average, we must have underflowed.
        if (lpf->theAve > oldAve)
        {
            lpf->theAve = INT32_MIN; //min this out
        }
    }

    return lpf->theAve;
}


// <editor-fold defaultstate="collapsed" desc="CRC15-CAN">


//For CRC15_POLY = 0x4599.
static const uint16_t CRC15_Table[256] = {
0x0000, 0xC599, 0xCEAB, 0x0B32, 0xD8CF, 0x1D56, 0x1664, 0xD3FD, 0xF407, 0x319E,
0x3AAC, 0xFF35, 0x2CC8, 0xE951, 0xE263, 0x27FA, 0xAD97, 0x680E, 0x633C, 0xA6A5, 
0x7558, 0xB0C1, 0xBBF3, 0x7E6A, 0x5990, 0x9C09, 0x973B, 0x52A2, 0x815F, 0x44C6, 
0x4FF4, 0x8A6D, 0x5B2E, 0x9EB7, 0x9585, 0x501C, 0x83E1, 0x4678, 0x4D4A, 0x88D3,
0xAF29, 0x6AB0, 0x6182, 0xA41B, 0x77E6, 0xB27F, 0xB94D, 0x7CD4, 0xF6B9, 0x3320,
0x3812, 0xFD8B, 0x2E76, 0xEBEF, 0xE0DD, 0x2544, 0x02BE, 0xC727, 0xCC15, 0x098C,
0xDA71, 0x1FE8, 0x14DA, 0xD143, 0xF3C5, 0x365C, 0x3D6E, 0xF8F7, 0x2B0A, 0xEE93,
0xE5A1, 0x2038, 0x07C2, 0xC25B, 0xC969, 0x0CF0, 0xDF0D, 0x1A94, 0x11A6, 0xD43F, 
0x5E52, 0x9BCB, 0x90F9, 0x5560, 0x869D, 0x4304, 0x4836, 0x8DAF, 0xAA55, 0x6FCC,
0x64FE, 0xA167, 0x729A, 0xB703, 0xBC31, 0x79A8, 0xA8EB, 0x6D72, 0x6640, 0xA3D9, 
0x7024, 0xB5BD, 0xBE8F, 0x7B16, 0x5CEC, 0x9975, 0x9247, 0x57DE, 0x8423, 0x41BA,
0x4A88, 0x8F11, 0x057C, 0xC0E5, 0xCBD7, 0x0E4E, 0xDDB3, 0x182A, 0x1318, 0xD681, 
0xF17B, 0x34E2, 0x3FD0, 0xFA49, 0x29B4, 0xEC2D, 0xE71F, 0x2286, 0xA213, 0x678A, 
0x6CB8, 0xA921, 0x7ADC, 0xBF45, 0xB477, 0x71EE, 0x5614, 0x938D, 0x98BF, 0x5D26, 
0x8EDB, 0x4B42, 0x4070, 0x85E9, 0x0F84, 0xCA1D, 0xC12F, 0x04B6, 0xD74B, 0x12D2,
0x19E0, 0xDC79, 0xFB83, 0x3E1A, 0x3528, 0xF0B1, 0x234C, 0xE6D5, 0xEDE7, 0x287E,
0xF93D, 0x3CA4, 0x3796, 0xF20F, 0x21F2, 0xE46B, 0xEF59, 0x2AC0, 0x0D3A, 0xC8A3, 
0xC391, 0x0608, 0xD5F5, 0x106C, 0x1B5E, 0xDEC7, 0x54AA, 0x9133, 0x9A01, 0x5F98,
0x8C65, 0x49FC, 0x42CE, 0x8757, 0xA0AD, 0x6534, 0x6E06, 0xAB9F, 0x7862, 0xBDFB, 
0xB6C9, 0x7350, 0x51D6, 0x944F, 0x9F7D, 0x5AE4, 0x8919, 0x4C80, 0x47B2, 0x822B,
0xA5D1, 0x6048, 0x6B7A, 0xAEE3, 0x7D1E, 0xB887, 0xB3B5, 0x762C, 0xFC41, 0x39D8, 
0x32EA, 0xF773, 0x248E, 0xE117, 0xEA25, 0x2FBC, 0x0846, 0xCDDF, 0xC6ED, 0x0374,
0xD089, 0x1510, 0x1E22, 0xDBBB, 0x0AF8, 0xCF61, 0xC453, 0x01CA, 0xD237, 0x17AE, 
0x1C9C, 0xD905, 0xFEFF, 0x3B66, 0x3054, 0xF5CD, 0x2630, 0xE3A9, 0xE89B, 0x2D02, 
0xA76F, 0x62F6, 0x69C4, 0xAC5D, 0x7FA0, 0xBA39, 0xB10B, 0x7492, 0x5368, 0x96F1, 
0x9DC3, 0x585A, 0x8BA7, 0x4E3E, 0x450C, 0x8095
};

uint16_t UTIL_AddBlockToCRC15(uint8_t const msg[], uint8_t nBytes, uint16_t init)
{
    uint16_t  remainder = init;
    uint16_t address;
    uint8_t ii;

    //Divide the message by the polynomial, a byte at a time.
    for (ii = 0; ii < nBytes; ii++)
    {
        address = ((remainder >> 7) ^ msg[ii]) & 0xFF; //calculate PEC table address
        remainder = (remainder << 8) ^ CRC15_Table[address];
    }

    return (remainder*2);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="CRC8">


/**
 * Statically intialized lookup table for the PEC-8 with polynomial
 *  x^8 + x^2 + x + 1 (0x07).
 * See:
 * http://www.sunshine2k.de/coding/javascript/crc/crc_js.html
 */
const uint8_t CRC8_Table_0x07[256] = {
0x00, 0x07, 0x0E, 0x09, 0x1C, 0x1B, 0x12, 0x15, 0x38, 0x3F, 0x36, 0x31, 0x24,
0x23, 0x2A, 0x2D, 0x70, 0x77, 0x7E, 0x79, 0x6C, 0x6B, 0x62, 0x65, 0x48, 0x4F,
0x46, 0x41, 0x54, 0x53, 0x5A, 0x5D, 0xE0, 0xE7, 0xEE, 0xE9, 0xFC, 0xFB, 0xF2, 
0xF5, 0xD8, 0xDF, 0xD6, 0xD1, 0xC4, 0xC3, 0xCA, 0xCD, 0x90, 0x97, 0x9E, 0x99,
0x8C, 0x8B, 0x82, 0x85, 0xA8, 0xAF, 0xA6, 0xA1, 0xB4, 0xB3, 0xBA, 0xBD, 0xC7,
0xC0, 0xC9, 0xCE, 0xDB, 0xDC, 0xD5, 0xD2, 0xFF, 0xF8, 0xF1, 0xF6, 0xE3, 0xE4, 
0xED, 0xEA, 0xB7, 0xB0, 0xB9, 0xBE, 0xAB, 0xAC, 0xA5, 0xA2, 0x8F, 0x88, 0x81,
0x86, 0x93, 0x94, 0x9D, 0x9A, 0x27, 0x20, 0x29, 0x2E, 0x3B, 0x3C, 0x35, 0x32, 
0x1F, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0D, 0x0A, 0x57, 0x50, 0x59, 0x5E, 0x4B,
0x4C, 0x45, 0x42, 0x6F, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7D, 0x7A, 0x89, 0x8E,
0x87, 0x80, 0x95, 0x92, 0x9B, 0x9C, 0xB1, 0xB6, 0xBF, 0xB8, 0xAD, 0xAA, 0xA3,
0xA4, 0xF9, 0xFE, 0xF7, 0xF0, 0xE5, 0xE2, 0xEB, 0xEC, 0xC1, 0xC6, 0xCF, 0xC8,
0xDD, 0xDA, 0xD3, 0xD4, 0x69, 0x6E, 0x67, 0x60, 0x75, 0x72, 0x7B, 0x7C, 0x51, 
0x56, 0x5F, 0x58, 0x4D, 0x4A, 0x43, 0x44, 0x19, 0x1E, 0x17, 0x10, 0x05, 0x02,
0x0B, 0x0C, 0x21, 0x26, 0x2F, 0x28, 0x3D, 0x3A, 0x33, 0x34, 0x4E, 0x49, 0x40,
0x47, 0x52, 0x55, 0x5C, 0x5B, 0x76, 0x71, 0x78, 0x7F, 0x6A, 0x6D, 0x64, 0x63, 
0x3E, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2C, 0x2B, 0x06, 0x01, 0x08, 0x0F, 0x1A,
0x1D, 0x14, 0x13, 0xAE, 0xA9, 0xA0, 0xA7, 0xB2, 0xB5, 0xBC, 0xBB, 0x96, 0x91,
0x98, 0x9F, 0x8A, 0x8D, 0x84, 0x83, 0xDE, 0xD9, 0xD0, 0xD7, 0xC2, 0xC5, 0xCC, 
0xCB, 0xE6, 0xE1, 0xE8, 0xEF, 0xFA, 0xFD, 0xF4, 0xF3, };

/**
 * * Statically initialized lookup table for the PEC-8 with polynomial
 *  x^8 + x^5 + x^4 + 1 (0x31).
 */
const uint8_t CRC8_Table_0x31[256] = {
0x00, 0x31, 0x62, 0x53, 0xC4, 0xF5, 0xA6, 0x97, 0xB9, 0x88, 0xDB, 0xEA, 0x7D, 0x4C, 0x1F, 0x2E, 
0x43, 0x72, 0x21, 0x10, 0x87, 0xB6, 0xE5, 0xD4, 0xFA, 0xCB, 0x98, 0xA9, 0x3E, 0x0F, 0x5C, 0x6D, 
0x86, 0xB7, 0xE4, 0xD5, 0x42, 0x73, 0x20, 0x11, 0x3F, 0x0E, 0x5D, 0x6C, 0xFB, 0xCA, 0x99, 0xA8, 
0xC5, 0xF4, 0xA7, 0x96, 0x01, 0x30, 0x63, 0x52, 0x7C, 0x4D, 0x1E, 0x2F, 0xB8, 0x89, 0xDA, 0xEB, 
0x3D, 0x0C, 0x5F, 0x6E, 0xF9, 0xC8, 0x9B, 0xAA, 0x84, 0xB5, 0xE6, 0xD7, 0x40, 0x71, 0x22, 0x13, 
0x7E, 0x4F, 0x1C, 0x2D, 0xBA, 0x8B, 0xD8, 0xE9, 0xC7, 0xF6, 0xA5, 0x94, 0x03, 0x32, 0x61, 0x50, 
0xBB, 0x8A, 0xD9, 0xE8, 0x7F, 0x4E, 0x1D, 0x2C, 0x02, 0x33, 0x60, 0x51, 0xC6, 0xF7, 0xA4, 0x95, 
0xF8, 0xC9, 0x9A, 0xAB, 0x3C, 0x0D, 0x5E, 0x6F, 0x41, 0x70, 0x23, 0x12, 0x85, 0xB4, 0xE7, 0xD6, 
0x7A, 0x4B, 0x18, 0x29, 0xBE, 0x8F, 0xDC, 0xED, 0xC3, 0xF2, 0xA1, 0x90, 0x07, 0x36, 0x65, 0x54, 
0x39, 0x08, 0x5B, 0x6A, 0xFD, 0xCC, 0x9F, 0xAE, 0x80, 0xB1, 0xE2, 0xD3, 0x44, 0x75, 0x26, 0x17, 
0xFC, 0xCD, 0x9E, 0xAF, 0x38, 0x09, 0x5A, 0x6B, 0x45, 0x74, 0x27, 0x16, 0x81, 0xB0, 0xE3, 0xD2, 
0xBF, 0x8E, 0xDD, 0xEC, 0x7B, 0x4A, 0x19, 0x28, 0x06, 0x37, 0x64, 0x55, 0xC2, 0xF3, 0xA0, 0x91, 
0x47, 0x76, 0x25, 0x14, 0x83, 0xB2, 0xE1, 0xD0, 0xFE, 0xCF, 0x9C, 0xAD, 0x3A, 0x0B, 0x58, 0x69, 
0x04, 0x35, 0x66, 0x57, 0xC0, 0xF1, 0xA2, 0x93, 0xBD, 0x8C, 0xDF, 0xEE, 0x79, 0x48, 0x1B, 0x2A, 
0xC1, 0xF0, 0xA3, 0x92, 0x05, 0x34, 0x67, 0x56, 0x78, 0x49, 0x1A, 0x2B, 0xBC, 0x8D, 0xDE, 0xEF, 
0x82, 0xB3, 0xE0, 0xD1, 0x46, 0x77, 0x24, 0x15, 0x3B, 0x0A, 0x59, 0x68, 0xFF, 0xCE, 0x9D, 0xAC,
};


uint8_t UTIL_AddBlockToCRC8(uint8_t poly, uint8_t init, uint8_t const msg[], uint8_t nBytes)
{
     uint8_t  remainder = init;
     uint8_t ii,address;
     
     const uint8_t* pTable = NULL;
     switch (poly)
     {
        case 0x07: 
            pTable = CRC8_Table_0x07;
        break;
        
        case 0x31:
             pTable = CRC8_Table_0x31;
        break;
        
        default:
             printf("No CRC8 lookup table for polynomial 0x%02X!\r\n", poly);
        break;
     }
     
     if (pTable != NULL)
     {
        //Divide the message by the polynomial, a byte at a time.
        for (ii = 0; ii < nBytes; ii++)
        {
            address = remainder ^ msg[ii];
            remainder = pTable[address];
        }
     }
    return (remainder);
}
// </editor-fold>

uint32_t UTIL_GetMedian_U32(uint32_t a, uint32_t b, uint32_t c)
{
    uint32_t m;
    //Determine which of a,b,c is the smallest. Then return the smaller of the
    //remaining two.

    if ( (a <= b) && (a <= c) )
    {
        m = (b <= c) ? b : c;
    }
    else if ((b <= a) &&  (b <= c))
    {
        m = (a <= c) ? a : c;
    }
    else
    {
        m = (a <= b) ? a : b;
    }

    return m;
}

int32_t UTIL_GetMedian_I32(int32_t a, int32_t b, int32_t c)
{
    int32_t m;
    //Determine which of a,b,c is the smallest. Then return the smaller of the
    //remaining two.

    if ( (a <= b) && (a <= c) )
    {
        m = (b <= c) ? b : c;
    }
    else if ((b <= a) &&  (b <= c))
    {
        m = (a <= c) ? a : c;
    }
    else
    {
        m = (a <= b) ? a : b;
    }

    return m;
}


uint16_t UTIL_CalculatePercent_x10(uint32_t num, uint32_t den)
{
    uint64_t num64 = ((uint64_t) num * 100ULL * 10ULL * 10UL);
    
    if (den)
    {
         return ((num64/den) + 5)/10;
    }
    else
    {
        LOG_Report(LOG_MOD_UTIL, LOG_LVL_ERROR, LOG_MSG_DIVIDE_BY_ZERO, __LINE__);
        return 0xFFFF;
    }
}


//#### MONTH PARSING

#define NUM_MONTHS (12)
static const char* monthNames[NUM_MONTHS] =
{
    "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
};


uint8_t UTIL_MonthStr2Num(const char* month_str)
{
    uint8_t ii;
    for (ii = 0; ii < NUM_MONTHS; ii++)
    {
       if (stricmp(monthNames[ii], month_str) == 0)
       {
           return (ii + 1);
       }
    }
    
    return 0;   
}

const char* UTIL_MonthNum2Str(uint8_t monthNum)
{
    monthNum--;
    if (monthNum < NUM_MONTHS)
    {
        return monthNames[monthNum];
    }
    
    return "???";
}        
    
static uint8_t hexchar2val(uint8_t c)
{
    switch (c)
    {
        case '0': return 0x0;
        case '1': return 0x1;
        case '2': return 0x2;
        case '3': return 0x3;
        case '4': return 0x4;
        case '5': return 0x5;
        case '6': return 0x6;
        case '7': return 0x7;
        case '8': return 0x8;
        case '9': return 0x9;
        case 'A': return 0xA;
        case 'B': return 0xB;
        case 'C': return 0xC;
        case 'D': return 0xD;
        case 'E': return 0xE;
        case 'F': return 0xF;
    }
    
    return 0;
}

uint32_t UTIL_HexStr2Int_U32(const char* str)
{
    uint32_t result = 0;
    
    if (str == NULL)
    {
        return result;
    }
          
    while (*str != '\0')
    {
        uint8_t nibble = hexchar2val(*str);
        result <<= 4;
        result |= nibble;
        str++;
    }
    
    
    return result;      
    
    
}

bool UTIL_RequireMinAssertionTime(bool inputNow, bool lastOutputState, T_Timer* pTimer, uint32_t requiredTime_ms)
{  
    bool toReturn = false;
    
    if (inputNow)
    {
        //Is the timer already running?
        if (TMR_IsRunning(pTimer))
        {
            if (TMR_HasTripped_ms(pTimer))
            {
                //Timer expired! Time to actually assert output.
                toReturn =  true;
            }
            else
            {
                //Don't return true yet - we fault not asserted long enough
                toReturn =  false;
            }
        }
        else if (lastOutputState)
        {
            //still triggered!
            toReturn = true;
        }
        else
        {
            //new condition. start timer
            TMR_StartOneshot(pTimer, requiredTime_ms);
            toReturn  = false;
        }    
    }
    else
    {
        if (TMR_IsRunning(pTimer))
        {
            TMR_Stop(pTimer);
        }
        
        toReturn = false;
    }
    
    
    return toReturn;
    

}


 bool UTIL_IsNumber(const char* str)
{
    if (str == NULL || *str == '\0')
    {
        return false;
    }
    
    uint8_t ind = 0;
    
    while (*str != '\0')
    {
        if (*str < '0' || *str > '9')
        {
            if (ind == 0 && *str == '-' || *str == '+')
            {
                //OK
            }
            else
            {
                return false;
            }
        }
        
        str++;
        ind++;
    }
    
    return true;
}
 
 
 void UTIL_PrintArray(const uint8_t* pData, uint16_t N)
 {
     if (!pData)
     {
         return;
     }
     uint16_t ii;
     for (ii = 0; ii < N; ii++)
     {
         printf("[%02d] 0x%02X\r\n", ii, pData[ii]);
     }
 }

 void UTIL_PrintConfigInfo(const char* label, const T_ConfigInfo* pConfigInfo)
{
     if (label && pConfigInfo)
     {
        printf("\r\n%s:\r\n", label);
        printf("\t%s\t%s\r\n", pConfigInfo->name, pConfigInfo->gitHash_str);
        printf("\t%s %s\r\n", pConfigInfo->buildDate_str, pConfigInfo->buildTime_str);
     }
     else
     {
         puts("NULL!");
     }
 
            
}

///########################### MAPPING #########################################
  
/**
 * 
 * Performs mapping from x-space to y-space using two point calibration:
 *   (x1, y1)
 *   (x2, y2)
 * 
 *        /  y2 - y1 \
 * y =   |------------ |*(x - x1) + y1
 *        \  x2 - x1  /
 */ 
 int32_t UTIL_MapNumber_I32(int32_t x, int32_t x1, int32_t x2, int32_t y1, int32_t y2)
 {
     
    int32_t dem = (x2 - x1);
    
    if (dem == 0)
    {
        LOG_Event(LOG_LVL_FATAL, "UTIL: X-points of calibration can't be same!");
        return INT32_MAX; //As close as infinity we can get...
    }
    
    int32_t dy = (y2 - y1);
    int32_t dx = (x - x1);
    int64_t num = (int64_t) dy * (int64_t) dx;
    int64_t res64 = num/((int64_t) dem);
    res64 += ((int64_t) y1);
    
    int32_t res32;
    if (res64 > INT32_MAX)
    {
        res32 = INT32_MAX;
    }
    else if (res64 < INT32_MIN)
    {
        res32 = INT32_MIN;
    }
    else
    {
        res32 = (int32_t) res64;
    }
 
    return (res32);
 }

 
