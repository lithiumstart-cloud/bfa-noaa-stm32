/**
 * @file datetime.c
 * @brief Datetime Module
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-03-28
 *
**/
//############################## INCLUDES ######################################
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "datetime.h"

#include <bluflex_core.h>
//############################## DEFINES ######################################


//########################### CONFIG VALUES ####################################



//######################## MODULE VARIABLES ####################################
#define LOCAL_BUFFER_SIZE (32)
static char localBuffer[LOCAL_BUFFER_SIZE];


//####################### MODULE PROTOTYPES ####################################

//######################### MODULE CODE #########################################

bool DT_Datetime2String(char* buffer, size_t bufferLength, const T_Datetime* pDt)
{
    if (pDt && buffer)
    {
        snprintf(buffer, bufferLength, "%04d-%02d-%02dT%02d:%02d:%02dZ", pDt->date.year, pDt->date.month, pDt->date.day, pDt->time.hour, pDt->time.min, pDt->time.sec);
        return true;
    }
    
    return false;
    
}

bool DT_Date2String(char* buffer, size_t bufferLength, const T_Date* pDate)
{
    if (pDate && buffer)
    {
        snprintf(buffer, bufferLength, "%04d-%02d-%02d", pDate->year, pDate->month, pDate->day);
        return true;
    }
    
    return false;
}

bool DT_Time2String(char* buffer, size_t bufferLength, const T_Time* pTime)
{
    if (pTime && buffer)
    {
        snprintf(buffer, bufferLength, "%02d:%02d:%02d", pTime->hour, pTime->min, pTime->sec);
        return true;
    }
    
    return false;
}


void DT_PrintTime(const T_Time* time)
{
    DT_Time2String(localBuffer, LOCAL_BUFFER_SIZE, time);
    puts(localBuffer);
}

void DT_PrintDate(const T_Date* date)
{
    DT_Date2String(localBuffer, LOCAL_BUFFER_SIZE, date);
    puts(localBuffer);
}

void DT_PrintDatetime(const T_Datetime* dt)
{
    DT_Datetime2String(localBuffer, LOCAL_BUFFER_SIZE, dt);
    puts(localBuffer);
}



bool DT_ValidateDate(const T_Date* date)
{
    if (date)
    {
        uint16_t yy = date->year;
        uint8_t mm = date->month;
        uint8_t dd = date->day;
    
        if (yy < 2000 || yy > 2100)
        {
            return false;
        }

        if (mm < 1 || mm > 12)
        {
            return false;
        }

        if ( dd < 1 || dd > 31 )
        {
            return false;
        }

        return true;
    }
    
    return false;
}


bool DT_ValidateTime(const T_Time* time)
{
    if (time)
    {
        uint8_t hour = time->hour;
        uint8_t min = time->min;
        uint8_t sec = time->sec;
        
        if (hour > 23)
        {
            return false;
        }
    
        if (min > 59)
        {
            return false;
        }

        if (sec > 59 )
        {
            return false;
        }

        return true;
    }
    
    return false;
}

