/**
 * @file console.c
 * @brief Implementation for debugging serial port
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-11-03
 *
 * This file connects the CLI to the "debug" UART Port. If you would like the 
 * CLI to route through a different interface (not UART), swap out this file for a
 * different implementation. If you just want to just a different UART port,
 * change the "debugUartPort" property of appConfigValues in your application
 * project.
 * 
**/



#include "console.h"
#include "bluflex_core.h"
#include <stdio.h>
void Console_WriteByte(uint8_t data)
{
    UART_SendByte(appConfigValues.consoleUartPort, data);
}


uint8_t Console_ReadByte()
{
    return UART_ReceiveByte(appConfigValues.consoleUartPort);
}


bool Console_IsByteWaiting()
{
    return UART_IsByteWaiting(appConfigValues.consoleUartPort);
}

void Console_InjectReceivedByte(uint8_t x)
{
    UART_InjectRecievedByte(appConfigValues.consoleUartPort, x);
}


