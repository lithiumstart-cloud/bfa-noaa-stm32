/**
 * @file cli.c
 * @brief Command line interface module
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-02
 *
**/
//############################## INCLUDES ######################################
#include "cli.h"
#include "core_config.h"
#include "bluflex_core.h"
#include <ctype.h>
//############################## DEFINES ######################################

//Hi-speed draw screen data is updated every 1.5s
#define DRAW_UPDATE_PERIOD (1000) ///<How often a draw screen is updated

//Lo-speed draw screen is updated once every 1.5s*5 = 7.5s
#define NUM_HI_SPEED_UPDATES_PER_LO (5) ///<Ratio of hi-speed updates to lo-speed update



//############################## VARIABLES ####################################

static uint8_t chr_inx;

static uint8_t* pTokens[CLI_MAX_NUM_TOKENS] = {NULL};

static uint8_t activeLine[CLI_MAX_NUM_CHARS] = {'\0'};
static uint8_t oldLine[CLI_MAX_NUM_CHARS] = {'\0'};


//############################### PROTOTYPES####################################

static void printStringAsLower(const char* str);
static uint8_t tokenizeLine(void);
static void lookupCallback(const char* tokens[], uint8_t nTokens);


#define MAX_NUM_CLI_CALLBACKS 32
#define MAX_NUM_FULLSCREEN_MODES 32

typedef struct
{
    const char* key;
    void (*fProcessInput) (const char const* tokens[], uint8_t nTokens);
} T_CLI_Callback;

static struct
{
    T_CLI_Callback callbacks[MAX_NUM_CLI_CALLBACKS];
    uint8_t length;
} callbackList;


typedef struct
{
    const char* key; ///< This is the string that is entered to activate this fullscreen mode
    void (*fInitDraw) (void); ///< This is the fullscreen mode init function 
    void (*fUpdateDraw) (void); ///< This is the fullscreen mode update function
    void (*fInput) (uint8_t); ///< This is an input function that is called when a key is pressed in this fullscreen mode
} T_CLI_Fullscreen;

static struct
{
     T_CLI_Fullscreen funcs[MAX_NUM_FULLSCREEN_MODES];
     uint8_t length;
} fullList;



static const T_CLI_Fullscreen* getNextFullscreenMode(const T_CLI_Fullscreen* pFull);
static void startFullScreenMode(const T_CLI_Fullscreen* pFull);

static const T_CLI_Fullscreen* pFullMode; ///< This is the current mode (NULL is normal CLI)

static bool localEchoEnabled;

typedef enum
{
    STARTUP=0,
    WAITING_FOR_ANY_KEY,
    NORMAL,
    FULLSCREEN,

}T_State;
static T_State currentState;
static uint8_t progressBar = 0;

static T_Timer tmrFullscreenDraw;
//############################### CODE #########################################

/**
 * This is the default Splash screen draw function, which is called on boot by CLI_Init().
 * To customize, user application can implement their own version.
 */
void __attribute__((weak)) CLI_DrawSplash(void)
{

    puts("__________              ______    _____________      ______");
    puts("___  ____/_____ _______ ___  /_______  __ \\__(_)________  /______________");
    puts("__  __/  _  __ `/_  __ `/_  /_  _ \\_  /_/ /_  /_  ___/_  __ \\  _ \\_  ___/");
    puts("_  /___  / /_/ /_  /_/ /_  / /  __/  ____/_  / / /__ _  / / /  __/  /");
    puts("/_____/  \\__,_/ _\\__, / /_/  \\___//_/     /_/  \\___/ /_/ /_/\\___//_/");
    puts("                /____/\n");

    
    UTIL_PrintConfigInfo("Core", &coreConfigInfo);
    
    if (appConfigValues.extraLibConfigSlot1)
    {
        UTIL_PrintConfigInfo("Lib1", appConfigValues.extraLibConfigSlot1);
    }
    
    if (appConfigValues.extraLibConfigSlot2)
    {
        UTIL_PrintConfigInfo("Lib2", appConfigValues.extraLibConfigSlot2);
    }
    
    UTIL_PrintConfigInfo("App", &appConfigInfo);

    puts("\nInitializing EaglePicher BluFlex Core...");

}


void CLI_Init()
{
   chr_inx = 0;

   callbackList.length = 0;
   fullList.length = 0;

   pFullMode = NULL;

   localEchoEnabled = true;
   currentState = STARTUP;

    VT100_CLEAR_SCREEN();
    VT100_MOVE_CURSOR_HOME();

    CLI_DrawSplash();
}



bool CLI_AddCallback(const char* key,  void (*fProcessInput) (const char* tokens[], uint8_t nTokens))
{
    if (callbackList.length < MAX_NUM_CLI_CALLBACKS && key && fProcessInput)
    {
        T_CLI_Callback * thisCB = &callbackList.callbacks[callbackList.length];
        thisCB->key = key;
        thisCB->fProcessInput = fProcessInput;
        callbackList.length++;
        return true;
    }

    LOG_Report(LOG_MOD_CLI, LOG_LVL_FATAL,  LOG_MSG_QUEUE_OVERFLOW, __LINE__);

    return false;
}


bool CLI_AddFullscreenCB(const char* key,  void (*fInitDraw) (void), void (*fUpdate) (void), void (*fInput)(uint8_t))
{
    if (fullList.length < MAX_NUM_FULLSCREEN_MODES && key && fUpdate)
    {
        T_CLI_Fullscreen * thisFull = &fullList.funcs[fullList.length];

        thisFull->fUpdateDraw = fUpdate;
        thisFull->fInitDraw = fInitDraw;
        thisFull->fInput = fInput;
        thisFull->key = key;

        //If this is the first call to add a fullscreen mode, add FULL as a top
        //level callback so they can actually be used.
        if (fullList.length == 0)
        {
            CLI_ADD_INPUT_CALLBACK(Full);
        }
        fullList.length++;
        return true;
    }

    return false;
}


bool CLI_AddFullscreen(const char* key,  void (*fInitDraw) (void), void (*fUpdate) (void))
{
    return CLI_AddFullscreenCB(key, fInitDraw, fUpdate, NULL);
}


/**
 * This function is called to initalize the CLI screen. Defined weak so can be overriden.
 */
void __attribute__((weak)) CLI_DrawInit(void)
{
    VT100_CLEAR_SCREEN();
    VT100_MOVE_CURSOR_HOME();

    VT100_FONT_UNDERLINE();
    puts("   EaglePicher BluFlex CLI");
    VT100_FONT_NORMAL();
    putstr("App: ");
    VT100_FONT_BOLD();
    puts(appConfigInfo.name);
    VT100_FONT_NORMAL();

    putstr("Vers: ");
    puts(appConfigInfo.gitHash_str);
    putstr("Built: ");
    putstr(appConfigInfo.buildDate_str);
    SEND_SPACE();
    puts(appConfigInfo.buildTime_str);
    
    uint32_t sn = SYS_GetSerialNumber();
    printf("SN: %lu (0x%08x)\r\n", sn, sn);
    

    
    VT100_FONT_UNDERLINE();
    putstr("Last reset: ");
    putstr(SYS_LastResetString());
    uint8_t ii;
    for (ii = 0; ii < 15; ii++)
    {
    	Console_WriteByte(' ');
    }
    VT100_FONT_NORMAL();
    LINE_FEED();
    NEW_LINE();
    COMMAND_PROMPT();
}

/*
 * This takes the module level LINE char array, and replaces all spaces
 * with nulls, and fills the pTokens array with the pointer to the first
 * character in each token. Returns number of tokens.
 */
static uint8_t tokenizeLine()
{
    uint8_t tokenInx = 1;
    pTokens[0] = &activeLine[0];
    uint8_t ii;
    for (ii = 1; ii < CLI_MAX_NUM_CHARS - 1; ii++)
    {
        if (activeLine[ii] == ' ')
        {
               activeLine[ii] = '\0';
               if (tokenInx < CLI_MAX_NUM_TOKENS)
               {
                    pTokens[tokenInx] = &activeLine[ii + 1];
               }
               tokenInx++;
        }
    }

    activeLine[CLI_MAX_NUM_CHARS - 1] = '\0';

    return tokenInx;
}


bool CLI_Update(void)
{

    uint8_t cin,ii;
    uint8_t nTokens;

    switch (currentState)
    {

        case STARTUP:
            //Do nothing here.
        break;

        case WAITING_FOR_ANY_KEY:
            if (Console_IsByteWaiting())
            {
                cin = Console_ReadByte();
                currentState = NORMAL;
                CLI_DrawInit();
            }
        break;

        case NORMAL:
             if (Console_IsByteWaiting())
              {
                cin = Console_ReadByte();

                switch (cin)
                {
                     case '\0':
					 break;

                     case '\r':
                     case '\n':
                         //Was anything even input?
                         if (chr_inx)
                         {
                           if (localEchoEnabled)
                           {
                               NEW_LINE();
                           }
                           activeLine[chr_inx] = '\0';

                           //Was this a request for the previous command?
                           if (chr_inx == 1 && activeLine[0] == '!')
                           {
                               //copy the old buffer back into active buffer
                               memcpy(activeLine, oldLine, CLI_MAX_NUM_CHARS);
                               nTokens = tokenizeLine(); //tokenize active buffer
                               lookupCallback((const char**) pTokens, nTokens); //execute callback
                           }
                           else
                           {
                                //Copy the active line into old line buffer BEFORE
                                //its been tokenized (so it still has spaces).
                                memcpy(oldLine, activeLine, CLI_MAX_NUM_CHARS);

                                //Parse the local buffer into tokens.
                                nTokens = tokenizeLine();

                                //Lookup and execute the appropriate callback function.
                                lookupCallback((const char**) pTokens, nTokens);
                           }

                           //Clear out the local buffer.
                           for (ii = 0; ii < chr_inx; ii++)
                           {
                               activeLine[ii] = '\0';
                           }

                           //Clear out the token buffer.
                           for (ii = 0; ii < nTokens; ii++)
                           {
                               pTokens[ii] = NULL;
                           }

                    	   chr_inx = 0;
                           nTokens = 0;
                         }

                         //Draw a new command prompt
                         if (pFullMode == NULL && localEchoEnabled)
                         {
                             NEW_LINE();
                            COMMAND_PROMPT();
                         }
                     break;

                     case '\b':
                         if (chr_inx)
                         {
                             chr_inx--;
                             if (localEchoEnabled)
                             {
                                 putstr("\b \b");
                             }
                         }

                         if (chr_inx < CLI_MAX_NUM_CHARS)
                         {
                             activeLine[chr_inx] = '\0'; //incase this doesn't get overwritten
                         }

                     break;

                     default:
                         if (chr_inx < CLI_MAX_NUM_CHARS)
                         {
                            activeLine[chr_inx] = cin;
                         }
                         chr_inx++;
                         if (localEchoEnabled)
                         {
                            Console_WriteByte(cin); //echo
                         }
                     break;
                 } //end switch on cin
               }//end new byte conditional
        break;



        case FULLSCREEN:
            if (TMR_HasTripped_ms(&tmrFullscreenDraw))
            {
                if (pFullMode != NULL)
                {
                    if (pFullMode->fUpdateDraw)
                    {
                        VT100_MOVE_CURSOR_HOME();
                        pFullMode->fUpdateDraw();
                    }
                }
                 else
                 {
                     puts("CLI: Error! In fullscreen state, but no fullscreen functions loaded.");
                 }
            }
            //This check should come second, to avoid checking timer after its been stopped.
            if (Console_IsByteWaiting())
            {
                cin = Console_ReadByte();
                if (cin == 'q' || cin == ASCII_ESC)
                {
                         pFullMode = NULL;
                         currentState = NORMAL;
                         TMR_Stop(&tmrFullscreenDraw);
                         CLI_DrawInit();
                }
                else if (cin == '\t')
                {
                    //go to next full string mode
                    startFullScreenMode(getNextFullscreenMode(pFullMode));
                }
                else
                {
                    //Pass this character into fullscreen input function, if
                    //it exists.
                    if (pFullMode->fInput)
                    {
                        pFullMode->fInput(cin);
                    }
                }
            }



        break;
    }



   return true; //Always ready to sleep
}



static void lookupCallback(const char* tokens[], uint8_t nTokens)
{

    uint8_t ii;

    resetLowPowerSm();  // If there is RS232 activity, reset the RS232/LEDs shutoff timer

    if (tokens[0][0] == '?')
    {
        //List all available top-level keys.
        for (ii =0; ii < callbackList.length; ii++)
        {
            printStringAsLower(callbackList.callbacks[ii].key);
            NEW_LINE();
        }
        return;
    }
    

    T_CLI_Callback *thisCb = NULL;
    for (ii = 0; ii < callbackList.length; ii++)
    {
        thisCb = &callbackList.callbacks[ii];
        if (stricmp(tokens[0], thisCb->key) == 0)
        {
            //Found it!
            thisCb->fProcessInput(&tokens[1], nTokens - 1);
            return;
        }
    }

    putstr("?\r\n");


}


static void startFullScreenMode(const T_CLI_Fullscreen* pFull)
{
    
    pFullMode = pFull;
    VT100_CLEAR_SCREEN();
    VT100_MOVE_CURSOR_HOME();
    progressBar = 0; //reset the progress bar, in case this draw function uses it
            
    if (pFullMode->fInitDraw)
    {                
        pFullMode->fInitDraw();
    }
            
    TMR_StartRepeating(&tmrFullscreenDraw, DRAW_UPDATE_PERIOD); 
    currentState = FULLSCREEN;
}

static const T_CLI_Fullscreen* getNextFullscreenMode(const T_CLI_Fullscreen* pFull)
{
    if (pFull)
    {
        uint8_t ii;
        for (ii = 0; ii < fullList.length; ii++)
        {
            if (&(fullList.funcs[ii]) == pFull)
            {
                //found it!
                break;
            }
        }
        
        ii++;
        
        if (ii >= fullList.length)
        {
            ii = 0;
        }
        
        return &(fullList.funcs[ii]);
    }
    
    return NULL;
}

void Full_ProcessInput(const char* tokens[], uint8_t nTokens)
{

    uint8_t ii;

    if (nTokens == 0 || tokens[0][0] == '?')
    {
       for (ii = 0; ii < fullList.length; ii++)
       {
           printStringAsLower(fullList.funcs[ii].key);
           NEW_LINE();
       }
       return;
    }


    for (ii = 0; ii < fullList.length; ii++)
    {
        if (stricmp(tokens[0], fullList.funcs[ii].key) == 0)
        {
            //Found it!
            startFullScreenMode(&fullList.funcs[ii]);
            return;
        }
    }

    puts("?");


}

void CLI_EndStartupMode()
{
    puts("\r\nPress any key to continue.\n");
    currentState = WAITING_FOR_ANY_KEY;
}

void CLI_SetLocalEcho(bool state)
{
    localEchoEnabled = state;
}

bool CLI_ProcessConfig(const char** configNames_str, uint32_t* configValues,  uint8_t nConfigVars, const char* tokens[], uint8_t nTokens)
{
    return CLI_ProcessSignedConfig(configNames_str,configValues, nConfigVars, 0x0000, tokens, nTokens);
}

bool CLI_ProcessSignedConfig(const char** configNames_str, uint32_t* configValues,  uint8_t nConfigVars, uint32_t configSignMask, const char* tokens[], uint8_t nTokens)
{
    uint8_t ii;
    union
    {
        uint32_t u32;
        int32_t i32;
    } val;
         
    if (nTokens == 3)
    {
        char* endPtr = NULL;
        int32_t ind = strtol(tokens[1], &endPtr, 0);
        
        if (endPtr == tokens[1])
        {
            //We didn't find valid input!
            printf("Invalid index: \"%s\".\r\n", tokens[1]);
            return false;
        }
        else if (ind < 0 || ind >= nConfigVars)
        {
            printf("Index out of range! Must be [0,%u]\r\n", nConfigVars - 1);
            return false;
        }
        
        //If we're here, then ind is valid #.
        //Now parse new value. 
        val.i32 = strtol(tokens[2], &endPtr, 0);
        
        if (endPtr == tokens[2])
        {
            //Not a valid input!
            printf("Invalid value: \"%s\"\r\n", tokens[2]);
            return false;
        }
        
        //If we're here, then both inputs are good.
        configValues[ind] = val.u32;
        //To print, find out if this value is signed or unsigned.
        if (configSignMask & (1 << ind))
        {
            printf("%s <-- %+ld", configNames_str[ind], val.i32);
        }
        else
        {
            printf("%s <-- %lu", configNames_str[ind], val.u32);
        }
    }
    else
    {
        for (ii = 0; ii < nConfigVars; ii++)
        {
            printf("[%d]\t%s\t", ii, configNames_str[ii]);
            val.u32 = configValues[ii];
            if (configSignMask & 0x1)
            {
                printf("%+ld\r\n", val.i32);
            }
            else
            {
                printf("%lu\r\n", val.u32);
            }
            
            configSignMask >>= 1;
        }
    }

    return true;
}
bool CLI_ProcessConfigCommand(T_ConfigWrapper* pConfig, const char* tokens[], uint8_t nTokens)
{
    if (pConfig)
    {
        return CLI_ProcessConfig(pConfig->configNames_str, pConfig->configValues,  pConfig->nConfigVars, tokens, nTokens);
    }

    return false;
}



void CLI_DrawProgressBar()
{
     
    if (progressBar == 0)
    {
        VT100_CLEAR_LINE();
    }
    else
    {
        uint8_t ii = progressBar - 1;
        while (ii)
        {
            SEND_TAB();
            ii--;

        }
        putstr(" *******");
    }

    progressBar++;
    if (progressBar >= 5)
    {
        progressBar = 0;
    }
}


static void printStringAsLower(const char* str)
{
    while (*str != '\0')
    {
    	Console_WriteByte(tolower(*str));
        str++;
    }
}
