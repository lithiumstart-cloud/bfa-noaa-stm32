/**
 * @file vt100.c
 * @brief Interface for controlling cursor on VT100 emulator
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-11-03
 *
 * See http://www.termsys.demon.co.uk/vtansi.htm
**/


//######################### MODULE INCLUDES ##################################

#include <stdint.h>
#include <stdio.h>
#include "vt100.h"
#include "console.h"

#define MAX_BUFFER_SIZE (32)
static char fbuf[MAX_BUFFER_SIZE] = {0};

void VT100_RelativeMoveCursor(T_VT100_CursorDirection dir, uint8_t x)
{
    snprintf(fbuf, MAX_BUFFER_SIZE, "\x1b[%d%c", x, 'A' + dir);
    putstr(fbuf);
}

void VT100_MoveCursorTo(uint8_t row, uint8_t col)
{
	snprintf(fbuf, MAX_BUFFER_SIZE,"\x1b[%d;%dH", row, col);
	putstr(fbuf);
}


static const char* specialCommandToString(T_VT100_SpecialCommand cmd)
{
    switch (cmd)
    {
        case VT100_SPC__MOVE_CURSOR_HOME:       return "H";
  
        case VT100_SPC__ERASE_END_OF_LINE:      return "K";
        case VT100_SPC__ERASE_START_OF_LINE:    return "1K";
        case VT100_SPC__ERASE_CURRENT_LINE:     return "2K";
        
        case VT100_SPC__ERASE_DOWN:             return "J";
        case VT100_SPC__ERASE_UP:               return "1J";
        case VT100_SPC__ERASE_WHOLE_SCREEN:     return "2J";
        
        case VT100_SPC__NORMAL_CHARS:           return "0m";
        case VT100_SPC__BOLD_CHARS:             return "1m";
        case VT100_SPC__UNDERLINE_CHARS:        return "4m";
        case VT100_SPC__BLINKING_CHARS:         return "5m";
    }

    return NULL;
}

void VT100_SpecialCommand(T_VT100_SpecialCommand cmd)
{
    const char* cmdStr = specialCommandToString(cmd);

    //If the parameter is invalid, we don't want to print anything.
    if (cmdStr)
    {
    	snprintf(fbuf, MAX_BUFFER_SIZE,"\x1b[%s", cmdStr);
    	putstr(fbuf);
    }   
}

void VT100_PrintBold(const char* str)
{
    VT100_SpecialCommand(VT100_SPC__BOLD_CHARS);
    putstr(str);
    VT100_SpecialCommand(VT100_SPC__NORMAL_CHARS);
}


void VT100_PrintUnderline(const char* str)
{
    VT100_SpecialCommand(VT100_SPC__UNDERLINE_CHARS);
    putstr(str);
    VT100_SpecialCommand(VT100_SPC__NORMAL_CHARS);
}

void VT100_PrintClearLinePrint(const char* preStr, const char* postStr)
{
    if (preStr)
    {
        putstr(preStr);
    }
    
    VT100_SpecialCommand(VT100_SPC__ERASE_END_OF_LINE);
    
    if (postStr)
    {
        putstr(postStr);
    }
}



