/**
 * @file  fifo.c
 * @brief  Static FIFO Buffers
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-07-03
 */

#include "fifo.h"
#include <stdlib.h>
#include <stdio.h>
 #include <string.h>


bool FIFO_Init(T_FIFO* pFifo, void* _bufferStart, uint32_t _maxLength, uint8_t _elementSize)
{
    pFifo->bufferStart = _bufferStart;
    pFifo->maxLength = _maxLength;
    pFifo->elementSize = _elementSize;
    
    //Initialize FIFO state vars...
    pFifo->numFilled = 0;
    pFifo->popInx = 0;
    pFifo->pushInx = 0;
    
    return true;
}

bool FIFO_EnqueueFrom(T_FIFO* pFifo, const void* pSrc)
{
    if (pFifo->numFilled < pFifo->maxLength)
    {
        
        void* pDst = ((char *) pFifo->bufferStart) + pFifo->elementSize*pFifo->pushInx;
        memcpy(pDst, pSrc, pFifo->elementSize);
        
        pFifo->pushInx++;
        if (pFifo->pushInx >= pFifo->maxLength)
        {
            pFifo->pushInx = 0;
        }
        pFifo->numFilled++;
    }
    else
    {
        return false;
    }
    
    return true;

}

bool FIFO_DequeueInto(T_FIFO* pFifo, void* pDst)
{
        if (pFifo->numFilled)
        {
            void* pSrc = ((char* )pFifo->bufferStart) + pFifo->elementSize*pFifo->popInx;
            memcpy(pDst, pSrc, pFifo->elementSize);
            
            pFifo->popInx++;
            if (pFifo->popInx >= pFifo->maxLength)
            {
                pFifo->popInx = 0;
            }
            
            pFifo->numFilled--;
            return true;
        }
    
    return false;
}


