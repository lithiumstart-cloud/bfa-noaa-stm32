/**
* @file alarm.c
* @brief Implements alarm controller.
* @copyright 2014 by Lithiumstart LLC
* @author A. Kessler
* @date   2014-04-22
**/


#include <ctype.h>

#include "bluflex_core.h"
#include "core_config.h"
#include "sig_helper.h"

//######################### DEFINES ##########################################


//########################## TYPES/CONSTS ####################################


#define MAX_NUM_ALARM_TEMPLATES (LIB_MAX_NUM_ALARM_TEMPLATES)
//########################### MODULE VARS/FUNCS###############################
static const char* getLimitName(uint8_t inx);
static bool areLimitsValid(T_AlarmTemplate* pAlarm);
static bool isDatumGreaterThan(T_SignalDatum a, T_SignalDatum b, T_TypeId type);
static bool isDatumLessThan(T_SignalDatum a, T_SignalDatum b, T_TypeId type);
static bool isNewLimitMoreRestrictive(T_AlarmTemplate* pAlarm, uint8_t limitInx, T_SignalDatum newLimit);
static void editAlarmLimit(T_AlarmTemplate* alarm, uint8_t inx, T_SignalDatum newValue);

static struct
{
     T_AlarmTemplate templates[MAX_NUM_ALARM_TEMPLATES];
     uint8_t length;
} templateList;

static const char* statusMsg = NULL;

//########################## MODULE CODE #####################################

void Alarm_Init()
{
    templateList.length = 0;
}

T_AlarmTemplate* Alarm_CreateTemplate(const char* name, T_UnitId unit, T_SignalDatum clearLo, T_SignalDatum clearHi, T_SignalDatum warnLo, T_SignalDatum warnHi, T_SignalDatum critLo, T_SignalDatum critHi)
{
    
    if (templateList.length >= MAX_NUM_ALARM_TEMPLATES)
    {
        //No more room for alarms :(
        putstr("No more room for alarms");
        return NULL;
    }

    T_AlarmTemplate* thisAlarm = &templateList.templates[templateList.length];

    thisAlarm->name = name;

    thisAlarm->limits.ClearLower = clearLo;
    thisAlarm->limits.ClearUpper = clearHi;
    thisAlarm->limits.WarnLower = warnLo;
    thisAlarm->limits.WarnUpper = warnHi;
    thisAlarm->limits.CriticalLower = critLo;
    thisAlarm->limits.CriticalUpper = critHi;

    thisAlarm->unit = unit;
    thisAlarm->type = sigh_Unit2Type(unit);
   
    if (!areLimitsValid(thisAlarm))
    {
        snprintf(logBuffer, LOG_BUFFER_LENGTH, "Alarm \"%s\" is not valid.", name);
        LOG_Event(LOG_LVL_FATAL, logBuffer);
    }
    
    templateList.length++;

    return thisAlarm;

}

T_AlarmTemplate* Alarm_CreateTemplate_I32(const char* name, T_UnitId unit,  int32_t clearLo, int32_t clearHi, int32_t warnLo, int32_t warnHi, int32_t critLo, int32_t critHi)
{
    T_SignalDatum cLo, cHi, wLo, wHi, crLo, crHi;

    cLo.i32 = clearLo;
    cHi.i32 = clearHi;
    wLo.i32 = warnLo;
    wHi.i32 = warnHi;
    crLo.i32 = critLo;
    crHi.i32 = critHi;

    if (sigh_Unit2Type(unit) != TYPE_I32)
    {
        return NULL;
    }
    return Alarm_CreateTemplate(name, unit, cLo, cHi, wLo, wHi, crLo, crHi);
}

T_AlarmTemplate* Alarm_CreateTemplate_U32(const char* name, T_UnitId unit,  uint32_t clearLo, uint32_t clearHi, uint32_t warnLo, uint32_t warnHi, uint32_t critLo, uint32_t critHi)
{
    T_SignalDatum cLo, cHi, wLo, wHi, crLo, crHi;

    cLo.u32 = clearLo;
    cHi.u32 = clearHi;
    wLo.u32 = warnLo;
    wHi.u32 = warnHi;
    crLo.u32 = critLo;
    crHi.u32 = critHi;

    if (sigh_Unit2Type(unit) != TYPE_U32)
    {
        return NULL;
    }
    return Alarm_CreateTemplate(name, unit, cLo, cHi, wLo, wHi, crLo, crHi);
}

T_AlarmTemplate* Alarm_ImportLimitsFrom(const char* name, T_UnitId unit, const T_AlarmLimits* pLimits)
{
    if (templateList.length >= MAX_NUM_ALARM_TEMPLATES)
    {
        //No more room for alarms :(
        putstr("No more room for alarms");
        return NULL;
    }

    if (pLimits)
    {
        T_AlarmTemplate* thisAlarm = &templateList.templates[templateList.length];
        templateList.length++;  

        thisAlarm->name = name;
        thisAlarm->limits = *pLimits;
        thisAlarm->unit = unit;
        thisAlarm->type = sigh_Unit2Type(unit);

        if (!areLimitsValid(thisAlarm))
        {
            snprintf(logBuffer, LOG_BUFFER_LENGTH, "Alarm \"%s\" is not valid.", name);
            LOG_Event(LOG_LVL_FATAL, logBuffer);
        }

        return thisAlarm;
    }
    
    puts("NULL limits pointer...");
    return NULL;
}

bool Alarm_SetLevel(T_AlarmBitmap* theAlarm, T_AlarmLevel theLvl)
{
    //No matter what, we want to update the RT value.
    //Grab the old state, to check for change.
    T_AlarmLevel oldLevel = theAlarm->now;

    theAlarm->now = theLvl;
    if (theLvl != oldLevel)
    {
        theAlarm->hasChanged = true;
    }

    if (theLvl > theAlarm->sinceBoot)
    {
        theAlarm->sinceBoot = theLvl;
    }

    if (theLvl > theAlarm->worstEver)
    {
        theAlarm->worstEver = theLvl;
    }


    return theAlarm->hasChanged;

}

T_AlarmLevel Alarm_Evaluate(const T_AlarmTemplate *pTemplate, T_SignalDatum datum)
{
    if (pTemplate == NULL)
    {
        return ALARM_LVL_STOP;
    }
    
    T_TypeId type = pTemplate->type;
    
    if (isDatumGreaterThan(datum, pTemplate->limits.CriticalUpper, type) || isDatumLessThan(datum, pTemplate->limits.CriticalLower, type))
    {
        return ALARM_LVL_STOP;
    }
    
    if (isDatumGreaterThan(datum, pTemplate->limits.WarnUpper, type) || isDatumLessThan(datum, pTemplate->limits.WarnLower, type))
    {
        return ALARM_LVL_CRIT;
    }
    
    if (isDatumGreaterThan(datum, pTemplate->limits.ClearUpper, type) || isDatumLessThan(datum, pTemplate->limits.ClearLower, type))
    {
        return ALARM_LVL_WARN;
    }
    
    return ALARM_LVL_CLEAR;
    
    
}


uint8_t Alarm_PackIntoByte(const T_AlarmBitmap* pAlarm)
{
    uint8_t u8 = 0;
    if (pAlarm)
    {
        //[0,1] Alarm Now
        //[2,3] Worst Alarm Since Boot
        //[4,5] Worst Alarm Ever
        u8 =  (pAlarm->worstEver << 4)| (pAlarm->sinceBoot << 2) | pAlarm->now;
    }

    return u8;
}


const char* Alarm_Level2Str(T_AlarmLevel lvl)
{
      switch (lvl)
      {
        case ALARM_LVL_CLEAR:   return " OK ";
        case ALARM_LVL_WARN:    return "WARN";
        case ALARM_LVL_CRIT:    return "CRIT";
        case ALARM_LVL_STOP:    return "STOP";
        default:                return "???";
     }
    
}


//Handle command line inputs that had first token 'alarm'
void Alarm_ProcessInput(const char* tokens[], uint8_t nTokens)
{
    
    uint8_t ii;
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        putstr("\r\nlist\t\tList all Alarm Limits.");
        putstr("\r\ntest [inx] [value]\t\tPlug value into an alarm and see what it evaluates to.");
        putstr("\r\ninfo [inx] \t\t Show info about a single alarm template");
        NEW_LINE();      

    }
    else if (stricmp(tokens[0], "list") == 0)
    {
        
        uint8_t ii,jj;
        
        //Print all templates.
        printf("There are %d alarm templates.\r\n", templateList.length);
        
        VT100_FONT_UNDERLINE();
        putstr(STR_name);
        for (jj = 0; jj < ALARM_LIMITS_NUM_LIMITS; jj++)
        {
            SEND_TAB();
            putstr(getLimitName(jj));
        }
        NEW_LINE();
        VT100_FONT_NORMAL();
       

        
        for (ii =0; ii < templateList.length; ii++)
        {
            T_AlarmTemplate* thisTemp = &templateList.templates[ii];
            
            putstr(thisTemp->name);
            
            SEND_TAB();
            putstr(sigh_Unit2Str(thisTemp->unit));
            
            for (jj = 0; jj < ALARM_LIMITS_NUM_LIMITS; jj++)
            {
                SEND_TAB();
                SIG_PrintSignalDatum(thisTemp->limits.array[jj], thisTemp->type);
            }
            
            NEW_LINE();
        }
    }
    else if (stricmp(tokens[0], "test") == 0)
    {
        
        if (nTokens >= 3)
        {
            int32_t inx = atoi(tokens[1]);
            int32_t val = atoi(tokens[2]);
            T_SignalDatum datum = {.i32 = val};
            
            if (inx >= 0 && inx < templateList.length)
            {
                T_AlarmTemplate* this = &templateList.templates[inx];
                printf("%s(%d) --> %s\r\n", this->name, val, Alarm_Level2Str(Alarm_Evaluate(this, datum)));
            }
            else
            {
                printf("Sorry, %d is an invalid alarm index.\r\n", inx);
            }
        }
        else
        {
            putstr("test [inx] [value]\r\n");
        }
    }
    else if (stricmp(tokens[0], "info") == 0)
    {
        if (nTokens >= 2)
        {
            int32_t inx = atoi(tokens[1]);
            if (inx >= 0 && inx < templateList.length)
            {
                T_AlarmTemplate* this = &templateList.templates[inx];
                printf("Alarm: \"%s\" (Valid? %c)\r\n", this->name, areLimitsValid(this) ? 'Y' : 'N');
                for (ii = 0; ii < ALARM_LIMITS_NUM_LIMITS; ii++)
                {
                    printf("[%d] (%s) %d\r\n", ii, getLimitName(ii), this->limits.array[ii].i32);
                }
            }
            else
            {
                printf("Sorry, %d is an invalid alarm index.\r\n", inx);
            }
        }
        else
        {
            putstr("info [inx] \r\n");
        }
    }
    else
    {
        //Print error string.
    	Console_WriteByte('?');
    }
}


///##### Helper  Functions
static bool isDatumGreaterThan(T_SignalDatum a, T_SignalDatum b, T_TypeId type)
{
    if (type == TYPE_U32)
    {
        return (a.u32 > b.u32);
    }
    else if (type == TYPE_I32)
    {
        return (a.i32 > b.i32);
    }
    else
    {
        LOG_Event(LOG_LVL_FATAL, "Unknown type!");
        return false;        
    }
}

static bool isDatumLessThan(T_SignalDatum a, T_SignalDatum b, T_TypeId type)
{
    if (type == TYPE_U32)
    {
        return (a.u32 < b.u32);
    }
    else if (type == TYPE_I32)
    {
        return (a.i32 < b.i32);
    }
    else
    {
        LOG_Event(LOG_LVL_FATAL, "Unknown type!");
        return false;        
    }
}

static bool isNewLimitMoreRestrictive(T_AlarmTemplate* pAlarm, uint8_t limitInx, T_SignalDatum newLimit)
{
    switch (limitInx)
    {
        case ALARM_LIMIT_INX_CRITICAL_UPPER:    
        case ALARM_LIMIT_INX_WARN_UPPER:        
        case ALARM_LIMIT_INX_CLEAR_UPPER:  
            return (isDatumLessThan(newLimit, pAlarm->limits.array[limitInx], pAlarm->type));
        break;
            
        case ALARM_LIMIT_INX_CLEAR_LOWER:      
        case ALARM_LIMIT_INX_WARN_LOWER:        
        case ALARM_LIMIT_INX_CRITICAL_LOWER:    
            return (isDatumGreaterThan(newLimit, pAlarm->limits.array[limitInx], pAlarm->type));
        break;
        
        default: break;
    }
    
    return false;
    
}

static const char* getLimitName(uint8_t inx)
{
    switch (inx)
    {
        case ALARM_LIMIT_INX_CRITICAL_UPPER:    return "CritUpr";
        case ALARM_LIMIT_INX_WARN_UPPER:        return "WarnUpr";
        case ALARM_LIMIT_INX_CLEAR_UPPER:       return "ClrUpr";
        case ALARM_LIMIT_INX_CLEAR_LOWER:       return "ClrLwr";
        case ALARM_LIMIT_INX_WARN_LOWER:        return "WarnLwr";
        case ALARM_LIMIT_INX_CRITICAL_LOWER:    return "CritLwr";
            
        default:
            //Fall though
        break;
    }
    
    return "???";
}

//These functions check the validity of an alarm template.
static bool areLimitsValid(T_AlarmTemplate* pAlarm)
{
    if (!pAlarm)
    {
        return false;
    }
    
    /*In order for a list of alarm limits to be valid, the following must hold:
     *
     * CritHi >= WarnHi >= ClearHi >= ClearLo >= WarnLo >= CritLo 
     * 
     * Note that this allows for the possibilty that all of the limits are the
     * same, which would essentially define a threshold value above which the
     * alarm is STOP+ and below STOP-. If the value is exactly as indicated, it is clear.
     */
    
    uint8_t ii;
    T_SignalDatum a, b;
    for (ii = 1; ii < ALARM_LIMITS_NUM_LIMITS; ii++)
    {        
        a = pAlarm->limits.array[ii-1];
        b = pAlarm->limits.array[ii];
        
        if (isDatumLessThan(a, b, pAlarm->type))
        {
            //Uh-oh, something is out of order.
            return false;
        }
            
    }
    
    return true;
    
}

static void fixAlarmLimits(T_AlarmTemplate* this)
{
    if (areLimitsValid(this))
    {
        return;
    }
    
    //Upper Limits
    if (isDatumLessThan(this->limits.CriticalUpper, this->limits.WarnUpper, this->type))
    {
        this->limits.WarnUpper = this->limits.CriticalUpper;
    }
    
    if (isDatumLessThan(this->limits.WarnUpper, this->limits.ClearUpper, this->type))
    {
        this->limits.ClearUpper = this->limits.WarnUpper;
    }
    
    //Lower Limits
    if (isDatumGreaterThan(this->limits.CriticalLower, this->limits.WarnLower, this->type))
    {
        this->limits.WarnLower = this->limits.CriticalLower;
    }
    
    if (isDatumGreaterThan(this->limits.WarnLower, this->limits.ClearLower, this->type))
    {
        this->limits.ClearLower = this->limits.WarnLower;
    }
    
}




///##### Drawing Functions
static const char* instructionsMsg = NULL;
static T_AlarmTemplate* selectedTemplate =NULL;
static uint8_t selectedLimitInx = 0;
#define MAX_BUFFER_LENGTH (16)
static char buffer[MAX_BUFFER_LENGTH] = {0};
static uint8_t bufferLength = 0;
typedef enum
{
    ST_SELECTING_NAME,
    ST_SELECTING_LIMIT,
    ST_ENTERING_VALUE,
    ST_DONE,
} T_DrawState;

static T_AlarmLimits stashedLimits;

static T_DrawState state;
static void drawAlarmSelectWindow(void)
{
    putstr("\t\tALARM EDITOR\r\n\n");
    
    putstr("#\tName\r\n");
    uint8_t ii;
    for (ii = 0; ii < templateList.length; ii++)
    {
        printf("[%d]\t%s\r\n", ii, templateList.templates[ii].name);
    }

    VT100_FONT_BOLD();
    putstr("\r\n\nEnter the # of the alarm you want to edit,\r\n");
    putstr("or press ESC to quit.\r\n");
    VT100_FONT_NORMAL();
}

static void drawAlarmEditFrame(void)
{
    putstr("\t\tALARM EDITOR\r\n\n\n");
    
    putstr("Editing alarm: ");
    putstr(selectedTemplate ? selectedTemplate->name : "NULL");
    printf(" (%s)", sigh_Unit2Str(selectedTemplate->unit));
    NEW_LINE();
    if (selectedTemplate->unit == UNIT_CURRENT_MA)
    {
        putstr("Note: Typically, positive currents are charge & negative currents are discharge.");
    }
    NEW_LINE();
    NEW_LINE();
    putstr("    | +STOP+ |\r\n");
    putstr("(a)-|--------|->\r\n");
    putstr("    | +CRIT+ |\r\n");
    putstr("(b)-|--------|->\r\n");
    putstr("    | +WARN+ |\r\n");
    putstr("(c)-|--------|->\r\n");
    putstr("    | CLEAR  |\r\n");
    putstr("(d)-|--------|->\r\n");
    putstr("    | -WARN- |\r\n");
    putstr("(e)-|--------|->\r\n");
    putstr("    | -CRIT- |\r\n");
    putstr("(f)-|--------|->\r\n");
    putstr("    | -STOP- |\r\n");


}

static void printBoldDatum(T_SignalDatum datum, bool isBold)
{
    if (isBold)
    {
        VT100_FONT_BOLD();
    }
    
    SIG_PrintSignalDatum(datum, selectedTemplate->type);
    
    if (isBold)
    {
        VT100_FONT_NORMAL();
    }
    
}

static void drawAlarmValues(void)
{
    putstr("\r\n\n\n\n\n");
    
    
    uint8_t ii;
    for (ii = 0; ii < ALARM_LIMITS_NUM_LIMITS; ii++)
    {
        putstr("\r\n\n\t\t");
        VT100_CLEAR_LINE();
        printBoldDatum(selectedTemplate->limits.array[ii], (selectedLimitInx == ii));
    }
    
    
    putstr("\r\n\n\n");
    VT100_CLEAR_LINE();
    if (instructionsMsg)
    {
        VT100_FONT_BOLD();
        putstr(instructionsMsg);
        VT100_FONT_NORMAL();
    }
    
    putstr("\r\n");
    VT100_CLEAR_LINE();
    if (statusMsg)
    {
        putstr(statusMsg);
    }
    
}

static void changeStateTo(T_DrawState newState)
{
    int32_t newValue;
    T_SignalDatum d;
    
    switch (newState)
    {
        case ST_SELECTING_NAME:
            drawAlarmSelectWindow();
    
            selectedTemplate = NULL;
       
        break;
        
        case ST_SELECTING_LIMIT:
            selectedLimitInx = ALARM_LIMITS_NUM_LIMITS;  //intitially set this out of range so nothing is selected
            bufferLength = 0;
            memset(buffer, 0, MAX_BUFFER_LENGTH);
            
            VT100_CLEAR_SCREEN();
            drawAlarmEditFrame();
            VT100_MOVE_CURSOR_HOME();
            instructionsMsg = "Enter the ID of the limit you want to edit.";
            statusMsg = NULL;
            drawAlarmValues();
        break;
        
        case ST_ENTERING_VALUE:
            VT100_MOVE_CURSOR_HOME();    
            putstr("\n\nInput:");
            VT100_MOVE_CURSOR_HOME();
            instructionsMsg = "Enter the new value of the limit. Press RETURN when done.";
            drawAlarmValues();
        break;
        
        
        case ST_DONE:
             newValue = atoi(buffer);
            d.i32 = newValue;
                       
            editAlarmLimit(selectedTemplate, selectedLimitInx, d);
            VT100_MOVE_CURSOR_HOME();
            instructionsMsg = "Hit ESC to exit, or SPACE to edit another limit.";
            drawAlarmValues();
        break;
        
    default: break;
    }
    
    
    state = newState;
}

void Alarm_DrawInit()
{
    changeStateTo(ST_SELECTING_NAME);   
}


void Alarm_DrawUpdate()
{

    //We don't actually do anything here - everything is done in DrawInit & DrawInput.
 
        
}


void Alarm_DrawInput(uint8_t cin)
{
    uint8_t ind;
    uint8_t cin_lower = tolower(cin);
    switch (state)
    {
        case ST_SELECTING_NAME:
            if (cin >= '0' && cin <= '9')
            {
                ind = cin - '0';
                if (ind < templateList.length)
                {
                    selectedTemplate = &templateList.templates[ind];
                    changeStateTo(ST_SELECTING_LIMIT);
                }
                else
                {
                    printf("Sorry, '%d' is not a valid choice!\r\n", ind);
                }
            }
        break;
        
        case ST_SELECTING_LIMIT:
            if (cin_lower >= 'a' && cin_lower <= ('a' + ALARM_LIMITS_NUM_LIMITS))
            {
                selectedLimitInx = cin_lower - 'a';
                changeStateTo(ST_ENTERING_VALUE);
            }
            else
            {
                printf("Sorry, '%c' is not a valid choice!\r\n", cin);
            }
            
        break;
        
        case ST_ENTERING_VALUE:
            if (cin == '\r')   
            {
                buffer[bufferLength] = '\0';
                changeStateTo(ST_DONE);
            }
            if (bufferLength < MAX_BUFFER_LENGTH)
            {
                buffer[bufferLength++] = cin;
                //Force an update of the screen here to give immeditate feedback
                VT100_MOVE_CURSOR_HOME();
                putstr("\n\n\t");
                VT100_CLEAR_LINE();
                putstr(buffer);
            }
            
        break;
        
        
        case ST_DONE:
            if (cin == ' ')
            {
                changeStateTo(ST_SELECTING_LIMIT);
            }
        break;
    }
    

}
    

static void editAlarmLimit(T_AlarmTemplate* alarm, uint8_t inx, T_SignalDatum newValue)
{
    if (inx < ALARM_LIMITS_NUM_LIMITS)
    {
        if (isNewLimitMoreRestrictive(alarm, inx, newValue))
        {
            memcpy(&stashedLimits, &(alarm->limits), sizeof(stashedLimits));
            
            alarm->limits.array[inx] = newValue; 
            if (!areLimitsValid(alarm))
            {
                fixAlarmLimits(alarm);
                statusMsg = "New limit applied. \x1b[1mOther limits adjusted to make consistent.\x1b[0m";
            }
            else
            {
                statusMsg = "New limit applied.";
            }
            
                    
            //If our algorithm didn't fix it, just restore old settings.
            if (!areLimitsValid(alarm))
            {
                memcpy(&(alarm->limits), &stashedLimits, sizeof(stashedLimits));
                statusMsg = "New limit rejected. Restoring previous.";
            }
        }
        else
        {
            statusMsg = "New limit is not more restrictive than present limit! Rejecting.";
        }
    }
    else
    {
        snprintf(logBuffer, LOG_BUFFER_LENGTH, "Limit inx %d out of range!", inx);
        LOG_Event(LOG_LVL_FATAL, logBuffer);
    }
}
