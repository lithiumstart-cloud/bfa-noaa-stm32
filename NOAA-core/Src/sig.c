/**
*  @file sig.c
 * @brief The Signal management module abstracts application access
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-09-11
 *
 *
 * Signal Tree Axioms
 *  (1) A node with no children is called a leaf.
 *  (2) If a node has an alarm template, then the child types of all descendants
 *      MUST be consistent.
 *  (3) By definition, a leaf node has consistent child types.
 *  (4) Signals can only be directly written to leaf nodes.
 *  (5) Exactly one node can have no parents - this is called the "root".
 *
 *                                Node has alarm template?
 *                          |      Yes      |        No
 *                      ----|---------------|-----------------------
 *  Do any descendents   No | Evaluate self | Do nothing (no alarm)
 *    have alarms?      ----+---------------+------------------------
 *                      Yes | Evaluate self |  Inherit alarm levels
 *                          |               |  from children with alarms
 *
 *
 * 
**/

//######################### MODULE INCLUDES ##################################
#include "sig.h"
#include "bluflex_core.h"
#include "core_config.h"
#include "print_as.h"
#include "heap.h"
#include "sig_helper.h"

//######################### MODULE TYPES #####################################

#define MAX_PRINT_DEPTH (8)
#define USE_STATIC_SIGS

#ifdef USE_STATIC_SIGS
#define MAX_NUM_NAME_BYTES (2048)
#endif

#define MAX_NUM_NODES (LIB_MAX_NUM_SIGNAL_NODES)

#define BITS_PER_ALARM_STATE (2)
#define ALARM_STATES_PER_WORD (32/BITS_PER_ALARM_STATE)
#define NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE CEILING_DIV((MAX_NUM_NODES), (ALARM_STATES_PER_WORD))



typedef enum
{
    ST_TREE_STABLE = 0, 
    ST_UPDATING_TREE,
} T_State;

// <editor-fold defaultstate="collapsed" desc="Configuration Values">
//########################### CONFIG VALUES ####################################
static struct
{
    uint32_t treeUpdatePeriod_ms;
    uint32_t maxRuntimePerCall_ms;
    uint32_t maxDrawDepth;
    uint32_t printStopValues;
    uint32_t maxExpNumCallsToProcessTree;
       

} configValues =
{
    /*Default Values*/
    .treeUpdatePeriod_ms = 20,
    .maxRuntimePerCall_ms = 1,
    .maxDrawDepth = 4,
    .printStopValues = true,
    .maxExpNumCallsToProcessTree = 6,
    
};

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "treeUpdatePeriod_ms",
    "maxRuntimePerCall_ms",
    "maxDrawDepth",
    "printStopValues?",
    "printNumCallsToUpdateMin",
};

// </editor-fold>
//######################### MODULE VARIABLES ###################################

static T_Timer tmrTreeUpdate;
static T_Timer tmrSingleUpdate;
static T_State currentState;

#ifdef USE_STATIC_SIGS

static struct
{
    uint8_t bytes[MAX_NUM_NAME_BYTES];
    uint32_t length;
} nameHeap;

//This is the list of ALL the nodes, in the order they are created.
static struct
{
    T_SignalNode nodes[MAX_NUM_NODES];
    uint16_t length;
} nodeList;

#else

static uint32_t numAllocatedNodes;

#endif

static bool drawInit;

static T_SignalNode* rootNode;
static int32_t drawRootFlatInd;
static struct
{
    T_SignalNode* nodes[MAX_NUM_NODES];
    uint8_t depths[MAX_NUM_NODES];
    uint16_t length;
} flatTree;

static T_LED* pMasterAlarmLed = NULL;

//Each "WorstAllTime" Alarm state is 2bits
static uint32_t worstEverBuffer[NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE];
static T_MemAddr vAddrWorstEver;
static uint16_t flatTreeUpdateInx = 0;  

//########################### PRIVATE PROTOTYPES ###############################
static T_SignalNode* literalNameLookup(const char* name);
static const T_SignalNode* complexNameLookup(const char* name);
static void updateNode(T_SignalNode* theNode);
static uint16_t flattenTree(T_SignalNode* start, T_SignalNode* destArr[], uint8_t depthsArr[], uint16_t length);
static void packIntoWorstEverBuffer(void);
static void unpackFromWorstEverBuffer(void);
static char* copyNamespace(const char* srcName);
static int32_t pointer2TreeInx(const T_SignalNode* pNode);
static const T_SignalNode* lookupFromAddressStr(const char* str);
static bool updateTreeUntilTimeout(void);
static void updateMasterAlarmLed(void);
static void printIndentedData(T_SignalNode* node, uint8_t depth);
//########################### PUBLIC CODE ######################################

void SIG_Init()
{

    Alarm_Init(); //SIG requires Alarms

#ifdef USE_STATIC_SIGS
    nodeList.length = 0;
    nameHeap.length = 0;
    memset(nameHeap.bytes, '\0', MAX_NUM_NAME_BYTES);
#else    
    numAllocatedNodes = 0;
#endif
    
    drawInit = false;

    uint32_t ii;
    for (ii = 0; ii < MAX_NUM_NODES; ii++)
    {
        flatTree.nodes[ii] = NULL;
        flatTree.depths[ii] = 0;
    }

    flatTree.length = 0;
    rootNode = NULL;
    drawRootFlatInd = -1;
    
    //Handle WorstEver Alarm states stored in NVRAM 
    vAddrWorstEver = MEM_AllocateData(NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE);
    memset(worstEverBuffer, 0x00, sizeof(worstEverBuffer));
    MEM_LoadDefaultData("Alm_WorstEver", vAddrWorstEver, worstEverBuffer, NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE);
    
    TMR_StartRepeating(&tmrTreeUpdate, configValues.treeUpdatePeriod_ms);
    currentState = ST_TREE_STABLE;
       
}
    
bool SIG_Update()
{
    static uint8_t updateCalls;
    
    switch (currentState)
    {
        case ST_TREE_STABLE:
            if (TMR_HasTripped_ms(&tmrTreeUpdate))
            {
                //Start a tree update!
                flatTreeUpdateInx = 0;
                updateCalls  = 1;
                currentState = ST_UPDATING_TREE;
            }
        break;
        
        case ST_UPDATING_TREE:
            if (TMR_HasTripped_ms(&tmrTreeUpdate))
            {
                printf("Warning! Tree update took long enough that next trigger was missed.\r\n");
            }
            
            //Update as much as we can
             if (updateTreeUntilTimeout())
             {
                 //We finished!
                 updateMasterAlarmLed();
                 currentState = ST_TREE_STABLE;
                 if (updateCalls >= configValues.maxExpNumCallsToProcessTree)
                 {
                     printf("SIG update took %d call(s).\r\n", updateCalls);
                 }
              }
             else
             {
                 updateCalls++;
                 //need to continue next time...
             }
        break;
              
    }
    
    return true;
}

bool SIG_IsReady()
{   
    return SIG_IsSignalReady(rootNode);
}
    
bool SIG_ValidateTree(T_SignalNode* root)
{
    if (!root)
    {
        return false;
    }

    rootNode = root;
    flatTree.length = flattenTree(rootNode, flatTree.nodes, flatTree.depths, MAX_NUM_NODES);

    if (flatTree.length == 0)
    {
        return false;
    }

#ifdef USE_STATIC_SIGS
    if (flatTree.length != nodeList.length)
    {
        puts("Signal tree length does NOT match # allocated nodes!");
        return false;
    }
#else
    if (flatTree.length != numAllocatedNodes)
    {
        puts("Signal tree length does NOT match # allocated nodes!");
        return false;
    }
#endif

            
  uint32_t ii;

    for(ii = 0; ii < flatTree.length; ii++)
    {
        T_SignalNode *node = flatTree.nodes[ii];

        if (node->pAlarmTemplate && !node->childTypesConsistent)
        {
            NEW_LINE();
            SIG_PrintName(node);
            puts(" has alarm but inconsistent child types.");
            return false;
        }

        if (node->pAlarmTemplate)
        {
            node = node->parentNode;
            //Start walking up the tree, and tell are parents they have a descendant
            //with an alarm.
            while (node)
            {
                node->anyDescendentsHaveAlarms = 1;
                node = node->parentNode;
            }
        }

    }
  
    unpackFromWorstEverBuffer();
    return true;
}

void SIG_ResetLatchedAlarms()
{
    uint32_t ii;
    for (ii = 0; ii < flatTree.length; ii++)
    {
        T_AlarmLevel now = flatTree.nodes[ii]->alarm.now;
        flatTree.nodes[ii]->alarm.sinceBoot = now;
        flatTree.nodes[ii]->alarm.worstEver = now;
    }
}

void SIG_SetTreeUpdatePeriod(uint32_t period_ms, bool announce)
{
    if (period_ms)
    {
        configValues.treeUpdatePeriod_ms = period_ms;
        TMR_StartRepeating(&tmrTreeUpdate, configValues.treeUpdatePeriod_ms);
        
        if (announce)
        {
            printf("SIG: Tree update period <-- %u ms.\r\n", configValues.treeUpdatePeriod_ms);
        }
    }
    else
    {
        puts("SIG: Tree updated period cannot be set to 0ms!");
    }
}

uint32_t SIG_GetTreeUpdatePeriod_ms()
{
    return configValues.treeUpdatePeriod_ms;
}

bool SIG_IsTreeStable()
{
    return (currentState == ST_TREE_STABLE);
}

void SIG_PrintSignalDatum(T_SignalDatum sigDat, T_TypeId type)
{
    sigh_PrintDatumWidth(sigDat, type, 0);
}

void SIG_AssignMasterAlarmLed(T_LED* pLed)
{
    pMasterAlarmLed = pLed;
}

void SIG_ListUninitialzed()
{
    putstr("Uninitalized signals:");
    uint32_t ii;
    for (ii =0; ii < flatTree.length; ii++)
    {
        T_SignalNode* this = flatTree.nodes[ii];
        if (!this->isInitialized)
        {
            putstr("\r\n\t");
            SIG_PrintName(this);
        }
    }
    NEW_LINE();
}

void SIG_PrepareForShutdown()
{
    packIntoWorstEverBuffer();
    if (MEM_WriteDataFromRamToNvm(vAddrWorstEver, worstEverBuffer, NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE))
    {
        puts("Saved WorstEver Alarm states to NVRAM.");
    }
    else
    {
        puts("FAILED to save WorstEver Alarm states to NVRAM.");
    }
}

void SIG_SetMaxExpNumCallsToProcessTree(uint8_t n)
{
    configValues.maxExpNumCallsToProcessTree = n;
}

//######################### OBJECT FUNCTIONS ###################################
// <editor-fold defaultstate="collapsed" desc="Object Functions">

T_SignalNode* SIG_CreateNode(const char* name, T_UnitId unitId, T_AlarmTemplate* pAlarmTemplate)
{
   
#ifdef USE_STATIC_SIGS
    if (nodeList.length >= MAX_NUM_NODES)
    {
        //No more room for nodes.
        puts("Exceeded the maximum allowed number of signal nodes!");
        return NULL;
    }
#else
    if (numAllocatedNodes >= MAX_NUM_NODES)
    {
        //No more room for nodes.
        puts("Exceeded the maximum allowed number of signal nodes!");
        return NULL;
    }
#endif   

    //Create new node!
    
#ifdef USE_STATIC_SIGS
    T_SignalNode* newNode = &(nodeList.nodes[nodeList.length]);
#else
    T_SignalNode* newNode = Heap_Allocate(sizeof(T_SignalNode));
    
    if (!newNode)
    {
        puts("Signal malloc failed!");
        return NULL;
    }
#endif
    
    newNode->numChildren = 0;
    newNode->minChildInx = 0;
    newNode->maxChildInx = 0;
    newNode->birthOrderInx = 0;
    newNode->parentNode = NULL;
    newNode->nextSibling = NULL;
    newNode->firstChild = NULL;
    newNode->bypass = false;
    
    if (name != ANONYMOUS)
    {
        newNode->name = copyNamespace(name);

        if (newNode->name == NULL)
        {
            printf("Failed to copy name: \"%s\".\r\n", name);
            return NULL;
        }   
    }

    newNode->unit = unitId;
    newNode->type = sigh_Unit2Type(unitId);
    
    newNode->pAlarmTemplate = pAlarmTemplate;

    
    
    newNode->childTypesConsistent = 1; //if no children, this is defined to be TRUE
    newNode->isInitialized = 0; //true once writeSignal() or updateNode() has been called on it
    newNode->anyDescendentsHaveAlarms = 0; //if no children, this is defined to be FALSE
    newNode->isAlarmSuspended = 0;
    
    /*
     * We want to initalize the max to the lowest value possible,
     * and the minimum to the highest value possible. That way, these will
     * automatically be (re)set the first time this node is updated.
     */
    newNode->max = sigh_Type2Min(newNode->type);
    newNode->min = sigh_Type2Max(newNode->type);

#ifdef USE_STATIC_SIGS
    nodeList.length++;
#else
    numAllocatedNodes++;
#endif
    return newNode;

}
    
bool SIG_MakeNodeChildOf(T_SignalNode* node, T_SignalNode* parent)
{
    if (node == NULL || parent == NULL)
    {
        return false;
    }

   if (node->parentNode)
    {
        //node already has parent! Caller is attempting kidnapping.
        return false;
    }

    if (parent->numChildren >= MAX_NUM_CHILDREN_PER_NODE)
    {
        //node can't support any more children
        return false;
    }


    //Tell the parent about its new child. (MAZEL TOV, IT'S A BOY!)
    if (parent->firstChild == NULL)
    {
        parent->firstChild = node;   
    }
    else
    {
        T_SignalNode* thisChild = parent->firstChild;
        //Iterate through list to get youngest child
        while (thisChild->nextSibling)
        {
            thisChild = thisChild->nextSibling;
        }

        thisChild->nextSibling = node;
    }

    //Tell the node about its new parent.
    node->parentNode = parent;
    node->nextSibling = NULL; //no younger siblings (yet)
    node->birthOrderInx = parent->numChildren;

    parent->numChildren++;
    
    //If the parent type doesn't match child type, set FALSE.
    if (node->type != parent->type)
    {
        parent->childTypesConsistent = 0;
    }

    return true;
}

bool SIG_WriteSignal(T_SignalNode* node, T_SignalDatum value)
{
    if (node == NULL)
    {
        LOG_Event(LOG_LVL_ERROR,  "Attempted post to NULL signal!");   
        return false;
    }

    if (node->numChildren > 0)
    {
        //You can only write to a leaf!
        snprintf(logBuffer, LOG_BUFFER_LENGTH, "Attempted post to non-leaf signal \"%s\".", node->name ? node->name : "ANONYMOUS");
        LOG_Event(LOG_LVL_ERROR, logBuffer);
        
        return false;
    }

    node->max = value;
    node->min = value;
    node->avg = value;
    node->sum = value;
    
    node->age = 0;

    //Update this node's alarm.
    if (node->pAlarmTemplate)
    {
        T_AlarmLevel newLevel = Alarm_Evaluate(node->pAlarmTemplate, node->avg);
        
        if (configValues.printStopValues && newLevel == ALARM_LVL_STOP && node->alarm.now != ALARM_LVL_STOP)
        {
            sigh_PrintStopAlarmValues(node);
        }
        
        Alarm_SetLevel(&node->alarm, newLevel);
    }

    
    node->isInitialized = 1; //This node has been written to, so it is "ready"!

    /*
     * We want to walk up the tree and tell all the nodes above us that they
     * need to update.
     */
    while (node->parentNode)
    {
        node = node->parentNode;
        node->needsUpdate = 1;
    }

    //DO NOT DO ANYTHING TO NODE AFTER THIS - POINTER HAS CHANGED!
    
    return true;
    
}

bool SIG_WriteToChildren_U32(T_SignalNode* parent, const uint32_t* data, uint8_t numData)
{
    if (!parent || parent->numChildren != numData)
    {
        return false;
    }

    T_SignalNode* theChild = parent->firstChild;

    //We don't call SIG_WriteSignal here, since there's no reason to tell
    //the same parents each time they need an update - we just do it at the
    //end.

    parent->sum.u32 = 0;
    parent->avg.u32 = 0;
    parent->max.u32 = 0;
    parent->min.u32 = UINT32_MAX;
    parent->minChildInx = 0;
    parent->maxChildInx = 0;
    uint8_t ii = 0;
    while (theChild)
    {
        if (theChild->numChildren > 0)
        {
            return false; //you can only write to a leaf!
        }

        uint32_t thisChildsData_U32 = data[ii];
        theChild->max.u32 = thisChildsData_U32;
        theChild->min.u32 = thisChildsData_U32;
        theChild->avg.u32 = thisChildsData_U32;
    
        theChild->age = 0;

        if  (thisChildsData_U32 > parent->max.u32)
        {
            parent->max.u32 = thisChildsData_U32;
            parent->maxChildInx = ii;
        }
        
        if (thisChildsData_U32 < parent->min.u32)
        {
            parent->min.u32 = thisChildsData_U32;
            parent->minChildInx = ii;
        }
        
        parent->sum.u32 += thisChildsData_U32;
        
        //Update this node's alarm.
      
        if (theChild->pAlarmTemplate)
        {
            T_AlarmLevel newLevel = Alarm_Evaluate(theChild->pAlarmTemplate, theChild->avg);

            if (configValues.printStopValues && newLevel == ALARM_LVL_STOP && theChild->alarm.now != ALARM_LVL_STOP)
            {
                sigh_PrintStopAlarmValues(theChild);
            }

            Alarm_SetLevel(&theChild->alarm, newLevel);
        }

        theChild->isInitialized = 1;

        theChild = theChild->nextSibling;
        ii++;

    }

    parent->avg.u32 = (parent->sum.u32)/ii;
    
    //Update parents alarm
     if (parent->pAlarmTemplate)
    {
        T_AlarmLevel newLevel_min = Alarm_Evaluate(parent->pAlarmTemplate, parent->min);
        T_AlarmLevel newLevel_max = Alarm_Evaluate(parent->pAlarmTemplate, parent->max);
        T_AlarmLevel newLevel = newLevel_min > newLevel_max ? newLevel_min : newLevel_max;

        if (configValues.printStopValues && newLevel == ALARM_LVL_STOP && parent->alarm.now != ALARM_LVL_STOP)
        {
            sigh_PrintStopAlarmValues(parent);
        }

        Alarm_SetLevel(&parent->alarm, newLevel);
    }
    
    parent->isInitialized = 1;
    parent->age = 0;
    /*
     * We want to walk up the tree and tell all the nodes above us that they
     * need to update.
     * We do not need to mark the parent as needing an update, since we did that.
     */
    parent = parent->parentNode;
    do
    {
        parent->needsUpdate = 1;
        parent = parent->parentNode;
    } while (parent);

    return true;
}

bool SIG_WriteToChildren(T_SignalNode* parent, const T_SignalDatum* data, uint8_t numData)
{
    if (!parent || parent->numChildren != numData)
    {
        return false;
    }

    T_SignalNode* node = parent->firstChild;

    //We don't call SIG_WriteSignal here, since there's no reason to tell
    //the same parents each time they need an update - we just do it at the
    //end.

    while (node)
    {
        if (node->numChildren > 0)
        {
            return false; //you can only write to a leaf!
        }

        node->max = *data;
        node->min = *data;
        node->avg = *data;
        node->sum = *data;
    
        node->age = 0;

        //Update this node's alarm.
      
        if (node->pAlarmTemplate)
        {
            T_AlarmLevel newLevel = Alarm_Evaluate(node->pAlarmTemplate, node->avg);

            if (configValues.printStopValues && newLevel == ALARM_LVL_STOP && node->alarm.now != ALARM_LVL_STOP)
            {
                sigh_PrintStopAlarmValues(node);
            }

            Alarm_SetLevel(&node->alarm, newLevel);
        }

        node->isInitialized = 1;

        node = node->nextSibling;
        data++;

    }

    /*
     * We want to walk up the tree and tell all the nodes above us that they
     * need to update.
     */
    node = parent;
    do
    {
        node->needsUpdate = 1;
        node = node->parentNode;
    } while (node);

    return true;
}

bool SIG_IsSignalReady(const T_SignalNode* this)
{
    if (this)
    {
        return this->isInitialized;
    }
    else
    {
        LOG_Event(LOG_LVL_FATAL, "Attempt to check NULL signal for readiness!");   
        return false;
    }
}

void SIG_ClearAlarmChangeFlag(const T_SignalNode* node)
{
    if (node)
    {
        //Un-const it..
        ((T_SignalNode* ) node)->alarm.hasChanged = false;
    }
}

void SIG_PrintData(const T_SignalNode* pNode, T_SigRequest rqst)
{
    if (pNode)
    {
        SIG_PrintSignalDatum(SIG_GetData(pNode, rqst), pNode->type);
    }
    else
    {
        LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    }
}

void SIG_PrintName(const T_SignalNode* node)
{

    static const T_SignalNode* stack[MAX_PRINT_DEPTH];
    static uint8_t stackInx = 0;

    if (!node)
    {
        putstr("null");
        return;
    }
    
    stackInx = 0;
    const T_SignalNode* originalNode = node;
    
    //Walk from child to parent, until we find a node with a name. Push each
    //node onto a stack for unwidning later.
    while (node->name == ANONYMOUS)
    {
        stack[stackInx] = node;
        stackInx++;
        
        if (stackInx >= MAX_PRINT_DEPTH)
        {
            //If we've gone too deep, just print address of original node and quit.
            PrintAs_Hex_U32((uint32_t) originalNode);
            return;
        }
        
        node = node->parentNode;
    }
    
    //Print the node with the local name!
    putstr(node->name);
    
    //Now unwind the stack and print out each child's birth order
    while (stackInx > 0)
    {
        stackInx--;
        node = stack[stackInx];
        printf(".%u", node->birthOrderInx + 1);
    }
    
}

void SIG_PrintQuickName(const T_SignalNode* node)
{
    if (node && (node->name != ANONYMOUS))
    {
        putstr(node->name);
    }
    else
    {
        PrintAs_Hex_U32((uint32_t) node);
    }
}

const char* SIG_GetName(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->name ? pNode->name : "Anonymous";
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);

    return NULL;
}

T_SignalDatum SIG_GetData(const T_SignalNode* pNode, T_SigRequest rqst)
{
    T_SignalDatum data = {.u32 = 0};

    if (pNode == NULL)
    {
        LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
        return data;
    }
    
    switch (rqst)
    {
        case SIG_RQST_AVG:
            data.u_all = pNode->avg.u_all;
        break;

        case SIG_RQST_MIN:
            data.u_all = pNode->min.u_all;
        break;

        case SIG_RQST_MAX:
            data.u_all = pNode->max.u_all;
        break;

        case SIG_RQST_STD:
            //NOTE: Standard deviation is not implemented...
            data.u_all = 0;
        break;

        case SIG_RQST_ALARM:
            data.u_all = 0;
            data.u8 = Alarm_PackIntoByte(&pNode->alarm);
        break;

        case SIG_RQST_SUM:
            data.u_all = pNode->sum.u_all;
        break;

        default:

        break;
    }

    return data;
}

uint32_t SIG_GetData_U32(const T_SignalNode* pNode, T_SigRequest rqst)
{
    T_SignalDatum d = SIG_GetData(pNode, rqst);
    return d.u32;
}

int32_t SIG_GetData_I32(const T_SignalNode* pNode, T_SigRequest rqst)
{
    T_SignalDatum d = SIG_GetData(pNode, rqst);
    return d.i32;
}

uint32_t SIG_GetAvg_U32(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->avg.u32;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return 0;

}

T_SignalNode* SIG_GetFirstChild(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->firstChild;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return NULL;
}

T_SignalNode* SIG_GetNextSibling(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->nextSibling;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return NULL;
}

T_SignalNode* SIG_GetParent(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->parentNode;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return NULL;
}

T_TypeId SIG_GetType(const T_SignalNode* pNode)
{

    if (pNode)
    {
        return pNode->type;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return TYPE_U32;
}

T_AlarmBitmap SIG_GetAlarm(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->alarm;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    T_AlarmBitmap am;
    return am;
}

T_AlarmLevel SIG_GetAlarmNow(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->alarm.now;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return ALARM_LVL_CLEAR;
}

bool SIG_HasAlarmChanged(const T_SignalNode* pNode)
{
     if (pNode)
    {
        return pNode->alarm.hasChanged;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return false;
}

uint32_t SIG_GetAge(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->age;
    }
    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return UINT32_MAX;
}

uint32_t SIG_GetNumChildren(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->numChildren;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return 0;
}

uint8_t SIG_GetMinChildInx(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->minChildInx;
    }
    
    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return 0;
}

uint8_t SIG_GetMaxChildInx(const T_SignalNode* pNode)
{
 
    if (pNode)
    {
        return pNode->maxChildInx;
    }
    
    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
    return 0;
}

T_SignalNode* SIG_GetChild(const T_SignalNode* pNode, uint32_t inx)
{
    if (inx < pNode->numChildren)
    {
        T_SignalNode* thisChild = pNode->firstChild;

        uint32_t ii;
        for (ii = 0; ii < inx; ii++)
        {
            if (thisChild)
            {
                thisChild = thisChild->nextSibling;
            }
            else
            {
                LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_SIZE_MISMATCH, __LINE__);
                return NULL;
            }
        }

        return thisChild;
    }

    LOG_Report(LOG_MOD_SIG, LOG_LVL_FATAL,  LOG_MSG_SIZE_MISMATCH, __LINE__);
    return NULL;

}

T_SignalNode* SIG_GetCousin(const T_SignalNode* pNode)
{
    if (pNode)
    {
        T_SignalNode* uncle = SIG_GetNextSibling(SIG_GetParent(pNode));
             
        //If the uncle exists, return its first child 
        if (uncle)
        {
            return SIG_GetFirstChild(uncle);
        }
        //If not, then this is the end of the line!
        return NULL;
    }
    
    snprintf(logBuffer, LOG_BUFFER_LENGTH, "Null ptr error at %d", __LINE__);
    LOG_Event(LOG_LVL_FATAL, logBuffer);
    
     return NULL;
    
}

T_SignalNode* SIG_GetNextInGeneration(const T_SignalNode* pNode)
 {
     T_SignalNode* next = NULL;
     if (pNode)
     {
         next = SIG_GetNextSibling(pNode);
         
         //If this node is out of siblings, try to find a "cousin".
         if (next == NULL)
         {
             next =  SIG_GetCousin(pNode);
         }
         else
         {
             //If the node has another sibling, use it. 
         }
         
         return next;
     }
  
    snprintf(logBuffer, LOG_BUFFER_LENGTH, "Null ptr error at %d", __LINE__);
    LOG_Event(LOG_LVL_FATAL, logBuffer);
    
     return NULL;
    
 }

T_AlarmTemplate* SIG_GetAlarmTemplate(const T_SignalNode* pNode)
{
    if (pNode)
    {
        return pNode->pAlarmTemplate;
    }
    
    return NULL;
}

void SIG_SetIsAlarmSuspended(T_SignalNode* pNode, bool set)
{
    if (pNode)
    {
        pNode->isAlarmSuspended = set;
    }
}

void SIG_SetSignalBypassFlag(T_SignalNode* pNode, bool set)
{
    if (pNode)
    {
        pNode->bypass = set;
    }
}
// </editor-fold>
//######################### DRAW/CLI FUNCTIONS #################################

void SIG_DrawInit()
{
    if (flatTree.length > 0)
    {
        if (drawRootFlatInd < 0)
        {
            drawRootFlatInd = flatTree.length - 1;
        }

        uint8_t startDepth = flatTree.depths[drawRootFlatInd];

        sigh_PrintIndentedNameUnits(flatTree.nodes[drawRootFlatInd], 0);

        uint32_t ii = drawRootFlatInd;
        while (ii)
        {
            ii--;
            int8_t relativeDepth = flatTree.depths[ii] - startDepth;
            if (relativeDepth > 0)
            {
                if (relativeDepth < configValues.maxDrawDepth)
                {
                    sigh_PrintIndentedNameUnits(flatTree.nodes[ii], relativeDepth);
                }
            }
            else
            {
                //We've gone back out further than where we started.
                break;
            }
        }
    }
    else
    {
        puts("Bad SIG Draw Init conditions!");
    }

    drawInit = true;
}

void SIG_DrawUpdate()
{

    if (flatTree.length && (drawRootFlatInd >= 0))
    {
        uint8_t startDepth = flatTree.depths[drawRootFlatInd];
        printIndentedData(flatTree.nodes[drawRootFlatInd],0);

        uint32_t ii = drawRootFlatInd;
        while (ii)
        {
            ii--;
            int8_t relativeDepth = flatTree.depths[ii] - startDepth;
            if (relativeDepth > 0)
            {
                if (relativeDepth < configValues.maxDrawDepth)
                {
                    printIndentedData(flatTree.nodes[ii], relativeDepth);
                }
            }
            else
            {
                //We've gone back further out than we started.
                break;
            }
        }
    }
    else
    {
        puts("Bad SIG Draw Update conditions!");
    }
    drawInit = false;
}



void SIG_ProcessInput(const char* tokens[], uint8_t nTokens)
{
    uint32_t ii;
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("config");
        puts("list");
        puts("info-name [name]\tPrint details about given signal");
        puts("info-addr [addr]\tPrint details about given signal");
        puts("draw-root [name]\tSet the root of FULL SIG");
        puts("list-uninit\tList any uninitalized signals, if any");
        puts("usage\tGet info about how many signals are used/free.");
        puts("shutdown\tPerform any NVRAM operations necessary for shutdown.");

    }
    else if (stricmp(tokens[0], "config") == 0)
    {
      CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, tokens, nTokens);
    }
    else if (stricmp(tokens[0],"list") == 0)
    {
        //Print nodes in reverse DFS order.
        printf("\r\nThere are %lu signal(s).\r\n", flatTree.length);

        VT100_FONT_UNDERLINE();
        puts("Name\tUnit\tAlarm\t#Kids");
        VT100_FONT_NORMAL();

        for (ii = 0; ii < flatTree.length; ii++)
        {
            T_SignalNode* node = flatTree.nodes[ii];
            SIG_PrintName(node);
            printf("\t%s\t%s\t%u\r\n", sigh_Unit2Str(node->unit), node->pAlarmTemplate ? node->pAlarmTemplate->name : "n/a", node->numChildren);
        }
    }
    else if (stricmp(tokens[0], "info-name") == 0)
    {
        if (nTokens == 2)
        {
            //Lookup node!
            const T_SignalNode* goalNode = complexNameLookup(tokens[1]);
            
            if (goalNode)
            {
                sigh_DetailedPrintNode(goalNode);
            }
            else
            {
                printf("Unable to find node \"%s\"...\r\n", tokens[1]);
            }
        }
        else
        {
            puts("sig info-name [name]");
        }
    }
    else if (stricmp(tokens[0], "info-addr") == 0)
    {
        if (nTokens == 2)
        {
            //Lookup node!
            const T_SignalNode* goalNode = lookupFromAddressStr(tokens[1]);
            
            if (goalNode)
            {
                sigh_DetailedPrintNode(goalNode);
            }
            else
            {
                printf("Unable to find node @ \"%s\"...\r\n", tokens[1]);
            }
        }
        else
        {
            puts("sig info-addr [addr]");
        }
    }
    else if (stricmp(tokens[0],"usage") == 0)
    {
        printf("Signals: %lu/%lu\r\n", flatTree.length, MAX_NUM_NODES);
        printf("sizeof(T_SignalNode) = %lu\r\n", sizeof(T_SignalNode));
        printf("sizeof(T_AlarmBitmap) = %lu\r\n", sizeof(T_AlarmBitmap));
        printf("sizeof(T_TypeId) = %lu\r\n", sizeof(T_TypeId));
        printf("sizeof(T_UnitId) = %lu\r\n", sizeof(T_UnitId));
        printf("NVRAM: %lu words starting at 0x%08X\r\n", NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE, vAddrWorstEver);
        
    }
    else if (stricmp(tokens[0], "draw-root") == 0)
    {
        if (nTokens == 2)
        {
            const T_SignalNode* foundNode = complexNameLookup(tokens[1]);
            int32_t foundInd = pointer2TreeInx(foundNode);
            
            if (foundInd >= 0)
            {
                drawRootFlatInd = foundInd;
                printf("Set DrawRoot to ");
                SIG_PrintName(foundNode);
                NEW_LINE();
            }
            else
            {
                printf("Unable to find signal \"%s\"...\r\n", tokens[1]);
            }
        }
        else
        {   
            drawRootFlatInd =  -1;
        }
    }
    else if (stricmp(tokens[0], "list-uninit") == 0)
    {
        printf("All signals ready? %c\r\n", SIG_IsReady() ? 'Y' : 'N');
        
        SIG_ListUninitialzed();
    }
    else if (stricmp(tokens[0], "shutdown") == 0)
    {
        SIG_PrepareForShutdown();
    }
    else
    {
    	Console_WriteByte('?');
    }

}


// <editor-fold defaultstate="collapsed" desc="Private Code">
//########################## PRIVATE CODE ######################################


/**
 * Request latest data from children nodes, and update alarms, min, max, avg.
 * Has no effect if node is leaf, or node does not have needsUpdate flag set.
 * @param theNode Pointer to node to update.
 * @param depth How many levels deep is this node?
 */
static void updateNode(T_SignalNode* theNode)
{
    uint8_t howManyChildrenIsBypassed = 0;

    if (theNode == NULL || theNode->numChildren == 0 || theNode->needsUpdate == 0)
    {
        return; //throw null pointer err
    }

    theNode->needsUpdate = 0; //we're updating, so we can deflag
    theNode->age = 0;

   
    T_AlarmLevel newLevel = ALARM_LVL_CLEAR;

    //A node's SUM is the sum of all of its child nodes' SUMs.
    //The AVG of a node is the average of all of its child nodes' AVGs.

    theNode->sum.u32 = 0; //clear the working reg, where we will put the sum
    theNode->avg.u32 = 0;
    theNode->max = sigh_Type2Min(theNode->type);
    theNode->min = sigh_Type2Max(theNode->type);
    theNode->minChildInx = 0;
    theNode->maxChildInx = 0;


    T_SignalNode* theChild = theNode->firstChild;
    bool areAllChildrenReadyThisTime = true;
    uint8_t ii = 0;
    while (theChild)
    {
        /* If this node isn't initialized ("ready"), check if ALL of its children are 
         * (for each child). After iterating over entire child list, this node 
         * will be marked ready if, and only if, all of its children are ready.
         */
        
        if (!theNode->isInitialized)
        {
            areAllChildrenReadyThisTime &= theChild->isInitialized;
        }
        
        //Include child's alarm in this node's alarm iff alarm is not suspended.
        if (!theChild->isAlarmSuspended)
        {
            newLevel = theChild->alarm.now > newLevel ? theChild->alarm.now : newLevel;
        }
        
        howManyChildrenIsBypassed += theChild->bypass ? 1 : 0;
        
        if(theChild->bypass == false)
        {
            //Inherit max,min,avg iff types match
            if (theNode->childTypesConsistent)
            {
                //Is this child's max greater than my running max?
                if (sigh_SmartIsGreaterThan(theNode->type, theChild->max, theNode->max))
                {
                    //Yes!
                    theNode->max = theChild->max;
                    theNode->maxChildInx = ii;
                }

                //Is this child's min less than my running min? (OR, is my running min greater than child's min?)
                if (sigh_SmartIsGreaterThan(theNode->type, theNode->min, theChild->min))
                {
                    //Yes!
                    theNode->min = theChild->min;
                    theNode->minChildInx = ii;
                }

                theNode->sum = sigh_SmartAdd(theNode->type, theNode->sum, theChild->sum);
                theNode->avg = sigh_SmartAdd(theNode->type, theNode->avg, theChild->avg);
            }            
        }

        theChild = theChild->nextSibling;
        ii++;
    }
            
    if(howManyChildrenIsBypassed == 0)
    {
        theNode->bypass = false; // clear bypass for parent if none of its children is bypassed 
    }
    else
    {
        theNode->bypass = true; // if any child is bypassed, make its parent bypassed
    }    

    uint8_t numChildrenVsBypassedChildren = theNode->numChildren - howManyChildrenIsBypassed;
    theNode->avg = sigh_SmartDivide(theNode->type, theNode->avg, numChildrenVsBypassedChildren);
    
    if( theNode->pAlarmTemplate)
    {
        if (theNode->childTypesConsistent)
        {
            //We do have a template. Reval with the new max, min, avg.
            T_AlarmLevel newLevel_min = Alarm_Evaluate(theNode->pAlarmTemplate, theNode->min);
            T_AlarmLevel newLevel_max = Alarm_Evaluate(theNode->pAlarmTemplate, theNode->max);
            newLevel = newLevel_min > newLevel_max ? newLevel_min : newLevel_max;
            
            if (configValues.printStopValues && newLevel == ALARM_LVL_STOP && theNode->alarm.now != ALARM_LVL_STOP)
            {
                sigh_PrintStopAlarmValues(theNode);
            }
        }
        else
        {
            //Error! We can't apply alarm template to inconsistent types.
            newLevel = ALARM_LVL_STOP;
            snprintf(logBuffer, LOG_BUFFER_LENGTH, "SIG: Node 0x%08X has alarm template but inconsistent children types.", (uint32_t) theNode);
            LOG_Event(LOG_LVL_FATAL, logBuffer);
            
        }
    }

    Alarm_SetLevel(&theNode->alarm, newLevel);

    //See if we can say this node is ready now (only if all of its children are)
    if (!theNode->isInitialized)
    {
        theNode->isInitialized = areAllChildrenReadyThisTime;
    }    
       
}

//return if done
static bool updateTreeUntilTimeout(void)
{   
        TMR_StartOneshot(&tmrSingleUpdate, configValues.maxRuntimePerCall_ms);
       //Do a depth-first walk of tree, updating as we go
       //Walk tree is ordered so deepest leafs are hit first!
       //Guaranteed to not need backtracking.
        //Pick up where we left off last time
        
        while (flatTreeUpdateInx < flatTree.length)
        {
           if (TMR_HasTripped_ms(&tmrSingleUpdate))
            {
                //we've taken too long! we'll finish next time
                return false;
            }
            T_SignalNode* thisNode = flatTree.nodes[flatTreeUpdateInx]; 
            
            thisNode->age++;
            if (thisNode->age == 0)
            {
               thisNode->age--;
            }

           if (thisNode->numChildren > 0 && thisNode->needsUpdate)
           {
                updateNode(thisNode);
           }
           
            flatTreeUpdateInx++;
        }
   
    //if we got here, then we made updated all nodes!
   return true;
}

static void updateMasterAlarmLed(void)
{
    //Now update the master alarm LED, if it exists
    if (pMasterAlarmLed)
    {
        T_LMode theMode = (T_LMode) 0;
        switch (rootNode->alarm.now)
        {
         case ALARM_LVL_CLEAR:
             if (rootNode->alarm.worstEver == ALARM_LVL_STOP)
             {
                 theMode = LMODE_BLIP;
             }
             else
             {
                 theMode = LMODE_OFF;
             }
          break;

          case ALARM_LVL_WARN:
              theMode = LMODE_BLINK_SLOW;
          break;

          case ALARM_LVL_CRIT:
              theMode = LMODE_BLINK_FAST;
          break;

          case ALARM_LVL_STOP:
              theMode = LMODE_WINK;
          break;

          default:
              puts("Bad alarm state for LED mode!");
        }

        LED_SetMode(pMasterAlarmLed, theMode);
    }
   
}


static uint16_t flattenTree(T_SignalNode* start, T_SignalNode* destArr[], uint8_t depthsArr[], uint16_t length)
{

    uint32_t inx  = 0;
    uint8_t depth = 0;
    T_SignalNode*  node = start;

    bool goingUp = false;
    uint16_t watcher = 0xFFFF; //timeout after 100 iterations, in case there's a loop or something...
    while (node && watcher)
    {
        if (!goingUp && node->firstChild)
        {
            node = node->firstChild;
            depth++;
        }
        else
        {
            //We're done with this node - add it to the list.
             if (inx < length)
            {
                destArr[inx] = node;
                depthsArr[inx] = depth;
                inx++;
            }
            else
            {
                puts("list full");
                break;
            }
        
            if (node->nextSibling)
            {
                node = node->nextSibling;
                goingUp = false;
            }
            else
            {
                node = node->parentNode;
                goingUp = true;
                depth--;
            }
        }

        watcher--;
    }

    if (!watcher)
    {
        puts("\r\nflat timed out!");
        return 0;
    }

    return inx;

}


static void packIntoWorstEverBuffer()
{
    uint32_t nodeInx;
    uint32_t wordInx = 0;
    uint8_t subCounter = 0;
    
    //Zero out the buffer before filling it, since we're or'ing in...
    memset(worstEverBuffer, 0x00, sizeof(worstEverBuffer));
    
    for (nodeInx = 0; nodeInx < flatTree.length; nodeInx++)
    {
        T_SignalNode* this = flatTree.nodes[nodeInx];
        worstEverBuffer[wordInx] |= (this->alarm.worstEver << (BITS_PER_ALARM_STATE*subCounter));
        subCounter++;
        if (subCounter >= ALARM_STATES_PER_WORD)
        {
            subCounter = 0;
            wordInx++;
        }
    }
}

static void unpackFromWorstEverBuffer()
{
    uint32_t nodeInx;
    uint32_t wordInx = 0;
    uint8_t subCounter = 0;
    
    for (nodeInx = 0; nodeInx < flatTree.length; nodeInx++)
    {
        T_SignalNode* this = flatTree.nodes[nodeInx];
        this->alarm.worstEver = (worstEverBuffer[wordInx] >> (BITS_PER_ALARM_STATE*subCounter)) & 0b011;
        subCounter++;
        if (subCounter >= ALARM_STATES_PER_WORD)
        {
            subCounter = 0;
            wordInx++;
        }
    }
}


static const T_SignalNode* complexNameLookup(const char* name)
{   
    if (name == NULL)
    {
        return NULL;
    }
    
    char parentName[8];
    char childName[8];
        
    int found = sscanf(name, "%s.%s", parentName, childName);
          
    const T_SignalNode* pNode = literalNameLookup(parentName);
    
    if (pNode)
    {        
       //Is there more crap after this?
       if (found == 2)
       {
            pNode = sigh_ChildStringToNode(pNode, childName);
       }
    }
    else
    {
        printf("No parent node found with name \"%s\"!\r\n", parentName);
    }
    
    return pNode;
}

static T_SignalNode* literalNameLookup(const char* name)
{   
    if (name == NULL)
    {
        return NULL;
    }
    
    uint32_t ii;
    for (ii = 0; ii < flatTree.length; ii++)
    {
        T_SignalNode* thisNode = flatTree.nodes[ii];
        
        if ((thisNode->name != NULL) && stricmp(name, thisNode->name) == 0)
        {
            return thisNode;
        }
    }
    
    return NULL;   
}

static int32_t pointer2TreeInx(const T_SignalNode* pNode)
{
    if (pNode == NULL)
    {
        return -1;
    }
    
    uint16_t ii;
    for (ii = 0; ii < flatTree.length; ii++)
    {
        if (flatTree.nodes[ii] == pNode)
        {
            return (int32_t) ii;
        }
    }
     
    return -1;
  
}

static const T_SignalNode* lookupFromAddressStr(const char* str)
{
    uint32_t addr =  UTIL_HexStr2Int_U32(str);           
    uint16_t ii;
    for (ii = 0; ii < flatTree.length; ii++)
    {
        if (((uint32_t) flatTree.nodes[ii]) == addr)
        {
            return ((const T_SignalNode*) addr);
        }
    }
    
    return NULL;  
}

static char* copyNamespace(const char* srcName)
{
    uint32_t len = strlen(srcName) + 1;
    
#ifdef USE_STATIC_SIGS
    if ((len + nameHeap.length) >= MAX_NUM_NAME_BYTES)
    {
        printf("Namesapce malloc failed \"%s\".", srcName);
        return NULL;
    }
    
    char* pStart = (char*) &nameHeap.bytes[nameHeap.length];
    uint32_t ii;
    for (ii = 0; ii < len; ii++)
    {
       pStart[ii] = srcName[ii];
    }

    nameHeap.length += len;
    
#else
    char* pStart = Heap_Allocate(len);
    
    if (pStart == NULL)
    {
        printf("Namesapce malloc failed \"%s\".", srcName);
        return NULL;
    }

    memcpy(pStart, srcName, len);
#endif 
    
    return pStart;
}

static void printIndentedData(T_SignalNode* node, uint8_t depth)
{

    depth+=1;
    while(depth)
    {
        SEND_TAB();
        depth--;
    }

    //Print the values of all leaf nodes, and any parent nodes that have units
    if (node->unit != UNIT_NONE || !node->firstChild)
    {
        VT100_RelativeMoveCursor(VT100_CD__MOVE_RIGHT, 3);
        sigh_PrintDatumWidth(node->avg, node->type, sigh_Unit2Width(node->unit));
        VT100_RelativeMoveCursor(VT100_CD__MOVE_RIGHT, 3);
    }

    if (node->alarm.hasChanged || drawInit)
    {
        if (node->alarm.now == ALARM_LVL_CLEAR)
        {
            VT100_CLEAR_LINE();
        }
        else
        {
            printf(", %s]", Alarm_Level2Str(node->alarm.now));
        }

        node->alarm.hasChanged = false;
    }
    NEW_LINE();
}

// </editor-fold>
