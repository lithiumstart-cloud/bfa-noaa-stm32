/**s
 * @file uart_pic32.c
 * @brief Software UART driver
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-11-03
 *
 * This file should NOT have any platform specific code in it. 
 * 
 * This module handles queues for incoming/outgoing bytes, and routes them to
 * the appropriate HW level fnuctions. 
**/



//######################### MODULE INCLUDES ##################################

#include <stdint.h>
#include <stdbool.h>

#include "uart.h"
#include "uart_hw.h"
#include "fifo.h"
#include "tmr.h"
#include "sch.h"
#include "heap.h"
//######################### MODULE DEFINES ###################################
#define UART_RX_SLEEP_TIMEOUT_MS (3000)

#define MAX_NUM_ALLOWED_PORTS (1)


//########################## MODULE TYPEDEFS ##################################

typedef struct
{
    bool isOpen;
    
    uint32_t baudRate;
    
    T_FIFO* rxQ;
    T_FIFO* txQ;
    
    T_LED* txLed;
    T_LED* rxLed;
    
} T_Port;


typedef enum
{
    ST_SLEEP_OK = 0,
    ST_STAYING_AWAKE,     
}T_State;

//######################### MODULE VARIABLES #################################
static T_Port portList[MAX_NUM_ALLOWED_PORTS];
static T_Timer tmrStayAwake;
static T_State rxState;


//######################### MODULE CODE ######################################


void UART_Init(void)
{
    //There isn't actually much to do here...   
    rxState = ST_SLEEP_OK;
            
}

void UART_AssignLEDs(uint8_t chn, T_LED* pLedTx, T_LED* pLedRx)
{
    //Find the correct channel.

    portList[chn].rxLed = pLedRx;
    portList[chn].txLed = pLedTx;

    LED_SetMode(pLedTx, LMODE_OFF);
    LED_SetMode(pLedRx, LMODE_OFF);
}

bool UART_OpenPort(uint8_t chn, uint32_t baud, uint16_t rxQueueSize, uint16_t txQueueSize)
{
    if (chn < MAX_NUM_ALLOWED_PORTS)
    {
        //Initiazlie new port obj
        T_Port* thisPort = &portList[chn];

        thisPort->baudRate = baud;

        UART_HW_Open(chn, baud);

        /*Initialize RX FIFO*/
        thisPort->rxQ = Heap_Allocate(sizeof(T_FIFO));
        if (thisPort->rxQ == NULL)
        {
            printf("malloc failed for UART%d rxQ.\r\n", chn);
            return false;
        }
        void* rxBuffer = Heap_Allocate(sizeof(uint8_t) * rxQueueSize);
        if (rxBuffer == NULL)
        {
            printf("malloc failed for UART%d rx buffer.\r\n", chn);
            return false;
        }
        FIFO_Init(thisPort->rxQ, rxBuffer, rxQueueSize, 1);


        /*Initialize TX FIFO*/
        thisPort->txQ = Heap_Allocate(sizeof(T_FIFO));
        if (thisPort->txQ == NULL)
        {
            printf("malloc failed for UART%d txQ.\r\n", chn);
            return false;
        }
        void* txBuffer = Heap_Allocate(sizeof(uint8_t) * txQueueSize);
        if (txBuffer == NULL)
        {
            printf("malloc failed for UART%d tx buffer.\r\n", chn);
            return false;
        }

        FIFO_Init(thisPort->txQ, txBuffer, txQueueSize, 1);

        thisPort->isOpen = 1;

        return true;
    }
    return false;
}


/*
 * Check for incoming/outgoing bytes, and handle any errors that arise.
 */
bool UART_Update(void)
{
    uint8_t ii;
    bool anyBytesRxd = false;
    bool wantToTxButCant = false;
    for (ii = 0; ii < MAX_NUM_ALLOWED_PORTS; ii++)
    {
        T_Port* thisPort = &portList[ii];
        if (thisPort->isOpen)
        {
            //Handle outgoing bytes
            T_FIFO* txQ = thisPort->txQ;
            if (txQ->numFilled)
            {
                if (UART_HW_IsTxReady(ii))
                {
                    uint8_t toSend;
                    FIFO_DequeueInto(txQ, &toSend);
                    UART_HW_SendByte(ii, toSend); //send it!
                }
                else
                {
                    wantToTxButCant = true;
                }
            }

            //Handle incoming bytes
            T_FIFO* rxQ = thisPort->rxQ;

            if (UART_HW_IsRxReady(ii))
            {
                uint8_t rxByte = UART_HW_ReadByte(ii);

                if (rxQ->numFilled < rxQ->maxLength)
                {
                    FIFO_EnqueueFrom(rxQ, &rxByte);
                }

                anyBytesRxd = true;
            }

        }
    }


    if (anyBytesRxd)
    {
        TMR_StartOneshot(&tmrStayAwake, UART_RX_SLEEP_TIMEOUT_MS);
        rxState = ST_STAYING_AWAKE;
    }
    else if (TMR_IsRunning(&tmrStayAwake) && TMR_HasTripped_ms(&tmrStayAwake))
    {
        rxState = ST_SLEEP_OK;
    }

    //We want to stay awake (sleep NOT ok, so return FALSE) if we want to TX but can't
    //todo: add check for transmission in progress, even if we don't want to send anything? (so last byte sent before gogoing to sleep doesn't get cut off?)
    return (rxState == ST_SLEEP_OK) && (!wantToTxButCant);

}

void UART_SendByte(uint8_t chn, uint8_t data)
{
    if (chn < MAX_NUM_ALLOWED_PORTS)
    {
        T_Port* this = &portList[chn];
        if (this->isOpen)
        {
            T_FIFO* txQ = this->txQ;
            if (txQ->numFilled < txQ->maxLength)
            {
                FIFO_EnqueueFrom(txQ, &data);
            }
        }

    }
}

void UART_SendBytes(uint8_t chn, uint8_t* pData, uint32_t N)
{
    if (!pData) { return; }

    if (chn >= MAX_NUM_ALLOWED_PORTS) { return; }

    T_Port* this = &portList[chn];

    if (!this->isOpen) {return;}

    T_FIFO* txQ = this->txQ;
    uint32_t ii;
    for (ii = 0; ii < N; ii++)
    {
        if (txQ->numFilled < txQ->maxLength)
        {
            FIFO_EnqueueFrom(txQ, pData);
            pData++;
        }
        else
        {
            break;
        }
    }
}


void UART_SendString(uint8_t chn, const char* x)
{
    if (!x)
    {
        return;
    }

    while (*x != '\0')
    {
        UART_SendByte(chn, *x);
        x++;
    }

}


//Check if a byte is waiting in the software buffer.
bool UART_IsByteWaiting(uint8_t chn)
{
    if (chn < MAX_NUM_ALLOWED_PORTS)
    {
        T_Port* this = &portList[chn];
        if (this->isOpen)
        {
            T_FIFO* rxQ = this->rxQ;
            return (rxQ->numFilled > 0);
        }
    }

    return false;
}

//Pop a byte off software buffer, and return data.
uint8_t UART_ReceiveByte(uint8_t chn)
{
    uint8_t c = 0;

    if (chn < MAX_NUM_ALLOWED_PORTS)
    {
        T_Port* this = &portList[chn];
        if (this->isOpen)
        {
            T_FIFO* rxQ = this->rxQ;

            //If there's data on the RX queue...
            FIFO_DequeueInto(rxQ, &c);
        }
    }


   return c;

}

bool UART_IsTransmitQueueEmpty(uint8_t chn)
{
    if (chn < MAX_NUM_ALLOWED_PORTS)
    {
        T_Port* this = &portList[chn];
        if (this->isOpen)
        {
            T_FIFO* txQ = this->txQ;
            return (txQ->numFilled == 0);
        }
    }

    return false;
}


void UART_InjectRecievedByte(uint8_t chn, uint8_t data)
{

    if (chn < MAX_NUM_ALLOWED_PORTS)
    {
        T_Port* this = &portList[chn];
        if (this->isOpen)
        {
            T_FIFO* rxQ = this->rxQ;
            if (rxQ->numFilled < rxQ->maxLength)
            {
                FIFO_EnqueueFrom(rxQ, &data);
            }
        }
    }


}
