/**
 * @file sch.c
 * @brief Implements task scheduler
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-07-24
 *
 * This a very simple cyclic executive scheduler. There are no priorities
 * among the tasks, and every task is called once per scheduler run. Each tasks
 * run/update function should return ASAP, indicating whether or not they can
 * tolerate the system going to sleep.
 *
 * If all of the modules return that sleep is OK within an alloted time frame,
 * the SCH_Sleep() function is called, which can put the device to sleep until
 * there is a Watchdog Timeout, or some other event. A project can override the
 * default SCH_Sleep() function if additional wake-up triggers /pre-sleep
 * preparation are necessary.
 *

 *
**/

//############################# MODULE INCLUDES ##############################

#include "sch.h"
#include "bluflex_core.h"
#include "core_config.h"
#include "mcu_hw.h"
#include "tmr_hw.h"


#include "main.h"


//############################ MODULE DEFINES ################################

/// This sturct is for storing a tasks name and Update function.
typedef struct
{
   T_RunFunc run; ///< Task update function: take argument of current time, returns whether or not task is OK sleeping right now
   const char* name; ///< Name of the task for reference
   
} T_Task;



//########################### MODULE VARIABLES ################################

//Set the Sleep function to the default MCU implementation. 
void (*SCH_SleepNow)(void) = MCU_HW_SleepNow;

static T_Task tasks[LIB_MAX_NUM_TASKS_ALLOWED];
static uint8_t numTasks;

static T_SCH_WakeupReason lastWakeupReason;


static uint32_t awakeRequests[LIB_MAX_NUM_TASKS_ALLOWED];




#if STM32
static uint32_t lastModuleInx __attribute__((section(".noinit")));	// this is GCC notation
#else
static uint32_t lastModuleInx __attribute__((persistent));
#endif

static uint32_t wdtPeriod_ms;

#if HAS_EXTERNAL_WDT
static T_Pin heartbeatPin;
#endif


static bool hasHeartbeatPin;

static T_Timer tmrSleepAttempt; ///< timer for  max amount of time we're allowed to try stuff before giving up on sleep for this loop
static T_Stopwatch stwGap; ///< stopwatch that times how long it takes to go through entire list of scheduled functions
static T_Stopwatch stwTask;
//########################### CONFIG VALUES ####################################
static struct
{
       uint32_t isSleepEnabled; ///< Boolean that determines if the BMS will go to sleep to save power
       uint32_t maxAttemptSleepTime_ms; ///< This is the longest the scheduler will wait for an OK from all tasks before giving up on sleep and returning. This allows tasks that expect regular sleep to function normally even if another tasks is preventing it
       uint32_t maxAllowedLoopDelay_ms; ///< Maximum allowed elapsed time between successive scheduler runs before an error is thrown
       uint32_t maxAllowedTaskDelay_ms; ///< Maximum allowed elapsed time between successive scheduler runs before an error is thrown
   
} configValues =
{
    .isSleepEnabled = 0,  ///< This gets overriden in SCH_Init with setting from appConfig
    .maxAttemptSleepTime_ms = 10, ///< This gets overrriden in SCH_Init with WDT period
    .maxAllowedLoopDelay_ms = 200,
    .maxAllowedTaskDelay_ms = 10,
};

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "isSleepEnabled",
    "maxAttemptSleepTime_ms",
    "maxAllowedLoopDelay_ms",
    "maxAllowedTaskDelay_ms",
};


//########################### MODULE FUNCTIONS #################################

void SCH_Init()
{

    wdtPeriod_ms = MCU_HW_GetWdtPeriod_ms();
    
    configValues.maxAttemptSleepTime_ms = wdtPeriod_ms;
    configValues.isSleepEnabled = 1;
    
    numTasks = 0;
           
    hasHeartbeatPin = false;
    
    TMR_Stopwatch_Start(&stwGap); //start the stopwatch the measures how long the SCH_Run() function runs for
    
    lastWakeupReason = SCH_SLEEP_PERIOD_ELAPSED_WHILE_AWAKE;
}



uint8_t SCH_Run()
{
    uint8_t ii;
           
    TMR_StartOneshot(&tmrSleepAttempt, configValues.maxAttemptSleepTime_ms);

    bool okToSleep;
    
    do
    {
        uint32_t gap_ms = TMR_Stopwatch_Split_ms(&stwGap);

        if (gap_ms > configValues.maxAllowedLoopDelay_ms)
        {
           LOG_Report(LOG_MOD_SCH, LOG_LVL_FATAL, LOG_MSG_RUNNING_SLOW, gap_ms);
        }
      
        okToSleep = true; //reset
        
        for (ii = 0; ii< numTasks; ii++)
        {
            //Every task.run() returns TRUE if it is OK with going to sleep
            //Otherwise, it wants the system to stay awake.
            
            lastModuleInx = ii;
            TMR_Stopwatch_Start(&stwTask);

            bool thisTaskCanSleep = tasks[ii].run();

//            if (thisTaskCanSleep == false)
//            	puts(tasks[ii].name);

            uint32_t taskLength_ms = TMR_Stopwatch_GetElapsed_ms(&stwTask);
            if (taskLength_ms > configValues.maxAllowedTaskDelay_ms)
            {
                printf("Warning! Task \"%s\" ran for %lu ms.\r\n", tasks[ii].name, taskLength_ms);
            }
                
            if (thisTaskCanSleep)
            {
                awakeRequests[ii] = 0;
            }
            else
            {
                awakeRequests[ii]++;
                if (configValues.isSleepEnabled)
                {
                    okToSleep = false;
                }
            }

            MCU_HW_ClearWDT();
        }
        
        lastWakeupReason = SCH_WAS_NOT_ASLEEP;
        
        if (TMR_HasTripped_ms(&tmrSleepAttempt))
        {
            lastWakeupReason = SCH_SLEEP_PERIOD_ELAPSED_WHILE_AWAKE;
            return 0; //it's been too long - give up on this round of sleep
        }
    }
    while (!okToSleep); //If any module says its not ready to sleep, then keep running

    //We got out of this loop without the sleep attempt timer expiring, so we
    //should stop it.
    TMR_Stop(&tmrSleepAttempt);
    


    if (configValues.isSleepEnabled)
    {
    	TMR_HW_PauseClock();
    	SCH_SleepNow();
    	TMR_HW_ResumeClock();
    }

    return 0;
}



uint8_t SCH_CreateTask(const char* name, T_RunFunc pRunFunc)
{
    if (numTasks < LIB_MAX_NUM_TASKS_ALLOWED)
    {
        tasks[numTasks].name = name;
        tasks[numTasks].run = pRunFunc;

        numTasks++;
        return true;
    }
    else
    {
        LOG_Report(LOG_MOD_SCH, LOG_LVL_FATAL,  LOG_MSG_INDEX_OUT_OF_RANGE, __LINE__);
    }

    return false;
}
//

T_SCH_WakeupReason SCH_DidProcessorJustWakeup()
{
    return lastWakeupReason;
}

void SCH_SetSleepEnable(bool _s)
{
    configValues.isSleepEnabled = _s;
}

void SCH_SetMaxAllowedTaskDelay(uint32_t t_ms, bool announce)
{
    configValues.maxAllowedTaskDelay_ms = t_ms;
    
    if (announce)
    {
        printf("SCH: Max allowed task delay <-- %u ms.", configValues.maxAllowedTaskDelay_ms);
    }
}

//<SRB>### NEED THIS IF THERE IS AN EXTERNAL WDOG ###
//<SRB>### FIGURE OUT WHERE TO TOGGLE THIS ##########
#if HAS_EXTERNAL_WDT
bool SCH_AssignHeartbeatPin(T_Pin pPin)
{

    heartbeatPin.port = pPin->port;
    heartbeatPin.pin  = pPin->pin;

    if (GPIO_IsValidPin(&heartbeatPin))
    {
        GPIO_SetDirection(&heartbeatPin, GPIO_IS_OUTPUT);
        GPIO_WriteOutput(&heartbeatPin, 1);
        hasHeartbeatPin = true;
        return true;
    }
    else
    {
        puts("\r\nSCH: Invalid WDT pin assignment: ");
        GPIO_PrintPin(&heartbeatPin);
        NEW_LINE();
    }

    hasHeartbeatPin = false;
    return false;
}


void SCH_Heartbeat(void)
{
    if (hasHeartbeatPin)
    {
        GPIO_ToggleOutput(&heartbeatPin);
    }
}
#else
bool SCH_AssignHeartbeatPin(T_Pin pPin)
{
	UNUSED(pPin);
	return( true );
}
void SCH_Heartbeat(void)
{
	return;
}
#endif


uint32_t SCH_GetLastModuleBeforeReset(void)
{
    return lastModuleInx;
}


//Handle command line inputs that had first token 'sch'
void SCH_ProcessInput(const char* tokens[], uint8_t nTokens)
{
    uint32_t ii;

    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("config\tModify module configuraton.");
        puts("list\tPrint all tasks and their consecutive awake requests.");
        puts("sleep\tForce sleep.");
        NEW_LINE();
    }
    else if (stricmp(tokens[0], "list") == 0)
    {
        for (ii = 0; ii < numTasks; ii++)
        {
            printf("[%02d] %s\t%u\r\n", ii, tasks[ii].name, awakeRequests[ii]);
        }
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
      CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, tokens, nTokens);
    }
    else if (stricmp(tokens[0], "sleep") == 0)
    {
        SCH_SleepNow();
    }
    else
    {
        puts("?\r\n");
    }
}



