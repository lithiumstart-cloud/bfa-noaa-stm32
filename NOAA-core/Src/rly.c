/**
 * @file rly.c
 * @brief Individual relay object
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-02
 *
**/
//############################## INCLUDES ######################################

#include "rly.h"
#include "bluflex_core.h"
#include "core_config.h"
#include "mem_hw.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
//############################## DEFINES ######################################

typedef enum
{
    ST_OPENED = 0,
    ST_CLOSING,
    ST_CLOSED,
    ST_OPENING,
    ST_PRECHARGING,
            
    ST_DISCONNECTED,
    ST_STUCK_CLOSED,
    ST_UNEXP_CLOSED,
    ST_PRECHARGE_ERROR,
    ST_PRECHARGE_TIMEOUT,
            
    ST_ERROR
} __attribute__((packed)) T_State;


#define OPEN (false)
#define CLOSE (true)

typedef enum
{
    NO_EVENT =0,
    RQST_OPEN_EVENT,
    RQST_CLOSE_EVENT,
} __attribute__((packed)) T_RelayEvent;

#define MAX_NUM_NAME_BYTES (256)

//############################ CONFIGURATIONS ##################################

static struct
{
    uint32_t stateCheckTime_ms; ///< How often the module will read the sense line to confirm relay state when contactor is open/closed (only applies if isStateCheckingActvie == true)
    uint32_t driveOpenOnUnexpectedOpen; ///< Whether or not to drive relays open if a relay unexpectedly opens
    uint32_t activePinResponse_ms;
    uint32_t printAllTransitions;
       
} configValues =
{
    /*Default Values*/
    .stateCheckTime_ms = 50,
    .driveOpenOnUnexpectedOpen = true,
    .activePinResponse_ms = 50,
    .printAllTransitions = false,
};

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "stateCheckTime_ms",
    "driveOpenOnUnexpOpen",
    "activePinResponseTime_ms",
    "printAllTransitions",
};

//############################ PRIVATE FUNCTIONS ##############################

static void commandRelay(T_Relay* rly, bool toClose);
static void changeStateTo(T_Relay* pRelay, T_State newState);
static const char* state2str(T_State state);
static char* copyNamespace(const char* srcName);
static T_Relay* allocateRelay(void);
static void stepRelayStateMachine(T_Relay *this);
static bool isSenseClosed(const T_Relay* rly);
static bool isDriveClosed(const T_Relay* rly);
static bool isActivePinAsserted(const T_Relay* rly);
static T_Relay* lookupRelay(const char* name);
static void syncStates(void);

static T_RLY_Config moduleConfig;
//############################## TYPES/FUNCS ###################################

struct sRelay
{
    char* name;
    T_State presentState; ///< present state of this state machine
    
    T_Pin drivePin; ///< Pin used to actuate relay
    T_Pin sensePin; ///< Pin used to sense state of relay. In case of Precharge SSR, this indicates "done precharging"
    T_Pin activePin; ///< Used to indicate Precharge SSR is conducting (Active) before fully latched/precharged, as indicated by sensePin.

    T_RelayEvent pendingEvent;
    T_Stopwatch stw;

    uint32_t allowableClosingTime_ms;
    uint32_t allowableOpeningTime_ms;
    
    union
    {
        uint8_t u8;
        struct
        {
            unsigned isIntegratedPrecharger: 1;
            unsigned hasSensePin : 1;
            unsigned hasActivePin : 1; 
            
            unsigned driveLogicInverted : 1;
            unsigned senseLogicInverted : 1;
            unsigned activeLogicInverted : 1;
            
            unsigned isStateCheckingEnabled : 1;
        };        
    } flags;
    
    void (*fPreAction) (void);
    void (*fPostAction) (void);

    T_Relay* pNext; ///<Next in Linked List, if appropriate
} __attribute__((packed));

static struct
{
    T_Relay xRelays[MAX_NUM_RELAYS];
    uint8_t length;
} relayList;

static struct
{
    uint8_t bytes[MAX_NUM_NAME_BYTES];
    uint32_t length;
} nameHeap;


//############################ PUBLIC CLASS FUNCTIONS ##########################

void RLY_Init(const T_RLY_Config* pRlyConfig)
{
    if (pRlyConfig)
    {
        moduleConfig = *pRlyConfig;
    }
    else
    {
        puts("RLY: NULL config struct on init!");
    }
    
   relayList.length = 0;
   nameHeap.length = 0;
   memset(nameHeap.bytes, '\0', MAX_NUM_NAME_BYTES);
}

bool RLY_Update()
{
    uint8_t ii;
    for (ii = 0; ii < relayList.length; ii++)
    {
        stepRelayStateMachine(&relayList.xRelays[ii]);
    }

    return true; //Always ready to sleep
}

//### Constructors 


static T_Relay* genericConstructor(const char* name, T_Pin* pDrive, T_Pin* pSense, T_Pin* pActive, uint16_t closingTime_ms, uint16_t openingTime_ms, bool isPrecharger)
{
    T_Relay* rsm = allocateRelay();
    if (rsm == NULL)
    {
        return NULL;
    }
    rsm->name = copyNamespace(name);
    if (rsm->name == NULL)
    {
        printf("Failed to copy name: \"%s\".\r\n", name);
        return NULL;
    }   
        
    rsm->flags.u8 = 0;
    rsm->flags.isIntegratedPrecharger = isPrecharger;
    
    if (GPIO_IsValidPin(pDrive))
    {
        rsm->drivePin = *pDrive;
        rsm->flags.driveLogicInverted = false;
        GPIO_SetDirection(&rsm->drivePin, GPIO_IS_OUTPUT);
    }
    else
    {
        printf("RLY: ERROR Drive pin on %s is invalid!\r\n", name);
        return false;
    }
    
    
    
    if (pSense != NULL)
    {
        if (GPIO_IsValidPin(pSense))
        {
            rsm->sensePin = *pSense;
            GPIO_SetDirection(&rsm->sensePin, GPIO_IS_INPUT);
        
            rsm->flags.hasSensePin = true;
            rsm->flags.senseLogicInverted = true;
            rsm->flags.isStateCheckingEnabled = true;
        }
        else
        {
            printf("RLY: %s Sense pin provided, but is invalid!\r\n", name);
            return false;
        }
    }
    else
    {
        rsm->flags.hasSensePin = false;
        rsm->flags.isStateCheckingEnabled = false;
    }
    
    if (rsm->flags.isIntegratedPrecharger && !rsm->flags.hasSensePin)
    {
        printf("RLY: %s is an Integrated Precharger but no Sense pin provided - invalid!\r\n", name);
    }
    
    
    //Only Prechargers can have Active Pins, but not all Prechargers need Active Pins.
    if (pActive != NULL)
    {
        if (!rsm->flags.isIntegratedPrecharger)
        {
            printf("RLY: %s has Active Pin but isn't a Precharger - invalid!\r\n", name);
            return false;
        }
        
        if (!GPIO_IsValidPin(pActive))
        {
            printf("RLY: %s Active pin provided, but is invalid!\r\n", name);
            return false;
        }
            
        rsm->activePin = *pActive;
        rsm->flags.hasActivePin = true;
        rsm->flags.activeLogicInverted = true;
        
    }
    else
    {
        rsm->flags.hasActivePin = false;
    }
    
    rsm->presentState = ST_OPENED;
    rsm->pendingEvent = NO_EVENT;
    
    rsm->fPreAction = NULL;
    rsm->fPostAction = NULL;

    rsm->allowableClosingTime_ms = closingTime_ms;
    rsm->allowableOpeningTime_ms = openingTime_ms;
    
    TMR_Stopwatch_Start(&(rsm->stw));
    
    commandRelay(rsm, OPEN);

    return rsm;
}

T_Relay* RLY_CreateMechanicalContactor(const char* name, T_Pin pinDrive, T_Pin pinSense)
{
    return genericConstructor(name, &pinDrive, &pinSense, NULL, moduleConfig.mechContactorMaxCloseTime_ms, moduleConfig.mechContactorMaxOpenTime_ms, false);
}

T_Relay* RLY_CreateIntegratedPrecharger(const char* name, T_Pin pinDrive, T_Pin pinSense, T_Pin pinActive)
{
    return genericConstructor(name, &pinDrive, &pinSense, &pinActive, moduleConfig.prechargeMaxCloseTime_ms, moduleConfig.prechargeMaxOpenTime_ms, true);
}

T_Relay* RLY_CreateIntegratedPrechargerWithoutActive(const char* name, T_Pin pinDrive, T_Pin pinSense)
{
    return genericConstructor(name, &pinDrive, &pinSense, NULL,  moduleConfig.prechargeMaxCloseTime_ms, moduleConfig.prechargeMaxOpenTime_ms, true);
}

T_Relay* RLY_CreateSimpleSwitch(const char* name, T_Pin pinDrive)
{
    return genericConstructor(name, &pinDrive, NULL, NULL, 1, 1, false); 
}


uint16_t RLY_GetRelayMask()
{
    uint8_t mask = 0x00;

    uint8_t ii;
    for (ii = 0; ii < relayList.length; ii++)
    {
        if (RLY_IsClosed(&relayList.xRelays[ii]))
        {
            mask |= (1 << ii);
        }
    }

    return mask;
}

uint8_t RLY_GetRelayCount(void)
{
	return(relayList.length);
}

void RLY_PrintNames()
{
    uint8_t ii;
    for (ii = 0; ii < relayList.length; ii++)
    {
        putstr(relayList.xRelays[ii].name);
        SEND_TAB();
    }
}

void RLY_FixDisconnected()
{
    uint8_t ind;    
    T_Relay* rly;
    for (ind = 0; ind < relayList.length; ind++)
    {
        rly = &relayList.xRelays[ind];
        if (rly->presentState == ST_DISCONNECTED)
        {
           rly->presentState = ST_OPENED;
        }
    }
}

void RLY_SetStateChecking(T_Relay* this, bool isEnabled)
{
    if (this)
    {
        if (this->flags.hasSensePin)
        {
            if (isEnabled)
            {
                snprintf(logBuffer, LOG_BUFFER_LENGTH, "RLY: State checking has been enabled for %s.", this->name);
                LOG_Event(LOG_LVL_DBG, logBuffer);       
                this->flags.isStateCheckingEnabled = true;
            }
            else
            {
                snprintf(logBuffer, LOG_BUFFER_LENGTH, "RLY: State checking has been disabled for %s.", this->name);
                LOG_Event(LOG_LVL_DBG, logBuffer); 
                this->flags.isStateCheckingEnabled = false;
            } 
        }
        else
        {
            puts("RLY: Cannot set stateChecking for relay with no sense HW!");
        }
    }
    
    //TODO: throw NULL error?
}
//############################ PUBLIC OBJECT FUNCTIONS #########################
bool RLY_AddPreAction(T_Relay* this, void (*fPreAction)(void))
{
    if (this)
    {
        this->fPreAction = fPreAction;
        return true;
    }
    
    return false;
}

bool RLY_AddPostAction(T_Relay* this, void (*fPostAction)(void))
{
    if (this)
    {
        this->fPostAction = fPostAction;
        return true;
    }
    
    return false;
}
bool RLY_SetDrivePinLogicLevel(T_Relay* this, bool isActiveLo)
{
    if (this)
    {   
        this->flags.driveLogicInverted = isActiveLo;
        return true;
    }
    return false;
}
bool RLY_SetSensePinLogicLevel(T_Relay* this, bool isActiveLo)
{
    if (this)
    {
        this->flags.senseLogicInverted = isActiveLo;
        return true;
    }
    return false;
}
bool RLY_SetActivePinLogicLevel(T_Relay* this, bool isActiveLo)
{
    if (this)
    {
        this->flags.activeLogicInverted = isActiveLo;
        return true;
    }
    return false;
}

bool RLY_SetMaxClosingTime_ms(T_Relay* this, uint32_t ms)
{
    if (this)
    {
        this->allowableClosingTime_ms = ms;
        return true;
    }
    
    return false;
}

bool RLY_SetMaxOpeningTime_ms(T_Relay* this, uint32_t ms)
{
    if (this)
    {
        this->allowableOpeningTime_ms = ms;
        return true;
    }
    
    return false;
}

bool RLY_IsClosed(const T_Relay* pRelay)
{
    if (pRelay == NULL)
    {
        return false;
    }


    switch (pRelay->presentState)
    {
        case ST_CLOSED:
        case ST_STUCK_CLOSED:
        case ST_UNEXP_CLOSED:
            return true;
        break;
    }

    return false;
}

bool RLY_IsOpened(const T_Relay* pRelay)
{

    if (pRelay == NULL)
    {
        return false;
    }


    switch (pRelay->presentState)
    {
        case ST_OPENED:
        case ST_DISCONNECTED:
        case ST_PRECHARGE_ERROR:
        case ST_PRECHARGE_TIMEOUT:
            return true;
        break;
    }

    return false;

}


bool RLY_IsBroken(const T_Relay* pRelay)
{
    if (pRelay == NULL)
    {
        return true;
    }

    switch (pRelay->presentState)
    {
        case ST_STUCK_CLOSED:
        case ST_UNEXP_CLOSED:
        case ST_DISCONNECTED:
        case ST_PRECHARGE_ERROR:
        case ST_PRECHARGE_TIMEOUT:
        case ST_ERROR:
            return true;
        break;
    }

    return false;
}


void RLY_PrintInfo(const T_Relay* this)
{
	char prtBfr[30] = {0};
    if (this)
    {
        sprintf(prtBfr,"%s%c\t%-10s\t%c\t%c\t%c", this->name, this->flags.isIntegratedPrecharger ? '*' : ' ', RLY_GetStateStr(this),
               isDriveClosed(this) ? 'X' : 'O', 
               this->flags.hasSensePin ? (isSenseClosed(this) ? 'X' : 'O') : '-', 
               this->flags.hasActivePin ? (isActivePinAsserted(this) ? 'X' : 'O') : '-');
        putstr(prtBfr);
    }
}

const char* RLY_GetName(const T_Relay* this)
{
    if (this){return this->name;}
    
    return "NULL";
}

T_Relay* RLY_GetNextInList(const T_Relay* rly)
{
    if (rly){return rly->pNext;}
    
    return NULL;
}

bool RLY_RequestAction(T_Relay* rly, bool requestClose)
{
    if (rly)
    {
        if (configValues.printAllTransitions)
        {
            printf("RLY: Request %s to %s\r\n",  rly->name, requestClose ? "CLOSE" :"OPEN");
        }
        
        rly->pendingEvent = requestClose ? RQST_CLOSE_EVENT : RQST_OPEN_EVENT;
        return true;
    }

    return false;
}

const char* RLY_GetStateStr(const T_Relay* rly)
{
    if (rly)
    {
        return state2str(rly->presentState);
    }

    return "NULL";

}

bool RLY_IsIntegratedPrecharger(const T_Relay* rly)
{
    if (rly)
    {
        return rly->flags.isIntegratedPrecharger;
    }
    
    return false;
}

//### Public List Manipulation Functions

void RLY_RequestActionOnList(T_Relay* pListHead, bool rqstClose)
{
    while (pListHead)
    {
        RLY_RequestAction(pListHead, rqstClose);
        pListHead = pListHead->pNext;
    }
}

bool RLY_AreAllRelaysInListOpened(T_Relay* pListHead)
{
    while (pListHead)
    {
        if (!RLY_IsOpened(pListHead))
        {
            return false;
        }

        pListHead = pListHead->pNext;
    }

    return true;
}


bool RLY_AreAllRelaysInListClosed(T_Relay* pListHead)
{
    while (pListHead)
    {
        if (!RLY_IsClosed(pListHead))
        {
            return false;
        }

        pListHead = pListHead->pNext;
    }

    return true;
}

bool RLY_AreAnyRelaysInListBroken(T_Relay* pListHead)
{
    while (pListHead)
    {
        if (RLY_IsBroken(pListHead))
        {
            return true;
        }

        pListHead = pListHead->pNext;
    }

    return false;
}

bool RLY_AddRelayToList(T_Relay* pListHead, T_Relay* pRelay)
{
    if (pListHead == NULL)
    {
        return false;
    }
        
            
    T_Relay* this = pListHead;
    while (this->pNext)
    {
        this = this->pNext;
    }
    
    this->pNext = pRelay;
    pRelay->pNext = NULL;
    
    return true;
}

///########################### DRAW FUNCTIONS ##################################

void RLY_ProcessInput(const char* tokens[], uint8_t nTokens)
{
    uint8_t ii;

    if (nTokens == 0 || tokens[0][0] == '?')
    {
      puts("list");
      puts("config");
      puts("close [name]");
      puts("open [name]");
      puts("info [name]");
      NEW_LINE();
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
        CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, tokens, nTokens);
    }
    else if (stricmp(tokens[0], "list") == 0)
    {
        printf("There are %d relays.\r\n", relayList.length);
        puts("*Indicates Relay is Integrated Precharger.\n");
        VT100_FONT_UNDERLINE();
        puts("Name\tState\t\tDrive\tState\tActive\r\n");
        VT100_FONT_NORMAL();
        
        for (ii = 0; ii < relayList.length; ii++)
        {
            RLY_PrintInfo(&relayList.xRelays[ii]);
            NEW_LINE();
        }
    }
    else if (stricmp(tokens[0], "open") == 0)
    {
        if (nTokens == 2)
        {
            if (tokens[1][0] == '*')
            {
                //Open ALL.
                puts("Opening all relays!");
                for (ii = 0; ii < relayList.length; ii++)
                {
                    relayList.xRelays[ii].pendingEvent = RQST_OPEN_EVENT;
                }

            }
            else
            {
                T_Relay* rly = lookupRelay(tokens[1]);
                if (rly)
                {
                    printf("Commanding %s to open.\r\n", rly->name);
                    rly->pendingEvent = RQST_OPEN_EVENT;
                }
                else
                {
                    printf("No relay \"%s\".\r\n", tokens[1]);
                }
            }
        }
        else
        {
            puts("Need relay name!");
        }
    }
    else if (stricmp(tokens[0], "close") == 0)
    {
        if (nTokens == 2)
        {
            if (tokens[1][0] == '*')
            {
                puts("Closing all relays!");
                for (ii = 0; ii < relayList.length; ii++)
                {
                    relayList.xRelays[ii].pendingEvent = RQST_CLOSE_EVENT;
                }
            }
            else
            {
                T_Relay* rly = lookupRelay(tokens[1]);
                if (rly)
                {
                    printf("Commanding %s to close.\r\n", rly->name);
                    rly->pendingEvent = RQST_CLOSE_EVENT;
                }
                else
                {
                    printf("No relay \"%s\".\r\n", tokens[1]);
                }
            }
        }
        else
        {
            puts("Need relay name!");
        }
    }
    else if (stricmp(tokens[0], "info") == 0)
    {
        if (nTokens == 2)
        {
        
            T_Relay* rly = lookupRelay(tokens[1]);
            if (rly)
            {
               
                printf("Name:%s\r\n", rly->name);
                printf("Integrated Precharger? %c\r\n", rly->flags.isIntegratedPrecharger ? 'Y': 'N');
                puts("Drive:\r\n\tPin: ");
                GPIO_PrintPin(&rly->drivePin);
                printf("\r\n\tGPIO Level: %d\r\n\tInvert? %c", GPIO_CheckOutput(&rly->drivePin), rly->flags.driveLogicInverted ? 'Y': 'N');
                printf("\r\n\tLogically: %s\r\n", isDriveClosed(rly) ? "CLOSE" : "OPEN");
                
                puts("Sense:\r\n\tPin: ");
                if (rly->flags.hasSensePin)
                {
                    GPIO_PrintPin(&rly->sensePin);
                    printf("\r\n\tGPIO Level: %d\r\n\tInvert? %c", GPIO_ReadInput(&rly->sensePin), rly->flags.senseLogicInverted ? 'Y' : 'N');
                    printf("\r\n\tLogically: %s\r\n", isSenseClosed(rly) ? "CLOSE" : "OPEN");
                }
                else
                {
                    puts("n/a");
                }
                
                puts("Active:\r\n\tPin: ");
                if (rly->flags.hasActivePin)
                {
                    GPIO_PrintPin(&rly->activePin);
                    printf("\r\n\tGPIO Level: %d\r\n\tInvert? %c", GPIO_ReadInput(&rly->activePin), rly->flags.activeLogicInverted ? 'Y' : 'N');
                    printf("\r\n\tLogically: %s\r\n", isActivePinAsserted(rly) ? "CLOSE" : "OPEN");
                }
                else
                {
                    puts("n/a");
                }
                
                printf("MaxCloseTime: %d ms\r\n", rly->allowableClosingTime_ms);
                printf("MaxOpenTime: %d ms\r\n", rly->allowableOpeningTime_ms);
            }
            else
            {
                printf("No relay \"%s\".\r\n", tokens[1]);
            }
            
        }
        else
        {
            puts("Need relay name!");
        }
    }
    else
    {
        //Print error string.
    	Console_WriteByte('?');
    }
}

void RLY_DrawInit()
{
     //Print title
    puts("\t\tRLY DEBUG\r\n\n");
    puts("Key\tName\tState\t\tDrive\tSense\tActive\r\n");

    uint8_t ii;
    for (ii = 0 ; ii < relayList.length; ii++)
    {
        if (ii < 9)
        {
            PrintAs_Dec_U8(ii+1,1);
        }
        else
        {
            Console_WriteByte((ii - 9) + 'a');
        }
        SEND_TAB();
        puts(relayList.xRelays[ii].name);
        NEW_LINE();
    }

    puts("\n*Indicates Relay is Integrated Precharger.");


}

void RLY_DrawUpdate()
{
    puts("\n\n\n");

    uint8_t ii;
    for (ii =0 ; ii < relayList.length; ii++)
    {
        puts("\t");
        VT100_CLEAR_LINE();
        RLY_PrintInfo(&relayList.xRelays[ii]);
        NEW_LINE();
    }


    puts("\r\n\n");
    CLI_DrawProgressBar();
    NEW_LINE();
}

void RLY_DrawInput(uint8_t cin)
{
     //Clear any lingering text
    VT100_SpecialCommand(VT100_SPC__ERASE_DOWN);
    
    uint8_t ind = 255;
    T_Relay* rly;
    switch (cin)
    {
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            ind = cin - '1';
        break;
        
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
            ind = cin - 'a' + 9;            
        break;

        case 'x':
            syncStates();
        break;
       
        default:
            //do nothing
            (void) (cin);
        break;
    }
    
    /*Only do this if we have a valid index*/
    if (ind < relayList.length)
    {
        rly = &relayList.xRelays[ind];
        if (rly->presentState == ST_CLOSED || rly->presentState == ST_CLOSING || rly->presentState == ST_PRECHARGING)
        {
            rly->pendingEvent = RQST_OPEN_EVENT;
        }
        else
        {
            rly->pendingEvent = RQST_CLOSE_EVENT;
        }        
    }
}


//######################## PRIVATE FUNCTIONS ###################################

/**
 *
 * Open/close relay based on input. OK to call if no hardware.
 *
 *             Drive Active Lo
 *              |   0   |   1
 *           ---+-------+-------
 *  To        0 | Clear |  Set
 *  Close    ---+-------+-------
 *            1 |  Set  | Clear
 *
 * @param rly Relay object to act on
 * @param toClose TRUE if you want to close relay, FALSE to open
 */
static void commandRelay(T_Relay* rly, bool toClose)
{
    if (rly == NULL)
    {
        LOG_Report(LOG_MOD_RLY, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
        return;
    }

    GPIO_WriteOutput(&rly->drivePin, (rly->flags.driveLogicInverted) ^ toClose);
    
}

/**
 * Returns whether or not relay is closed based on sense line. Check before
 * calling if relay has sense line! Otherwise result is meaningless.
 * @param rly
 * @return TRUE if relay is closed, FALSE if open (or invalid).
 *              Sense Active Lo
 *              |   0   |   1
 *           ---+-------+-------
 *  Sense     0 |  Open | Close
 *   Bit     ---+-------+-------
 *            1 | Close | Open
 *
 */
static bool isSenseClosed(const T_Relay* rly)
{
    if (!rly)
    {
        LOG_Report(LOG_MOD_RLY, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
        return false;
    }

    return (GPIO_ReadInput(&rly->sensePin) ^ (rly->flags.senseLogicInverted));
     
}

/**
 * Check if Active pin is asserted. Relay MUST have active pin. 
 * @param rly
 * @return 
 */
static bool isActivePinAsserted(const T_Relay* rly)
{
    if (!rly)
    {
        LOG_Report(LOG_MOD_RLY, LOG_LVL_FATAL,  LOG_MSG_NULL_PTR_REF, __LINE__);
        return false;
    }
    else if (!rly->flags.hasActivePin)
    {
        printf("RLY: Tried to check state of Active pin on Rly %s without Active pin!\r\n", rly->name);
        return false;
    }

    return (GPIO_ReadInput(&rly->activePin) ^ (rly->flags.activeLogicInverted));
     
}

/**
 * Returns whether or not relay is closed based on DRIVE line. Check before
 * calling if relay has drive line! Otherwise result is meaningless.
 * @param rly
 * @return TRUE if relay is closed, FALSE if open (or invalid).
 *              Drive Active Lo
 *              |   0   |   1
 *           ---+-------+-------
 *  Drive     0 |  Open | Close
 *   Bit     ---+-------+-------
 *            1 | Close | Open
 *
 */
static bool isDriveClosed(const T_Relay* rly)
{
    if (!rly)
    {
        LOG_Event(LOG_LVL_FATAL, "RLY: Attempted to check drive state of NULL relay!");
        return false;
    }

    return (rly->flags.driveLogicInverted) ^ GPIO_CheckOutput(&rly->drivePin);
    
}

//We will abstract the state machine logic (because we're AWESOME)
static void stepRelayStateMachine(T_Relay *this)
{
    if (this == NULL)
    {
        return;
    }

    uint32_t elapsed = TMR_Stopwatch_GetElapsed_ms(&(this->stw));
   

    switch (this->presentState)
    {
        case ST_OPENED:
            if (this->pendingEvent == RQST_CLOSE_EVENT)
            {
                changeStateTo(this, ST_CLOSING);
            }
            else if (this->flags.isStateCheckingEnabled && elapsed >= configValues.stateCheckTime_ms)
            {
                //We assume that state checking is only enabled on pins with valid sense HW.
                //Check if the relay is actually open right now..
                if (isSenseClosed(this))
                {
                    //Uh-oh - its closed!
                    changeStateTo(this, ST_UNEXP_CLOSED);
                }
               
                TMR_Stopwatch_Start(&(this->stw));
            }
        break;

        case ST_CLOSING:
            if (this->pendingEvent == RQST_OPEN_EVENT)
            {
                changeStateTo(this, ST_OPENING);
            }
            else if (!this->flags.hasSensePin)
            {
                //If there's no sense HW (which implies now Active HW either), just go straight to close
                changeStateTo(this, ST_CLOSED);
            }
            else if (this->flags.hasActivePin)
            {
                //We should see the Active Pin go high very quickly, if there is one
                if (isActivePinAsserted(this))
                {
                    changeStateTo(this, ST_PRECHARGING);
                }
                else if (elapsed >= configValues.activePinResponse_ms) 
                {
                    printf("RLY: %s Active pin was not asserted in allowed time!\r\n", this->name);
                    changeStateTo(this, ST_DISCONNECTED);
                }
            }
            else if (isSenseClosed(this))
            {
                //If we get here, we DO have Sense Pin, but Don't have Active Pin
                changeStateTo(this, ST_CLOSED);
            }
            else if (elapsed >= this->allowableClosingTime_ms)
            {   
                changeStateTo(this, ST_DISCONNECTED);
            } 
        break;

        case ST_PRECHARGING:
            if (this->pendingEvent == RQST_OPEN_EVENT)
            {
              changeStateTo(this, ST_OPENING);
            }
            else if (!isActivePinAsserted(this))
            {
                //Active had an error while we were precharging!
                changeStateTo(this, ST_PRECHARGE_ERROR);
            }
            else if (isSenseClosed(this))
            {
                //Yay! Done precharging
                changeStateTo(this, ST_CLOSED);
            }
            else if (elapsed >= this->allowableClosingTime_ms)
            {   
                //Been waiting too long for PC to happen. 
                changeStateTo(this, ST_PRECHARGE_TIMEOUT);
            } 
        break;
        
        case ST_CLOSED:
            if (this->pendingEvent == RQST_OPEN_EVENT)
            {
              changeStateTo(this, ST_OPENING);
            }
            else if (this->flags.isStateCheckingEnabled && elapsed >= configValues.stateCheckTime_ms)
            {
                //Check if the relay is actually closed right now
                if (this->flags.hasSensePin && !isSenseClosed(this))
                {
                    //Uh-oh - its open!
                   changeStateTo(this, ST_DISCONNECTED);
                }
                else if (this->flags.hasActivePin && !isActivePinAsserted(this))
                {
                    //If sense is still asserted, but active isn't, then something is wrong with our board.
                    //In all likelyhood active pin was disconnected.
                   changeStateTo(this, ST_DISCONNECTED); 
                }
                
                TMR_Stopwatch_Start(&(this->stw));
            }
        break;

        case ST_OPENING:
            if (this->pendingEvent == RQST_CLOSE_EVENT)
            {
                changeStateTo(this, ST_CLOSING);
            }
            else if (!this->flags.hasSensePin || !isSenseClosed(this))
            {
                //If there's no sense HW OR (there is sense hardware, and sense is closed),  go straight to Opened
                changeStateTo(this, ST_OPENED);
            }
            else if (elapsed >= this->allowableOpeningTime_ms)
            {
                changeStateTo(this, ST_STUCK_CLOSED);
            }
        break;

        case ST_PRECHARGE_ERROR:
        case ST_PRECHARGE_TIMEOUT:
        case ST_DISCONNECTED:
            if (this->flags.isStateCheckingEnabled && elapsed > configValues.stateCheckTime_ms)
            {
                //The relay is driven open in this state, but check that the
                //sense line agrees.
                if (isSenseClosed(this))
                {
                    //Uh-oh - its closed!
                    printf("RLY: %s reconnected and closed!\r\n", this->name);
                    changeStateTo(this, ST_UNEXP_CLOSED);
                }
                TMR_Stopwatch_Start(&(this->stw));
            }
        break;

        case ST_UNEXP_CLOSED:
        case ST_STUCK_CLOSED:
            //Change state here even if state checking isn't active - we're in an error anyway
            if (this->flags.isStateCheckingEnabled &&  elapsed > configValues.stateCheckTime_ms)
            {
                //Check if the relay is actually closed right now
                if (!isSenseClosed(this))
                {
                    //Uh-oh - its opened!
                    changeStateTo(this, ST_DISCONNECTED);
                }

                TMR_Stopwatch_Start(&(this->stw));
           }
        break;
        
        case ST_ERROR:
            //Stay here, do nothing
        break;

        default:
            printf("RLY: State 0x%02X unhandled @ line %u!\r\n", this->presentState, __LINE__);
            changeStateTo(this, ST_ERROR);
            
        break;

    } //end switch on state

    //clear the pending event flag
    this->pendingEvent = NO_EVENT;

}

static void changeStateTo(T_Relay* pRelay, T_State newState)
{
    /* Exit Events*/
    switch (pRelay->presentState)
    {
        case ST_OPENING:
        case ST_CLOSING:
            if (pRelay->fPostAction)
                {
                    pRelay->fPostAction();
                }
        break;
        default:
            //Nothing to do
        break;
    }

    /* Entry Events */
    switch (newState)
    {
        case ST_CLOSING:

                if (pRelay->fPreAction)
                {
                    pRelay->fPreAction();
                }

                commandRelay(pRelay, CLOSE); //physically close the relay

        break;

        case ST_OPENING:
                if (pRelay->fPreAction)
                {
                    pRelay->fPreAction();
                }
                commandRelay(pRelay, OPEN); //physically open relay
        break;

        case ST_DISCONNECTED:
            printf("RLY: Relay %s disconnected!\r\n", pRelay->name);            
            commandRelay(pRelay, OPEN);
        break;

        case ST_STUCK_CLOSED:
            printf("RLY: Timed out waiting for relay %s to open.\r\n", pRelay->name);
        break;

        case ST_UNEXP_CLOSED:
            printf("RLY: %s unexpectedly closed.\r\n", pRelay->name);
        break;
        
        case ST_CLOSED:
        case ST_OPENED:
        case ST_PRECHARGING:
            //do nothing
        break;
        
        case ST_PRECHARGE_ERROR:
            printf("RLY: %s experienced error during Precharge!\r\n", pRelay->name);
            commandRelay(pRelay, OPEN);
        break;
        
        case ST_PRECHARGE_TIMEOUT:
            printf("RLY: %s timed out waiting to Precharge!\r\n", pRelay->name);
            commandRelay(pRelay, OPEN);
        break;
                
        case ST_ERROR:
            commandRelay(pRelay, OPEN);
        break;
        
        default:
            printf("RLY: Unhandled state %s in changeStateTo!\r\n", state2str(newState));
        break;
               
    }
    
    pRelay->presentState = newState;
    TMR_Stopwatch_Start(&(pRelay->stw));
    
    if (configValues.printAllTransitions)
    {
        printf("RLY: %s <-- %s\r\n",  pRelay->name, state2str(pRelay->presentState));
    }
}

static char* copyNamespace(const char* srcName)
{
    uint32_t len = strlen(srcName) + 1;
    
    if ((len + nameHeap.length) >= MAX_NUM_NAME_BYTES)
    {
        printf("Relay namespace malloc failed \"%s\".", srcName);
        return NULL;
    }
    
    char* pStart = (char*) &nameHeap.bytes[nameHeap.length];
    uint32_t ii;
    for (ii = 0; ii < len; ii++)
    {
       pStart[ii] = srcName[ii];
    }

    nameHeap.length += len;
    
    return pStart;
}

static T_Relay* allocateRelay(void)
{
    T_Relay* rsm = NULL;
    if (relayList.length < MAX_NUM_RELAYS)
    {
        rsm = &relayList.xRelays[relayList.length];
        relayList.length++;
    }
    else
    {
        puts("RLY: No more room for relays!");
    }

    return rsm;
}


static const char* state2str(T_State state)
{
    switch (state)
    {
        case ST_OPENED:             return "Open";
        case ST_CLOSING:            return "Clsing";
        case ST_CLOSED:             return "Closed";
        case ST_OPENING:            return "Opning";
        case ST_STUCK_CLOSED:       return "!CLOSD!";
        case ST_UNEXP_CLOSED:       return "?CLOSD?";
        case ST_DISCONNECTED:       return "NotConn";
        case ST_PRECHARGING:        return "Prechrging";
        case ST_PRECHARGE_ERROR:    return "PcErr";
        case ST_PRECHARGE_TIMEOUT:  return "PcTimeout";
        case ST_ERROR:              return "Error";
        default:                    return "???";
    }
}


/**
 * Given a name of a relay, return pointer to relay object. If not found, return
 * null.
 * @param name
 * @return Pointer to relay if found; null otherwise
 */
static T_Relay* lookupRelay(const char* name)
{
    uint8_t ii;
    for (ii= 0; ii < relayList.length; ii++)
    {
        T_Relay* thisRelay = &(relayList.xRelays[ii]);
        if (thisRelay->name && stricmp(name, thisRelay->name) == 0)
        {
            return thisRelay;
        }
    }

    return NULL;
}

static void syncStates(void)
{
    uint8_t ind;    
    T_Relay* rly;
    for (ind = 0; ind < relayList.length; ind++)
    {
        rly = &relayList.xRelays[ind];
        if (RLY_IsBroken(rly))
        {
            //Go into which ever state the sense thinks it is
           rly->presentState = isSenseClosed(rly) ? ST_CLOSED : ST_OPENED;

        }
    }
}

void RLY_ResetStopwatch(T_Relay* pRelay)
{
    TMR_Stopwatch_Start(&(pRelay->stw));
}
