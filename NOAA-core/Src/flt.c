/**
 * @file flt.c
 * @brief Fault Manager
 * @copyright 2015 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2015-08-17
 *
**/
//############################## INCLUDES ######################################


#include "flt.h"
#include "bluflex_core.h"
#include "util.h"
//############################## DEFINES ######################################

#define FAULTS_MAX_ALLOWED (32) //this needs to be divisible by 4
#define FAULT_TREE_UPDATE_TIME_MS (10)

//########################### CONFIG VALUES ####################################

static struct
{
       uint32_t printStateChanges;
} configValues =
{
    /*Default Values*/
    .printStateChanges = false,
};

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "printStateChanges",
};

//######################### MODULE  TYPES ####################################

struct Struct_Fault
{
  const char* name;
  
  T_FaultType type;
  union
  {
    T_FaultChecker fCheck;    
    T_SignalNode* pSig;
  }linkedObj;
  
  uint32_t linkedObjParam;
  
  T_FaultBitmap lvl;
  
  T_FaultLevel nativeLevel;
  uint8_t numChildren;
  
  uint32_t age;
    
  T_Fault* parent;
  T_Fault* firstChild;
  T_Fault* nextSibling;
  
  bool needsUpdate;
  
    
};

//Each "WorstAllTime" fault state is 2bits. If there are N total possible faults,
//the number of bytes of storage needed is 2*N/8 = N/4 bytes. 
#define BITS_PER_FAULT_STATE (2)
#define FAULT_STATES_PER_WORD (32/BITS_PER_FAULT_STATE)

#define NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE (CEILING_DIV(FAULTS_MAX_ALLOWED,FAULT_STATES_PER_WORD))
static uint32_t worstEverBuffer[NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE];

//###################### MODULE VARIABLES ####################################
static T_Timer timer;

static T_MemAddr vAddrWorstEver;
static struct
{
    T_Fault faults[FAULTS_MAX_ALLOWED];
    uint8_t length;
} faultList;

static struct
{
    T_Fault* faults[FAULTS_MAX_ALLOWED];
    uint8_t depths[FAULTS_MAX_ALLOWED];        
    uint8_t length;
} flatTree;

static T_Fault* root = NULL;
static T_LED* faultLed;
//############################## PROTOTYPES ####################################

static void detailedPrintFault(const T_Fault* this);
static void printTree(void);
static T_Fault* lookupFault(const char* name);
static uint32_t flattenTree(T_Fault* start);
static void updateFault(T_Fault* this);
static void printWorstEverBuffer(void);
static void packIntoWorstEverBuffer(void);
static void unpackFromWorstEverBuffer(void);
static const char* faultType2str(T_FaultType t);
//######################### MODULE CODE ########################################



void FLT_Init()
{
    faultList.length = 0;
    
    vAddrWorstEver = MEM_AllocateData(NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE);
    
    memset(worstEverBuffer, 0x00, sizeof(worstEverBuffer));

    MEM_LoadDefaultData("Flt_WorstEver", vAddrWorstEver, worstEverBuffer, NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE);
    
    
    
    
}
bool FLT_Update()
{
    if (TMR_HasTripped_ms(&timer))
    {
        
        uint32_t ii;
        
        for (ii =0; ii < flatTree.length; ii++)
        {
            T_Fault* this = flatTree.faults[ii];
            this->age++;
            if (this->age == 0)
            {
                this->age--;
            }
            
            updateFault(this);
        }
        
        
         if (faultLed && root)
        {   
           
            if (root->lvl.now)
            {
                LED_SetMode(faultLed, LMODE_BLINK_FAST);
            }
            else if (root->lvl.sinceBoot)
            {
                LED_SetMode(faultLed, LMODE_BLIP);
            }
            else
            {
                LED_SetMode(faultLed, LMODE_OFF);
            }

        }
    }
 
    return true;
}


static T_Fault* allocateFault(void)
{
    T_Fault* this = NULL;
    if (faultList.length < FAULTS_MAX_ALLOWED)
    {
        this = &faultList.faults[faultList.length];
        faultList.length++;
        
        this->firstChild = NULL;
        this->nextSibling = NULL;
        this->parent = NULL;
    
        this->lvl.now = FLT_LVL_CLEAR;
        this->lvl.sinceBoot = FLT_LVL_CLEAR;
        this->lvl.worstEver = FLT_LVL_CLEAR;
        
        this->nativeLevel = INHERIT_FAULT;
        
        this->numChildren = 0;
    }
    return this;
        
}

T_Fault* FLT_CreateCallbackFault(const char* name,  T_FaultLevel lvl, T_FaultChecker fCheck)
{
    
    T_Fault* this = allocateFault();
    if (this)
    {
        this->name = name;
        this->type = FLT_TYPE_CALLBACK;
        this->linkedObj.fCheck = fCheck;
        this->nativeLevel = lvl;
    }
    
    return this;

}

T_Fault* FLT_CreateSignalAgeFault(const char* name, T_FaultLevel lvl, T_SignalNode* sig, uint32_t maxAge_ms)
{
    T_Fault* this = allocateFault();
    if (this)
    {
        this->name = name;
        this->type = FLT_TYPE_SIG_AGE;
        this->linkedObj.pSig = sig;
        this->linkedObjParam = maxAge_ms;
        this->nativeLevel = lvl;
        
    }
    
    return this;
}
T_Fault* FLT_CreateManualFault(const char* name,  T_FaultLevel lvl)
{
    T_Fault* this =  FLT_CreateCallbackFault(name, lvl, NULL);
    if (this)
    {
        this->type = FLT_TYPE_MANUAL;
    }
    return this;
}

T_Fault* FLT_CreateEnclosureFault(const char* name)
{
    T_Fault* this =  FLT_CreateCallbackFault(name, INHERIT_FAULT, NULL);
    if (this)
    {
        this->type = FLT_TYPE_MANUAL;
    }
    return this;
}

bool FLT_Trigger(T_Fault* this, bool set)
{
    if (this == NULL)
    {
        LOG_Event(LOG_LVL_FATAL, "Attempt to report NULL Fault!");
        return false;
    }
    
    if (this->numChildren)
    {
        snprintf(logBuffer, LOG_BUFFER_LENGTH, "Can't trigger - %s is not a leaf Fault!", this->name);
        LOG_Event(LOG_LVL_FATAL, logBuffer);
        
    }
    
    this->age = 0;
    
    if (set)
    {
        //If it wasn't already, just set...
        if (this->lvl.now == FLT_LVL_CLEAR)
        {
            snprintf(logBuffer, LOG_BUFFER_LENGTH, "Fault \"%s\" triggered!", this->name);
            LOG_Event(LOG_LVL_INFO, logBuffer);    
            
            //Since this is a child node, its worst state will be its own level once triggered
            this->lvl.now = this->nativeLevel; 
            this->lvl.sinceBoot = this->lvl.now;
            this->lvl.worstEver = this->lvl.now;
            
        }
        else
        {
            return true;
        }
    }
    else
    {
        //If it wasn't already, just clear...
        if (this->lvl.now)
        {
            snprintf(logBuffer, LOG_BUFFER_LENGTH, "Fault \"%s\" cleared!", this->name);
            LOG_Event(LOG_LVL_INFO, logBuffer);    
            
            //don't touch historical states
            this->lvl.now = FLT_LVL_CLEAR;
        }
        else
        {
            return true;
        }
        
    }
           
    
    
    
    this = this->parent;
    
    //Warn everybody upstream
    while(this)
    {
        this->needsUpdate = true;
        this = this->parent;
    }
    
    //DO NOT DO ANYTHING TO NODE AFTER THIS - POINTER HAS CHANGED!
    
    return true;

}


bool FLT_MakeChildOf(T_Fault* this, T_Fault* parent)
{
    if (this == NULL || parent == NULL)
    {
        return false;
    }
    
    if (this->parent)
    {
        snprintf(logBuffer, LOG_BUFFER_LENGTH, "%s already has parent!", this->name);
        LOG_Event(LOG_LVL_FATAL, logBuffer);
        
        return false;
    }
    
    //Tell parent about new child
    if (parent->firstChild == NULL)
    {
        parent->firstChild = this;   
    }
    else
    {
        T_Fault* thisChild = parent->firstChild;
        //Iterate through list to get youngest child
        while (thisChild->nextSibling)
        {
            thisChild = thisChild->nextSibling;
        }

        thisChild->nextSibling = this;
    }

    
    parent->numChildren++;

    //Tell the node about its new parent.
    this->parent = parent;
    this->nextSibling = NULL; //no younger siblings (yet)
    
    return true;
}

 T_FaultBitmap FLT_GetBitmap(const T_Fault* this)
{
     T_FaultBitmap toRet = {0};
     
    if (this)
    {
       toRet =  this->lvl;
    }
    else
    {
        LOG_Event(LOG_LVL_FATAL, "Attempt to read NULL Fault!");
    }
       
    return toRet;
}

 const char* FLT_GetName(const T_Fault* this)
 {
     if (this)
     {
         return this->name;
     }
     
     return "???";
 }
 
bool FLT_ValidateTree(T_Fault* _root)
{
    flatTree.length = flattenTree(_root);
    root = _root;
    unpackFromWorstEverBuffer();
    if (flatTree.length)
    {
        TMR_StartRepeating(&timer, FAULT_TREE_UPDATE_TIME_MS);
        return true;
    }
    
    return false;
}

void FLT_AssignMasterFaultLed(T_LED* pLed)
{
    faultLed = pLed;
}

void FLT_ResetAllHistory()
{
    uint32_t ii;
        
    for (ii =0; ii < flatTree.length; ii++)
    {
        T_Fault* this = flatTree.faults[ii];
        
        //Only reset historical states
        this->lvl.worstEver = this->lvl.now; 
        this->lvl.sinceBoot = this->lvl.now;
    }
}

void FLT_PrepareForShutdown()
{
    packIntoWorstEverBuffer();
    if (MEM_WriteDataFromRamToNvm(vAddrWorstEver,(uint32_t*) worstEverBuffer, NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE))
    {
        puts("Saved WorstEver Fault states to NVRAM.");
    }
    else
    {
        puts("FAILED to save WorstEver Fault states to NVRAM.");
    }
}

void FLT_ChangeLevel(T_Fault* flt, T_FaultLevel lvl)
{
    if (flt)
    {
        flt->nativeLevel = lvl;
        printf("Fault \"%s\" will now have level \"%s\" when triggered.\r\n", flt->name, FLT_Level2Str(lvl));
    }
        
}

const char* FLT_Level2Str(T_FaultLevel lvl)
{
    
    switch (lvl)
    {
        case FLT_LVL_CLEAR: return "Clear";
        case FLT_LVL_MINIMAL: return "Minimal";
        case FLT_LVL_MODERATE: return "Moderate";
        case FLT_LVL_EXTREME: return "Extreme";
        default:    break;
    }
    
    return "???";
}

bool FLT_ChangeSignalAgeFaultTimeout(T_Fault* flt, uint32_t newTime_ms)
{
    if (flt)
    {
        if (flt->type == FLT_TYPE_SIG_AGE)
        {
            flt->linkedObjParam = newTime_ms;
            return true;
        }
        else
        {
            printf("FLT: Invalid attempt to set timeout param of non-signal age fault \"%s\".\r\n", flt->name);
        }            
    }
    else
    {
        puts("Attempt to SetSigAgeFaultTimeout on null flt!");
    }
    
    return false;
    
}


uint32_t FLT_GetSignalAgeFaultTimeout_ms(T_Fault* flt)
{
    uint32_t toReturn = 0;
    if (flt)
    {
        if (flt->type == FLT_TYPE_SIG_AGE)
        {
            toReturn = flt->linkedObjParam;
        }
        else
        {
            printf("FLT: Invalid attempt to get timeout param of non-signal age fault \"%s\".\r\n", flt->name);
        }            
    }
    else
    {
        puts("Attempt to GetSigAgeFaultTimeout on null flt!");
    }
    
    return toReturn;
    
}


//Handle command line inputs that had first token 'relay'
void FLT_ProcessInput(const char* tokens[], uint8_t nArgs)
{   
    if (nArgs == 0 || tokens[0][0] == '?')
    {
      puts("config\t\tView/adjust module configuration.");
      puts("tree\t\tPrint the Fault tree.");
      puts("info [name]\tGet info on a specific Fault.");
      puts("set [name]\tManually trigger a Fault.");
      puts("clear [name]\tManually clear a Fault.");
      puts("change-level [name] [lvl]\tChange the seriousness level of a given fault.");
      puts("usage\tGet info about memory usage, etc");
      
      //Debug options
      puts("worst-buff\tPrint the WorstEver buffer.");
      puts("worst-pack\tForce a packing of WorstEver states into buffer.");
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
        CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, tokens, nArgs);
    }
    else if (stricmp(tokens[0], "tree") == 0)
    {
       printTree();
    }
    else if (stricmp(tokens[0], "info") == 0)
    {
        if (nArgs == 2)
        {
            //Lookup node!
            T_Fault* goalNode = lookupFault(tokens[1]);
            
            if (goalNode)
            {
                detailedPrintFault(goalNode);
            }
            else
            {
                printf("Unable to find Fault \"%s\".\r\n", tokens[1]);

            }
        }
        else
        {
            puts("Need Fault name!");
        }
    }
    else if (stricmp(tokens[0], "set") == 0)
    {
        if (nArgs == 2)
        {
            //Lookup node!
            T_Fault* goalNode = lookupFault(tokens[1]);
            
            if (goalNode)
            {
                FLT_Trigger(goalNode, true);
            }
            else
            {
                printf("Unable to find Fault \"%s\".\r\n", tokens[1]);

            }
        }
        else
        {
            puts("Need Fault name!");
        }
    }
    else if (stricmp(tokens[0], "clear") == 0)
    {
        if (nArgs == 2)
        {
            //Lookup node!
            T_Fault* goalNode = lookupFault(tokens[1]);
            
            if (goalNode)
            {
                FLT_Trigger(goalNode, false);
            }
            else
            {
                puts("Unable to find Fault ");
                puts(tokens[1]);
            }
        }
        else
        {
            puts("Need Fault name!");
        }
    }
    else if (stricmp(tokens[0], "change-level") == 0)
    {
        if (nArgs == 3)
        {
            //Lookup node!
            T_Fault* goalNode = lookupFault(tokens[1]);
            int32_t lvl = atoi(tokens[2]);
            if (goalNode)
            {
                if (lvl >=0 && lvl <= FLT_LVL_EXTREME)
                {
                    FLT_ChangeLevel(goalNode, (T_FaultLevel) lvl);
                }
                else
                {
                    printf("Fault level \"%s\" is invalid.\r\n", tokens[2]);
                }
            }
            else
            {
                printf("Unable to find Fault \"%s\".\r\n", tokens[1]);
            }
        }
        else
        {
            puts("change-level [name] [lvl]");
        }
    }
    else if (stricmp(tokens[0], "worst-buff") == 0)
    {
        printWorstEverBuffer();
    }
    else if (stricmp(tokens[0], "worst-pack") == 0)
    {
        packIntoWorstEverBuffer();
    }
    else if (stricmp(tokens[0], "usage") == 0)
    {
        printf("Faults: %lu/%lu used\r\n", faultList.length, FAULTS_MAX_ALLOWED);
        printf("NVRAM: %lu words starting at 0x%08X\r\n", NUM_WORDS_NEEDED_FOR_WORST_EVER_STORAGE, vAddrWorstEver);
    }
    else
    {
    	Console_WriteByte('?');
    }
}

//########################## PRIVATE CODE ######################################
static void detailedPrintFault(const T_Fault* this)
{
    printf("Name: %s\r\n", this->name);
    printf("Parent: %s\r\n", this->parent ? this->parent->name : "n/a");
    printf("Type: %s\r\n", faultType2str(this->type));
    if (this->type == FLT_TYPE_SIG_AGE)
    {
        printf("\tSig @ 0x%08X, Timeout = %ums\r\n", (uint32_t) this->linkedObj.pSig, this->linkedObjParam);
    }
    else if (this->type == FLT_TYPE_CALLBACK)
    {
        printf("\tFunc @ 0x%08X\r\n", (uint32_t) this->linkedObj.fCheck);
    }
    
    printf("Level Now: %d\r\n", this->lvl.now);
    printf("Level Since Boot: %d\r\n", this->lvl.sinceBoot);
    printf("Level Worst Ever: %d\r\n", this->lvl.worstEver);
}
static void printTree()
{
       //Print the flattened tree in reverse order (read-friendly).
        uint32_t ii = flatTree.length;

        while (ii > 0)
        {
            ii--;
            uint8_t d = flatTree.depths[ii];
            
            while (d)
            {
               SEND_TAB();
               d--;
            }

            puts(flatTree.faults[ii]->name);
            SEND_SPACE();
            PrintAs_Dec_U32(flatTree.faults[ii]->lvl.now, 0);        
            NEW_LINE();
        }
  
    
}

static T_Fault* lookupFault(const char* name)
{
    T_Fault* this = NULL;

    uint8_t ii;
    for (ii = 0; ii < faultList.length; ii++)
    {
        if (stricmp(name, faultList.faults[ii].name) == 0)
        {
            //Found it!
            this = &faultList.faults[ii];
            break;
        }
    }
             
    return this;
}

static uint32_t flattenTree(T_Fault* start)
{
    uint32_t inx  = 0;
    uint8_t depth = 0;
    T_Fault*  node = start;

    bool goingUp = false;
    uint16_t watcher = 0xFFFF; //timeout after 100 iterations, in case there's a loop or something...
    while (node && watcher)
    {
        if (!goingUp && node->firstChild)
        {
            node = node->firstChild;
            depth++;
        }
        else
        {
            //We're done with this node - add it to the list.
             if (inx < FAULTS_MAX_ALLOWED)
            {
                flatTree.faults[inx] = node;
                flatTree.depths[inx] = depth;
                inx++;
            }
             else
             {
                 puts("list full");
                    break;
             }
        
            if (node->nextSibling)
            {
                node = node->nextSibling;
                goingUp = false;
            }
            else
            {
                node = node->parent;
                goingUp = true;
                depth--;
            }
        }

        watcher--;
    }

    if (!watcher)
    {
        puts("\r\nflat timed out!");
        return 0;
    }

    return inx;
}


static void updateFault(T_Fault* this)
{
    if (this == NULL)
    {
        LOG_Event(LOG_LVL_FATAL, "Attempt to update NULL Fault!");
        return;
    }
        
    /*If this is a leaf Fault, check its linked behavior. Then quit.*/
    if (this->numChildren == 0)
    {
        bool faultThen = this->lvl.now ? true : false;
        bool faultNow = faultThen;
        
        switch (this->type)
        {
            case FLT_TYPE_MANUAL:
            
            break;
            
            case FLT_TYPE_CALLBACK:
                if (this->linkedObj.fCheck)
                {
                    faultNow = this->linkedObj.fCheck();
                }        
            break;
            
            case FLT_TYPE_SIG_AGE:
                if (this->linkedObj.pSig)
                {
                    faultNow = (SIG_GetAge(this->linkedObj.pSig)*SIG_GetTreeUpdatePeriod_ms() > this->linkedObjParam);
                }
            break;
            
            default:
                snprintf(logBuffer, LOG_BUFFER_LENGTH, "Unhandled fault type %d", this->type);
                LOG_Event(LOG_LVL_FATAL, logBuffer);
            break;
        }
        
        if (faultNow != faultThen)
        {
            FLT_Trigger(this, faultNow);
        }
        
        return;
    }
        
    /*If this is not a leaf Fault, look to see if it needs an update.*/
    if (this->needsUpdate == false)
    {
        return;
    }
    
    this->needsUpdate = false;
    
    T_FaultLevel newLevel = FLT_LVL_CLEAR;
    
    //Find the highest level of all children
    T_Fault* theChild = this->firstChild;
    
    while (theChild)
    {
        if (theChild->lvl.now > newLevel)
        {
            newLevel = theChild->lvl.now;
        }
        
        theChild = theChild->nextSibling;
    }
    
    
    this->lvl.now = newLevel;
    
    if (newLevel > this->lvl.sinceBoot)
    {
        this->lvl.sinceBoot = newLevel;
    }
    
    if (newLevel > this->lvl.worstEver)
    {
        this->lvl.worstEver = newLevel;
    }
    
        
}

static void packIntoWorstEverBuffer()
{
    uint32_t faultInx;
    uint32_t wordInx = 0;
    uint8_t subCounter = 0;
    
    //Zero out the buffer before filling it, since we're or'ing in...
    memset(worstEverBuffer, 0x00, sizeof(worstEverBuffer));
    
    for (faultInx = 0; faultInx < faultList.length; faultInx++)
    {
        worstEverBuffer[wordInx] |= (faultList.faults[faultInx].lvl.worstEver << (BITS_PER_FAULT_STATE*subCounter));
        subCounter++;
        if (subCounter >= FAULT_STATES_PER_WORD)
        {
            subCounter = 0;
            wordInx++;
        }
    }
}

static void unpackFromWorstEverBuffer()
{
    uint32_t faultInx;
    uint32_t wordInx = 0;
    uint8_t subCounter = 0;
    
    for (faultInx = 0; faultInx < faultList.length; faultInx++)
    {
        faultList.faults[faultInx].lvl.worstEver = (T_FaultLevel) ((worstEverBuffer[wordInx] >> (BITS_PER_FAULT_STATE*subCounter)) & 0x03); //0b011
        subCounter++;
        if (subCounter >= FAULT_STATES_PER_WORD)
        {
            subCounter = 0;
            wordInx++;
        }
    }
}

static uint8_t lookupWorstEverFromBuffer(uint8_t faultInx)
{
    if (faultInx < FAULTS_MAX_ALLOWED)
    {
        uint8_t wordInx = faultInx/FAULT_STATES_PER_WORD;
        uint8_t bitInx = BITS_PER_FAULT_STATE*(faultInx % FAULT_STATES_PER_WORD);
        return ((worstEverBuffer[wordInx] >> bitInx) & 0x03); //0b011;
    }
    
    return 0xFF;
}


static void printWorstEverBuffer()
{
    uint8_t ii;
    for (ii = 0; ii < faultList.length; ii++)
    {
        T_Fault* this = &faultList.faults[ii];
        printf("[%02u]\t%s\t%u\r\n", ii, this->name, lookupWorstEverFromBuffer(ii));
    }
}

const char* faultType2str(T_FaultType t)
{
    switch (t)
    {
        case FLT_TYPE_MANUAL: return "Manual";
        case FLT_TYPE_SIG_AGE: return "SigAge";
        case FLT_TYPE_CALLBACK: return "Callback";
        
        default : break;
    }
    
    return "???";
}