/**
 * @file tbl.c
 * @brief Table manager
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2014-11-06
 *
 *
 **/



#include "tbl.h"

#include "bluflex_core.h"
#include <stdio.h>


#define MAX_NUM_TABLES 16
static struct
{
   T_Table tables[MAX_NUM_TABLES];
   uint32_t length;

} tableList;



static int32_t interpolate(int32_t x, const T_Point* lessor, const T_Point* greator);


T_Table* TBL_AddTable(const char* name, const T_Point* data, uint32_t dataLength)
{
    if (tableList.length < MAX_NUM_TABLES)
    {
        T_Table* thisTable = &(tableList.tables[tableList.length]);
        thisTable->name = name;
        thisTable->data = data;
        thisTable->nPoints = dataLength;

        tableList.length++;

        return thisTable;
    }

    return NULL;
}


void TBL_Init()
{
    tableList.length = 0;

}



static int32_t interpolate(int32_t x, const T_Point* lessor, const T_Point* greator)
{
    if (!lessor || !greator)
    {
        puts("null points!");
        return 0;
    }

    int32_t deltaX = greator->x - lessor->x;
    int32_t deltaY = greator->y - lessor->y;
    int32_t partialX = x - lessor->x;

    // If deltaX is zero, the math below will break.
    if (deltaX == 0)
    {
        //Just pick one.  Either is equally valid.
        return greator->y;
    }

    // How much precision is needed?

    // Fastest is keep it in INT32, my potentially roll over.
    //INT32 y = lessor.y + partialX * deltaY / deltaX;

    // Better (but slower) is to convert to INT64, avoids overflows
    int32_t y = lessor->y + (int64_t) partialX * deltaY / deltaX;

    // Best (and slowest) is switch to floating point.
    //INT32 y = lessor.y + (float) partialX * deltaY / deltaX;

    return y;
}



// binary search lookup for x point, and return of interpolated value.
int32_t TBL_Lookup(const T_Table* table, int32_t x)
{
    if (table == NULL)
    {
      puts("NULL table");
      return 0;
    }


    if (table->nPoints < 2)
    {
        
        puts("Table too small!");
        return 0;
    }

    uint32_t first_ind = 0;
    uint32_t last_ind = table->nPoints - 1;

    uint32_t middle_ind = (first_ind + last_ind)/2;

    while( first_ind < last_ind )
    {
        if ( table->data[middle_ind].x < x )
        {
            first_ind = middle_ind + 1;
        }
        else
        {
            last_ind = middle_ind;
        }
        middle_ind = (first_ind + last_ind)/2;
    }
    if (last_ind < 1)
    {
        last_ind = 1;
    }

    // return interpolated (extrapolaed) point
    return interpolate(x, &(table->data[last_ind-1]), &(table->data[last_ind]) );
}

void TBL_ProcessInput(const char* tokens[], uint8_t nArgs)
{
    uint8_t ii;
    if (nArgs == 0 || tokens[0][0] == '?')
    {
        puts("\r\nlist");
        puts("\r\nlookup");
    }
    else if (stricmp("list", tokens[0]) == 0)
    {
        for (ii = 0; ii < tableList.length; ii++)
        {
            T_Table* thisTable = &tableList.tables[ii];
            printf("%s\t%ull\r\n", thisTable->name, thisTable->nPoints);            
        }
    }
    else if (stricmp("lookup", tokens[0]) == 0)
    {
        if (nArgs >= 3)
        {
            //Lookup the table by name.
            T_Table* theTable = NULL;
            for (ii = 0; ii < tableList.length; ii++)
            {
                if (stricmp(tableList.tables[ii].name, tokens[1]) == 0)
                {
                    //Found it!
                    theTable = &tableList.tables[ii];
                    break;
                }
            }

            if (theTable)
            {
                //Get user input, evaluate, parse results
                int32_t input = strtol(tokens[2], NULL, 0);
                int32_t result = TBL_Lookup(theTable, input);
                printf("%s(%d)=%d", theTable->name, input, result);
            }
            else
            {
                printf("Table\"%s\" was not found.", tokens[1]);
            }
        }
        else
        {
            puts("tbl lookup <name> <inputs>");
        }
    }
    else
    {
    	Console_WriteByte('?');
    }

}


