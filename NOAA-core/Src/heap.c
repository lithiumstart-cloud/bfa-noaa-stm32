/**
 * @file  heap.c
 * @brief  Dynamic Memory Driver
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-01-21
 *
 * This driver uses standard malloc() dyanmic memory.
 * 
**/

//############################## INCLUDES ######################################

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "console.h"
#include "heap.h"


//############################## DEFINES ######################################



//########################### PRIVATE VARIABLES ################################

static uint32_t bytesAllocated = 0;

//############################## PROTOTYPES ####################################




//############################## PUBLIC CODE ###################################

void * Heap_Allocate(size_t n)
{
    void* ptr = malloc(n);
    
    bytesAllocated += n;
    
    return ptr;
}


uint32_t Heap_GetNumBytesAllocated(void)
{
    return bytesAllocated;
}


void Heap_ProcessInput(const char* tokens[], uint8_t nTokens)
{
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("usage");
    }
    else if (stricmp(tokens[0], "usage") == 0)
    {
        printf("# bytes allocated = %lu\r\n", bytesAllocated);
    }
    else
    {
    	Console_WriteByte('?');
    }
}
