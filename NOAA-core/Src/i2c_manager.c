/**
 * @file i2c_manager.c
 * @brief  BluFlex I2C Manager
 * @copyright 2018 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2018-06-12
 *
**/
//######################### MODULE INCLUDES ##################################
#include <stdint.h>
#include <stdbool.h>

#include "i2c_manager.h"
#include "i2c_hw.h"
#include <bluflex_core.h>
#include "bq769x0.h"

//###################### MODULE DEFINES ###################################

//#define I2C_ERROR_TIMEOUT_MS (15)
//#define I2C_MAX_ALLOWABLE_TERMINATION_MS (200)
//#define I2C_SEND_BYTE_TIMEOUT_MS (100)
//#define I2C_GET_BYTE_TIMEOUT_MS (100)
//
//#define MAX_NUM_PORTS (2) //TODO: Move to hardware specific level?
//
//#define RX_BUFFER_DEFAULT_VALUE (0xFF)





//<SRB>##########################################################################################################
//<SRB>### MOVE TO BSP DIRECTORY ################################################################################
//<SRB>##########################################################################################################

/**
  * @brief  Read an amount of data in blocking mode from a specific memory address
  * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  DevAddress Target device address: The device 7 bits address value
  *         in datasheet must be shifted to the left before calling the interface
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef I2C_ReadMem( uint16_t MemAddress,
				  	  	  	  uint8_t *pData,
							  uint16_t Size,
							  uint32_t Timeout )
{
	return ( HAL_I2C_Mem_Read( &hi2c2, (BQ769x0_I2C_ADDR7 <<1), MemAddress, I2C_MEMADD_SIZE_8BIT, pData, Size, Timeout ) );
}


/**
  * @brief  Write an amount of data in blocking mode to a specific memory address
  * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  DevAddress Target device address: The device 7 bits address value
  *         in datasheet must be shifted to the left before calling the interface
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef I2C_WriteMem( uint16_t MemAddress,
				   	   	   	   uint8_t *pData,
							   uint16_t Size,
							   uint32_t Timeout )
{
	return( HAL_I2C_Mem_Write( &hi2c2, (BQ769x0_I2C_ADDR7 <<1), MemAddress, I2C_MEMADD_SIZE_8BIT, pData, Size, Timeout) );
}











