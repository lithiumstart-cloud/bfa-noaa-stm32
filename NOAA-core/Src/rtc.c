/**
 * @file rtc.c
 * @brief RTC Module
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-03-16
 *
**/
//############################## INCLUDES ######################################
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "rtc.h"
#include "rtc_hw.h"
#include "datetime.h"
#include <bluflex_core.h>
//############################## DEFINES ######################################


//########################### CONFIG VALUES ####################################

static struct
{
       uint32_t syncPeriod_ms; ///< How often will the MCU poll the RTC?
       uint32_t errorPeriod_ms;///< How long can we go without new data 
       uint32_t printTime;
      
} configValues =
{
    .syncPeriod_ms = 500,
    .errorPeriod_ms = 2000,
    .printTime = 0,
    
};
#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "syncPeriod_ms",
    "errorPeriod_ms",
    "printTime?",
};

//######################## MODULE VARIABLES ####################################
static T_Datetime datetimeBoot;
static T_Datetime datetimeNow;
static T_Timer freshDataTimer;
static bool firstRun;
//####################### MODULE PROTOTYPES ####################################

//######################### MODULE CODE #########################################

void RTC_Init(T_BF_I2C_Port* pPort)
{
    //Do any intializations you have here. 
    memset(&datetimeNow, 0x00, sizeof(T_Datetime));
    
    
    RTC_HW_Init(configValues.syncPeriod_ms, pPort);
    
    TMR_StartOneshot(&freshDataTimer, configValues.errorPeriod_ms);
    
    firstRun = 1;
}



bool RTC_Update()
{
    
    bool toReturn = RTC_HW_Update();
    
    if (RTC_HW_GetFreshDatetime(&datetimeNow))
    {
        if (configValues.printTime)
        {
           printf("RTC: Got fresh hw reading:  ");
           RTC_PrintDatetimeNow();
           NEW_LINE();
        }
        
        if (firstRun)
        {
            firstRun = 0;
            datetimeBoot = datetimeNow;
            puts("RTC booted @ ");
            RTC_PrintDatetimeNow();
            NEW_LINE();
        }
        
        if (!TMR_IsRunning(&freshDataTimer))
        {
            LOG_Event(LOG_LVL_INFO, "RTC is back in action.");
        }
        
        TMR_StartOneshot(&freshDataTimer, configValues.errorPeriod_ms);
    }
    else if (TMR_IsRunning(&freshDataTimer) && TMR_HasTripped_ms(&freshDataTimer))
    {
        LOG_Event(LOG_LVL_FATAL, "RTC has not gotten fresh data recently!");
        
    }
    
    
    //Return false if you want the module to block any sleep.
    return toReturn;
}

void RTC_PrintDatetimeNow(void)
{
    DT_PrintDatetime(&datetimeNow);
}

bool RTC_GetDatetimeNow_str(char* buffer, size_t bufferLength)
{
    return DT_Datetime2String(buffer, bufferLength, &datetimeNow);
}

bool RTC_GetDatetimeNow(T_Datetime* dt)
{
    if (dt)
    {
        *dt = datetimeNow;
        return true;
    }
    
    return false;
}

void RTC_FillDatetime(uint8_t* dst, uint8_t N)
{
    if (N >= 6)
    {
        dst[0] = datetimeNow.time.sec;
        dst[1] = datetimeNow.time.min;
        dst[2] = datetimeNow.time.hour;
        dst[3] = datetimeNow.date.day;
        dst[4] = datetimeNow.date.month;
        dst[5] = datetimeNow.date.year - 2000; //Years since 2000...
    }
}
//########################### DRAWING FUNCTIONS ################################


static T_Datetime dtBuffer;
 void RTC_ProcessInput(const char* tokens[], uint8_t nArgs)
{
    
    if (nArgs == 0 || tokens[0][0] == '?')
    {
        
        puts("config\tView/adjust module configuration");
        puts("now\tPrint date/time now.");
        puts("boot\tPrint date/time when system was last booted.");
        puts("set-rt [yyyy-mm-dd] [hh:min]");
        puts("save-rt\tSave buffered date/time to RTC.");
        puts("hw\tAccess HW layer menu");
  
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
      CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, tokens, nArgs);
    }
    else if (stricmp(tokens[0], "now") == 0)
    {
        printf("Time now:  ");
        RTC_PrintDatetimeNow();
        NEW_LINE();
    }
    else if (stricmp(tokens[0], "boot") == 0)
    {
        printf("Boot time: ");
        DT_PrintDatetime(&datetimeBoot);
        NEW_LINE();
    }
    else if (stricmp(tokens[0], "hw") == 0)
    {
        RTC_HW_ProcessInput(&tokens[1], nArgs - 1);
    }
    else if (stricmp(tokens[0], "save-rt") == 0)
    {
        if (nArgs == 1)
        {
            RTC_HW_SetDateTime(&dtBuffer);
        }
        else
        {
             puts("This command doesn't accept arguments. Did you mean \"set-rt\"?");
        }
    }
    else if (stricmp(tokens[0], "set-rt") == 0)
    {
        if (nArgs == 3)   
        {
            uint32_t year, month, day, hour, min;
            int dateResult = sscanf(tokens[1], "%u-%u-%u", &year, &month, &day);
            int timeResult = sscanf(tokens[2], "%u:%u", &hour, &min);

            if (dateResult == 3 && timeResult == 2)
            {
                dtBuffer.date.year = year;
                dtBuffer.date.month = month;
                dtBuffer.date.day = day;

                dtBuffer.time.hour = hour;
                dtBuffer.time.min = min;
                dtBuffer.time.sec = 0;    

                if (DT_ValidateDate(&dtBuffer.date) && DT_ValidateTime(&dtBuffer.time))
                {
                    DT_PrintDatetime(&dtBuffer);
                    puts(" saved to buffer. Use \"save-rt\" to write to RTC.");
                }
                else
                {
                    puts("Invalid date/time!");   
                }
            }
            else
            {
                printf("Unable to parse \"%s\" and/or \"%s\".\r\n", tokens[1], tokens[2]);
                puts("set-rt [yyyy-mm-dd] [hh:min]");
            }
        }
        else
        {
            puts("set-rt [yyyy-mm-dd] [hh:min]");
        }
    }
    else
    {
    	Console_WriteByte('?');
    }

}
