/** 
* @file alarm.h
* @brief Defines constants and macros for BMS alarm values.
* @copyright 2014 by Lithiumstart LLC
* @author A. Kessler
* @date   2014-04-22
**/

#ifndef ALARM_H
#define ALARM_H

#include <stdint.h>
#include <stdbool.h>

#include "sig_types.h"

#ifndef INHERIT_ALARM
#define INHERIT_ALARM 0
#endif

#ifndef NO_ALARM
#define NO_ALARM 0
#endif


/**
 * This ordering must be consistent with the struct in T_AlarmTemplate.
 */
enum
{
    ALARM_LIMIT_INX_CRITICAL_UPPER = 0,
    ALARM_LIMIT_INX_WARN_UPPER,
    ALARM_LIMIT_INX_CLEAR_UPPER,
    ALARM_LIMIT_INX_CLEAR_LOWER,
    ALARM_LIMIT_INX_WARN_LOWER,
    ALARM_LIMIT_INX_CRITICAL_LOWER,
            
    ALARM_LIMITS_NUM_LIMITS,
};


typedef union
{
    T_SignalDatum array [ALARM_LIMITS_NUM_LIMITS];
    
    struct
    {
        T_SignalDatum CriticalUpper; ///< Highest value in "critical" state - above is stop
        T_SignalDatum WarnUpper; ///< Highest value in "warn" state - above is critical
        T_SignalDatum ClearUpper; ///< Highest value in "clear" state - above is warn
            
        T_SignalDatum ClearLower; ///< Lowest value in "clear" state - below is warn
        T_SignalDatum WarnLower; ///< Lowest value in "warn" state - below is critical
        T_SignalDatum CriticalLower; ///< Lowest value in "critical" state - below is stop
    }__attribute__((packed));
    
} __attribute__((packed)) T_AlarmLimits;

typedef struct
{
    const char* name;
   
    T_AlarmLimits limits;
    
    T_TypeId type;
    T_UnitId unit;

} T_AlarmTemplate;




/// Valid alarm levels, indicating seriousness.
typedef enum
{
    ALARM_LVL_CLEAR = 0, ///< Everything is dandy.
    ALARM_LVL_WARN, ///< Hm, this is strange...
    ALARM_LVL_CRIT, ///< Houston, we have a problem!
    ALARM_LVL_STOP, ///< Shut. Down. EVERYTHING!
} __attribute__ ((__packed__)) T_AlarmLevel;

typedef union
{
    uint8_t u8;
    struct
    {
        T_AlarmLevel now : 2;
        T_AlarmLevel sinceBoot : 2;
        T_AlarmLevel worstEver : 2;
        bool hasChanged : 1;
    } __attribute__ ((__packed__));
}  T_AlarmBitmap;


typedef T_AlarmBitmap T_Alarm __attribute__ ((deprecated));

/**
 * Intialize the alarm module.
 */
void Alarm_Init(void);


/**
 *
 * @param
 * @param clearLo
 * @param clearHi
 * @param warnLo
 * @param warnHi
 * @param critLo
 * @param critHi
 * @return
 */
T_AlarmTemplate* Alarm_CreateTemplate_I32(const char* , T_UnitId unit,  int32_t clearLo, int32_t clearHi, int32_t warnLo, int32_t warnHi, int32_t critLo, int32_t critHi);

/**
 *
 * @param
 * @param clearLo
 * @param clearHi
 * @param warnLo
 * @param warnHi
 * @param critLo
 * @param critHi
 * @return
 */
T_AlarmTemplate* Alarm_CreateTemplate_U32(const char*,  T_UnitId unit, uint32_t clearLo, uint32_t clearHi, uint32_t warnLo, uint32_t warnHi, uint32_t critLo, uint32_t critHi);


T_AlarmTemplate* Alarm_ImportLimitsFrom(const char* name, T_UnitId unit, const T_AlarmLimits* pLimits);

/**
 *
 * @param theAlarm
 * @param theLvl
 * @return
 */

bool Alarm_SetLevel(T_AlarmBitmap* theAlarm, T_AlarmLevel theLvl);

/**
 * 
 * @param pTemplate
 * @param pData
 * @return
 */
T_AlarmLevel Alarm_Evaluate(const T_AlarmTemplate *pTemplate, T_SignalDatum data);


/**
 * Process command line inputs for this module.
 * @param buf Two-dimensional array of CL inputs.
 * @param nArgs The number of tokens in the input.
 */
void Alarm_ProcessInput(const char* tokens[], uint8_t nTokens);

/**
 *
 * @param lvl
 * @return
 */
const char* Alarm_Level2Str(T_AlarmLevel lvl);

/**
 * Take the given alarm object, and return a byte with all 3 types of states
 * packed in, as:
 *
 * Bit |   7   |   6   |   5   |   4   |   3   |   2   |   1   |   0   |
 * ----+---------------+---------------+---------------+---------------+
 *     |    Reserved   |  Worst Ever   | Worst (Latch) |   Alarm Now   |
 *
 * @param pAlarm
 * @return Packed byte with alarm states
 */
uint8_t Alarm_PackIntoByte(const T_AlarmBitmap* pAlarm);

void Alarm_DrawInit(void);
void Alarm_DrawUpdate(void);
void Alarm_DrawInput(uint8_t cin);



#endif

