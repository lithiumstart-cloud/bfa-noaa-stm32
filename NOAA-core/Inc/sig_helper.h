/* 
 * File:   sig_helper.h
 * Author: akessler
 *
 * Created on May 19, 2016, 3:25 PM
 */

#ifndef SIG_HELPER_H
#define	SIG_HELPER_H


#define MAX_NUM_CHILDREN_PER_NODE (32) //2^5

struct xSignalNode
{
    const char* name; 

    
    struct
    {
        T_TypeId type : 3; 
        T_UnitId unit : 3;
        unsigned needsUpdate            : 1;
        unsigned isInitialized          : 1;
        
        unsigned numChildren : 6; ///< The number of children this node has [0, 32]
        unsigned childTypesConsistent   : 1; ///< Do the types of all the children match the type of the parent (this node)
        unsigned anyDescendentsHaveAlarms : 1;

        unsigned minChildInx :5; //Index of child with smallest MIN [0, 31]
        unsigned maxChildInx :5; //Index of child with largest MAX [0, 31]
        unsigned birthOrderInx : 5; ///< What child index this node of its parent?  [0, 31]
        unsigned isAlarmSuspended       : 1; //Should be ignore alarm (it is has one)    
    } __attribute__ ((__packed__));
        
   
    T_SignalNode* parentNode; ///< Step up 
    T_SignalNode* nextSibling; ///< Step to the right 
    T_SignalNode* firstChild; ///< Step down 

    T_AlarmTemplate* pAlarmTemplate; 
 
    T_SignalDatum max;
    T_SignalDatum min;
    T_SignalDatum avg;
    T_SignalDatum sum;
   
    T_AlarmBitmap alarm; ///< The Alarm state now (8 bits)
    struct
    {
        unsigned age : 16;
        unsigned :      8;        
    } __attribute__ ((__packed__));

    bool bypass;    ///< If CB is bypassed, all signals coming from that CB, will be excluded from calculating min, max and avg of a parent signal
    
} __attribute__ ((__packed__));

/**
 * 
 * @param node
 */
void sigh_DetailedPrintNode(const T_SignalNode* node);

/**
 * 
 * @param node
 * @param depth
 */
void sigh_PrintIndentedNameUnits(T_SignalNode* node, uint8_t depth);

/**
 * 
 * @param u
 * @return 
 */
uint8_t sigh_Unit2Width(T_UnitId u);

/**
 * 
 * @param type
 * @return 
 */
T_SignalDatum sigh_Type2Max(T_TypeId type);

/**
 * 
 * @param sigDat
 * @param type
 * @param width
 */
T_SignalDatum sigh_Type2Min(T_TypeId type);

/**
 * 
 * @param sigDat
 * @param type
 * @param width
 */
void sigh_PrintDatumWidth(T_SignalDatum sigDat, T_TypeId type, uint8_t width);

/**
 * Sum two SignalDatums, and return result. References and saves to runnignSum->*all.
 * @param type
 * @param x Add to
 * @param y
 * @return A+B
 */
T_SignalDatum sigh_SmartAdd(T_TypeId type, T_SignalDatum a, T_SignalDatum b);

/**
 * Divide a SignalDatum by integer, and size result for type. This function is
 * made to work with SmartAdd, so we assume Datums of type U8/I8 are actually in 
 * their respective 16-bit field. 
 * @param type
 * @param pA
 * @param n
 * @return A/n
 */
T_SignalDatum sigh_SmartDivide(T_TypeId type, T_SignalDatum x, uint32_t n);


/**
 * Returns TRUE is x > y. Assumes of same type.
 * @param type
 * @param x
 * @param y
 * @return (x > y) 
 */
bool sigh_SmartIsGreaterThan(T_TypeId type, T_SignalDatum x, T_SignalDatum y);

/**
 * 
 * @param node
 */
void sigh_PrintStopAlarmValues(const T_SignalNode* node);

/**
 * 
 * @param parent
 * @param str
 * @return 
 */
const T_SignalNode* sigh_ChildStringToNode(const T_SignalNode* parent, const char* str);



#endif	/* SIG_HELPER_H */

