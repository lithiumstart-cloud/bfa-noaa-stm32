/* 
 * File:   sig_types.h
 * Author: akessler
 *
 * Created on September 29, 2014, 10:55 AM
 */

#ifndef SIG_TYPES_H
#define	SIG_TYPES_H

#include <stdbool.h>
#include <stdint.h>

#define UALL_MAX    UINT32_MAX
#define UALL_MIN         0
#define IALL_MAX    INT32_MAX
#define IALL_MIN    INT32_MIN

typedef int32_t  T_iAll;
typedef uint32_t T_uAll;

#define TYPE_UMAX   TYPE_U32
#define TYPE_IMAX   TYPE_I32

#define SIGNAL_DATUM_NUM_BYTES 4



///This with a U16/U32 with a ton of options for accesssing the data.
typedef union
{

      uint32_t u32;
      int32_t  i32;

      T_uAll u_all;
      T_iAll i_all;


      uint16_t u16;
      int16_t  i16;

      uint8_t u8;
      int8_t i8;

      uint8_t arr_u8[SIGNAL_DATUM_NUM_BYTES];

} T_SignalDatum;


typedef enum
{
    TYPE_U32 = 0,
    TYPE_I32,

    TYPE_NUM_TYPES

} __attribute__ ((__packed__)) T_TypeId;


typedef enum
{
    UNIT_NONE = 0,
    UNIT_CURRENT_MA,
    UNIT_VOLTAGE_MV,
    UNIT_TEMP_DC,
    UNIT_PERCENTAGE,
            
    UNIT_NUM_UNITS
} __attribute__ ((__packed__)) T_UnitId;

typedef enum
{
    SIG_POST = 0,
    SIG_RQST_AVG = 0,
    SIG_RQST_MIN,
    SIG_RQST_MAX,
    SIG_RQST_ALARM,
    SIG_RQST_STD,
    SIG_RQST_SUM,
            
    SIG_RQST_NUM_RQSTS

} __attribute__ ((__packed__)) T_SigRequest;





#endif	/* SIG_TYPES_H */

