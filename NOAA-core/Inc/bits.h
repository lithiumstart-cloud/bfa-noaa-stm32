/**
 * @file bits.h
 * @brief Provides some useful defintions for use with bitwise operations.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-04-08
 *
**/

#ifndef BITS_H
#define BITS_H

#define BIT0HI  (0x0001) ///< All bits zero except bit 0
#define BIT1HI  (0x0002) ///< All bits zero except bit 1
#define BIT2HI  (0x0004) ///< All bits zero except bit 2
#define BIT3HI  (0x0008) ///< All bits zero except bit 3
#define BIT4HI  (0x0010) ///< All bits zero except bit 4
#define BIT5HI  (0x0020) ///< All bits zero except bit 5
#define BIT6HI  (0x0040) ///< All bits zero except bit 6
#define BIT7HI  (0x0080) ///< All bits zero except bit 7

#define BIT8HI  (0x0100) ///< All bits zero except bit 8
#define BIT9HI  (0x0200) ///< All bits zero except bit 9
#define BIT10HI (0x0400) ///< All bits zero except bit 10
#define BIT11HI (0x0800) ///< All bits zero except bit 11
#define BIT12HI (0x1000) ///< All bits zero except bit 12
#define BIT13HI (0x2000) ///< All bits zero except bit 13
#define BIT14HI (0x4000) ///< All bits zero except bit 14
#define BIT15HI (0x8000) ///< All bits zero except bit 15

#define BIT0LO  (~BIT0HI) ///< All bits one except bit 0
#define BIT1LO  (~BIT1HI) ///< All bits one except bit 1
#define BIT2LO  (~BIT2HI) ///< All bits one except bit 2
#define BIT3LO  (~BIT3HI) ///< All bits one except bit 3
#define BIT4LO  (~BIT4HI) ///< All bits one except bit 4
#define BIT5LO  (~BIT5HI) ///< All bits one except bit 5
#define BIT6LO  (~BIT6HI) ///< All bits one except bit 6
#define BIT7LO  (~BIT7HI) ///< All bits one except bit 7

#define BIT8LO  (~BIT8HI) ///< All bits one except bit 8
#define BIT9LO  (~BIT9HI) ///< All bits one except bit 9
#define BIT10LO (~BIT10HI) ///< All bits one except bit 10
#define BIT11LO (~BIT11HI) ///< All bits one except bit 11
#define BIT12LO (~BIT12HI) ///< All bits one except bit 12
#define BIT13LO (~BIT13HI) ///< All bits one except bit 13
#define BIT14LO (~BIT14HI) ///< All bits one except bit 14
#define BIT15LO (~BIT15HI) ///< All bits one except bit 15

#define SET_BITS_HIGH(x)  ((1 << (x)) - 1) //set first N bits high


#endif 
