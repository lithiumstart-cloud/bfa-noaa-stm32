/**
 * @file mem.h
 * @brief Non-Volatile Memory Interface
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2012-01-16
 *
**/


#ifndef MEM_H
#define MEM_H

#include <stdint.h>
#include <stdbool.h>


typedef uint32_t T_MemAddr; ///< An abstracted "virtual" address for our data. Only non-zero address are valid.

/**
 * Initalize the Memory module.
 */
void MEM_Init(void);
void MEM_Update(void);
void MEM_ProcessInput(const char* tokens[], uint8_t nArgs);

/**
 * Allocate a word (32bits) of NV memory space
 * @return Virtual address in NV memory - returns 0 on fail
 */
T_MemAddr MEM_AllocateWord(void);

T_MemAddr MEM_AllocateData(uint8_t nWords);

/**
 * Read data from given address in nonvolatile memory, and store in given
 * destination pointer.
 * @param vAddr Virtual address to read from
 * @param nBytes Number of bytes to read
 * @param pDest Destination address
 * @return Success
 */
bool MEM_ReadWordFromNvmToRam(T_MemAddr vAddr, uint32_t* pDest);

/**
 * Write data to given address in nonvolatile memory
 * @param vAddr Virtual addressto write to
 * @param pSrc Pointer to source data
 * @param nBytes Number of bytes to write
 * @return Success
 */
bool MEM_WriteWordFromRamToNvm(T_MemAddr vAddr, uint32_t data);

bool MEM_WriteDataFromRamToNvm(T_MemAddr vAddr, uint32_t* pSrc, uint8_t nWords);
bool MEM_ReadDataFromNvmToRam(T_MemAddr vAddr, uint32_t* pDest, uint8_t nWords);

bool MEM_WasJustProgrammed(void);
bool MEM_LoadDefaultData(const char* name, T_MemAddr memAddr, uint32_t* data, uint8_t nWords);

#endif
