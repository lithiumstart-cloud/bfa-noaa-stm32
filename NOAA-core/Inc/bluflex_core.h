/*
 * @file   bluflex_core.h
 * @brief BluFlex Core Config Header - This is the external interfact to the lib
 * @copyright 2015 by Lithiumstart Inc
 * @author: akessler
 * @date: March 18, 2015
 */



#ifndef BLUFLEX_CORE_H
#define	BLUFLEX_CORE_H

#include <stdint.h>
#include <stdbool.h>

#ifndef UNUSED
	#define UNUSED(x) ((void)(x))
#endif

#include "datetime.h"

#define STM32	(GSTM32)	//This define will be used to separate STM32 from PIC32 code in order to keep the same code base for both MCUs


typedef struct
{
   char* name;
   char* buildDate_str;
   char* buildTime_str;
   char* gitHash_str;

}T_ConfigInfo;

typedef struct
{
    uint8_t consoleUartPort;
    uint32_t consoleUartBaud;
    uint16_t consoleUartTxQueueLength;
    uint16_t consoleUartRxQueueLength;

    uint8_t primaryCanPort;
    uint32_t primaryCanBaud;


    bool hasSecondaryCan;
    uint8_t secondaryCanPort;
    uint32_t secondaryCanBaud;

    uint8_t defaultOscillatorId;
    uint32_t sysClock_Hz;

    bool isSleepEnabled;
    bool clearPicWdtOnGeneralExcepetion;

    const T_ConfigInfo* extraLibConfigSlot1;
    const T_ConfigInfo* extraLibConfigSlot2;
    
} T_AppConfigValues;

typedef struct
{
    uint8_t x; ///< Major Version #
    uint8_t y; ///< Minor Version #
    uint8_t z; ///< Revision #
    uint8_t p; ///< Patch #
} T_Version;



typedef struct
{
    T_Date buildDate;
    T_Version gitVersion;
} T_ParsedInfo;

extern const T_AppConfigValues appConfigValues;
extern const T_ConfigInfo appConfigInfo;

extern const T_ConfigInfo coreConfigInfo;
extern const T_ConfigInfo* extraLibConfigSlot1;

/*
 * This macro should be called in the the main file (outside of function).
 * It creates a global constant that contains the app name, Git hash, build date,
 * and build time. Although this macro is defined in Core, the Date, Time, and 
 * Ghash macros are not evaluated until the application project builds, so they
 * will reference the properties of the application project.
 * 
 * Note that:
 *      - application name should be delivered as a constant string.
 *      - a trailing semicolon is requried 
 *      - behavior may be IDE/Pc dependent?
 * 
 * Example "main.c":
 * 
 *   CORE_SET_APP_NAME("My Project");
 * 
 *  int main()
 *  {    
 *      ...
 *  }
 */
#define CORE_SET_APP_NAME(name_str) \
const T_ConfigInfo appConfigInfo = {\
    .name =name_str,\
    .gitHash_str = STRINGIZE_VALUE_OF(GHASH_STR),\
    .buildDate_str = __DATE__, \
    .buildTime_str = __TIME__,\
}

#include "tmr.h"
#include "sys.h"
#include "uart.h"
#include "util.h"
#include "led.h"
#include "log.h"
#include "sch.h"
#include "cli.h"
#include "mem.h"
//#include "buttons.h"
#include "gpio.h"

#include "alarm.h"
#include "sig.h"
//#include "can.h"
#include "rly.h"
#include "tbl.h"
#include "fifo.h"
#include "flt.h"

#include "vt100.h"
#include "console.h"
#include "str_lib.h"
#include "print_as.h"

#include "spi_hw.h"
#include "tmr_hw.h"
#include "mcu_hw.h"

#include "i2c_manager.h"
#include "i2c_slave_hw.h"

#include "heap.h"

#include "rtc.h"
#include "thrm.h"

#include "spi_manager.h"

bool CORE_Init(void);
bool CORE_Init_And_Schedule(void);
bool CORE_GetParsedCoreVersion(T_Version* pVersion);
bool CORE_GetParsedCoreDate(T_Date* pDate);

bool CORE_GetParsedAppVersion(T_Version* pVersion);
bool CORE_GetParsedAppDate(T_Date* pDate);

void CORE_GetParsedAppDate_Array(uint8_t* dest, uint8_t nBytes);
void CORE_GetParsedAppVersion_Array(uint8_t* dest, uint8_t nBytes);

#endif	/* BF_CONFIG_H */

