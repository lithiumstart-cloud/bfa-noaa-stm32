/**
 * @file tmr_hw.h
 * @brief Hardware Timer Interface
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-01-20
 *
 * This is a platform agnostic interface for hardware level TMR functions. Any
 * driver that implements this interface must use non-blocking implementations.
**/

#ifndef TMR_HW_H
#define TMR_HW_H

#include <stdint.h>
#include <stdbool.h>

/**
 * Do whatever hardware intialization is necesary to start an 8KHz timer.
 * @param tickFreq_Hz Desired hardware timer frequency. 
 */
void TMR_HW_Init(uint32_t tickFreq_Hz);

/**
 * Retreive the current value of the free running clock.
 * @return 
 */
uint32_t TMR_HW_GetClock_ticks(void);

/**
 * Set the value of the free running clock.
 * @param x Value to set to
 */
void TMR_HW_SetClock_ticks(uint32_t x);


/**
 * Pause the free running counter without clearing state.
 */
void TMR_HW_PauseClock(void);

/**
 * Resume free running counter at last state after TMR_HW_PauseClock(). 
 * Only works if TMR_HW_Init() was called previously to initialize.
 */
void TMR_HW_ResumeClock(void);

/**
 * Start a microsecond level timer with specifed length. 
 * Must have a least 0.2us resolution, with max time >= 13.1ms.
 * @param time_us Timer duration, is microseconds.
 */
void TMR_HW_MicroTimer_Start(uint16_t time_us);

/**
 * Check if microsecond timer has expired.
 */
bool TMR_HW_MicroTimer_HasExpired(void);

#endif
