/**
 * @file i2c_hw.h
 * @brief I2C Hardware Interface (Master)
 * @copyright 2018 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2018-06-12
 *
 * This is a platform agnostic interface for hardware level I2C functions. Any
 * driver that implements this interface must use non-blocking implementations.
**/

#ifndef I2C_HW_H
#define I2C_HW_H

#include <stdint.h>
#include <stdbool.h>

#include "i2c_manager.h"

//This type should be fully defined in the platform specific implementation file.
struct sI2Cx_Registers;
typedef struct sI2Cx_Registers T_I2Cx;

/**
 * Do whatever hardware init is necessary to open an I2C port
 * @param tickFreq_Hz Desired hardware timer frequency. 
 */
volatile T_I2Cx* I2C_HW_Open(uint8_t id, uint32_t i2c_clock_freq_hz, bool useSMB);


void I2C_HW_ProcessInput(const char* tokens[], uint8_t nArgs);

volatile T_I2Cx* I2C_HW_GetI2CxPtr(uint8_t i2c_mod_id);
void I2C_HW_DebugPortPeripheral(volatile T_I2Cx* pI2Cx);
const char* I2C_HW_GetPeripheralState(volatile T_I2Cx* pI2Cx);

bool I2C_HW_IsOkToTx(volatile T_I2Cx* pI2Cx);

bool I2C_HW_IsTransmitComplete(volatile T_I2Cx* pI2Cx);
bool I2C_HW_IsReceiveComplete(volatile T_I2Cx* pI2Cx);
bool I2C_HW_WasByteAcknowledged(volatile T_I2Cx* pI2Cx);
bool I2C_HW_IsStartComplete(volatile T_I2Cx* pI2Cx);
bool I2C_HW_IsStopComplete(volatile T_I2Cx* pI2Cx);
bool I2C_HW_IsAckComplete(volatile T_I2Cx* pI2Cx);
bool I2C_HW_IsRstartComplete(volatile T_I2Cx* pI2Cx);

uint8_t I2C_HW_GetByte(volatile T_I2Cx* pI2Cx);
void I2C_HW_SendByte(volatile T_I2Cx* pI2Cx, uint8_t byte);

void I2C_HW_SendStart(volatile T_I2Cx* pI2Cx);
void I2C_HW_SendRstart(volatile T_I2Cx* pI2Cx);
void I2C_HW_SendStop(volatile T_I2Cx* pI2Cx);
void I2C_HW_SendAck(volatile T_I2Cx* pI2Cx);
void I2C_HW_SendNack(volatile T_I2Cx* pI2Cx);

void I2C_HW_EnableReceive(volatile T_I2Cx* pI2Cx);

#endif
