/**
 * @file util.h
 * @brief Implements the misc utility functions
 * @copyright 2019 EaglePicher, LLC
 * @author A. Kessler
 * @date   2019-04-26
 *
**/


#ifndef _UTIL_H
#define _UTIL_H

#include <stdint.h>
#include <stdbool.h>

#include "tmr.h"

#define PTR_TO_BOOL(p) ((p) > 0 ? true : false)

//This calculates ceil(x/y)
#define CEILING_DIV(x,y) (((x) + (y) - 1)/(y))


///######################### LOW PASS FILTER ###################################
/*
 * See the Utilities chapter in BluFlex Documentation for full theoretical 
 * background and usage instructions. 
 * 
 * Basically, this code implements a simple first  order low pass filter of the 
 * form 
 *   y[k] = (1/N)x[k] + (1 - 1/N)y[k-1] 
 *        =  y[k-1] + (x[k] - y[k-1])/N
 * 
 * where N = 2^nExp. The user provides "nExp" to define N. By enforcing N to be 
 * a power of two, it makes the math more efficient.
 * 
 * The cutoff frequency, fc, of the filter can be calculated as
 * 
 *              ln(1 - 1/N)
 *   fc = -fs * -----------
 *                  2*pi
 * 
 * where fs is the sampling frequency in Hz. 
 * 
 * Note that application code shouldn't ever directly modify the T_LowPassFilter
 * members - only use the provided utility functions. 
 */

typedef struct
{
    uint32_t theAve; ///< The moving average
    uint32_t theErr; ///< Accumulated error
    uint8_t nExp; ///< Filter length (N = 2^nExp)
    
} T_LowPassFilter_U32;

typedef struct
{
    int32_t theAve; ///< The moving average
    uint32_t theErr; ///< Accumulated error
    uint8_t nExp; ///< Filter length (N = 2^nExp)
    
} T_LowPassFilter_I32;


/**
 * Initialize an unsigned LPF  with given parameterization. 
 * Filter is a first order infinite impulse response (IIR) filter:
 *
 *   y[k] = (1/N)x[k] + (1 - 1/N)y[k-1] 
 *        =  y[k-1] + (x[k] - y[k-1])/N
 * 
 * where N = 2^(nExp). 
 * The cutoff frequency, fc, can be calculated as
 * 
 *              ln(1 - 1/N)
 *   fc = -fs * -----------
 *                  2*pi
 * 
 * where fs is the sampling frequency in Hz. 
 * 
 * @param lpf Low Pass Filter object to initialize
 * @param nExp Filter parameter exponent (N = 2^nExp)
 */
void UTIL_InitLowPassFilter_U32(T_LowPassFilter_U32 * lpf, uint8_t nExp);

/**
 * Update the LowPassFilter object with a new input, and return the new output.
 * LPF object must already be initialized before calling. 
 * @param lpf Low Pass Filter object to update
 * @param newRead New input to filter
 * @return Latest output from filter
 */
uint32_t UTIL_UpdateLowPassFilter_U32(T_LowPassFilter_U32 *lpf, uint32_t newRead);

/**
 * Initialize an signed LPF with given parameterization. 
 * Filter is a first order infinite impulse response (IIR) filter:
 *
 *   y[k] = (1/N)x[k] + (1 - 1/N)y[k-1] 
 *        =  y[k-1] + (x[k] - y[k-1])/N
 * 
 * where N = 2^(nExp). 
 * 
 * The cutoff frequency, fc, can be calculated as
 * 
 *              ln(1 - 1/N)
 *   fc = -fs * -----------
 *                  2*pi
 * 
 * where fs is the sampling frequency in Hz. 
 * 
 * @param lpf Low Pass Filter object to initialize
 * @param nExp Filter parameter exponent (N = 2^nExp)
 */
void UTIL_InitLowPassFilter_I32(T_LowPassFilter_I32 * lpf, uint8_t nExp);

/**
 * Update the LowPassFilter object with a new input, and return the new output. 
 * LPF object must already be initialized before calling. 
 * @param lpf Low Pass Filter object to update
 * @param newRead New input to filter
 * @return Latest output from filter
 */
int32_t UTIL_UpdateLowPassFilter_I32(T_LowPassFilter_I32 *lpf, int32_t newRead);


//############################# CRC #############################################
/**
 * Add a block of data to the running CRC calculation.
 * This function uses a statically initialized lookup table for PEC-8. 
 * Supported polynomials:
 *    -  x^8 + x^2 + x + 1   (0x07)
 *    -  x^8 + x^5 + x^4 + 1 (0x31)
 * @param poly      The desired polynomial for the CRC (see above).
 * @param init      The existing CRC remainder. If none, use 0x00.
 * @param msg[in]   Consecutive bytes to hash.
 * @param nBytes    The number of bytes in msg[].
 * @return          The updated CRC remainder.
 */
uint8_t UTIL_AddBlockToCRC8(uint8_t poly, uint8_t init, uint8_t const msg[], uint8_t nBytes);


/**
 * Add a block of data to the running CRC calculation.
 * This function uses the statically intialized lookup table for the CRC-15-CAN
 * with polynomial 0x4599.
 * @param msg[in]   Consecutive bytes to hash.
 * @param nBytes    The number of bytes in msg[].
 * @param init      The existing CRC remainder. If none, use 0x00.
 * @return          The updated CRC remainder.
 */
uint16_t UTIL_AddBlockToCRC15(uint8_t const msg[], uint8_t nBytes, uint16_t init);


//############################## MEDIAN ########################################
/**
 * Return the median of three unsigned inputs. 
 * @param a Input A
 * @param b Input B
 * @param c Input C
 * @return Median of (A,B,C)
 */
uint32_t UTIL_GetMedian_U32(uint32_t a, uint32_t b, uint32_t c);

/**
 * Return the median of three signed inputs. 
 * @param a Input A
 * @param b Input B
 * @param c Input C
 * @return Median of (A,B,C)
 */
int32_t UTIL_GetMedian_I32(int32_t a, int32_t b, int32_t c);

//################################## MISC ######################################
/**
 * Given two numbers,calculate percentage x 10, or (num/den)*1000. 
 * Least signficant digit is rounded.  
 * Return: (num/den)*1000 in % x 10 [0,1000]
 */
uint16_t UTIL_CalculatePercent_x10(uint32_t num, uint32_t den);

uint8_t UTIL_MonthStr2Num(const char* month_str);
const char* UTIL_MonthNum2Str(uint8_t monthNum);

/**
 * Convert a string of hex digits to decimal U32. Accepts 0x prefix.
 * Only support capital letters so far.
 * @param str Input string
 * @return value of digit in string
 */
uint32_t UTIL_HexStr2Int_U32(const char* str);



/**
 * This helper function low pass filters an input boolean such that output is 
 * true only when input has been asserted for a minimum required time. 
 * If input is deasserted, output immediately deasserts as well. 
 * 
 *  y(t) = f( y(t-1), x(t) )
 * 
 * @param inputNow Unfiltered input
 * @param lastOutputState Last state of output
 * @param pTimer Timer object dedicated to this filter
 * @param requiredTime_ms Minimum required time input mus tbe asserted
 * @return 
 */
bool UTIL_RequireMinAssertionTime(bool inputNow, bool lastOutputState, T_Timer* pTimer, uint32_t requiredTime_ms);


/**
 * Determine if given string is valid number. First character of '+' or '-'
 * is allowed.
 * @param str
 * @return TRUE if valid number
 */
bool UTIL_IsNumber(const char* str);


/**
 * Print the contents of an array in hexadecimal.
 * @param pData First location of array
 * @param N Number of bytes to print
 */
void UTIL_PrintArray(const uint8_t* pData, uint16_t N);


/**
 * Print configuration info from a T_ConfigInfo struct to CLI.
 * @param label Name of what configuration is for
 * @param pConfigInfo Pointer to configuration struct
 */
 void UTIL_PrintConfigInfo(const char* label, const T_ConfigInfo* pConfigInfo);

  /**
  * Maps a number (x) from one space (x1,x2) to new space (y1,y2). 
  * Note that input (x) does NOT necessarily need to be within range x1-->x2, 
  * nor is output (y) constrained to be within range y1-->y2.
  * 
  * Range do not have to be in "direction" - so an increasing x can be mapped to 
  * a decreasing y. 
  * 
  * Alternative interpretation:
  * Given two calibration points (x1,y1) & (x2,y2), map the input x to output y.
  * 
  *        /  y2 - y1 \
  * y =   |------------ |*(x - x1) + y1
  *        \  x2 - x1  /
  * 
  * Note that x1 must not equal x2! Otherwise everything blows up.
  * 
  * @param x Input to map, in x-space
  * @param x1 Boundary 1 of x-space (AKA x-value of Point 1)
  * @param x2 Boundary 2 of x-space (AKA x-value of Point 2)
  * @param y1 Boundary 1 of y-space (AKA y-value of Point 1)
  * @param y2 Boundary 2 of y-space (AKA y-value of Point 2)
  * @return Output mapped to y-space
  */
 int32_t UTIL_MapNumber_I32(int32_t x, int32_t x1, int32_t x2, int32_t y1, int32_t y2);
 
 
#endif
