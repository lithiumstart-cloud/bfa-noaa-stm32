/**
 * @file mcu_hw.h
 * @brief MCU specific misc. functions Interface
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-01-20
 *
 * This is a platform agnostic interface for misc. hardware level functions. Any
 * driver that implements this interface must use non-blocking implementations.
**/

#ifndef MCU_HW_H
#define MCU_HW_H

#include <stdint.h>
#include <stdbool.h>

typedef enum
{
    RST_POWER_ON = 0,
    RST_CONFIG_MISMATCH,
    RST_SOFTWARE,
    RST_BROWN_OUT,
    RST_MCLR,
    RST_WDT,
    RST_STACK_OVERFLOW,
    RST_STACK_UNDERFLOW,
    RST_INT_EXIT_MANAGED_PWR,
    RST_IDLE,
    RST_SLEEP,
    RST_VREGS,
    RST_UNKNOWN,

	// STM32 specific reset types
	RST_FIREWALL,
	RST_OPTION_BYTE_LOADER,
	RST_WINDOW_WD,
	RST_LOW_POWER,
} T_MCU_ResetType;

/**
 * This function should restart the microcontroller.
 */
void MCU_HW_Restart(void);

/**
 * This function should perform any hardware intiazlation necessary for system
 * to work properly.
 * @param sysClock_Hz
 */
void MCU_HW_Configure(uint32_t sysClock_Hz);


/**
 * This function should disable system interrupts.
 */
void MCU_HW_DisableInterrupts(void);

/**
 * This function should enable system interrupts.
 */
void MCU_HW_EnableInterrupts(void);

/**
 * 
 */
T_MCU_ResetType MCU_HW_GetLastReset(void);

/**
 * 
 */
void MCU_HW_ClearWDT(void);

/**
 * 
 */
void MCU_HW_EnableWDT(void);

/**
 * 
 */
void MCU_HW_DisableWDT(void);

/**
 * Get the length of the WDT period.
 * Note that this function MUST return a nonzero value, even if WDT is not used.
 * Otherwise, SCH will try to set a timer for 0ms and throw an error.
 * @return WDT period, ms
 */
uint32_t MCU_HW_GetWdtPeriod_ms(void);

/**
 * Check if a WDT expiration has occured. Useful for checking what reason for 
 * wakeup from sleep was. If the flag is set, clear the flag.
 * @return Has WDT expired
 */
bool MCU_HW_HasWdtExpired(void);

/**
 * 
 */
void MCU_HW_SleepNow(void);
    
    

#endif
