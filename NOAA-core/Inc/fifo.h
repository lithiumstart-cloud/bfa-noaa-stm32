/**
 * @file  fifo.h
 * @brief  Static FIFO Buffers
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-07-03
 *
 * A FIFO (First-In-First-Out) circular buffer implementation that does not dynamically allocate
 * any memory. This module uses VOID* to pass around pointers to typeless elements, so care should 
 * be taken to ensure that types/size match and proper space is allocated.
 */

#ifndef __FIFO_H
#define __FIFO_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    uint32_t pushInx;
    uint32_t popInx;
    uint32_t numFilled;
    uint32_t maxLength;
    void* bufferStart;
    uint8_t elementSize;
} T_FIFO;

/*
 * Intialize a FIFO by connecting to a independentally allocated memory buffer.
 */
bool FIFO_Init(T_FIFO* pFifo, void* buffer, uint32_t _maxLength, uint8_t _elementSize);

/*
 * Copy element from sour ce location and push onto end of FIFO.
 * Return TRUE if successul, FALSE if failure (e.g. buffer full)
 */
bool FIFO_EnqueueFrom(T_FIFO* pFifo, const void* pSrc);

/*
 * Copy element from front of FIFO into given destination.
 * Returns TRUE is successful, FALSE if failure (e.g. buffer empty)
 */
bool FIFO_DequeueInto(T_FIFO* pFifo, void* pDst);




#endif /* defined(__QueueTest__fifo__) */
