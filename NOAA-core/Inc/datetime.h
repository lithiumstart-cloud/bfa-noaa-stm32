/**
 * @file datetime.h
 * @brief  Date/Time Utility Module
 * @copyright 2016 Lithiumstart Inc.
 * @author A. Kessler
 * @date   2016-03-28
 **/

#ifndef DATETIME_H
#define	DATETIME_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/*
 * An idealized naive date, assuming the current Gregorian calendar always was, 
 * and always will be, in effect. 
 */
typedef struct
{
    uint16_t year; ///< Year [2000,2100]
    uint8_t month; ///< Month [1,12]
    uint8_t day; ///< Day of the Month [1,31]
} T_Date;

/**
 * An idealized time, independent of any particular day, assuming that every day
 * has exactly 24*60*60 seconds (there is no notion of "leap seconds" here). 
 */
typedef struct
{
    uint8_t sec; ///< [0,59]
    uint8_t min;///< [0,59]
    uint8_t hour; //< [0,23]
} T_Time;


/*
 * A combination of a date and a time. 
 */
typedef struct
{
    T_Date date;
    T_Time time;
} T_Datetime;


/*
 * A duration expressing the difference between two date, time, or datetime
 * instances.
 */
typedef struct
{
    uint8_t secs; ///< 0,59
    uint8_t mins; ///< 0,59
    uint8_t hours; ///< 0,23
    uint16_t days; ///< 0,65k = 179 years.
} T_TimeDelta;

/**
 * Fill a string with a Date object. In format "yyyy-mm-dd".
 * @param str
 * @param len
 * @param date
 */
bool DT_Date2String(char* str, size_t len, const T_Date* date);

/**
 * Fill a string with a Time object. In format "hh:mm:ss".
 * @param str
 * @param len
 * @param time
 */
bool DT_Time2String(char* str, size_t len, const T_Time* time);

/**
 *  Fill a string with a Datetime object. In format "yyyy-mm-ddThh:mm:ssZ"
 * @param str
 * @param len
 * @param datetime
 */
bool DT_Datetime2String(char* str, size_t len, const T_Datetime* datetime);

/**
 * Print a Time object to CLI.
 * @param time
 */
void DT_PrintTime(const T_Time* time);

/**
 * Print a Date object to CLI.
 * @param date
 */
void DT_PrintDate(const T_Date* date);

/**
 * Print a Datetime object to CLI.
 * @param dt
 */
void DT_PrintDatetime(const T_Datetime* dt);

/**
 * Check if contents of time object are valid.
 * @param time
 * @return 
 */
bool DT_ValidateTime(const T_Time* time);

/**
 * Check if contents of Date object are valid.
 * @param date
 * @return 
 */
bool DT_ValidateDate(const T_Date* date);
#endif
