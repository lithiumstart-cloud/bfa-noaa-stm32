/**
 * @file led.h
 * @brief Manages the board LEDs
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-06
 *
**/

#ifndef LED_H
#define LED_H

#include <stdint.h>
#include <stdbool.h>

#include "gpio.h"

/*Forward declaration of struct so we can reference pointers in other files.*/
struct sLed;
typedef struct sLed T_LED;


/// These are the availble LED behaivor modes
typedef enum
{
   LMODE_OFF = 0, ///< LED is always off
   LMODE_SOLID_ON, ///< LED is continuously on

   LMODE_BLINK_SLOW, ///< LED blinks slowly
   LMODE_BLINK_FAST, ///< LED blinks quickly
   LMODE_BLINK_ULTRA_FAST, ///< Ultra fast blink

   LMODE_BLINK_DOUBLE, ///< LED blinks in a dot-dot pattern
   LMODE_BLINK_TRIPLE, ///< LED blinks in a dot-dot-dot pattern
   LMODE_BLINK_QUADRUPLE, ///< LED blinks in a dot-dot-dot-dot pattern
   
   LMODE_BLIP_SLOW, ///< Infrequent blip - min possible on/off ratio (1/64)
   LMODE_BLIP, ///< LED blinks a quick "dot"
   LMODE_BLIP_FAST, ///< A more frequent "dot"
   LMODE_BLIP_ULTRA_FAST, ///< An even more frequent "dot"

   LMODE_WINK, ///< Mostly on, with short "dot" OFF
   LMODE_WINK_FAST, ///< Mostly on, with short "dot" OFF
   LMODE_WINK_SLOWER, ///< Mostly on, with "dash" OFF
   
   LMODE_CONNIPTION,

   LMODE_NUMBER_MODES ///< The number of available LED modes

} T_LMode;


#define ACTIVE_LOW   1
#define ACTIVE_HIGH  0


/**
 * Create a new LED object on given hardware port/pin. Return pointer to obj.
 * @param name Name of LED
 * @param portLetter Hardware port ['A','G']
 * @param pin Hardware pin number [0,31]
 * @param isActiveLo Is the pin that has this LED active low? Default = false
 * @return Pointer to new LED obj. NULL if failed.
 */
T_LED* LED_Create(const char* name, T_Pin xPin, bool isActiveLo);


/**
 * Create a new LED object on given hardware port/pin struct. Return pointer to obj.
 * @param name Name of LED
 * @param pinObj
 * @return Pointer to new LED obj. NULL if failed.
 */
T_LED* LED_CreatePin(const char* name, T_Pin xPin);


/**
 * Update the blink pattern an LED object.
 * @param pLed Pointer to LED object to modify
 * @param mode (TLMode) desired new blink mode
 * @return Success
 */
bool LED_SetMode(T_LED* pLed, T_LMode mode);


/**
 * Override the blink pattern manager, and just set the state of the LED ASAP
 * @param pLed
 * @param isOn
 * @return
 */
bool LED_SetNow(T_LED* pLed, bool isOn);

/**
 * Intialzie the LED module.
 */
void LED_Init(void);

/**
 * Update the module with system level events.
 * @return OK to sleep?
 */
bool LED_Update(void);



/**
 * Override LED functionarlity and blink out firmware version pattern.
 */
void LED_DisplayVersion(uint32_t vInd);


/**
 * Process command line inputs for this module.
 * @param buf Two-dimensional array of CL inputs.
 * @param nArgs The number of tokens in the input.
 */
void LED_ProcessInput(const char* tokens[], uint8_t nTokens);


/**
 * Lock designated LEDs in an off state until unlocked.
 * Stash the current modes of the LEDs for later reference.
 */
void LED_StashAndLockOthers(T_LED*);

/**
 * Unlock all of the LEDs so other modules can control them.
 * Restore previously stashed states. 
 */
void LED_UnstashAndUnlockAll(void);

void LED_SetNormalBitPeriod_ms(uint32_t t_ms, bool announce);
void LED_SetVersionBitPeriod_ms(uint32_t t_ms, bool announce);

#endif

