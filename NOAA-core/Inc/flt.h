/**
 * @file flt.h
 * @brief Fault Manager
 * @copyright 2015 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2015-08-17
 *
**/

#ifndef FLT_H
#define	FLT_H

#include <stdint.h>
#include <stdbool.h>
#include "led.h"
#include "sig.h"
typedef struct Struct_Fault T_Fault;



typedef enum
{
    FLT_LVL_CLEAR = 0,
    FLT_LVL_MINIMAL,
    FLT_LVL_MODERATE,
    FLT_LVL_EXTREME,
} __attribute__ ((__packed__)) T_FaultLevel;

#define INHERIT_FAULT ((T_FaultLevel) 0)

typedef union
{
    uint8_t u8;
    struct
    {
      T_FaultLevel now : 2;
      T_FaultLevel sinceBoot : 2;
      T_FaultLevel worstEver : 2;
    };
} T_FaultBitmap;

typedef bool (*T_FaultChecker) (void);

typedef enum
{
    FLT_TYPE_MANUAL = 0,
    FLT_TYPE_CALLBACK,
    FLT_TYPE_SIG_AGE,
    
            
} __attribute__ ((__packed__)) T_FaultType;

//Class Functions
void FLT_Init(void);
bool FLT_Update(void);
bool FLT_ValidateTree(T_Fault* root);
void FLT_AssignMasterFaultLed(T_LED* pLed);

void FLT_ResetAllHistory(void);
void FLT_ProcessInput(const char* tokens[], uint8_t nArgs);

void FLT_PrepareForShutdown(void);
const char* FLT_Level2Str(T_FaultLevel lvl);


//Object Methods
T_Fault* FLT_CreateEnclosureFault(const char* name);
T_Fault* FLT_CreateManualFault(const char* name,  T_FaultLevel lvl);
T_Fault* FLT_CreateCallbackFault(const char* name,  T_FaultLevel lvl, T_FaultChecker fCheck);
T_Fault* FLT_CreateSignalAgeFault(const char* name, T_FaultLevel lvl, T_SignalNode* sig, uint32_t maxAge_ms);

bool FLT_Trigger(T_Fault* flt, bool set);


bool FLT_MakeChildOf(T_Fault* this, T_Fault* parent);
T_FaultBitmap FLT_GetBitmap(const T_Fault* this);
const char* FLT_GetName(const T_Fault* this);
void FLT_ChangeLevel(T_Fault* flt, T_FaultLevel lvl);

bool FLT_ChangeSignalAgeFaultTimeout(T_Fault* flt, uint32_t newTime_ms);
uint32_t FLT_GetSignalAgeFaultTimeout_ms(T_Fault* flt);

#endif	/* FLT_H */

