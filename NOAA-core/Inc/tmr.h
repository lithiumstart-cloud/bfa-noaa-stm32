/**
 * @file  TMR.h
 * @brief  Software Timer Handler
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-06-26
 *
 * 
 * 
 * The Timer (TMR) module provides an interface for two types of software 
 * timers: "Timers" and "Stopwatches".
 * 
 *     "Timers" are designed to be used when the time of when an event is to
 * occur is known at the moment when the Timer is started- think starting a 
 * kitchen timer, where you know you want to cook something for exactly two 
 * minutes. They can be thought of "counting down" from a preset period until 
 * zero, when  the Timer "trips". Timers can be set as "one-shot" or
 * "repeating". A one-shot timer will stop running once it has tripped, and will 
 * not generate any future events without being explicity restarted. A repeating 
 * timer will automatically  reset its trigger time after it trips, to enable 
 * future events at the same period.
 *     Note that a Timer is only checked for expiration when the 
 * TMR_HasTripped() function is called. Therefore, once a timer has started, it 
 * should be checked at intervals much more frequently than the Timer period.
 * This function will throw an error if the timer has been sitting in a tripped
 * state for a length of time greater than the timer's period.
 *  A one-shot timer that has been started will return TRUE from TMR_IsRunning()
 * (regardless of elapsed time) until TMR_HasTripped() is called AND the
 * designated time has elapsed. After that, TMR_IsRunning() will return false.
 *  A repeating timer, once started, will return TRUE from TMR_IsRunning() until
 * it is explicity stopped using TMR_Stop().
 * 
 * 
 *    "Stopwatches" are designed to be used when the user wants to measure
 * how long an external event will take. Stopwatches can be thought of "counting 
 * up" from zero. Like an actual stopwatch, Stopwatch objects can be "split", 
 * where they return the elapsed time and start counting again from zero.
 * 
 * 
**/

#ifndef TMR_H
#define	TMR_H

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    bool isRunning;
    uint32_t startTime_ticks;
    
} T_Stopwatch;
    


typedef struct
{
    struct
    {
      unsigned isRunning: 1;
      unsigned isRepeating: 1;
    };
     
    uint32_t startTime_ticks;
    uint32_t timerLength_ticks;
    
} T_Timer;

/**
 * Intialized the free running counter which drives software timers.
 */
void TMR_Init(void);


/**
 * Resest the given stopwatch to start at current time.
 * @param Stopwatch to start.
 */
void TMR_Stopwatch_Start(T_Stopwatch*);

/**
 * Check how much time has elapsed since this Stopwatch was last started. 
 * @param Stopwatch to check
 * @return Elapsed time, milliseconds.
 */
uint32_t TMR_Stopwatch_GetElapsed_ms(const T_Stopwatch*);

/**
 * Check how much time has elapsed since Stopwatch start, and restart Stopwatch.
 * @param Stopwatch to split
 * @return Elapsed time since last start
 */
uint32_t TMR_Stopwatch_Split_ms(T_Stopwatch*);


/**
 * Start a one-shot timer with the given period. Once timer triggers, it will 
 * stop running.
 * @param Timer to start
 * @param time_ms Period of timer, in milliseconds
 */
void TMR_StartOneshot(T_Timer*, uint32_t time_ms);

/**
 * Start a repeating timer with the given period. Once the timer triggers, it
 * will continue to run. An error is thrown is a timer is not checked after it
 * has triggered twice.
 * @param Timer to check
 * @param time_ms Timer period, milliseconds
 */
void TMR_StartRepeating(T_Timer*, uint32_t time_ms);


/**
 * Check if a timer has tripped since the last time this function was called.
 * @param Timer to check
 * @return Elapsed time since time has tripped, in milliseconds 
 */
uint32_t TMR_HasTripped_ms(T_Timer*);


/**
 * Retroactively adjust the start time of a timer (without adjusting the period)
 * to shorten elapsed time until timer triggers. This is useful for creating an 
 * offset amongst several timers with the same period that are initialized at 
 * the same time.
 * 
 * @param this Timer to stagger
 * @param stagger_ms Amount by which to stagger timer
 */
void TMR_SetStagger(T_Timer* this, uint32_t stagger_ms);

/**
 * Stop a timer from running.
 * @param Timer to stop
 */
void TMR_Stop(T_Timer*);


/**
 * Check if a timer is running.
 * @param Timer to check
 * @return Is timer running?
 */
bool TMR_IsRunning(const T_Timer*);

/**
 * Get the value of the free running clock.
 * @return Free running clock value (ticks)
 */
uint32_t TMR_GetClock_ticks(void);

/**
 * Jump the free running clock forward a certain amount of time. Use only for
 * coming out of sleep and accounting for elapsed time. 
 * @param Amount to jump forward (ms)
 */
void TMR_FastForwardFreeRunningClock_ms(uint32_t);





#endif	/* TMR_H */

