/**
 * @file str_lib.h
 * @brief One place to keep strings together, to avoid duplication.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-09
 *
**/

#ifndef STR_LIB_H
#define STR_LIB_H

    #include <string.h>

    #define ASCII_DEG   248
    #define ASCII_ESC   27


    extern const char STR_NewLine[];
    extern const char STR_dC[];
    extern const char STR_CommandPrompt[];
    extern const char STR_cd[];
    extern const char STR_EscOpen[];
    extern const char STR_NewLineTab[];

    const char STR_CurHome[];


    //Literals
    extern const char STR_full[];
    extern const char STR_bms[];
    extern const char STR_daq[];
    extern const char STR_soc[];
    extern const char STR_bal[];
    extern const char STR_cell[];
    extern const char STR_temp[];
    extern const char STR_stat[];
    extern const char STR_pack[];
    extern const char STR_relay[];
    extern const char STR_led[];
    extern const char STR_open[];
    extern const char STR_close[];
    extern const char STR_auto[];
    extern const char STR_ovrd[];
    extern const char STR_alarm[];
    extern const char STR_mV[];
    extern const char STR_mA[];
    extern const char STR_sim[];

    extern const char STR_volts[];
    extern const char STR_amps[];
    extern const char STR_stick[];
    extern const char STR_unstick[];
    extern const char STR_board[];
    extern const char STR_amb[];
    extern const char STR_name[];
    extern const char STR_level[];
    extern const char STR_stuck[];
    extern const char STR_ave[];
    extern const char STR_now[];
    extern const char STR_lpf[];
    extern const char STR_snap[];
    extern const char STR_restart[];
    extern const char STR_count[];
    extern const char STR_clear[];
    extern const char STR_error[];
    extern const char STR_mode[];
    extern const char STR_sbs[];
    extern const char STR_status[];
    extern const char STR_time[];
    extern const char STR_show[];
    extern const char STR_log[];
    extern const char STR_send[];
    extern const char STR_reset[];
    extern const char STR_smb[];
    extern const char STR_ccu[];
    extern const char STR_acc[];
    extern const char STR_empty[];
    extern const char STR_sys[];
    extern const char STR_cli[];
    extern const char STR_uart[];
    extern const char STR_can[];
    extern const char STR_a2d[];
    extern const char STR_sig[];
    extern const char STR_full[];
    extern const char STR_afe[];

#ifndef stricmp

#define stricmp(...)    strcasecmp(__VA_ARGS__)

#endif

#endif
