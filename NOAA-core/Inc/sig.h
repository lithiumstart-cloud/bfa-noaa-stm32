/**
 * @file sig.h
 * @brief The Signal management module abstracts application access
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-09-11
 *
 * Note that the Signal manager does not directly take any analog inputs. All
 * actual data is reported by *other* modules, such as a local A2D reader or
 * CdBus handler. This is meant to repalce the gStructCentral, etc. in V2.
**/

#ifndef SIG_H
#define SIG_H

#include <stdint.h>
#include <stdbool.h>

#include "sig_types.h"
#include "alarm.h"
#include "led.h"

/*Declare incomplete type for pointer use.*/
struct xSignalNode;
typedef struct xSignalNode T_SignalNode;

#define ANONYMOUS (NULL)

typedef struct
{
    T_SignalNode* nRoot;
    T_SignalNode* nVltgRoot;
    T_SignalNode* nTempRoot;
    T_SignalNode* nCurrRoot;
    T_SignalNode* nSoc;
} T_CoreSignals;



/**
 * Initialize the Signal module.
 */
void SIG_Init(void);


/**
 * "Run" the SIG. Call in main loop.
 * @return Did the state machine do "something"?
 */
bool SIG_Update(void);

/**
 *
 * @param
 * @param
 * @return
 */
bool SIG_WriteSignal(T_SignalNode*,  T_SignalDatum);

/**
 * 
 * @param parent
 * @param data
 * @param numData
 * @return 
 */
bool SIG_WriteToChildren(T_SignalNode* parent, const T_SignalDatum* data, uint8_t numData);

/**
 * There's a certain level of trust associated with call. Make sure conditions are met.
 * TODO: list preconditions
 * @param parent
 * @param data
 * @param numData
 * @return 
 */
bool SIG_WriteToChildren_U32(T_SignalNode* parent, const uint32_t* data, uint8_t numData);

/**
 *
 * @param name
 * @param type
 * @param pAlarmTemplate
 * @return
 */
T_SignalNode* SIG_CreateNode(const char* name, T_UnitId unitId, T_AlarmTemplate* pAlarmTemplate);


/**
 *
 * @param node
 * @param parent
 * @return
 */
bool SIG_MakeNodeChildOf(T_SignalNode* node, T_SignalNode* parent);

/**
 *
 * @param root
 * @return
 */
bool SIG_ValidateTree(T_SignalNode* root);


/**
 * Initalize fullscreen DAQ mode.
 */
void SIG_DrawInit(void);

 /**
  * Update fullscreen DAQ mode.
  */
void SIG_DrawUpdate(void);

/**
 * Print the given data with given type, indpendent of any particular signal.
 * @param sigDat
 * @param type
 */
void SIG_PrintSignalDatum(T_SignalDatum sigDat, T_TypeId type);

/**
 * 
 * @return 
 */
bool SIG_IsReady(void);

bool SIG_IsSignalReady(const T_SignalNode* this);

bool SIG_IsTreeStable(void);
/**
 * Set/clear alarm suspended state
 * @param pNode Signal with alarm to modify
 * @param set Whether or not alarm is suspended
 */
void SIG_SetIsAlarmSuspended(T_SignalNode* pNode, bool set);

/**
 * Assign an LED that will update according to the state of the alarm on the
 * root node.
 * @param pLed
 */
void SIG_AssignMasterAlarmLed(T_LED* pLed);

 /**
  * CLI hook function.
  * @param tokens
  * @param nTokens
  */
 void SIG_ProcessInput(const char* tokens[], uint8_t nTokens);

/**
 * Save worst alarm ever states to NVRAM
 */
 void SIG_PrepareForShutdown(void);

 /**
  * Set the threshold at which the SIG module will print a warning if it takes 
  * more than this number of calls of SIG_Update to update the entire tree. 
  * This warning is intended to give developers an early warning of any latency
  * issues due to the signal tree being very large. 
  * @param New threshold
  */
 void SIG_SetMaxExpNumCallsToProcessTree(uint8_t);
/**
 *
 * @param t
 * @return
 */
const char* sigh_Unit2Str(T_UnitId t);

/**
 *
 * @param t
 * @return
 */
const char* sigh_Type2Str(T_TypeId t);

/**
 * 
 * @param uid
 * @return 
 */
T_TypeId sigh_Unit2Type(T_UnitId uid);

/**
 *
 * @param t
 * @return
 */
uint8_t sigh_Type2CharLength(T_TypeId t);

/**
 * Retrieve the name of the given signal.
 * @param pNode 
 * @return 
 */
const char* SIG_GetName(const T_SignalNode* pNode);

/**
 * Get a given data field from the given signal as T_SignalDatum
 * @param pNode 
 * @param rqst Type of data request
 * @return Requested data as T_SignalDatum
 */
T_SignalDatum SIG_GetData(const T_SignalNode* pNode, T_SigRequest rqst);

/**
 * Get the requested data field from given signal as uint32_t
 * @param pNode
 * @param rqst Type of data request
 * @return Requested data as uint32_t
 */
uint32_t SIG_GetData_U32(const T_SignalNode* pNode, T_SigRequest rqst);

/**
 * Get the requested data field from given signal as int32_t
 * @param pNode
 * @param rqst Type of data request
 * @return Requested data as int32_t
 */
int32_t SIG_GetData_I32(const T_SignalNode* pNode, T_SigRequest rqst);

/**
 * Print the requested data appropriately given its type/units.
 * @param pNode
 * @param rqst Type of data request
 */
void SIG_PrintData(const T_SignalNode* pNode, T_SigRequest rqst);

/**
 * Get the average of the given signal as a uint32_t
 * @param pNode
 * @return Signal average as unsigned int
 */
uint32_t SIG_GetAvg_U32(const T_SignalNode* pNode);

/**
 * Get the first child of the given signal, if any.
 * @param pNode Parent node
 * @return First child of given node. NULL if none.
 */
T_SignalNode* SIG_GetFirstChild(const T_SignalNode* pNode);

/**
 * Get the next sibling node of given signal, if any.
 * @param pNode
 * @return Next sibling node, if any. NULL if none. 
 */
T_SignalNode* SIG_GetNextSibling(const T_SignalNode* pNode);

/**
 * Get a node's parent (if it exists).
 * @param pNode
 * @return Pointer to parent node if it has one. NULL if not/
 */
T_SignalNode* SIG_GetParent(const T_SignalNode* pNode);

/**
 * Get the type of the given signal.
 * @param pNode
 * @return Signal type
 */
T_TypeId SIG_GetType(const T_SignalNode* pNode);

/**
 * Get the alarm struct of the given signal.
 * @param pNode
 * @return Alarm struct of given signal
 */
T_AlarmBitmap SIG_GetAlarm(const T_SignalNode* pNode);

/**
 * Get the instantaneous alarm state of given signal 
 * @param pNode
 * @return Instantaneous alarm state
 */
T_AlarmLevel SIG_GetAlarmNow(const T_SignalNode* pNode);

/**
 * Check if inst. alarm state has changed since last time flag has been cleared.
 * @param pNode
 * @return 
 */
bool SIG_HasAlarmChanged(const T_SignalNode* pNode);

/**
 * Clear the has alarm changed flag for this signal.
 * @param node
 */
void SIG_ClearAlarmChangeFlag(const T_SignalNode* node);

/**
 * Reset the SinceBoot and WorstEver alarm states (to the inst. Alarm state).
 */
void SIG_ResetLatchedAlarms(void);

/**
 * Get the number of children the given signal node has.
 * @param pNode
 * @return 
 */
uint32_t SIG_GetNumChildren(const T_SignalNode* pNode);

/**
 * Returns elapsed time since signal was last written to. Age is in units of
 * treeUpdatePeriod_ms (see SIG_GetTreeUpdatePeriod_ms)
 * @param pNode
 * @return Signal age in update periods
 */
uint32_t SIG_GetAge(const T_SignalNode* pNode);

/**
 * Get a signal's alarm template, if it has one.
 * @param pNode
 * @return 
 */
T_AlarmTemplate* SIG_GetAlarmTemplate(const T_SignalNode* pNode);

/**
 * If a node has a sibling, return it. If not, attempt to get next cousin and
 * return it. Returns NULL if no more siblings or cousins.
 * @param pNode
 * @return Next node in the same "generation" as the given node.
 */
T_SignalNode* SIG_GetNextInGeneration(const T_SignalNode* pNode);

/**
 * Get the first child of a node's parent's next sibling. 
 * Or, in other words,
 *  - get node's parent
 *  - get parent's next sibling ("Uncle")
*   - return Uncle's first child
 * @param pNode
 * @return The first child of a node's parent's next sibling. NULL if none.
 */
T_SignalNode* SIG_GetCousin(const T_SignalNode* pNode);

/**
 * Print any uninitalized signals to the CLI. 
 * 
 */
void SIG_ListUninitialzed(void);

/**
 * Get the ith child of given node. Throws error if i > numChildren, and
 * returns NULL.
 * @param pNode
 * @param inx Index of child. (First child is 0).
 * @return ith child of given signal
 */
T_SignalNode* SIG_GetChild(const T_SignalNode* pNode, uint32_t inx);

/**
 * Get the index of this node's child with the lowest value.
 * @param pNode
 * @return Index of child
 */
uint8_t SIG_GetMinChildInx(const T_SignalNode* pNode);

/**
 * Get the index of this node's child with the highest value.
 * @param pNode
 * @return Index of child
 */
uint8_t SIG_GetMaxChildInx(const T_SignalNode* pNode);

/**
 * Change the update period of the signal tree. Must be >0ms!
 * @param period_ms New tree update period time
 * @param announce Should notice of change be printed to CLI?
 */
void SIG_SetTreeUpdatePeriod(uint32_t period_ms, bool announce);

/**
 * Get the update period of the signal tree. One update period is one "age" unit
 * of a signal.
 * @return Update period, ms
 */
uint32_t SIG_GetTreeUpdatePeriod_ms(void);

/**
 * Prints name of node by walking up tree until it finds a parent with
 * local name, then unwinds and prints successve children numbers.
 * @param node
 */
void SIG_PrintName(const T_SignalNode* node);


/**
 * Fast node name print - if node is anonymous- prints pointer.
 * @param node
 */
void SIG_PrintQuickName(const T_SignalNode* node);

/**
 * Set/clear signal bypass flag.
 * @param pNode Signal whose bypass flag is to be modified.
 * @param set Whether or not signal should be bypassed.
 */
void SIG_SetSignalBypassFlag(T_SignalNode* pNode, bool set);
#endif

