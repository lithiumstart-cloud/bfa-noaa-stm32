/**
 * @file sch.h
 * @brief Implements task scheduler
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-07-24
 *
**/


#ifndef SCH_H
#define	SCH_H

#include <stdint.h>
#include <stdbool.h>
#include "gpio.h"

typedef bool (*T_RunFunc) (void);

#define SCH_CREATE_TASK(NAME) SCH_CreateTask(#NAME, NAME ## _Update)

typedef enum
{
    SCH_WAS_NOT_ASLEEP = 0,
    SCH_AWOKEN_BY_WDT,
    SCH_AWOKEN_BY_PERIPHERAL,
    SCH_SLEEP_PERIOD_ELAPSED_WHILE_AWAKE,
            
}T_SCH_WakeupReason;

/**
 * Initialize the scheduler module. Call once at startup, before creating tasks.
 */
void SCH_Init(void);

/**
 * Create a new task for the scheduler to run.
 * @param name The name of task, for error reporting/logging.
 * @param runFunc The Update function of the task to be called.
 * @return Whether or not task creation was successful (1 = OK, 0= failed)
 */
uint8_t SCH_CreateTask(const char* name,  T_RunFunc pRunFunc);

/**
 * Call each of the Update functions the scheduler has. Call inside
 * while(1) loop to run program.
 * @return Whether or not error occured (0 = no error, 1 = error)
 */
uint8_t SCH_Run(void);

/**
 * Assign a pin to toggle every iteration through the main loop. Useful for 
 * analyzing main loop latency externally, and for kicking an external WDT.
 * @param port
 * @param pin
 * @return Was successful
 */
bool SCH_AssignHeartbeatPin(T_Pin pin);

uint32_t SCH_GetLastModuleBeforeReset(void);


void SCH_ProcessInput(const char* tokens[], uint8_t nTokens);


T_SCH_WakeupReason SCH_DidProcessorJustWakeup(void);
void SCH_SetSleepEnable(bool _s);

/**
 * Change the maximum allowed task delay parameter with optional print to CLI
 * announcing change.
 * @param t_ms New maximum allowed task delay time, in ms
 * @param announce Should notice of change be print to CLI
 */
void SCH_SetMaxAllowedTaskDelay(uint32_t t_ms, bool announce);

//We define this externally so it can be overriden if needed later. By default,
//it is set to MCU_HW_SleepNow().
extern void (*SCH_SleepNow)(void);

#endif	/* SCH_H */

