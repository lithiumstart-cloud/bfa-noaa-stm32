/**
 * @file log.h
 * @brief Log errors, messages, and warnings reported by other modules.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-26
 *
**/


#ifndef LOG_H
#define LOG_H

#include <stdint.h>
#include <stdbool.h>

/// Module Reference Codes
typedef enum
{
    LOG_MOD_MAIN, ///< main.c
    LOG_MOD_ALARM, ///< Alarm controller
    LOG_MOD_BAL, ///< Balancing controller
    LOG_MOD_BMS, ///< BMS
    LOG_MOD_SOC, ///< SoC manager
    LOG_MOD_SIG, ///< Signal Manager (SIG)
    LOG_MOD_LED, ///< LED controller
    LOG_MOD_RLY, ///< Low level Relay controller
    LOG_MOD_SBS, ///< Smart Battery Specification
    LOG_MOD_SMB, ///< SMBus
    LOG_MOD_SYS, ///< System
    LOG_MOD_TMR, ///< Timer manager
    LOG_MOD_UART, ///< UART module
    LOG_MOD_UTIL, ///< Utilities
    LOG_MOD_LOG, ///< Logging Manager
    LOG_MOD_CLI, ///<Command Line Interface
    LOG_MOD_SCH, ///<Scheduler
    LOG_MOD_ACC, ///< Accumulator
    LOG_MOD_A2D, ///< A2D driver
    LOG_MOD_AFE, ///< AFE (Analog Front End)
    LOG_MOD_RSC, ///< RSC (Relay Sequence Controller)
    LOG_MOD_COM, ///< Generic Communication Module
    LOG_MOD_CAN, ///< CAN Controller

    LOG_MOD_NUM_MODS ///< Total number of module codes
} T_ModRef;

/// Message Reference Codes
typedef enum
{
    LOG_MSG_INDEX_OUT_OF_RANGE, ///< Index out of range (arrays, etc.)
    LOG_MSG_DIVIDE_BY_ZERO, ///< Attempt to divide by zero
    LOG_MSG_NULL_PTR_REF, ///< Attempt to dereference null pointer
    LOG_MSG_EARLY_NACK, ///< No ACK rx'd on communication bus
    LOG_MSG_TIMEOUT, ///< Timeout occured
    LOG_MSG_MESSAGE_TOO_LONG, ///< Incoming message was too long
    LOG_MSG_NO_REPLY, ///< No reply rx'd on communicatio bus
    LOG_MSG_UNHANDLED_STATE, ///< State not handeled in state machine
    LOG_MSG_NOT_SUPPORTED, ///< Functionality not supported
    LOG_MSG_EXTRA_ACK, ///< An unexpected acknowledge was recieved
    LOG_MSG_COMM_COLLISION, ///< collision on communication bus
    LOG_MSG_QUEUE_OVERFLOW, ///< Queue overflow
    LOG_MSG_TICK, ///< Basic "tick" event
    LOG_MSG_CALC_OVERFLOW, ///< Overflow occured in calculation
    LOG_MSG_CALC_UNDERFLOW, ///< Underflow occured in calculation
    LOG_MSG_REJECTED_BAD_DATA, ///< The Daq module rejected bad data
    LOG_MSG_EARLY_STOP, ///< A process stopped earlier than expected
    LOG_MSG_RUNNING_SLOW, ///< A process is not getting called fast enough!
    LOG_MSG_ASSERTION_FAILED, ///< An assertion has failed
    LOG_MSG_FRAMING_ERROR, ///<A framing error occured
    LOG_MSG_UNINITIALIZED, ///< Attempt to use uninitialized element
    LOG_MSG_SIZE_MISMATCH, ///< Given length does not match expected length
    LOG_MSG_SENSOR_DISCONNECTED, ///< A sensor is not responding as expected, assume not connected
    LOG_MSG_SENSOR_FAIL, ///< A sensor is connected, but not acting like it should
    LOG_MSG_SENSOR_BAD_DATA, ///< The sensor returned data consistent with error checking protocols, but the data itself was useless
    LOG_MSG_RLY_STUCK_OPENED, ///< A relay has been commanded to close, but sense line indicates it is open
    LOG_MSG_RLY_STUCK_CLOSED, ///< A relay has been commanded to open, but sense line indicates it is closed
    LOG_MSG_RLY_PRECHARGE_FAIL, ///< Precharging failed for some reason

    LOG_MSG_NUM_ERRORS ///< Total numuber of message codes
} T_MsgRef;

/// Logging levels of "severity"
//Adapted from confusing RFC 5424 Wikipedia summary (syslog).
typedef enum
{
    LOG_LVL_DBG = 0,///< Debug - Info useful to developers for debugging the application, not useful during operations.
    LOG_LVL_INFO,   ///< Message - Normal operational messages - may be harvested for reporting, measuring throughput, etc. - no action required.
    LOG_LVL_WARN,   ///< Warn - Event that is an exception to normal operation, but is unlikely compromise overall system stabililty, even if ignored.
    LOG_LVL_ERROR,  ///< Error - Event that is an exception to normal operation, but is expected to be a one-time event.
    LOG_LVL_FATAL,  ///< Fatal - Event that is an exception to normal operation, and HAS compromised system stability. E.g. hardware unresponsive

    LOG_LVL_NUM_LEVELS ///< Number of logging levels
} T_Level;

typedef uint16_t T_Param;

#define LOG_BUFFER_LENGTH (128)
extern char logBuffer[];

/**
 * Initialize the logging module.
 */
void LOG_Init(void);


void LOG_SetUseRealtime(bool x);

/**
 * Report a log event.
 * @param Module reference code
 * @param Severity level reference code
 * @param Message type reference code
 * @param User parameter
 */
void LOG_Report(T_ModRef, T_Level, T_MsgRef, T_Param);

/**
 * Process command line inputs for this module.
 * @param buf Two-dimensional array of CL inputs.
 * @param nArgs The number of tokens in the input.
 */
void LOG_ProcessInput(const char* tokens[], uint8_t nTokens);

void LOG_Event(T_Level lvl, const char* str);

#if 0
void LOG_Eventf(T_Level lvl, const char* str, ...);
#endif

#ifdef CONFIG_ENABLE_ASSERTIONS
#define LOG_ASSERT(mod, test)  if (!(test)) { LOG_Report(mod, LOG_LVL_FATAL, LOG_MSG_ASSERTION_FAILED, __LINE__);}
#else
#define LOG_ASSERT(mod, test)
#endif










#endif
