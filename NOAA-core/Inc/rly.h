/**
 * @file rly.h
 * @brief Relay Object Module.
 * @copyright 2018 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2018-06-01
 *
 * 
 * The RELAY module is an Object Oriented interface for abstracting control of
 * mechanical/solid state relay/contactors. Any client code can "construct" a 
 * Relay object, then request open/close actions on it and check its state. 
 * Constructors will handle GPIO initialization of any drive/sense/active pins 
 * as necessary.
 * 
 * There are 3 different types of Relay objects that can be made:
 * 
 * 
 *  SIMPLE SWITCH - this type of object can control a relay/contactor with 
 *  a Drive pin, but no Sense. Because it has no feedback, it essentially a 
 *  GPIO, but wrapped in a RELAY object which allows it to be interfaced with
 *  other modules that are expecting a RELAY object.
 *  @ see RLY_CreateSimpleSwitch()
 * 
 *             Close                      
 *            Request                     
 *   OPENED ----------------> CLOSING --------------> CLOSED
 *           /assert                                               
 *            Drive                      
 *               
 *                  SIMPLE SWITCH Closing State Diagram
 * 
 *  ############################################################################
 *  
 *  MECHANICAL CONTACTOR - this object type is designed to control a 
 *  mechanical contactor with a Drive/Sense pins. It is assumed that the Sense
 *  pin will not be asserted until the Contactor is fully latched. On 
 *  construction, default max allowable open/closing times are pulled from 
 *  provided values in the T_RLY_Config struct. 
 *  @see RLY_CreateMechanicalContactor()
 *
 *             Close                      Sense Pin
 *            Request                     Asserted
 *   OPENED -----------> CLOSING ------------------------> CLOSED
 *           /assert       |                                        
 *            Drive        | Timeout             
 *                         |                    
 *                         |                    
 *                         +--> DISCONNECTED    
 * 
 *                MECHANICAL CONTACTOR Closing State Diagram
 * 
 *  ############################################################################
 * 
 *  INTEGRATED PRECHARGER - this object type is designed to control a relay
 *  that has integrated precharger - i.e. the Sense line will not be asserted 
 *  until the connection is fully precharged. Integrated Precharge objects must
 *  have Drive/Sense pins, but may also have an optional Active pin. The Active
 *  pin should indicate whether or not the relay is trying to close (i.e. is
 *  precharging active). This extra signal allows the the controller to 
 *  distinguish between a situation where there is an error with the relay, and
 *  when it is  just taking a very long time to precharge. 
 * 
 * If an Active pin is provided, on closing, the Relay object will transition
 * through a PRECHARGING state once Active is asserted, until Sense is asserted.
 * If the Active Pin is deasserted before Sense is asserted, this is interpreted
 * as a Precharge error. If the maxAllowedClosing time is reached before Sense
 * is asserted, this is considered a Precharge Timeout. 
 *     
 *          
 * 
 *                                              +------> PRECHARGE_FAILED
 *                                              | 
 *                                              | Active Pin
 *                                              | Deasserted
 *                                              |
 *             Close             Active Pin     |         Sense Pin
 *            Request             Asserted      |          Asserted
 *   OPENED -----------> CLOSING ---------> PRECHARGING -----------> CLOSED
 *           /assert       |                    |                      
 *            Drive        | Timeout            | Timeout
 *                         |                    |
 *                         |                    |
 *                         +--> DISCONNECTED    +---> PRECHARGE_TIMEOUT
 *
 *        INTEGRATED PRECHARGER WITH ACTIVE PIN Closing State Diagram                      
 * 
 * If an Integrated Precharger object is created without an Active Pin, then 
 * the object will transition straight through Closing, and wait in Precharging 
 * until the Sense Pin is asserted or a timeout occurs. 
 * 
 *    
 *                                            
 *             Close                                     Sense Pin
 *            Request                                    Asserted
 *   OPENED -----------> CLOSING ---------> PRECHARGING -----------> CLOSED
 *           /assert       |                    |                      
 *            Drive        | Timeout            | Timeout
 *                         |                    |
 *                         |                    |
 *                         +--> DISCONNECTED    +---> PRECHARGE_TIMEOUT
 *       
 *         INTEGRATED PRECHARGER WITHOUT ACTIVE PIN Closing State Diagram                      
 *        
 * 
**/

#ifndef RLY_H
#define RLY_H

#include <stdint.h>
#include <stdbool.h>

#include "gpio.h"
#include "tmr.h"

//################################ Module Types ################################

typedef struct sRelay T_Relay;

#define RLY_RQST_OPEN (false)
#define RLY_RQST_CLOSE (true)

typedef struct
{
    uint16_t mechContactorMaxCloseTime_ms; ///<Default max allowed closing time for mechanical contactors.
    uint16_t mechContactorMaxOpenTime_ms; ///<Default max allowed opening time for mechanical contactors.
    
    uint16_t prechargeMaxCloseTime_ms; ///<Default max allowed closing time for Integrated Prechargers. This includes actual precharge time!
    uint16_t prechargeMaxOpenTime_ms; ///<Default max allowed opening time for Integrated Prechargers
    
} T_RLY_Config;
//############################# Module Init/Update #############################

/**
 * Initialization function for module. Must be called before any other module 
 * functions (including constructors). Note that default timing parameters 
 * provided in config struct can be overridden on a per Relay basis. 
 * @see RLY_SetMaxClosingTime_ms()
 * @see RLY_SetMaxOpeningTime_ms()
 * 
 * @param pRlyConfig Configuration struct pointer with default timing parameters
 */
void RLY_Init(const T_RLY_Config* pRlyConfig);

/**
 * Module update function - should be called as fast as possible by scheduler.
 * @return Is ok to sleep?
 */
bool RLY_Update(void);


//######################### Object Constructors ################################

/**
 * Construct a MECHANICAL CONTACTOR Relay object with given Drive/Sense pins.
 * Timing parameters will be pulled from defaults provided in RLY_Init().
 * By default, the Drive pin logic is not inverted, and Sense pin logic is inverted. 
 * This can be changed using RLY_SetDrivePinLogicLevel() and RLY_SetSensePinLogicLevel().
 * 
 * @param name Name of Relay object (null terminated), copied into local namespace
 * @param pinDrive BluFlex Pin object for driving the contactor
 * @param pinSense BluFlex Pin object for sensing the state of the contactor
 * @return Pointer to created Relay object if successful, NULL if failed.
 */
T_Relay* RLY_CreateMechanicalContactor(const char* name, T_Pin pinDrive, T_Pin pinSense);

/**
 * Construct a SIMPLE SWITCH Relay object with given Drive pin.
 * Because a Simple Switch has no feedback, there are no applicable timing parameters.
 * By default, the Drive pin logic is not inverted.
 * This can be changed using @see RLY_SetDrivePinLogicLevel().
 * 
 * @param name Name of Relay object (null terminated), copied into local namespace
 * @param pinDrive BluFlex Pin object for driving the contactor
 * @return Pointer to created Relay object if successful, NULL if failed.
 */
T_Relay* RLY_CreateSimpleSwitch(const char* name, T_Pin pinDrive);

/**
 * Construct an INTEGRATED PRECHARGER Relay object with given Drive/Sense/Active pins.
 * Timing parameters will be pulled from defaults provided in RLY_Init().
 *
 * The Sense pin should indicate when the relay is done precharging. The Active 
 * pin should indicate when the Precharger is conducting  (i.e. trying to precharge).
 * 
 * By default the Drive pin logic is not inverted, Sense pin logic is inverted, 
 * Active pin logic is inverted. 
 * @see RLY_SetDrivePinLogicLevel() 
 * @see RLY_SetSensePinLogicLevel()
 * @see RLY_SetActivePinLogicLevel()
 * 
 * 
 * @param name Name of Relay object (null terminated), copied into local namespace
 * @param pinDrive BluFlex Pin object for driving the contactor
 * @param pinSense BluFlex Pin object for sensing the state of the contactor
 * @param pinActive BluFlex Pin object for reading IsActive state of Precharger.
 * @return Pointer to created Relay object if successful, NULL if failed.
 */
T_Relay* RLY_CreateIntegratedPrecharger(const char* name, T_Pin pinDrive, T_Pin pinSense, T_Pin pinActive);

/**
 * Construct an INTEGRATED PRECHARGER Relay object with given Drive/Sense, but 
 * without an Active pin.
 * Timing parameters will be pulled from defaults provided in RLY_Init().
 *
 * The Sense pin should indicate when the relay is done precharging. Because 
 * there is no Active pin, the controller will not be able to determine between
 * a failed precharge due to timeout/overtemp/board error or a disconnected board.
 * 
 * By default the Drive pin logic is not inverted, Sense pin logic is inverted.
 * @see RLY_SetDrivePinLogicLevel() 
 * @see RLY_SetSensePinLogicLevel()
 * 
 * 
 * @param name Name of Relay object (null terminated), copied into local namespace
 * @param pinDrive BluFlex Pin object for driving the contactor
 * @param pinSense BluFlex Pin object for sensing the state of the contactor
 * @return Pointer to created Relay object if successful, NULL if failed.
 */
T_Relay* RLY_CreateIntegratedPrechargerWithoutActive(const char* name, T_Pin pinDrive, T_Pin pinSense);


//####################### Relay List Functions #################################

/**
 * Get the next relay object in linked list. 
 * @param rly Relay object to look at
 * @return Next Relay object in linked list, NULL if none
 */
T_Relay* RLY_GetNextInList(const T_Relay* rly);

/**
 * Iterate over entire linked list of Relays; check if any are broken.
 * @param pListHead First object in linked list
 * @return True if any relays in list are broken, False if no objects are broken
 */
bool RLY_AreAnyRelaysInListBroken(T_Relay* pListHead);

/**
 * Iterate over entire linked list of Relays; check if ALL are closed.
 * @param pListHead First object in linked list
 * @return True if all relays in list are closed, False if at least one is not closed
 */
bool RLY_AreAllRelaysInListClosed(T_Relay* pListHead);

/**
 * Iterate over entire linked list of Relays; check if ALL are opened.
 * @param pListHead First object in linked list
 * @return True if all relays in list are opened, False if at least one is not opened
 */
bool RLY_AreAllRelaysInListOpened(T_Relay* pListHead);

/**
 * Request Open/Close action on all Relays in list.
 * @param pListHead First object of linked list
 * @param rqstClose TRUE to close all relays, FALSE to open all relays
 */
void RLY_RequestActionOnList(T_Relay* pListHead, bool rqstClose);

/**
 * Add the given relay object to the end of the linked list.
 * @param pListHead Head of list to add to
 * @param pRelay Object to add to end of list
 * @return Was successful
 */
bool RLY_AddRelayToList(T_Relay* pListHead, T_Relay* pRelay);

//########################### Relay Object Functions ###########################

/**
 * Check if a Relay object is closed. Note that this will also return true for 
 * error states where the Relay is closed, such as STUCK_CLOSED and
 * UNEXPECTED_CLOSE. 
 * To determine whether or not relay is in an error state, use RLY_IsBroken().
 * @param Relay to check
 * @return TRUE if relay closed
 */
bool RLY_IsClosed(const T_Relay*);

/**
 * Check if a Relay object is opened. Note that this will also return true for 
 * error states where the Relay is opened, such as DISCONNECTED, PRECHARGE_FAILED,
 * and PRECHARGE_TIMEOUT.
 * To determine whether or not relay is in an error state, use RLY_IsBroken().
 * @param Relay to check
 * @return TRUE if relay opened
 */
bool RLY_IsOpened(const T_Relay*);

/**
 * Check if a Relay object is in an error/broken state.
 * @param Relay to check
 * @return TRUE if relay is a broken state
 */
bool RLY_IsBroken(const T_Relay*);

/**
 * Request a Relay object to Open/Close. Note that return value only indicates
 * that the call was valid, not whether or not is was successful in closing/opening.
 * @param Relay object
 * @param requestClose
 * @return TRUE if request accepted.
 */
bool RLY_RequestAction(T_Relay*, bool requestClose);

/**
 * Print info about the given Relay object to the console.
 * @param rly Relay object
 */
void RLY_PrintInfo(const T_Relay* rly);

/** 
 * Return the name of the Relay object as a null-terminated string. 
 * @param rly Relay object
 * @return String name
 */
const char* RLY_GetName(const T_Relay* rly);

/** 
 * Return the state of the Relay object as a null-terminated string. 
 * @param rly Relay object
 * @return String state
 */
const char* RLY_GetStateStr(const T_Relay* rly);

/**
 * Add a function that should be executed before a relay object is opened/closed.
 * This is useful for situation where actuating a relay causes noise in system 
 * etc., and you may wish to temporarily disable some measurements while this 
 * action takes place. If a PreAction function is already associated with 
 * this relay object, it will be overridden.
 * 
 * @param Relay object to associate function with
 * @param fPreAction Pointer to function to execute before an open/close action.
 * @return Was successful 
 */
bool RLY_AddPreAction(T_Relay*, void (*fPreAction)(void));


/**
 * Add a function that should be executed after a relay object is opened/closed.
 * This is useful for situation where actuating a relay causes noise in system 
 * etc., and you may wish to temporarily disable some measurements while this 
 * action takes place. If a PostAction function is already associated with 
 * this relay object, it will be overridden.
 * 
 * @param Relay object to associate function with
 * @param fPostAction Pointer to function to execute after an open/close action.
 * @return Was successful 
 */
bool RLY_AddPostAction(T_Relay*, void (*fPostAction)(void));

/**
 * Set the logic level polarity of the DRIVE pin for the given relay object (i.e.
 * should the Drive GPIO be be logically inverted?
 * 
 * @param Relay Object 
 * @param isActiveHi TRUE if Drive pin should be Active Hi
 * @return Was successful
 */
bool RLY_SetDrivePinLogicLevel(T_Relay*, bool isActiveHi);

/**
 * Set the logic level polarity of the SENSE pin for the given relay object (i.e.
 * should the Sense GPIO be be logically inverted?
 * 
 * @param Relay Object 
 * @param isActiveHi TRUE if Sense pin should be Active Hi
 * @return Was successful
 */
bool RLY_SetSensePinLogicLevel(T_Relay*, bool isActiveHi);

/**
 * Set the logic level polarity of the ACTIVe pin for the given relay object (i.e.
 * should the Active GPIO be logically inverted?
 * 
 * @param Relay Object 
 * @param isActiveHi TRUE if Active pin should be Active Hi
 * @return Was successful
 */
bool RLY_SetActivePinLogicLevel(T_Relay*, bool isActiveHi);

/**
 * Set the maximum allowed closing time for the given relay object.
 * 
 * If Relay is not an Integrated Precharger, this time defined how long the
 * controller will wait in the CLOSING state for the Sense line to be asserted 
 * before throwing a timeout error. 
 * 
 * If Relay is an Integrated Precharger, this time defined how long the
 * controller will wait in the PRECHARGING state for the Sense line to be asserted 
 * before throwing a timeout error. 
 * 
 * 
 * @param Relay object to modify
 * @param ms Max allowed closing time, in milliseconds
 * @return Was successful
 */
bool RLY_SetMaxClosingTime_ms(T_Relay*, uint32_t ms);

/**
 * Set the maximum allowed opening time for the given relay object.
 *  
 * @param Relay object to modify
 * @param ms Max allowed opening time, in milliseconds
 * @return Was successful
 */
bool RLY_SetMaxOpeningTime_ms(T_Relay*, uint32_t ms);

/**
 * Check if the given Relay object is a designated as an Integrated Precharger.
 * @param rly Relay object to check
 * @return TRUE if is Integrated Precharger
 */
bool RLY_IsIntegratedPrecharger(const T_Relay* rly);

/**
 * Restart stopwatch for given relay 
 * @param pRelay Restart stopwatch for this relay 
 */
void RLY_ResetStopwatch(T_Relay* pRelay);

//############################### Class Functions ##############################

/**
 * Return a bitmask representing the state of every Relay object. 0=open, 1= closed
 * Bit number corresponds to creation order of Relay object.
 * @return Relay state bitmask
 */
uint16_t RLY_GetRelayMask(void);


/*
 * Return the number of assigned relays
 */
uint8_t RLY_GetRelayCount(void);


/**
 * Print the names of all Relay objects to Console.
 */
void RLY_PrintNames(void);

/**
 * For each relay, if DISCONNECTED, set to OPEN.
 * 
 */
void RLY_FixDisconnected(void);

/**
 * Set whether or not state checking is active.
 * @param isOn
 */
void RLY_SetStateChecking(T_Relay* rly, bool isOn);


//######################## BluFlex Draw Functions ##############################
/**
 * Process command line inputs for this module.
 * @param buf Two-dimensional array of CL inputs.
 * @param nArgs The number of tokens in the input.
 */
void RLY_ProcessInput(const char* tokens[], uint8_t nTokens);

/**
 * Initialization function for Fullscreen mode.
 */
void RLY_DrawInit(void);

/**
 * Update function for Fullscreen mode.
 */
void RLY_DrawUpdate(void);

/**
 * Process keypresses while in Fullscreen mode.
 * @param cin
 */
void RLY_DrawInput(uint8_t cin);

#endif
