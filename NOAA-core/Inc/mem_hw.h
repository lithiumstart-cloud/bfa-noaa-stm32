/**
 * @file mem_hw.h
 * @brief Nonvolatile Memory Hardware Interface
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-03-30
 *
 * This is a platform agnostic interface for hardware level NVRAM functions. Any
 * driver that implements this interface must use non-blocking implementations.
**/

#ifndef MEM_HW_H
#define MEM_HW_H

#include <stdint.h>

 typedef enum
 {
     MEM_SUCCESS = 0,
     ADDR_NOT_FOUND = 1,
     EXPIRED_PAGE = 2,
     PACK_SKIPPED = 4,
     ILLEGAL_ADDR = 5,
     PAGE_CORRUPT_STATUS = 6,
     WRITE_ERROR = 7,
     LOW_VOLTAGE_OP = 8,
 } T_MEM_HW_Result;

 /**
  * Intialize module.
  * @return Error status (result)
  */
T_MEM_HW_Result MEM_HW_Init(void);

/**
 * 
 * @param data Where to put read data
 * @param addr Address of data to read
 * @return Error status (result)
 */
T_MEM_HW_Result MEM_HW_Read(unsigned int *data, unsigned int addr);

/**
 * 
 * @param data Data to write
 * @param addr Address of data to write
 * @return Error status (result)
 */
T_MEM_HW_Result MEM_HW_Write(unsigned int data, unsigned int addr);

/**
 * Request the max number of words in space
 * @return Number of words in space
 */
uint32_t MEM_HW_GetDiskSize_Words(void);

#endif
