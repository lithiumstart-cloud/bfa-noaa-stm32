/**
 * @file print_as.h
 * @brief "Print As" library - helpful printing functions
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-11-03
 *
 *
**/

#ifndef _PRINT_AS_H
#define _PRINT_AS_H

    #include <stdint.h>
 	#include <string.h>

    extern void (*PrintAs_OutputByte) (uint8_t byte);

    
    void PrintAs_U16(uint16_t data);
    void PrintAs_str(const char*);
    void PrintAs_strl(const char*);
    void PrintAs_stru(const char*);

    
    void PrintAs_Hex_U8(uint8_t data);
    void PrintAs_Hex_U16(uint16_t data);
    void PrintAs_Hex_U32(uint32_t data);

    void PrintAs_Dec_U16(uint16_t x, uint8_t d_nDigits);
    void PrintAs_Dec_I16(int16_t y, uint8_t d_nDigits);

    void PrintAs_Dec_U8(uint8_t x, uint8_t d_nDigits);
    void PrintAs_Dec_I8(int8_t y, uint8_t d_nDigits);

    void PrintAs_Dec_U32(uint32_t x, uint8_t d_nDigits);
    void PrintAs_Dec_I32(int32_t y, uint8_t d_nDigits);

    void PrintAs_Millis_U16(uint16_t mU);
    void PrintAs_Millis_I16(int16_t mU);
    void PrintAs_Millis_U16_f2(uint16_t mU);
    void PrintAs_Millis_I16_f2(int16_t mU);

    void PrintAs_Millis_U32_f2(uint32_t mU);
    void PrintAs_Millis_I32_f2(int32_t mU);

    void PrintAs_Millis_U64_f2(uint64_t mU);
    void PrintAs_Millis_I64_f2(int64_t mU);

    void PrintAs_Scaled_U32(uint32_t raw, uint8_t scaleExp);
    
    uint8_t GetNumberDigits_U8(uint8_t x);

#endif
