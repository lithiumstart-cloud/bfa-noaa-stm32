/**
 * @file sys.h
 * @brief Implements the system setup for hardware, timers, reset.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-04-17
 *
**/

#ifndef SYS_H
#define SYS_H

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "datetime.h"


/**
 * Intialize SYS module. 
 */
void SYS_Init(void);

/**
 * Update function for SYS module. Call as fast as possible.
 * @return  Is ok to sleep?
 */
bool SYS_Update(void);

/**
 * Print the current system up-time to Console.
 */
void SYS_PrintUptime(void);

/**
 * Create a string with the current system up-time
 * @param buffer
 * @param bufferLength
 * @return 
 */
bool SYS_GetUptimeAsString(char* buffer, size_t bufferLength);
/**
 * Print the system serial number.
 */
void SYS_PrintSerialNumber(void);

/**
 * Get system serial number.
 * @return System serial number
 */
uint32_t SYS_GetSerialNumber(void);

/**
 * Get the lower word of the system serial number.
 * @return Lower word (2-bytes) of system serial number.
 */
uint16_t SYS_GetSerialNumber_LSW(void);

/**
 * Get the last reset reason as a string.
 * @return String of last reset reason
 */
char const * SYS_LastResetString(void);

/**
 * Process input strings from CLI.
 * @param tokens
 * @param nTokens
 */
void SYS_ProcessInput(const char* tokens[], uint8_t nTokens);

/**
 * CAN callback for getting system time as byte string.
 * @param dest
 * @param nBytes
 */
void SYS_FillUpTime(uint8_t* dest, uint8_t nBytes);

/**
 * Retrieve contents of the system uptime structure.
 * @param dest
 */
void SYS_GetUpTime(T_TimeDelta* dest);

void SYS_Restart(void);

#endif
