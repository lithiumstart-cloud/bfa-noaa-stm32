/**
 * @file tbl.h
 * @brief Table manager
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2014-11-06
 *
 *
 **/

#ifndef TBL_H
#define	TBL_H

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    int32_t x;
    int32_t y;
} T_Point;

typedef struct
{
    const T_Point* data;
    uint32_t nPoints;
    const char* name;
} T_Table;


void TBL_Init(void);

T_Table* TBL_AddTable(const char* name, const T_Point* data, uint32_t dataLength);
int32_t TBL_Lookup(const T_Table* table, int32_t x);
void TBL_ProcessInput(const char* tokens[], uint8_t nArgs);



#endif	

