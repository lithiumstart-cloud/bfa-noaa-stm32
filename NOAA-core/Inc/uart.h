/**
 * @file uart.h
 * @brief Hardware UART driver
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-11-03
 *
 *
**/

#ifndef UART_H
#define UART_H

    #include <stdint.h>
    #include "led.h"

    //"Class" Functions

    /**
     * Intialize UART module. Call before opening any ports. 
     */
    void UART_Init(void);
    
    /**
     * Iterate over all active UART ports and enqueue/dequeue bytes as necessary.
     * Call as fast as possible.
     * @return Is OK to sleep?
     */
    bool UART_Update(void);
    
    //"Object" Functions
    
    /**
     * Intialize a UART port object in software, and do any hardware setup
     * necessary to open peripheral. 
     * 
     * This should only be called at startup, as it allocated memory for queues. 
     * 
     * @param chn Channel of UART port to open 
     * @param baud Desired Baud rate
     * @param rxQueueSize Size of RX queue
     * @param txQueueSize Size of TX queue
     * @return Success
     */
    bool UART_OpenPort(uint8_t chn, uint32_t baud, uint16_t rxQueueSize, uint16_t txQueueSize);
    
    /**
     * Check if a byte is waiting on the given Rx queue. If so, call 
     * UART_ReceiveByte() to pop it off queue.
     * @param chn Channel to check for received bytes
     * @return Is byte waiting
     */
    bool UART_IsByteWaiting(uint8_t chn);
    
    /**
     * Pop byte waiting off Rx queue. If no byte is waiting, returns garbage. 
     * Call UART_IsByteWaiting() to check if data is available before calling.
     * @param chn Channel to check
     * @return Received byte
     */
    uint8_t UART_ReceiveByte(uint8_t chn);
    
    /**
     * Enqueue a byte for transmission on the given UART channel. 
     * @param chn Channel to send byte on
     * @param data Byte to send
     */
    void UART_SendByte(uint8_t chn, uint8_t data);

    /**
     * Enqueue a null-terminated string for transmission on given UART channel.
     * @param chn Channel to send string over
     * @param x Null-termianted string 
     */
    void UART_SendString(uint8_t chn, const char* x);
    
    /**
     * Enqueue an array of bytes for transmisson on given UART channel.
     * @param chn Channel to send data over
     * @param pData Pointer to first location of data
     * @param N Number of bytes to send
     */
    void UART_SendBytes(uint8_t chn, uint8_t* pDdata, uint32_t N);
    
    /**
     * Assign activity LEDs for a given UART port. 
     * @param chn
     * @param pLedTx
     * @param pLedRx
     */
    void UART_AssignLEDs(uint8_t chn, T_LED* pLedTx, T_LED* pLedRx);


    /**
     * Check if the transmit queue on given channel is empty (no bytes waiting
     * to send). 
     * @param chn Channel to check
     * @return Is transmit queue empty? 
     */
    bool UART_IsTransmitQueueEmpty(uint8_t chn);
    
    /**
     * Push a byte onto the end of the recieve queue, as if it was received over
     * UART.
     * @param chn Channel to inject on
     * @param data Data to inject
     */
    void UART_InjectRecievedByte(uint8_t chn, uint8_t data);
#endif
