/**
 * @file rtc_hw.h
 * @brief RTC Hardware Interface
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-03-16
 *
 * This is a platform agnostic interface for hardware level RTC functions. Any
 * driver that implements this interface must use non-blocking implementations.
**/

#ifndef RTC_HW_H
#define RTC_HW_H

#include <stdint.h>
#include <stdbool.h>

#include "rtc.h"
#include "i2c_manager.h"
/**
 * Do whatever hardware intialization is necesary to start talking to RTC
 * @param tickFreq_Hz Desired hardware timer frequency. 
 */
void RTC_HW_Init(uint32_t, T_BF_I2C_Port* pPort);

/**
 * Set the RTC internal time to the given time.
 * @param srcDt
 */
void RTC_HW_SetDateTime(const T_Datetime* srcDt);
             

/**
 * Get the latest RTC datetime, if it is available.
 * @param destDt Where to put datetime
 * @return Is new data available?
 */
bool RTC_HW_GetFreshDatetime(T_Datetime* destDt);

/**
 * Update the RTC HW layer as necessary.
 * @return 
 */
bool RTC_HW_Update(void); 

void RTC_HW_ProcessInput(const char* tokens[], uint8_t nArgs);

#endif
