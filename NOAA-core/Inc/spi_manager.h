/**
 * @file spi_manager.h
 * @brief Manages SPI transmissions and SPI ports 
 * @copyright 2020 EaglePicher Technologies
 * @author Miroslav Grubic
 * @date   October 25, 2020
**/

#ifndef SPI_MANAGER_H
#define SPI_MANAGER_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

void SPI_Init(void);
bool SPI_Update(void);

/**
 * Calls function which initializes SPI hardware with given parameters. Will be setup for full duplex 
 * DMA. All four SPI channels will use separate DMA ports. 
 * @param portInx       ID of SPI port to initialize
 * @param spiClockHz    Clock frequency of SPI port to init
 * @param cpol          Clock polarity select (0= active hi, 1 = active lo)
 * @param cpha          Clock phase select (0= sample on first edge, 1 = second edge)
 * @param mssen         Master Slave Select Enable (does SPI module control SS?)
 * @return Success
 */
bool SPI_Init_Master(uint8_t portInx, unsigned int spiClockHz, bool cpol, bool cpha, bool mssen);

/** 
 * @param portInx   Port index on microcontroller
 * @param data      Buffer which holds data
 * @param count     Number of bytes to be transferred 
 * @param isMsgSent Flag which indicates if transfer is done
 * @return 
 */
bool SPI_StartMasterTxRx(uint8_t portInx, uint8_t* data, int count, bool* isMsgSent);


#endif	// #endif	/* SPI_MANAGER_H */
