/**
 * @file gpio.h
 * @brief GPIO hardware interaface. 
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-03-09
 *
 * This is a platform agnostic interface for hardware level GPIO functions. Any
 * driver that implements this interface must use non-blocking implementations.
**/

#ifndef GPIO_H
#define	GPIO_H


#include <stdint.h>
#include <stdbool.h>
#include <stm32l476xx.h>


typedef struct
{
	GPIO_TypeDef* port;
    uint16_t 	  pin;
} T_Pin;

#define GPIO_IS_INPUT (true)
#define GPIO_IS_OUTPUT (false)


/**
 * Read the state of a digital input pin. Pin must already be set as input.
 * @param pPin Pin to read
 * @return State of pin (true = HI, false = LO)
 */
bool GPIO_ReadInput(const T_Pin* pPin);

/**
 * Set the state of a digital output pin. Pin must already be set as output
 * @param pPin Pin to write to
 * @param setVal State of pin to set to (true = HI, false = LO)
 * @return Was operation successful
 */
bool GPIO_WriteOutput(const T_Pin* pPin, bool setVal);

/**
 * Read the last state written to a digital output pin. Pin must already be set
 * as an output.
 * @param pPin
 * @return Last output state written (true = HI, false = LO)
 */
bool GPIO_CheckOutput(const T_Pin* pPin);

/**
 * Set the direction of a digital pin.
 * @param pPin Pin to set direction of
 * @param isInput True = INPUT, false = OUTPUT
 * @return Was operation successful
 */
bool GPIO_SetDirection(const T_Pin* pPin, bool isInput);

/**
 * Invert the level of an output pin. Pin must already be output.
 * @param pPin Pin to invert.
 * @return Was operation successful
 */
bool GPIO_ToggleOutput(const T_Pin* pPin);

/**
 * Check if a pin is valid.
 * @param pPin Pin to check
 * @return Is valid
 */
bool GPIO_IsValidPin(const T_Pin* pPin);

/**
 * Print the port and number of a pin.
 * @param pPin Pin to print
 * @return Was operation successful
 */
bool GPIO_PrintPin(const T_Pin* pPin);


/*
 * CLI functionality
 */
void GPIO_ProcessInput(const char* tokens[], uint8_t nArgs);

#endif	

