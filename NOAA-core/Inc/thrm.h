/**
 * @file thrm.h
 * @brief  Implements Thermistor lookup tables
 * @copyright 2017 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2017-02-15
 *
**/


#ifndef __THERMISTOR_H
#define __THERMISTOR_H

#include <stdint.h>
#include <stdbool.h>



/*
 * These lookup table maps a normalized divider voltage reading, v, in [0,255] 
 * to thermistor temperature. Circuit is assumed to look like...
 *
 *       Vref
 *       --+--                   
 *         |                                  
 *         |
 *        | |                      / Vin  \
 *        | | R_T             v = | ------ | * 255
 *        | |                      \ Vref /
 *         |
 *         +-----> Vin                   B
 *         |                 T = ------------------
 *                               ln(1/v - 1) + B/T0
 *         |
 *         \
 *         /  R_D
 *         \
 *         |
 *       --+--
 *        ---
 *         -
 *
 *  It is assumed R_D is equal to R_0 of thermistor (temperature at which beta 
 *  is calculated). 
 *  
 */

/**
 * Supported thermistor types...
 */
typedef enum
{
    THRM_TYPE__B4500 = 0, ///< This is the "normal" verison
    THRM_TYPE__B4250,
	THRM_TYPE__B3380,
            
    THRM_TYPE__NUM_TYPES,
    
} T_THRM__Type;

/**
 * Return Thermistor type enum as string
 * @param t Thermistor type enum
 * @return String representation of enum type
 */
const char* THRM_Type2Str(T_THRM__Type t);

/**
 * Lookup temperature for thermistor of given type.
 * @param d Normalized divider voltage, scaled to [0,255]
 * @param type Enumerated type of thermistor
 * @return Thermistor temperature in degrees Celsius 
 */
int8_t THRM_LookupTemp__dC(uint8_t d, T_THRM__Type type);



#endif

