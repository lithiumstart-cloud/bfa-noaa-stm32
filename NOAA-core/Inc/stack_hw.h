/* 
 * File:   stack.h
 * Author: akessler
 *
 * Created on November 29, 2015, 4:10 PM
 * This module allows you to track how much stack space is used by the application.
 * Needs processor specific implementation.
 */

#ifndef STACK_H
#define	STACK_H

#include <stdint.h>
#include <stdbool.h>

/**
 * Initialize the Stack Watcher. Be sure to call as early in the application 
 * as possible as already used stack space is not going to be tracked.
 */
void Stack_Init(void);

/**
 * 
 * @return 
 */
bool Stack_Update(void);

/**
 * 
 * @param buf
 * @param nArgs
 */
void Stack_ProcessInput(const char* buf[], uint8_t nArgs);

#endif	/* STACK_H */

