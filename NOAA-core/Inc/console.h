/**
 * @file console.h
 * @brief Interface for debugging serial port
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-11-03
 *
 *
**/

#ifndef _CONSOLE_H
#define _CONSOLE_H

    #include <stdbool.h>
    #include <stdint.h>
    #include <stdio.h>
    #include "str_lib.h"

	void Console_Init(void);
    void Console_WriteByte(uint8_t data);
    uint8_t Console_ReadByte(void);
    bool Console_IsByteWaiting(void);
    void Console_InjectReceivedByte(uint8_t x);
//    void puts(const char* x);
 

    //these need to eventually not be here
    #define MOVE_CURSOR_RETURN()    Console_WriteByte('\r')
    #define SEND_SPACE()            Console_WriteByte(' ')
    #define SEND_TAB()              Console_WriteByte('\t')
    #define LINE_FEED()             Console_WriteByte('\n')

    #define NEW_LINE()          putstr(STR_NewLine)
    #define COMMAND_PROMPT()    putstr(STR_CommandPrompt)

    #define SEND_STR_LN(x)      putstr(x)
                          
    #define NEW_LINE_TAB()      putstr(STR_NewLineTab)
    

#endif
