/**
 * @file spi_hw.h
 * @brief SPI Hardware Abstraction Layer
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-03-11
 *
 *
**/

#ifndef SPI_HW_H
#define SPI_HW_H

    #include <stdbool.h>
    #include <stdint.h>

    #define NUM_SPI_PORTS 4
#if 0
    enum
    {
        SPI1 =0,
        SPI2,
        SPI3,
        SPI4
    };
#endif
#define SPI_IS_MASTER (true)
#define SPI_IS_SLAVE  (false)

/**
 *        CPOL = 0 (Active Hi, Idle Lo Clock)
 *  H |           ______          ______
 *     |          /      \        /      \
 *  LO |...______/        \______/        \_____...
 *               ^        ^
 *        1st Edge       2nd Edge
 *        (CPHA = 0)     (CPHA = 1)
 *        [Rising]       [Falling]
 *
 *
 *           CPOL = 1 (Active Lo, Idle Hi Clock)
 *  HI |...______          ______         ______...
 *     |         \        /      \       /
 *  LO |          \______/        \_____/
 *                ^      ^
 *         1st Edge      2nd Edge
 *       (CPHA = 0)     (CPHA = 1)
 *        [Falling]     [Rising]
 *
 *                       CPHA
 *                 0      |    1
 *              +---------+---------+
 *      C     0 | Rising  | Falling |
 *      P    ---+---------+---------+   (Edge Captured)
 *      O     1 | Falling | Rising  |
 *      L       +---------+---------+
 */


/**
 * Initialize SPI hardware with given parameters. Will be setup for full duplex 
 * DMA. All four SPI channels will use separate DMA ports. 
 * @param portInx ID of SPI port to initialize
 * @param spiClockHz Clock frequency of SPI port to init
 * @param cpol Clock polarity select (0= active hi, 1 = active lo)
 * @param cpha Clock phase select (0= sample on first edge, 1 = second edge)
 * @param mssen Master Slave Select Enable (does SPI module control SS?)
 * @return Success
 */
bool SPI_HW_Init_Master(uint8_t portInx, unsigned int spiClockHz, bool cpol, bool cpha, bool mssen);
bool SPI_HW_Init_Slave(uint8_t portInx, bool cpol, bool cpha);

bool SPI_HW_Enable(uint8_t portInx);
bool SPI_HW_Disable(uint8_t portInx);

bool SPI_HW_StartSlaveRxInto(uint8_t portInx, uint8_t *ptr, int count);

bool SPI_HW_StopReceive(uint8_t portInx);
bool SPI_HW_IsReceiveDone(uint8_t portInx, uint16_t* crc);

uint8_t SPI_HW_TransmitRecieve(uint8_t portInx, uint8_t out);

bool SPI_HW_IsMaster(uint8_t portInx);
bool SPI_HW_Flush(uint8_t portInx);

/**
 * Attach the CRC generator to the RX side of a given SPI port. Note PIC32 only 
 * has a single CRC generator, and can only be attached to one DMA channel at a
 * time!
 * @param portInx SPI port to attached CRC generator to on RX side
 * @param polynomial Polynomial that defines config of CRC. See datasheet
 * @param polyLength Polynomial length
 * @param seed Staring value of CRC
 * @return 
 */
bool SPI_HW_AttachDmaCrc(uint8_t portInx, unsigned int polynomial, uint8_t polyLength, unsigned int seed);

/**
 * Detach the CRC hardware from where ever it is attached to. 
 * @return 
 */
bool SPI_HW_DetachDmaCrc(void);

/**
 * Setup and start packet. 
 * @param portInx
 * @param data
 * @param count
 * @return 
 */

bool SPI_HW_StartMasterTxRx(uint8_t portInx, uint8_t* data, int count);

/**
 * Setup packet, with option to start immedieatly. 
 * @param portInx
 * @param data
 * @param count
 * @param Start 
 * @return 
 */
bool SPI_HW_SetupMasterTxRx(uint8_t portInx, uint8_t* data, int count, bool startNow);

/**
 * Start exchange of already setup packet (see @SPI_HW_SetupMasterTxRx)
 * @param portInx
 * @return 
 */
bool SPI_HW_ManualStartMasterTxRx(uint8_t portInx);

//These do not use DMA!
void SPI_HW_Oneshot_Tx(uint8_t portInx, uint8_t byte);
bool SPI_HW_Oneshot_IsDone(uint8_t portInx);

#endif
