/**
 * @file  heap.h
 * @brief  Dynamic Memory Wrapper Interface
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-01-21
 *
 * 
**/

#ifndef HEAP_H
#define	HEAP_H

#include <stdint.h>
#include <stdbool.h>


/**
 * Wrapper for malloc(). Request a particular number of bytes off heap.
 * @param n Number of bytes to allocate.
 * @return Pointer to allocated memory. NULL if unsuccesful.
 */
void * Heap_Allocate(size_t n);

/**
 * Request how many bytes have been allocated so far.
 * @return Number of allocated bytes.
 */
uint32_t Heap_GetNumBytesAllocated(void);

/**
 * CLI callback.
 * @param tokens
 * @param nTokens
 */
void Heap_ProcessInput(const char* tokens[], uint8_t nTokens);


#endif

