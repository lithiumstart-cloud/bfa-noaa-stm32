/**
 * @file uart_hw.h
 * @brief Hardware UART Interface
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-01-19
 *
 * This is a platform agnostic interface for hardware level UART functions. Any
 * driver that implements this interface must use non-blocking implementations.
**/

#ifndef UART_HW_H
#define UART_HW_H

#include <stdint.h>
#include <stdbool.h>

bool getUartHwInitStats(void);

typedef enum
{
    UART_HW_PD__8B_NO_PARITY = 0,
    UART_HW_PD__8B_EVEN_PARITY,
    UART_HW_PD__8B_ODD_PARITY,
    UART_HW_PD__9B_NO_PARITY,           
            
} T_UART_HW_ParityData;


/*We wrap the parity data option in a struct for now, so we can add more options
 later without having to go back and change old code.*/
typedef struct
{
   T_UART_HW_ParityData pd;
     
} T_UART_HW_CustomOpts;

/**
 * Open a UART port in hardware with custom settings. You should recall after using the standard UART_Open()
 * @param id
 * @param desired_baud
 * @param opts Options sutrct
 * @return 
 */
bool UART_HW_OpenCustom(uint8_t id, uint32_t desired_baud, T_UART_HW_CustomOpts opts);

/**
 * Do whatever hardware intialization is necessary to open a UART port in hardware.
 * @param id
 * @param desired_baud
 * @return Success
 */
 bool UART_HW_Open(uint8_t id, uint32_t desired_baud);
 
 /**
  * Write byte to UART port HW. Should be non-blocking. Software buffering is not
  * handled here. 
  * @param id
  * @param data
  * @return Success
  */
 bool UART_HW_SendByte(uint8_t id, uint8_t data);
 
 /**
  * Read byte from UART port HW. Should be non-blocking. Assumes byte is ready.
  * @param id
  * @return Success
  */
 uint8_t UART_HW_ReadByte(uint8_t id);
 
 /**
  * Check if an incoming byte is waiting in HW.
  * @param id
  * @return Is byte waiting
  */
 bool UART_HW_IsRxReady(uint8_t id);
 
 /**
  * Check if UART port HW is able to accept another byte to transmit.
  * @param id
  * @return Is UART port able to accept byte to transmit
  */
 bool UART_HW_IsTxReady(uint8_t id);


 void UART_HW_SendBlockingString(uint8_t id, const char* data, uint16_t len);

 
#endif
