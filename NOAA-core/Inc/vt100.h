/**
 * @file vt100.h
 * @brief Interface for controlling cursor on VT100 emulator
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-11-03
 *
 *
**/

#ifndef _VT100_H
#define _VT100_H

    #include <stdint.h>
 	#include <string.h>

    typedef enum
    {
        VT100_CD__MOVE_UP =0,
        VT100_CD__MOVE_DOWN,
        VT100_CD__MOVE_RIGHT,
        VT100_CD__MOVE_LEFT
    } T_VT100_CursorDirection;

   /*These are here to maintain backwards compatibility...remove eventually*/
#define VT100_MOVE_UP (VT100_CD__MOVE_UP)    
#define VT100_MOVE_DOWN (VT100_CD__MOVE_DOWN)


    typedef enum
    {
        VT100_SPC__MOVE_CURSOR_HOME = 0,
        VT100_SPC__ERASE_END_OF_LINE, ///< Erases from the current cursor position to the end of the current line.
        VT100_SPC__ERASE_START_OF_LINE, ///< Erases from the current cursor position to the start of the current line.
        VT100_SPC__ERASE_CURRENT_LINE, ///<Erase the entire current line
    
        VT100_SPC__ERASE_DOWN, ///< Erase the screen from the current line down to the bottom of the screen
        VT100_SPC__ERASE_UP, ///< Erase the screen from the current line up to the top of the screen
        VT100_SPC__ERASE_WHOLE_SCREEN, ///<Erases the screen with the background colour and moves the cursor to home.
    
        VT100_SPC__NORMAL_CHARS,
        VT100_SPC__BOLD_CHARS,
        VT100_SPC__UNDERLINE_CHARS,
        VT100_SPC__BLINKING_CHARS,
                
    } T_VT100_SpecialCommand;

    void VT100_RelativeMoveCursor(T_VT100_CursorDirection dir, uint8_t);
    void VT100_MoveCursorTo(uint8_t row, uint8_t col);
    void VT100_SpecialCommand(T_VT100_SpecialCommand cmdKey);
   
    /**
     * Set VT100 mode to Bold, print given string, and set mode back to normal.
     * @param str Null terminated string to print
     */
    void VT100_PrintBold(const char* str);
    
    /**
     * Set VT100 mode to Underline, print string, and set mode back to normal.
     * @param str Null terminated string to print
     */
    void VT100_PrintUnderline(const char* str);
    
    /**
     * Print a string (preStr), clear the rest of the line, then print another
     * string. Prestring option is provided to allow user to move cursor (e.g.
     * by tabbing) before clearing line.
     * 
     * VT100_ClearLineAndPrint("\t" ,"2000"); //tab, clear rest of line, print 2000 
     * VT100_ClearLineAndPrint(NULL ,"2000"); //clear entire line, print 2000
     * VT100_ClearLineAndPrint("\t" , NULL); //tab, clear rest of line
     * 
     * @param preStr String to print before clearing line 
     * @param postStr Sting to print after clearing line
     */
    void VT100_PrintClearLinePrint(const char* preStr, const char* postStr);
    
    //We define this externally so it can be overriden if needed later
    //extern int (*VT100_FormatOutput)(const char* format, ...);

    
    #define VT100_MOVE_CURSOR_HOME()      VT100_SpecialCommand(VT100_SPC__MOVE_CURSOR_HOME)

    #define VT100_CLEAR_SCREEN()          VT100_SpecialCommand(VT100_SPC__ERASE_WHOLE_SCREEN)
    #define VT100_CLEAR_LINE()            VT100_SpecialCommand(VT100_SPC__ERASE_END_OF_LINE)

    #define VT100_FONT_BOLD()             VT100_SpecialCommand(VT100_SPC__BOLD_CHARS)
    #define VT100_FONT_UNDERLINE()        VT100_SpecialCommand(VT100_SPC__UNDERLINE_CHARS)
    #define VT100_FONT_BLINKING()         VT100_SpecialCommand(VT100_SPC__BLINKING_CHARS)
    #define VT100_FONT_NORMAL()           VT100_SpecialCommand(VT100_SPC__NORMAL_CHARS)
    
    
#endif
