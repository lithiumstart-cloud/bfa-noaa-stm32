/**
 * @file cli.h
 * @brief Command line interface module
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-02
 *
**/

#ifndef CLI_H
#define	CLI_H

#include <stdbool.h>
#include <stdint.h>


#ifndef STRINGIZE
    #define STRINGIZE(x)    #x
#endif

#ifndef STRINGIZE_VALUE_OF
    #define STRINGIZE_VALUE_OF(x)   STRINGIZE(x)
#endif

#define CLI_ADD_INPUT_CALLBACK(NAME) CLI_AddCallback(#NAME, NAME ## _ProcessInput)
#define CLI_ADD_FULLSCREEN_MODE(NAME) CLI_AddFullscreen(#NAME,   NAME ## _DrawInit, NAME ## _DrawUpdate);
#define CLI_ADD_FULLSCREEN_MODE_WITH_INPUT(NAME) CLI_AddFullscreenCB(#NAME,  NAME ## _DrawInit, NAME ## _DrawUpdate, NAME ## _DrawInput);

typedef struct
{
    const char** configNames_str;
    uint32_t* configValues;
    uint8_t nConfigVars;

} T_ConfigWrapper;

void CLI_Init(void);
bool CLI_Update(void);

/**
 * Draws splash screen on boot. User can override implementation in app code.
 */
void CLI_DrawSplash(void);

/**
 * Add a callback function that will be called when <key> is entered at CLI.
 * Callback function must have argumetns (const char* tokens[], uint8_t nTokens)
 * The token array passed into callback function has the hook token removed.
 * Ex:
 *
 * CLI_AddCallback("BMS", &BMS_ProcessInput);
 * //or
 * CLI_ADD_INPUT_CALLBACK(BMS);
 * ...
 * >> bms this is a test
 *
 * When in BMS_ProcessInput, nTokens = 3 and tokens = {"this", "is", "a", "test"}
 *
 *
 * @param key String which will act as hook for this function
 * @param fProcessInput callback function pointer,
 * @return Success
 */
bool CLI_AddCallback(const char* key,  void (*fProcessInput) (const char*[], uint8_t));
bool CLI_AddFullscreen(const char* key,  void (*fInitDraw) (void), void (*fUpdate) (void));
bool CLI_AddFullscreenCB(const char* key,  void (*fInitDraw) (void), void (*fUpdate) (void), void (*fInput)(uint8_t));

void Full_ProcessInput(const char* tokens[], uint8_t nTokens);

void CLI_EndStartupMode(void);



/**
 * Set whether or not local echo of user inputs to the CLI is enabled.
 * Local echo is enabled by default.
 * @param state
 */
void CLI_SetLocalEcho(bool state);

bool CLI_ProcessConfigCommand(T_ConfigWrapper* pConfig, const char* tokens[], uint8_t nTokens);
bool CLI_ProcessConfig(const char** configNames_str, uint32_t* configValues,  uint8_t nConfigVars, const char* tokens[], uint8_t nTokens);
bool CLI_ProcessSignedConfig(const char** configNames_str, uint32_t* configValues,  uint8_t nConfigVars, uint32_t configSignMask, const char* tokens[], uint8_t nTokens);

void CLI_DrawProgressBar(void);
#endif	/* CLI_H */

