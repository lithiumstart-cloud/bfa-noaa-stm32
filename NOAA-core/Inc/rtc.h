/* 
 * File:   rtc.h
 * Author: akessler
 *
 * Created on March 16, 2016, 11:14 AM
 */

#ifndef RTC_H
#define	RTC_H

#include <stdint.h>
#include <stdbool.h>

#include "datetime.h"
#include "i2c_manager.h"

/**
 * Initialize RTC module
 */
void RTC_Init(T_BF_I2C_Port* pPort);
    
/**
 * Update the RTC module 
 * @return 
 */
bool RTC_Update(void);
    
/**
 * Process command line inputs.
 * @param tokens
 * @param nArgs
 */
void RTC_ProcessInput(const char* tokens[], uint8_t nArgs);

void RTC_PrintDatetimeNow(void);
bool RTC_GetDatetimeNow_str(char* buffer, size_t bufferLength);
bool RTC_GetDatetimeNow(T_Datetime* dt);

/**
 * Callback function for CANbus to populate a message with the current date/time.
 * @param dst
 * @param N
 */
void RTC_FillDatetime(uint8_t* dst, uint8_t N);

#endif	/* RTC_H */

