/* 
 * File:   core_config.h
 * Author: akessler
 *
 * Created on April 2, 2015, 2:56 PM
 */

#ifndef CORE_CONFIG_H
#define	CORE_CONFIG_H


#define LIB_MAX_NUM_TASKS_ALLOWED (32)


#define LIB_MAX_NUM_SIGNAL_NODES (950)

#define LIB_MAX_NUM_CHILDREN_TO_PRINT (16)
#define LIB_MAX_NUM_ALARM_TEMPLATES (8)

#define NUM_VERSION_VALUES (3)

#define CLI_MAX_NUM_CHARS 32
#define CLI_MAX_NUM_TOKENS 6

#define MAX_NUM_LEDS (16)

#define LOG_DUMP_TO_CLI_PERIOD_MS (200)
#define LOG_LENGTH 16

#define EMU_MAX_NUM_SIGS 16

#define MAX_NUM_RELAYS (16)

#endif	/* CORE_CONFIG_H */

