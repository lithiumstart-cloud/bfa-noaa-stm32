/* 
 * File:   i2c_slave_hw.h
 * Author: akessler
 *
 * Created on April 6, 2016, 10:28 AM
 */

#ifndef I2C_SLAVE_HW_H
#define	I2C_SLAVE_HW_H

#include <stdint.h>
#include <stdbool.h>

#include "i2c_manager.h"

struct xI2C_Slave;
typedef struct xI2C_Slave T_I2C_Slave;

typedef bool (*T_I2C_Slave_ReceiveHandler) (uint8_t);
typedef uint8_t (*T_I2C_Slave_RequestHandler) (void);
typedef void (*T_I2C_Slave_BeginHandler) (T_I2C_SlaveAddress);
typedef void (*T_I2C_Slave_EndHandler) (void);

T_I2C_Slave* I2C_Slave_Init(uint8_t portId, uint8_t slaveAddr, bool useSMB);

void I2C_Slave_AttachReceiveHandler(T_I2C_Slave* obj, T_I2C_Slave_ReceiveHandler func);
void I2C_Slave_AttachRequestHandler(T_I2C_Slave* obj, T_I2C_Slave_RequestHandler func);
void I2C_Slave_AttachBeginHandler(T_I2C_Slave* obj, T_I2C_Slave_BeginHandler func);
void I2C_Slave_AttachEndHandler(T_I2C_Slave* obj, T_I2C_Slave_EndHandler func);
   
bool I2C_Slave_Update(T_I2C_Slave* obj);

const char* I2C_Slave_GetStateStr(const T_I2C_Slave* obj);
void I2C_Slave_PrintStatus(const T_I2C_Slave* obj);




#endif	/* TWI_HW_H */

