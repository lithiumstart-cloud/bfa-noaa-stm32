/**
 * @file i2c_manager.h
 * @brief  BluFlex I2C Manager (Master Only)
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-03-21
 *
 * 
 *  The BluFlex I2C Manager abstracts the details of I2C Master transactions 
 *  from application modules ("Clients") that wish to communicate with devices
 *  that may or may not be on shared I2C buses. 
 * 
 * 
 * BluFlex I2C Manager architecture shown, using Client/Server Model. Application 
 * modules ("clients") wishing to send packets over an I2C port push locally owned 
 * packets onto Port's packet queue, at which point the packet becomes "Active". 
 * The I2C manager will send out packets in the  order on the queue, and mark 
 * completed  packets as no longer "Active". 
 * 
 * Clients can then check packets for whether or not they are done. 
 * 
 * Multiple Clients can post packets to the same Port - packets are transacted 
 * in the order received! But note that I2C_OpenPort should only be called once
 * for each hardware I2C port. Multiple clients should share I2C ports by sharing 
 * a single BF_I2C_Port handle (pointer).
 * 
 * Typical usage:
 * 
 *   1. Open BluFlex I2C Port, and save handle. Share Port handle with any Client
 *      modules that will need it. 
 *          T_BF_I2C_Port* myPort = I2C_OpenPort(0, 100e3, false); //Open I2C port on MCU's I2C1, at 100Khz, no SMBus
 *          
 *          RTC_AssignI2CPort(myPort); //Tell the RTC module (client) which port to use
 *          RHS_AssignI2CPort(myPort); //Tell another client which port to use
 *   2. Clients should allocate any Packets they want to use up front. Clients 
 *      then Enqueue packets as desired.
 *          
 *      In Client code...
 *          T_BF_I2C_Packet* myPacket = I2C_AllocatePacket();
 *
 *          static uint8_t myData[NUM_DATA_BYTES] = {0};
 *
 *          I2C_PushWriteTransfer(myPacket, myPort, SLAVE_ADDR, myData, NUM_DATA_BTYES, true)
 * 
 *   3. Clients can check whether their packet has completed before moving on.
 * 
 *          if (!I2C_IsPacketActive(myPacket))
 *          {
 *            failCode = I2C_DidPacketFailureOccur(myPacket);
 *            if (failCode == BF_I2C_PACKET_FAILURE__NONE)
 *            {
 *              //Success! Move on to next state.
 *              changeStateTo(ST_WAITING);
 *             }
 *             else
 *             {
 *                printf("I2C Failure 0x%02X (%s)\r\n", failCode, I2C_FailReasonToStr(failCode));
 *                changeStateTo(ST_ERROR);
 *             }
 *           }
 * 
 * 
**/

#ifndef _BF_I2C_H
#define _BF_I2C_H

#include <stdint.h>
#include <stdbool.h>
#include "stm32l4xx_hal.h"


#define I2C_MAX_NUM_BYTES_PER_PACKET (16)

typedef enum
{
    BF_I2C_PACKET_FAILURE__NONE =0,
    BF_I2C_PACKET_FAILURE__DEVICE_MISSING,
    BF_I2C_PACKET_FAILURE__RXTX_TIMEOUT,
    BF_I2C_PACKET_FAILURE__ADDR_TIMEOUT,
    BF_I2C_PACKET_FAILURE__NOT_ENOUGH_BYTES,
    BF_I2C_PACKET_FAILURE__UNKNOWN, 
} T_BF_I2C_FailReason;

typedef union
{
    uint8_t u8; ///< Address byte in 8-bit format (u8[0] = R/nW, u8[1:7] = Addr)

    struct
    {
        unsigned   isRead       : 1; ///< The read/nWrite bit of the I2C address.
        unsigned   addr_7bit  : 7; ///< The right aligned 7-bit slave address.
    }__attribute__((packed));

} __attribute__((packed)) T_I2C_SlaveAddress;


extern I2C_HandleTypeDef hi2c2;

//Forward define I2C Packet Handle
struct xBF_I2C_Packet;
typedef struct xBF_I2C_Packet T_BF_I2C_Packet;

//Forward define I2C Port Handle
struct xI2C_Port;
typedef struct xI2C_Port T_BF_I2C_Port;

///### BluFlex Framework Functions
/**
 * Initialize the I2C Port Manager task!
 */
//void I2C_Init(void);
//
//
///**
// * Update the I2C Manager Task
// * @param port Port object to update
// * @return Is ok to sleep?
// */
//bool I2C_Update(void);
//
//
///**
// * Input handler for BluFlex CLI.
// * @param tokens
// * @param nArgs
// */
//void I2C_ProcessInput(const char* tokens[], uint8_t nArgs);
//
//
/////### Constructor Functions
//
///**
// * Open an I2C port object. This "opens" the port in hardware.
// * @param id Hardware identifier of which I2C port to connect to
// * @param i2c_clock_freq I2C clock frequency (Hz)
// * @param useSMB Whether or not to enable SMBus level shifting
// * @return Pointer to new I2C object
// */
//T_BF_I2C_Port* I2C_OpenPort(uint8_t id, uint32_t i2c_clock_freq_hz, bool useSMB);
//
///**
// * Request a Packet from the I2C Manager. If successful, this Packet is now owned
// * by the Client. It can be reused as desired. All packets include I2C_MAX_NUM_BYTES_PER_PACKET
// * of local buffer storage.
// *
// * THIS SHOULD ONLY BE DONE AT INTIALIZATION (because we don't want to be doing
// * any sort of allocation during runtime for safety reasons).
// * @return Handle to packet, if successful
// */
//T_BF_I2C_Packet* I2C_AllocatePacket(void);
//
//
/////### I2C Port Object Functions
///**
// * Check if the given I2C Port Object is IDLE.
// * For port to be Idle, a packet must not be in progress AND there must not
// * be any packets on the queue.
// *
// * @param port Port to check
// * @return Is Port Idle?
// */
//bool I2C_IsPortIdle(const T_BF_I2C_Port* pPort);
//
//
///**
// * Check how many Packets are on the given Port's FIFO right now. This count
// * includes any Packets that are actively been transacted.
// *
// * @param port Handle of port to check
// * @return Number of Packets on given Port's FIFO
// */
//uint32_t I2C_GetNumPacketsOnPortQueue(const T_BF_I2C_Port* port);
//
///**
// * Get the present state of the given Port as a string.
// *
// * @param port Handle of Port
// * @return State name as null terminated string
// */
//const char* I2C_GetPortStateStr(const T_BF_I2C_Port* port);
//
///**
// * Print a ton of peripheral level debugging information to the screen for the
// * given I2C Port object. Useful for printing information about operaiton details
// * when a given situation arrises in Client code.
// * @param pPort Port object to check
// */
//void I2C_DebugPortPeripheral(const T_BF_I2C_Port* pPort);
//
//
/////### Packet Object Functions
//
///**
// * Push the given Packet onto the end of the given Port's packet FIFO.
// * Note this function does not do any construction/populating of packet object.
// * The Client is responsible for constructing Packet before pushing to the port.
// *
// * @param pPacket Packet Handle to send
// * @param pPort Port Handle for packet
// * @return Was Packet successfully added to Port's FIFO?
// */
//bool I2C_EnqueuePacket(T_BF_I2C_Packet* pPacket, T_BF_I2C_Port* pPort);
//
///**
// * Construct a WRITE packet, but do not enqueue yet. Must call I2C_EnqueuePacket
// * to actually send.
// * @param pPacket
// * @param slave_addr
// * @param data
// * @param nBytes
// * @param terminate
// * @return
// */
//bool I2C_MakeWritePacket(T_BF_I2C_Packet* pPacket, uint8_t slave_addr, const uint8_t* data, uint32_t nBytes, bool terminate);
//
///**
// * Construct a READ packet, but do not enqueue yet. Must call I2C_EnqueuePacket
// * to actually send.
// * @param pPacket
// * @param slave_addr
// * @param dest
// * @param nBytes
// * @return
// */
//bool I2C_MakeReadPacket(T_BF_I2C_Packet* pPacket, uint8_t slave_addr, uint8_t* dest, uint32_t nBytes);
//
//
///**
// * Construct and enqueue a message that writes an arbitrary number of bytes to
// * the given Slave Addr.
// * @param pPacket Packet to use for this message
// * @param port BluFlex I2C Port object to push to
// * @param slave_addr Slave address for message
// * @param data Pointer to location of first byte of data
// * @param nBytes Number of bytes of data to send
// * @param terminate Should message be terminated
// * @return Was message successfully enqueued
// */
//bool I2C_PushWriteTransfer(T_BF_I2C_Packet* pPacket, T_BF_I2C_Port* port, uint8_t slave_addr, const uint8_t* data, uint32_t nBytes, bool terminate);
//
///**
// * Construct and enqueue a message that reads an arbitrary number of bytes from
// * the given Slave Addr. Destination buffer must be large enough to store the
// * number of bytes in message!
// * @param pPacket Packet to use for this message
// * @param port BluFlex I2C Port object to push to
// * @param slave_addr Slave address for message
// * @param data Pointer to location of where to save first byte of data
// * @param nBytes Number of bytes of data to receive.
// * @return Was message successfully enqueued
// */
//bool I2C_PushReadTransfer(T_BF_I2C_Packet* pPacket, T_BF_I2C_Port* port, uint8_t slave_addr, uint8_t* dest, uint32_t nBytes);
//
//
//
///**
// * Check if the given packet's transaction had any failure conditions. If so,
// * return the FailureReason code associated with the type of error.
// * If no failure occurred, returns zero.  Should not be called until Packet is
// * no longer "Active".
// * @param pPacket
// * @return FALSE is no error occurs, otherwise FailureReason enum
// */
//T_BF_I2C_FailReason I2C_DidPacketFailureOccur(const T_BF_I2C_Packet* pPacket);
//
//
///**
// * Check if a packet is Active. A packet is Active if the I2C Manager has
// * ownership of packet (it enqueued or being sent). Once the packet is "done",
// * it is no longer Active.
// * @param pPacket
// * @return Is Packet Active?
// */
//bool I2C_IsPacketActive(const T_BF_I2C_Packet* pPacket);


/**
  * @brief  Read an amount of data in blocking mode from a specific memory address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef I2C_ReadMem( uint16_t MemAddress,
				  	  	  	  uint8_t *pData,
							  uint16_t Size,
							  uint32_t Timeout );


/**
  * @brief  Write an amount of data in blocking mode to a specific memory address
  * @param  MemAddress Internal memory address
  * @param  MemAddSize Size of internal memory address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
HAL_StatusTypeDef I2C_WriteMem( uint16_t MemAddress,
							   uint8_t *pData,
							   uint16_t Size,
							   uint32_t Timeout );


/**
 * Get the provided failure reason as a string. 
 * @param f FailReason to convert to string
 * @return 
 */
//const char* I2C_FailReasonToStr(T_BF_I2C_FailReason f);
#endif

