/**
 * @file spi_pic32.h
 * @brief Manages PIC's SPI module 
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-08-14
 *
 *
**/


#include "spi_hw.h"
#include "bluflex_core.h"

//############################ TYPES AND DEFINES ###############################




//################################# MODULE PROTOTYPES ##########################


//######################### MODULE VARS ########################################

static bool isPortMaster[NUM_SPI_PORTS];

//######################### MODULE CODE ########################################


static bool SPI_HW_Init(uint8_t portInx, bool isMaster, unsigned int spiClockHz, bool cpol, bool cpha, bool mssen)
{
    return false;
}

bool SPI_HW_Init_Master(uint8_t portInx, unsigned int spiClockHz, bool cpol, bool cpha, bool mssen)
{
    return SPI_HW_Init(portInx, true, spiClockHz, cpol, cpha, mssen);
}

bool SPI_HW_Init_Slave(uint8_t portInx, bool cpol, bool cpha)
{
    return SPI_HW_Init(portInx, false, 0, cpol, cpha, false);
}

bool SPI_HW_Enable(uint8_t portInx)
{
    return false;
}

bool SPI_HW_Disable(uint8_t portInx)
{
    return false;
}

bool SPI_HW_ManualStartMasterTxRx(uint8_t portInx)
{
    return false;
}



bool SPI_HW_StartSlaveRxInto(uint8_t portInx, uint8_t *ptr, int count)
{
    return false;
}


bool SPI_HW_StartMasterTxRx(uint8_t portInx, uint8_t* data, int count)
{
   return SPI_HW_SetupMasterTxRx(portInx, data, count, true);
}


bool SPI_HW_SetupMasterTxRx(uint8_t portInx, uint8_t* data, int count, bool startNow)
{
	return false;
}

bool SPI_HW_StopReceive(uint8_t portInx)
{
	return false;
}


bool SPI_HW_IsReceiveDone(uint8_t portInx, uint16_t* crc)
{
   return false;
}



bool SPI_HW_IsMaster(uint8_t portInx)
{
    return false;
}

void SPI_HW_Oneshot_Tx(uint8_t portInx, uint8_t byte)
{
    (portInx);
    (byte);
}

bool SPI_HW_Oneshot_IsDone(uint8_t portInx)
{
    return false;
}



uint8_t SPI_HW_TransmitRecieve(uint8_t portInx, uint8_t out)
{
    uint8_t toRet = 0xFF;
    return toRet;
}

// Flush RX and reset any overrun errors
bool SPI_HW_Flush(uint8_t portInx)
{
    return false;
}

