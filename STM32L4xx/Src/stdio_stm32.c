/**
 * @file stdio_stm32.c
 * @brief Allows stdio stack to work on STM32/GCC
 * @copyright 2017 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2017-05-12
 *
 *
**/



//######################### MODULE INCLUDES ##################################

#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "console.h"
//######################### MODULE DEFINES ###################################



//########################## MODULE TYPEDEFS ##################################



//######################### MODULE VARIABLES #################################


#include "noaa_bms.h"
#include "bluflex_core.h"
#include "afe_bq76920.h"
#include "adc.h"
#include "sys.h"
#include "sbs.h"

extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart3;

//This came from STMCUbe. TODO: Figure out if this is better than what we have.
//
//#ifdef __GNUC__
//  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
//     set to 'Yes') calls __io_putchar() */
//  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
//#else
//  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
//#endif /* __GNUC__ */

/* USER CODE BEGIN 0 */
/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
//PUTCHAR_PROTOTYPE
//{
//  /* Place your implementation of fputc here */
//  /* e.g. write a character to the EVAL_COM1 and Loop until the end of transmission */
//  while ( huart3.gState != HAL_UART_STATE_READY ) {;}
//
////  HAL_UART_Transmit_IT(&huart3, (uint8_t *)&ch, 1);
//  HAL_UART_Transmit_IT(&huart3, &ch, 2);
//
//  return ch;
//}



//######################### MODULE CODE ######################################

/*This is bottom level printf for ARM-GCC*/
int _write(int file, char *ptr, int len)
{
  int ii;

  for (ii = 0; ii < len; ii++)
  {
  	  Console_WriteByte(*ptr);
	  ptr++;
  }

  return len;
}


int puts(const char * s)
{
	putstr(s);
	putstr("\r\n");
	return 0;
}

void putstr(const char* s)
{
    while (*s != '\0')
    {
    	Console_WriteByte(*s);
    	s++;
    }

}

