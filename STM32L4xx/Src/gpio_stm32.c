
/**
 * @file gpio_pic32.c
 * @brief Device specific driver for GPIO interface.
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-03-09
 *
 *
**/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_gpio.h"

#include "gpio.h"
#include "str_lib.h"
#include "console.h"
#include "main.h"


///######################## PRIVATE FUNCTIONS ##################################

static GPIO_TypeDef * getGPIOx(char letter)
{
    switch (letter)
    {
        case 'A': return GPIOA;
        case 'B': return GPIOB;
        case 'C': return GPIOC;
        case 'D': return GPIOD;
        case 'E': return GPIOE;
        case 'F': return GPIOF;
        case 'G': return GPIOH;
    }

    return NULL;
}


char getPortLtr( GPIO_TypeDef* pBase)
{
    switch ((uint32_t) pBase)
    {
        case (uint32_t)GPIOA: return 'A';
        case (uint32_t)GPIOB: return 'B';
        case (uint32_t)GPIOC: return 'C';
        case (uint32_t)GPIOD: return 'D';
        case (uint32_t)GPIOE: return 'E';
        case (uint32_t)GPIOF: return 'F';
        case (uint32_t)GPIOH: return 'G';
        default:    return '\0';
    }
}


static uint8_t getPinNum(uint16_t pin)
{
    switch (pin)
    {
		case GPIO_PIN_0:  	return  0;
		case GPIO_PIN_1:  	return  1;
		case GPIO_PIN_2:  	return  2;
		case GPIO_PIN_3:  	return  3;
		case GPIO_PIN_4:  	return  4;
		case GPIO_PIN_5:  	return  5;
		case GPIO_PIN_6:  	return  6;
		case GPIO_PIN_7:  	return  7;
		case GPIO_PIN_8:  	return  8;
		case GPIO_PIN_9:  	return  9;
		case GPIO_PIN_10: 	return 10;
		case GPIO_PIN_11: 	return 11;
		case GPIO_PIN_12: 	return 12;
		case GPIO_PIN_13: 	return 13;
		case GPIO_PIN_14: 	return 14;
		case GPIO_PIN_15: 	return 15;
		default:			return 0xFF;
    }
}

#define PIN_NUMBER_TO_MASK(n) (1 << (n))
///###################### PUBLIC FUNCTIONS ########################################




bool GPIO_ReadInput(const T_Pin* pPin)
{
    if (pPin)
    {
        GPIO_PinState s =  HAL_GPIO_ReadPin( pPin->port, pPin->pin );
        return (s == GPIO_PIN_SET ? true : false);        
    }

    return false;
}

bool GPIO_WriteOutput(const T_Pin* pPin, bool setVal)
{
    if (pPin)
    {
        HAL_GPIO_WritePin( pPin->port, pPin->pin, (setVal ? GPIO_PIN_SET : GPIO_PIN_RESET));
        return true;
    }
    return false;
}


bool GPIO_CheckOutput(const T_Pin* pPin)
{
    if (pPin)
    {
        GPIO_PinState s =  HAL_GPIO_ReadPin( pPin->port, pPin->pin );
        return (s == GPIO_PIN_SET ? true : false);
    }
}



bool GPIO_SetDirection(const T_Pin* pPin, bool isInput)
{
   
    if (pPin)
    {
        GPIO_InitTypeDef GPIO_InitStruct;
        
        GPIO_InitStruct.Pin = pPin->pin;
  
        if (isInput)
        {
            GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        }
        else
        {
            GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
            GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        }

        GPIO_InitStruct.Pull = GPIO_NOPULL;
        
        HAL_GPIO_Init( pPin->port, &GPIO_InitStruct);
        
        return true;
    }

    return false;
}



bool GPIO_IsValidPin(const T_Pin* pPin)
{
    if ( pPin && pPin->port )
    {
        return true;
    }

    return false;
}

bool GPIO_PrintPin(const T_Pin* pPin)
{
    if (pPin)
    {
        printf("%c%d", getPortLtr(pPin->port), getPinNum(pPin->pin));
        return true;
    }
    return false;

}

bool GPIO_ToggleOutput(const T_Pin* pPin)
{
    if (pPin)
    {
        HAL_GPIO_TogglePin( pPin->port, pPin->pin );
        
        return true;
    }
    return false;

}

void GPIO_ProcessInput(const char* tokens[], uint8_t nArgs)
{
    if (nArgs == 0 || tokens[0][0] == '?')
    {
      puts("info [Port] [Pin#]");
    }
    else if (stricmp(tokens[0], "info") == 0)
    {
      puts("TODO :(");
#if 0
        if (nArgs == 3)
        {
            char portLetter = tokens[1][0];
            int8_t pin = atoi(tokens[2]);
            if (pin >= 0 && pin < 32)
            {
                T_Pin this = {portLetter, pin};
                if (GPIO_IsValidPin(&this))
                {
                    GPIO_PrintPin(&this);
                    putstr(" is an ");
                    bool isInput = *(getTRISx(this.portLetter)) & (1 << this.pin);
                    putstr(isInput ? "INPUT" : "OUTPUT");
                    putstr(". Level = ");
                    if (isInput)
                    {
                    	Console_WriteByte(GPIO_ReadInput(&this) ? '1' : '0');

                    }
                    else
                    {
                    	Console_WriteByte(GPIO_CheckOutput(&this) ? '1' : '0');
                    }
                    putstr("\r\n");
                }
                else
                {
                    GPIO_PrintPin(&this);
                    puts(" is not a valid pin!");
                }
            }
            else
            {
                puts("Invalid pin number.");
            }
        }
        else
        {
            puts("info [Port] [Pin#]");
        }
#endif
    }
    else
    {
    	Console_WriteByte('?');
    }
}










void GPIO_SetPin_Debug1(void)
{
	HAL_GPIO_WritePin(Debug1_GPIO_Port, Debug1_Pin, GPIO_PIN_SET);
}

void GPIO_ClearPin_Debug1(void)
{
	HAL_GPIO_WritePin(Debug1_GPIO_Port, Debug1_Pin, GPIO_PIN_RESET);
}

void GPIO_SetPin_Debug2(void)
{
	HAL_GPIO_WritePin(Debug2_GPIO_Port, Debug2_Pin, GPIO_PIN_SET);
}

void GPIO_ClearPin_Debug2(void)
{
	HAL_GPIO_WritePin(Debug2_GPIO_Port, Debug2_Pin, GPIO_PIN_RESET);
}

void GPIO_SetPin_Debug3(void)
{
	HAL_GPIO_WritePin(Debug3_GPIO_Port, Debug3_Pin, GPIO_PIN_SET);
}

void GPIO_ClearPin_Debug3(void)
{
	HAL_GPIO_WritePin(Debug3_GPIO_Port, Debug3_Pin, GPIO_PIN_RESET);
}

void GPIO_SetPin_Debug4(void)
{
	HAL_GPIO_WritePin(Debug4_GPIO_Port, Debug4_Pin, GPIO_PIN_SET);
}

void GPIO_ClearPin_Debug4(void)
{
	HAL_GPIO_WritePin(Debug4_GPIO_Port, Debug4_Pin, GPIO_PIN_RESET);
}

void GPIO_SetPin_Debug5(void)
{
	HAL_GPIO_WritePin(Debug5_GPIO_Port, Debug5_Pin, GPIO_PIN_SET);
}

void GPIO_ClearPin_Debug5(void)
{
	HAL_GPIO_WritePin(Debug5_GPIO_Port, Debug5_Pin, GPIO_PIN_RESET);
}

void GPIO_SetPin_Debug6(void)
{
	HAL_GPIO_WritePin(Debug6_GPIO_Port, Debug6_Pin, GPIO_PIN_SET);
}

void GPIO_ClearPin_Debug6(void)
{
	HAL_GPIO_WritePin(Debug6_GPIO_Port, Debug6_Pin, GPIO_PIN_RESET);
}







