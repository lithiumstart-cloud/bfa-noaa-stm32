/**
 * @file mcu_stm32.c
 * @brief Implements misc. device specific system functions
 * @copyright 2017 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2017-03-03
 *
**/

//############################## INCLUDES ######################################
#include "bluflex_core.h"
#include "mcu_hw.h"
#include "print_as.h"

#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_gpio.h"
#include "stm32l4xx_hal_rcc.h"

#include "main.h"

//############################## DEFINES ######################################
#define LSI_TIME	3125	// Low Speed Internal Clock (1 / 32KHz) * 100

//########################### PRIVATE VARIABLES ################################


//############################## PUBLIC CODE ###################################
extern void Error_Handler(void);

void MCU_HW_Restart(void)
{
   HAL_NVIC_SystemReset();
}

void MCU_HW_Configure(uint32_t sysClock_Hz)
{
}

void MCU_HW_DisableInterrupts()
{
	__disable_irq();
}

void MCU_HW_EnableInterrupts()
{
	__enable_irq();
}

T_MCU_ResetType MCU_HW_GetLastReset(void)
{
    T_MCU_ResetType toRet;

    if (__HAL_RCC_GET_FLAG(RCC_FLAG_FWRST))
	{
		toRet = RST_FIREWALL;
	}
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_OBLRST))
	{
		toRet = RST_OPTION_BYTE_LOADER;
	}
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST))
	{
		toRet = RST_WDT;
	}
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_BORRST))
	{
		toRet = RST_BROWN_OUT;
	}
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST))
	{
		toRet = RST_SOFTWARE;
	}
    else if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST))
	{
		toRet = RST_WINDOW_WD;
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST))
	{
		toRet = RST_LOW_POWER;
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST))
	{
		toRet = RST_POWER_ON;
	}
	else
	{
		toRet = RST_UNKNOWN;
	}

    __HAL_RCC_CLEAR_RESET_FLAGS();

    return toRet;
}

extern IWDG_HandleTypeDef hiwdg;  //<SRB>### REMOVE THIS IN FINAL BUILD ###
uint32_t MCU_HW_GetWdtPeriod_ms(void)
{
	uint32_t prescaler = 4 * (1 << hiwdg.Init.Prescaler);	// 4 * 2^Prescaler
	uint32_t reloadValue = hiwdg.Init.Reload + 1;
	uint32_t result_ms = (LSI_TIME * prescaler * reloadValue)/100000;

	return result_ms;
}

void MCU_HW_ClearWDT(void)
{
    if(HAL_IWDG_Refresh(&hiwdg) != HAL_OK)
	{
    	LOG_Event(LOG_LVL_INFO, "MCU_STM32: Internal WDG couldn't refresh on time!");
	}
}

//void MCU_HW_EnableWDT(void)
//{
//    if(HAL_IWDG_Init(&IwdgHandle) != HAL_OK)
//	{
//    	LOG_Event(LOG_LVL_INFO, "MCU_STM32: Internal WDG initialization didn't succeed!");
//	}
//}
//
//void MCU_HW_DisableWDT(void)
//{
//	// IWDG can't be disabled once it is started
//}
//
//bool MCU_HW_HasWdtExpired(void)
//{
//    return 0;
//}

void MCU_HW_SleepNow()
{
	HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}

//##################### GENERAL EXCEPTION HANDLER ##############################







