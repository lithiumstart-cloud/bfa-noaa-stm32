/**
 * @file uart_stm32.c
 * @brief Hardware UART driver
 * @copyright 2017 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2017-03-03
 *
 *
**/
/************** About HAL_UART API **************************************

(#) There are two mode of transfer:
       (+) Blocking mode: The communication is performed in polling mode.
           The HAL status of all data processing is returned by the same function
           after finishing transfer.
       (+) Non-Blocking mode: The communication is performed using Interrupts
           or DMA, These API's return the HAL status.
           The end of the data processing will be indicated through the
           dedicated UART IRQ when using Interrupt mode or the DMA IRQ when
           using DMA mode.
           The HAL_UART_TxCpltCallback(), HAL_UART_RxCpltCallback() user callbacks
           will be executed respectively at the end of the transmit or Receive process
           The HAL_UART_ErrorCallback()user callback will be executed when a communication error is detected

    (#) Blocking mode API's are :
        (+) HAL_UART_Transmit()
        (+) HAL_UART_Receive()

    (#) Non-Blocking mode API's with Interrupt are :
        (+) HAL_UART_Transmit_IT()
        (+) HAL_UART_Receive_IT()
        (+) HAL_UART_IRQHandler()

    (#) Non-Blocking mode API's with DMA are :
        (+) HAL_UART_Transmit_DMA()
        (+) HAL_UART_Receive_DMA()
        (+) HAL_UART_DMAPause()
        (+) HAL_UART_DMAResume()
        (+) HAL_UART_DMAStop()

    (#) A set of Transfer Complete Callbacks are provided in Non_Blocking mode:
        (+) HAL_UART_TxHalfCpltCallback()
        (+) HAL_UART_TxCpltCallback()
        (+) HAL_UART_RxHalfCpltCallback()
        (+) HAL_UART_RxCpltCallback()
        (+) HAL_UART_ErrorCallback()

    (#) Non-Blocking mode transfers could be aborted using Abort API's :
        (+) HAL_UART_Abort()
        (+) HAL_UART_AbortTransmit()
        (+) HAL_UART_AbortReceive()
        (+) HAL_UART_Abort_IT()
        (+) HAL_UART_AbortTransmit_IT()
        (+) HAL_UART_AbortReceive_IT()

    (#) For Abort services based on interrupts (HAL_UART_Abortxxx_IT), a set of Abort Complete Callbacks are provided:
        (+) HAL_UART_AbortCpltCallback()
        (+) HAL_UART_AbortTransmitCpltCallback()
        (+) HAL_UART_AbortReceiveCpltCallback()

    (#) In Non-Blocking mode transfers, possible errors are split into 2 categories.
        Errors are handled as follows :
       (+) Error is considered as Recoverable and non blocking : Transfer could go till end, but error severity is
           to be evaluated by user : this concerns Frame Error, Parity Error or Noise Error in Interrupt mode reception .
           Received character is then retrieved and stored in Rx buffer, Error code is set to allow user to identify error type,
           and HAL_UART_ErrorCallback() user callback is executed. Transfer is kept ongoing on UART side.
           If user wants to abort it, Abort services should be called by user.
       (+) Error is considered as Blocking : Transfer could not be completed properly and is aborted.
           This concerns Overrun Error In Interrupt mode reception and all errors in DMA mode.
           Error code is set to allow user to identify error type, and HAL_UART_ErrorCallback() user callback is executed.

    -@- In the Half duplex communication, it is forbidden to run the transmit
        and receive process in parallel, the UART state HAL_UART_STATE_BUSY_TX_RX can't be useful.

*/



//######################### MODULE INCLUDES ##################################

#include <string.h>
#include <stdint.h>
#include <stdbool.h>


#include "uart_hw.h"
#include "bluflex_core.h"

#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_uart.h"

//######################### MODULE DEFINES ###################################



//########################## MODULE TYPEDEFS ##################################



//######################### MODULE VARIABLES #################################

extern UART_HandleTypeDef huart3;


//static volatile bool transmittingNow = 0;

 uint8_t byteToTransmit;
static uint8_t byteToReceive;
//static volatile bool isRxByteWaiting = 0;

static bool wasInitSuccess = false;
//######################### MODULE CODE ######################################

bool getUartHwInitStats(void)
{
   return wasInitSuccess;
}

bool UART_HW_Open(uint8_t id, uint32_t desired_baud)
{
//  huart.Instance = USART3;
//  huart.Init.BaudRate = desired_baud;
//  huart.Init.WordLength = UART_WORDLENGTH_8B;
//  huart.Init.StopBits = UART_STOPBITS_1;
//  huart.Init.Parity = UART_PARITY_NONE;
//  huart.Init.Mode = UART_MODE_TX_RX;
//  huart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
//  huart.Init.OverSampling = UART_OVERSAMPLING_16;
//  huart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
//  huart.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
//
//  if (HAL_UART_Init(&huart) != HAL_OK)
//  {
//    return false;
//  }

  //Enable receive
//   HAL_StatusTypeDef halStatus = HAL_UART_Receive_IT(&huart3, &byteToReceive, 1);

//   wasInitSuccess = (halStatus == HAL_OK);

   return true;
}

bool UART_HW_SendByte(uint8_t id, uint8_t data)
{
	UNUSED(id);

	if ( HAL_UART_GetState(&huart3) == HAL_UART_STATE_READY )
	{
		HAL_UART_Transmit_IT(&huart3, &data, 1);
		return true;
	}

	return false;

}


void UART_HW_SendBlockingString(uint8_t id, const char* data, uint16_t len)
{
    HAL_UART_Transmit_IT(&huart3, (uint8_t*) data, len);
}

uint8_t UART_HW_ReadByte(uint8_t id)
{
    uint8_t byteToReturn;

    UNUSED(id);

	//turn on interrupt
	HAL_UART_Receive_IT(&huart3, &byteToReturn, 1);

	return byteToReturn;
}

bool UART_HW_IsRxReady(uint8_t id)
{
    UNUSED(id);

    if ( HAL_UART_GetState(&huart3) != HAL_UART_STATE_READY )
	{
		return(false);
	}
	else
	{
		return( (__HAL_UART_GET_FLAG(&huart3, UART_FLAG_RXNE) == SET));
	}
}

bool UART_HW_IsTxReady(uint8_t id)
{
//	HAL_UART_StateTypeDef uartState = HAL_UART_GetState(&huart3);
//    return ( uartState == HAL_UART_STATE_READY );
	return( huart3.gState == HAL_UART_STATE_READY );

}



void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  UNUSED(huart);
  
  /* Clear this flag so next byte can be pushed out. */
//  transmittingNow = false;
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  UNUSED(huart);
  
  /* Set flag to indicate a byte it waiting to read. */
//  isRxByteWaiting = true;
}


/**
* @brief This function handles USART3 global interrupt.
*/
//void USART3_IRQHandler(void)
//{
//    HAL_UART_IRQHandler(&huart3);
//}

