/**
 * @file tmr_stm32.c
 * @brief Hardware Timer driver
 * @copyright 2017 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2017-03-03
 *
 *
**/


//############################## INCLUDES ######################################

#include "bluflex_core.h"
#include "tmr_hw.h"

#include "stm32l4xx_hal.h"

//############################## DEFINES ######################################



//########################### PRIVATE VARIABLES ################################


//############################## PROTOTYPES ####################################


//############################## PUBLIC CODE ###################################
void TMR_HW_Init(uint32_t tickFreq_Hz)
{
   /**Configure the Systick interrupt time */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/tickFreq_Hz);

  /**Configure the Systick */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
  
  /**Enable MSI Auto calibration  */
  HAL_RCCEx_EnableMSIPLLMode();

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

void TMR_HW_PauseClock(void)
{
    HAL_SuspendTick();
}

void TMR_HW_ResumeClock(void)
{
  HAL_ResumeTick();
}


uint32_t TMR_HW_GetClock_ticks(void)
{
  return HAL_GetTick();
}

void TMR_HW_SetClock_ticks(uint32_t x)
{
  //todo
  UNUSED(x);
  
}

void TMR_HW_MicroTimer_Start(uint16_t time_us)
 {
      UNUSED(time_us);
 }

 bool TMR_HW_MicroTimer_HasExpired()
 {
    return false;
 }


