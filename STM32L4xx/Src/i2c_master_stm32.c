///**
// * @file i2c_hw_pic32.c
// * @brief  Implements I2C Master Hardware Interface on PIC32
// * @copyright 2016 by Lithiumstart Inc
// * @author A. Kessler
// * @date   2016-03-21
// *
//**/
////######################### MODULE INCLUDES ##################################
//
////#include "i2c_master_hw.h"
//
//#include <bluflex_core.h>
//#include "stm32l4xx_hal.h"
//
////###################### MODULE DEFINES ###################################
//
//#define I2C_MAX_NUM_BYTES_PER_PACKET (16)
//#define I2C_QUEUE_LENGTH (12)
//#define I2C_ERROR_TIMEOUT_MS (15)
//#define I2C_MAX_ALLOWABLE_TERMINATION_MS (200)
//#define I2C_SEND_BYTE_TIMEOUT_MS (100)
//#define I2C_GET_BYTE_TIMEOUT_MS (100)
////######################### MODULE TYPEDEF ###################################
//typedef enum
//{
//    //Master Transmit
//    MSTR_IDLE = 0,
//    MSTR_TRANSMITTING,
//    MSTR_RECEIVING,
//    MSTR_ERROR,
//
//} T_State;
//
//typedef struct
//{
//    uint8_t slaveAddr;
//
//    uint8_t nBytes; ///< How many bytes are in this packet
//
//    uint8_t* dest; ///<Where is written data coming from/going?
//
//    struct
//    {
//    	unsigned isWrite : 1; ///< True if this is a write packet
//        unsigned terminatePacket : 1; ///< Should you send a stop command after this packet?
//        unsigned                 :  15;
//    }__attribute__((packed));
//
//    uint8_t data[I2C_MAX_NUM_BYTES_PER_PACKET]; ///< Working buffer for this packet. Data to transmit is copied here when queued up
//
//} __attribute__((packed)) T_Packet;
//
//
//struct sI2C_Port
//{
//
//	I2C_HandleTypeDef* hi2c; //STM32 HAL I2C handler
//
//    T_Packet packetBuffer[I2C_QUEUE_LENGTH];
//    T_FIFO packetFifo;
//
//    T_Packet workingPacket;
//    T_State currentState;
//    T_Timer tmr;
//
//    uint8_t id;
//};
//
//struct xI2C_Port
//{
//    volatile T_I2Cx* pI2Cx; ///< Pointer to I2C registers for this port
//    uint8_t peripheralPortId;
//
//    //Packet Queue (implemented as LL)
//    struct
//    {
//        T_BF_I2C_Packet* pHead;
//        T_BF_I2C_Packet* pTail;
//        uint8_t length;
//    } packetQueue;
//
//
//    //Working packet info...
//    uint8_t numBytesSent;
//    uint8_t numBytesGot;
//
//    T_State presentState;
//    T_Timer tmr;
//};
//
//
////######################### MODULE VARIABLES #################################
//static volatile bool txComplete = 0;
//static volatile bool rxComplete = 0;
//
//
////###################### MODULE PROTOTPYES ###################################
//static I2C_HandleTypeDef hi2c2; //STM32 HAL I2C handler -- put in array
//
////######################### MODULE CODE ######################################
//
////##################### OBJECT METHODS #########################################
//
//T_BF_I2C_Port* I2C_Init(uint8_t id, uint32_t i2c_clock_freq, bool useSMB)
//{
//	UNUSED(id);
//	UNUSED(i2c_clock_freq);
//	UNUSED(useSMB);
//
//	T_BF_I2C_Port* this = Heap_Allocate(sizeof(T_BF_I2C_Port));
//	if (!this)
//	{
//		printf("FAILED to malloc BF I2C[%d]\r\n", id);
//		return NULL;
//	}
//
//	if(!FIFO_Init(&this->packetFifo, this->packetBuffer, I2C_QUEUE_LENGTH, sizeof(T_Packet)))
//	{
//		printf("FAILED to initialize FIFO on I2C[%d].\r\n", id);
//		return NULL;
//	}
//
//	this->id = id;
//
//	this->hi2c = &hi2c2;
//	this->hi2c->Instance = I2C2;
//	this->hi2c->Init.Timing = 0x30408CFF; //100Khz, probs?
//	this->hi2c->Init.OwnAddress1 = 0;
//	this->hi2c->Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
//	this->hi2c->Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
//	this->hi2c->Init.OwnAddress2 = 0;
//	this->hi2c->Init.OwnAddress2Masks = I2C_OA2_NOMASK;
//	this->hi2c->Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
//	this->hi2c->Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
//
//	  if (HAL_I2C_Init(this->hi2c) != HAL_OK)
//	  {
//		 puts("Failed to init HAL_I2C");
//		 return false;
//	  }
//
//	  //TODO: Do we need this???
//		/**Configure Analog filter */
//	  if (HAL_I2CEx_ConfigAnalogFilter((this->hi2c), I2C_ANALOGFILTER_ENABLE) != HAL_OK)
//	  {
//		  puts("Failed to configure I2C analog filter");
//		  return false;
//	  }
//
//
//	  this->currentState = MSTR_IDLE;
//
//	  rxComplete = 0;
//	  txComplete = 0;
//	  return this;
//
//
//}
//
///**
// * Update function - called as frequently as possible.
// */
//bool I2C_Update(T_BF_I2C_Port* this)
//{
//
//	if (!this)
//	{
//		return false;
//	}
//
//	switch (this->currentState)
//	{
//		case MSTR_IDLE:
//			if (FIFO_DequeueInto(&(this->packetFifo), &(this->workingPacket)))
//			{
//				//Packet has been loaded into working packet.
//				if (this->workingPacket.isWrite)
//				{
//					HAL_I2C_Master_Transmit_IT(this->hi2c, this->workingPacket.slaveAddr, this->workingPacket.data, this->workingPacket.nBytes);
//					this->currentState = MSTR_TRANSMITTING;
//				}
//				else
//				{
//					HAL_I2C_Master_Receive_IT(this->hi2c, this->workingPacket.slaveAddr, this->workingPacket.dest, this->workingPacket.nBytes);
//					this->currentState = MSTR_RECEIVING;
//				}
//			}
//		break;
//
//		case MSTR_RECEIVING:
//			if (rxComplete)
//			{
//				//Clear flag
//				rxComplete = 0;
//				this->currentState = MSTR_IDLE;
//			}
//		break;
//
//		case MSTR_TRANSMITTING:
//			if (txComplete)
//			{
//				txComplete = 0;
//				this->currentState = MSTR_IDLE;
//			}
//		break;
//
//
//
//	}
//
//
//	return true;
//
//}
//
//bool I2C_PushWriteByte(T_BF_I2C_Port* port, uint8_t slave_addr, uint8_t data, bool terminate)
//{
//    return I2C_PushWriteTransfer(port, slave_addr, &data, 1, terminate);
//}
//
//bool I2C_PushDummyByte(T_BF_I2C_Port* port, uint8_t dummy_addr)
//{
//    return I2C_PushWriteTransfer(port, dummy_addr, NULL, 0, true);
//
//}
//
//bool I2C_PushWriteTransfer(T_BF_I2C_Port* this, uint8_t slave_addr, const uint8_t* data, uint32_t nBytes, bool terminate)
//{
//
//	  //Ok, so normally we would use FIFO functions to pop/push, but to avoid
//	  //extra copying of data, we directly access FIFO internals.
//
//	    if (this->packetFifo.numFilled < this->packetFifo.maxLength)
//	    {
//	        T_Packet* thisPacket = &(this->packetBuffer[this->packetFifo.pushInx]);
//	        this->packetFifo.pushInx++;
//	        if (this->packetFifo.pushInx >= this->packetFifo.maxLength)
//	        {
//	            this->packetFifo.pushInx = 0;
//	        }
//	        this->packetFifo.numFilled++;
//
//	        thisPacket->slaveAddr = slave_addr;
//	        thisPacket->isWrite = 1;
//	        //Copy data over
//		    memcpy(thisPacket->data, data, nBytes);
//
//	        thisPacket->nBytes = nBytes;
//	        thisPacket->terminatePacket = terminate;
//
//
//	        return true;
//	    }
//	    else
//	    {
//	        puts("I2C: Buffer overflow pushing write!");
//	    }
//
//	    return false;
//
//}
//
//
//bool I2C_PushReadTransfer(T_BF_I2C_Port* this, uint8_t slave_addr, uint8_t* dest, uint32_t nBytes)
//{
//
//    if (this->packetFifo.numFilled < this->packetFifo.maxLength)
//    {
//        T_Packet* thisPacket = &(this->packetBuffer[this->packetFifo.pushInx]);
//
//        this->packetFifo.pushInx++;
//        if (this->packetFifo.pushInx >= this->packetFifo.maxLength)
//        {
//            this->packetFifo.pushInx = 0;
//        }
//        this->packetFifo.numFilled++;
//
//        thisPacket->slaveAddr = slave_addr;
//        thisPacket->isWrite = 0;
//
//
//        thisPacket->dest = dest;
//
//        thisPacket->nBytes = nBytes;
//        thisPacket->terminatePacket = true;
//
//        return true;
//    }
//    else
//    {
//        puts("I2C: Buffer overflow pushing read!");
//    }
//
//
//    return false;
//
//
//}
//bool I2C_PushReadByte(T_BF_I2C_Port* this, uint8_t slave_addr, uint8_t* dest)
//{
//     return I2C_PushReadTransfer(this, slave_addr, dest, 1);
//}
//
//
//
//bool I2C_IsPortIdle(const T_BF_I2C_Port* this)
//{
//    if (this)
//    {
//        return (this->currentState == MSTR_IDLE && (this->packetFifo.numFilled == 0));
//    }
//
//    return false;
//}
//
//
//uint32_t I2C_GetQueueNumFilled(const T_BF_I2C_Port* this)
//{
//	if (this)
//	{
//		return this->packetFifo.numFilled;
//	}
//
//	 return UINT32_MAX;
//}
//
//
//const char* I2C_GetStateStr(const T_BF_I2C_Port* this)
//{
//
//	if (this)
//	{
//		switch (this->currentState)
//		{
//			case MSTR_IDLE: return "Idle";
//			case MSTR_RECEIVING: return "Recieving";
//			case MSTR_TRANSMITTING: return "Transmitting";
//			default: return "???";
//		}
//	}
//    return "NULL";
//
//}
//
//
//void I2C_DebugPeripheral(const T_BF_I2C_Port* this)
//{
//    UNUSED(this);
//}
//
//
//
//void I2C_ProcessInput(const char* tokens[], uint8_t nArgs)
//{
//    UNUSED(tokens);
//    UNUSED(nArgs);
//
//
//}
//
//void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *_hi2c)
//{
//	if (_hi2c->Instance == I2C2)
//	{
//		txComplete = true;
//	}
//}
//
//void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *_hi2c)
//{
//	if (_hi2c->Instance == I2C2)
//	{
//		rxComplete = true;
//	}
//}
//
//
//void I2C2_EV_IRQHandler(void)
//{
//  HAL_I2C_EV_IRQHandler(&hi2c2);
//}
