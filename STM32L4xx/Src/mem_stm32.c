/**
 * @file mem_stm32.c
 * @brief Implements NVRAM interface on STM32
 * @copyright 2020 by Eagle Picher Technologies
 * @author Miroslav Grubic
 * @date   08-14-2020
**/

#include "bluflex_core.h"
#include "mem_hw.h"
#include "eeprom_emul.h"


//############################## DEFINES ######################################
#define DATA_EE_SIZE        (1000) // Total number of 32-bit data
#define PWR_FLAG_WUF 		PWR_FLAG_WUF1

//######################### PRIVATE FUNCTIONS PROTOTYPES ######################
static void PVD_Config(void);

//########################### PRIVATE VARIABLES ###############################
__IO uint32_t ErasingOnGoing = 0;

//########################### PUBLIC FUNCTIONS #################################
T_MEM_HW_Result MEM_HW_Init(void)
{
	EE_Status ee_status = EE_OK;

	/* Enable and set FLASH Interrupt priority */
	/* FLASH interrupt is used for the purpose of pages clean up under interrupt */
	HAL_NVIC_SetPriority(FLASH_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(FLASH_IRQn);

	/* Unlock the Flash Program Erase controller */
	HAL_FLASH_Unlock();

	/* Enable Power Control clock */
	__HAL_RCC_PWR_CLK_ENABLE();

	/* PVD interrupt is used to suspend the current application flow in case
	 a power-down is detected, allowing the flash interface to finish any
	 ongoing operation before a reset is triggered. */
	PVD_Config();

	/* Set EEPROM emulation firmware to erase all potentially incompletely erased
	   pages if the system came from an asynchronous reset. Conditional erase is
	   safe to use if all Flash operations where completed before the system reset */
	if(__HAL_PWR_GET_FLAG(PWR_FLAG_SB) == RESET)
	{
	    /* System reset comes from a power-on reset: Forced Erase */
	    /* Initialize EEPROM emulation driver (mandatory) */
	    ee_status = EE_Init(EE_FORCED_ERASE);

	    if(ee_status != EE_OK)
	    {
	    	LOG_Event(LOG_LVL_INFO, "MEM_STM32: EEPROM initialization after POR didn't succeed!");
	    }
	}
	else
	{
	    /* Clear the Standby flag */
	    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);

	    /* Check and Clear the Wakeup flag */
	    if(__HAL_PWR_GET_FLAG(PWR_FLAG_WUF) != RESET)
	    {
	    	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF);
	    }

	    /* System reset comes from a STANDBY wakeup: Conditional Erase*/
	    /* Initialize EEPROM emulation driver (mandatory) */
	    ee_status = EE_Init(EE_CONDITIONAL_ERASE);

	    if(ee_status != EE_OK)
	    {
	    	LOG_Event(LOG_LVL_INFO, "MEM_STM32: EEPROM initialization after Standby didn't succeed!");
	    }
	}

	/* Lock the Flash Program Erase controller */
	HAL_FLASH_Lock();

	return (T_MEM_HW_Result) ee_status;
}


T_MEM_HW_Result MEM_HW_Write(unsigned int data, unsigned int addr)
{
	EE_Status ee_status = EE_OK;

	HAL_FLASH_Unlock();

	/* Wait any cleanup is completed before accessing flash again */
	while (ErasingOnGoing == 1) { }

	ee_status = EE_WriteVariable32bits((uint16_t)addr, data);

	/* Start cleanup IT mode, if cleanup is needed */
	if ((ee_status & EE_STATUSMASK_CLEANUP) == EE_STATUSMASK_CLEANUP)
	{
		ErasingOnGoing = 1;
		ee_status |= EE_CleanUp_IT();
	}

	HAL_FLASH_Lock();

	if((ee_status & EE_STATUSMASK_ERROR) == EE_STATUSMASK_ERROR)
	{
		Error_Handler();
	}

    return (T_MEM_HW_Result) ee_status;
}

T_MEM_HW_Result MEM_HW_Read(unsigned int *data, unsigned int addr)
{
	EE_Status ee_status = EE_OK;
    ee_status = EE_ReadVariable32bits((uint16_t)addr, (uint32_t *)data);

    if(ee_status == EE_NO_DATA)
    {
    	ee_status -= 6;		/* This is done so there is no need to change code in mem.c where return from this
    						 * function is compared against ADDR_NOT_FOUND = 1   */
    }

    return (T_MEM_HW_Result) ee_status;
}

uint32_t MEM_HW_GetDiskSize_Words(void)
{
	return DATA_EE_SIZE;
}

//########################## PRIVATE FUNCTIONS #################################

/**
  * @brief  FLASH end of operation interrupt callback.
  * @param  ReturnValue: The value saved in this parameter depends on the ongoing procedure
  *                  Mass Erase: Bank number which has been requested to erase
  *                  Page Erase: Page which has been erased
  *                    (if 0xFFFFFFFF, it means that all the selected pages have been erased)
  *                  Program: Address which was selected for data program
  * @retval None
  */
void HAL_FLASH_EndOfOperationCallback(uint32_t ReturnValue)
{
	/* Call CleanUp callback when all requested pages have been erased */
	if (ReturnValue == 0xFFFFFFFF)
	{
		EE_EndOfCleanup_UserCallback();
	}
}

/**
  * @brief  Clean Up end of operation interrupt callback.
  * @param  None
  * @retval None
  */
void EE_EndOfCleanup_UserCallback(void)
{
	ErasingOnGoing = 0;
}

/**
  * @brief  Programmable Voltage Detector (PVD) Configuration
  *         PVD set to level 6 for a threshold around 2.9V.
  * @param  None
  * @retval None
  */
static void PVD_Config(void)
{
	PWR_PVDTypeDef sConfigPVD;
	sConfigPVD.PVDLevel = PWR_PVDLEVEL_6;
	sConfigPVD.Mode     = PWR_PVD_MODE_IT_RISING;

	if (HAL_PWR_ConfigPVD(&sConfigPVD) != HAL_OK)
	{
		LOG_Event(LOG_LVL_INFO, "MEM_STM32: PVD Configuration didn't succeed!");
	}

	/* Enable PVD */
	HAL_PWR_EnablePVD();

	/* Enable and set PVD Interrupt priority */
	HAL_NVIC_SetPriority(PVD_PVM_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(PVD_PVM_IRQn);
}
