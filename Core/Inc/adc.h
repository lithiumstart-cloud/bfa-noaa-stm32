/**
 * @file ADC.h
 * @brief Generic ADC Interface
 * @copyright 2015 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2015-03-04
 *
**/

#ifndef ADC_H
#define	ADC_H

#include "bluflex_core.h"

#define NUM_HEATER_THERMISTORS 			(4)


typedef struct
{
   T_SignalNode* onboardTempRoot;
   T_SignalNode* heaterTempRoot;
   T_SignalNode* baseboardTemp;
} T_ADC_Sigs;


void ADC_Init(const T_ADC_Sigs*);
bool ADC_Update(void);

void ADC_StartConversion(void);


void ADC_DrawInit(void);
void ADC_DrawUpdate(void);
void ADC_ProcessInput(const char* tokens[], uint8_t nArgs);


#endif

