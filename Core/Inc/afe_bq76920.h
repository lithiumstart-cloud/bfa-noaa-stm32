/**
 * @file afe_bq76920.h
 * @brief AFE driver for TI BQ76920
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-11-24
 *
**/

#ifndef AFE_BQ76920_H
#define	AFE_BQ76920_H

#include <stdint.h>
#include <stdbool.h>
#include <bluflex_core.h>


typedef struct
{
   T_SignalNode* cellVoltageRoot;
   T_SignalNode* currentRoot;
   T_Relay* dischargeRelay;
} T_AFE_BQ76920_Sigs;


void AFE_Init(T_AFE_BQ76920_Sigs* pSigs);
bool AFE_Update(void);

uint8_t AFE_GetBalanceMask(void);
void    AFE_SetBalanceMask(uint8_t balMask);

void AFE_TriggerReset(void);
void AFE_ReleaseReset(void);


void AFE_DrawInit(void);
void AFE_DrawUpdate(void);
void AFE_ProcessInput(const char* tokens[], uint8_t nArgs);


void AFE_EnableShortCircuitDetect(void);
void AFE_DisableShortCircuitDetect(void);

HAL_StatusTypeDef bq769x0_ClearAlerts(void);

#endif

