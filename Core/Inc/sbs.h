/**
 * @file sbs.h
 * @brief Implements the Smart Battery Specification (SBS) software layer.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-04-16
 *
**/

#ifndef SBS_H
#define SBS_H

#include <stdint.h>
#include <stdbool.h>
#include <bluflex_core.h>

#include "smbus.h" //need this for T_Packet
#include "sbs_defs.h"

typedef struct
{
    T_SignalNode* ambientTemp;
    T_SignalNode* balanceTemp;
    T_SignalNode* boardTemp;
    T_SignalNode* cellTemp;
} T_SBS_Sigs;

void SBS_Init(const T_CoreSignals*, T_SBS_Sigs*);
bool SBS_Update(void);


bool SBS_GetRequestedData(T_SMBUS_Packet*);
bool SBS_ProcessWrittenData(T_SMBUS_Packet*);
void SBS_ProcessInput(const char* tokens[], uint8_t nTokens);


#endif
