/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
//#define HAS_EXTERNAL_WDT 1
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/**
 * Functions for setting and clearing debug pins
 */
void GPIO_SetPin_Debug1(void);
void GPIO_SetPin_Debug2(void);
void GPIO_SetPin_Debug3(void);
void GPIO_SetPin_Debug4(void);
void GPIO_SetPin_Debug5(void);
void GPIO_SetPin_Debug6(void);
void GPIO_ClearPin_Debug1(void);
void GPIO_ClearPin_Debug2(void);
void GPIO_ClearPin_Debug3(void);
void GPIO_ClearPin_Debug4(void);
void GPIO_ClearPin_Debug5(void);
void GPIO_ClearPin_Debug6(void);





/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define AFE_Power_Kill_Pin GPIO_PIN_2
#define AFE_Power_Kill_GPIO_Port GPIOE
#define DCDC_Power_Good_Pin GPIO_PIN_4
#define DCDC_Power_Good_GPIO_Port GPIOE
#define AFE_Alert_Pin GPIO_PIN_5
#define AFE_Alert_GPIO_Port GPIOE
#define AFE_Alert_EXTI_IRQn EXTI9_5_IRQn
#define PowerOff_Serial_Pin GPIO_PIN_6
#define PowerOff_Serial_GPIO_Port GPIOE
#define Temperature_9_Pin GPIO_PIN_0
#define Temperature_9_GPIO_Port GPIOC
#define Temperature_10_Pin GPIO_PIN_1
#define Temperature_10_GPIO_Port GPIOC
#define Temperature_11_Pin GPIO_PIN_2
#define Temperature_11_GPIO_Port GPIOC
#define Temperature_12_Pin GPIO_PIN_3
#define Temperature_12_GPIO_Port GPIOC
#define Temperature_8_Pin GPIO_PIN_0
#define Temperature_8_GPIO_Port GPIOA
#define Temperature_7_Pin GPIO_PIN_1
#define Temperature_7_GPIO_Port GPIOA
#define Temperature_6_Pin GPIO_PIN_2
#define Temperature_6_GPIO_Port GPIOA
#define Temperature_5_Pin GPIO_PIN_3
#define Temperature_5_GPIO_Port GPIOA
#define Temperature_1_Pin GPIO_PIN_4
#define Temperature_1_GPIO_Port GPIOA
#define Temperature_2_Pin GPIO_PIN_5
#define Temperature_2_GPIO_Port GPIOA
#define Temperature_3_Pin GPIO_PIN_6
#define Temperature_3_GPIO_Port GPIOA
#define Temperature_4_Pin GPIO_PIN_7
#define Temperature_4_GPIO_Port GPIOA
#define SD_Inserted_Pin GPIO_PIN_4
#define SD_Inserted_GPIO_Port GPIOC
#define SDPwrOff_Pin GPIO_PIN_5
#define SDPwrOff_GPIO_Port GPIOC
#define LED1_Green_Pin GPIO_PIN_0
#define LED1_Green_GPIO_Port GPIOB
#define LED2_Blue_Pin GPIO_PIN_1
#define LED2_Blue_GPIO_Port GPIOB
#define LED3_Orange_Pin GPIO_PIN_2
#define LED3_Orange_GPIO_Port GPIOB
#define LED4_Red_Pin GPIO_PIN_7
#define LED4_Red_GPIO_Port GPIOE
#define Heater_B_Pin GPIO_PIN_9
#define Heater_B_GPIO_Port GPIOE
#define Button1_Pin GPIO_PIN_11
#define Button1_GPIO_Port GPIOE
#define Button1_EXTI_IRQn EXTI15_10_IRQn
#define Button2_Pin GPIO_PIN_12
#define Button2_GPIO_Port GPIOE
#define Button2_EXTI_IRQn EXTI15_10_IRQn
#define AFE_Clock_Pin GPIO_PIN_10
#define AFE_Clock_GPIO_Port GPIOB
#define AFE_Data_Pin GPIO_PIN_11
#define AFE_Data_GPIO_Port GPIOB
#define SerialTxA_Pin GPIO_PIN_8
#define SerialTxA_GPIO_Port GPIOD
#define SerialRxA_Pin GPIO_PIN_9
#define SerialRxA_GPIO_Port GPIOD
#define Heater_A_Pin GPIO_PIN_12
#define Heater_A_GPIO_Port GPIOD
#define Osc_3v3_Pin GPIO_PIN_13
#define Osc_3v3_GPIO_Port GPIOD
#define Discharge_Enable_Pin GPIO_PIN_8
#define Discharge_Enable_GPIO_Port GPIOA
#define Charge_Enable_Pin GPIO_PIN_9
#define Charge_Enable_GPIO_Port GPIOA
#define Allow_Heat_From_Battery_Pin GPIO_PIN_10
#define Allow_Heat_From_Battery_GPIO_Port GPIOA
#define PGED_Pin GPIO_PIN_13
#define PGED_GPIO_Port GPIOA
#define PGEC_Pin GPIO_PIN_14
#define PGEC_GPIO_Port GPIOA
#define SerialTxB_Pin GPIO_PIN_10
#define SerialTxB_GPIO_Port GPIOC
#define SerialRxB_Pin GPIO_PIN_11
#define SerialRxB_GPIO_Port GPIOC
#define Debug5_Pin GPIO_PIN_0
#define Debug5_GPIO_Port GPIOD
#define Debug6_Pin GPIO_PIN_1
#define Debug6_GPIO_Port GPIOD
#define Debug4_Pin GPIO_PIN_2
#define Debug4_GPIO_Port GPIOD
#define Debug3_Pin GPIO_PIN_3
#define Debug3_GPIO_Port GPIOD
#define Debug2_Pin GPIO_PIN_4
#define Debug2_GPIO_Port GPIOD
#define Debug1_Pin GPIO_PIN_5
#define Debug1_GPIO_Port GPIOD
#define SDC_CS_Pin GPIO_PIN_7
#define SDC_CS_GPIO_Port GPIOD
#define SDC_SCK_Pin GPIO_PIN_3
#define SDC_SCK_GPIO_Port GPIOB
#define SDC_MISO_Pin GPIO_PIN_4
#define SDC_MISO_GPIO_Port GPIOB
#define SDC_MOSI_Pin GPIO_PIN_5
#define SDC_MOSI_GPIO_Port GPIOB
#define Smart_Module_SMB_Data_Pin GPIO_PIN_7
#define Smart_Module_SMB_Data_GPIO_Port GPIOB
#define Smart_Module_SMB_Clock_Pin GPIO_PIN_8
#define Smart_Module_SMB_Clock_GPIO_Port GPIOB
#define AFE_Boot_Pin GPIO_PIN_0
#define AFE_Boot_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
