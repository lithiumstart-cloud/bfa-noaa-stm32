/* Register definitions for the BQ76920 battery management AFE
 * bq76920.h
 *
 *  Created on: May 31, 2017
 *      Author: akessler
 */

#ifndef BQ769x0_H_
#define BQ769x0_H_

#include "assert.h"
#include "bluflex_core.h"

//Select ONE device
#define BQ76920
//#define BQ76930
//#define BQ76940

#define BQ769x0_USE_CRC 0

#if (defined BQ76920) 
	#pragma message("BQ76920 selected.")
    #define BQ769x0_NUM_CELL_CNXS (5)
    #define BQ769x0_NUM_THERMISTORS (1)
#elif (defined BQ76930)
	#pragma message("BQ76930 selected.")
    #define BQ769x0_NUM_CELL_CNXS (10)
    #define BQ769x0_NUM_THERMISTORS (2)
#elif (defined BQ76940)
	#pragma message("BQ76940 selected.")
    #define BQ769x0_NUM_CELL_CNXS (15)
    #define BQ769x0_NUM_THERMISTORS (3)

#else
    #error "No specific BQ769x0 defined!"
 #endif

/** 7bit I2C address of all BQ769x0 chips */
#define BQ769x0_I2C_ADDR7	(0x08)

#define BQ769x0_NUM_REGS 	(0x5A)

#define BQ769x0_BOOT_HOLD_TIME	(100)
#define BQ769x0_BOOT_WAIT_TIME	(400)  //<SRB>### SEE HOW MUCH THIS CAN BE REDUCED ###


/** Registers on the BQ769x0 are 8bits wide */
typedef uint8_t                 BQ769x0_Register_t;

#if BQ769x0_USE_CRC
#   define BQ769x0_CONVERSION_STEP  (4)
#   define BQ769x0_REGISTER_STEP    (2)
#else
#   define BQ769x0_CONVERSION_STEP  (2)
#   define BQ769x0_REGISTER_STEP    (1)
#endif

/** Register addresses on the BQ769x0
 *
 * Registers can be accessed individually, however sequential reads/writes will
 * automatically increment the address, allowing larger "logical" registers to
 * be addressed.
 *
 * It is important to note that while the PIC is little endian, the 16bit
 * conversion registers are big-endian. This is in order to provide atomic
 * access by latching the LSB of the conversion value whenever the MSB is
 * read.
 */
typedef enum
{
	BQ769x0_ADDR__SYS_STAT = 0x00,
	BQ769x0_ADDR__CELLBAL1,

	BQ769x0_ADDR__CELLBAL2, ///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__CELLBAL3, ///< Only valid for bq76940

	BQ769x0_ADDR__SYS_CTRL1,
	BQ769x0_ADDR__SYS_CTRL2,
	BQ769x0_ADDR__PROTECT1,
	BQ769x0_ADDR__PROTECT2,
	BQ769x0_ADDR__PROTECT3,
	BQ769x0_ADDR__OV_TRIP,
	BQ769x0_ADDR__UV_TRIP,
	BQ769x0_ADDR__CC_CFG,
	BQ769x0_ADDR__VC1_HI,
	BQ769x0_ADDR__VC1_LO,
	BQ769x0_ADDR__VC2_HI,
	BQ769x0_ADDR__VC2_LO,
	BQ769x0_ADDR__VC3_HI,     //0x10
	BQ769x0_ADDR__VC3_LO,
	BQ769x0_ADDR__VC4_HI,
	BQ769x0_ADDR__VC4_LO,
	BQ769x0_ADDR__VC5_HI,
	BQ769x0_ADDR__VC5_LO,


	BQ769x0_ADDR__VC6_HI, 	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC6_LO, 	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC7_HI, 	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC7_LO, 	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC8_HI, 	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC8_LO, 	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC9_HI, 	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC9_LO, 	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC10_HI,	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC10_LO,	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__VC11_HI,  ///< Only valid for bq76940  //0x20

	BQ769x0_ADDR__VC11_LO,	///< Only valid for bq76940
	BQ769x0_ADDR__VC12_HI,	///< Only valid for bq76940
	BQ769x0_ADDR__VC12_LO,	///< Only valid for bq76940
	BQ769x0_ADDR__VC13_HI,	///< Only valid for bq76940
	BQ769x0_ADDR__VC13_LO,	///< Only valid for bq76940
	BQ769x0_ADDR__VC14_HI,	///< Only valid for bq76940
	BQ769x0_ADDR__VC14_LO,	///< Only valid for bq76940
	BQ769x0_ADDR__VC15_HI,	///< Only valid for bq76940
	BQ769x0_ADDR__VC15_LO,	///< Only valid for bq76940

	BQ769x0_ADDR__BAT_HI = 0x2A,
	BQ769x0_ADDR__BAT_LO,
	BQ769x0_ADDR__TS1_HI,
	BQ769x0_ADDR__TS1_LO,

	BQ769x0_ADDR__TS2_HI, 	///< Only valid for bq76930 and bq76940
	BQ769x0_ADDR__TS2_LO, 	///< Only valid for bq76930 and bq76940

	BQ769x0_ADDR__TS3_HI,   ///< Only valid for bq76940  //0x30
	BQ769x0_ADDR__TS3_LO,   ///< Only valid for bq76940

	BQ769x0_ADDR__CC_HI = 0x32,
	BQ769x0_ADDR__CC_LO,
    // Padding length: 0x1C
	BQ769x0_ADDR__ADCGAIN1 = 0x50,
	BQ769x0_ADDR__ADCOFFSET = 0x51,
    // Padding length: 0x07
	BQ769x0_ADDR__ADCGAIN2 = 0x59,

} T_BQ769x0_RegisterAddress;

static_assert(BQ769x0_ADDR__VC3_HI == 0x10, "VC3_HI addr");
static_assert(BQ769x0_ADDR__VC5_LO == 0x15, "VC5_LO addr");

#if BQ769x0_USE_CRC
#   define BQ769x0_REGISTER_MAP_LEN     (2*BQ769x0_NUM_REGS)
#else
#   define BQ769x0_REGISTER_MAP_LEN     BQ769x0_NUM_REGS
#endif


//#define BQ769x0_NUM_ADC_CHANNELS        (20) //TODO: what is this?
//#define BQ769x0_REGS_PER_CHANNEL        (2)

typedef union 
{
    uint8_t u8;
    struct
    {
        unsigned char OCD           : 1; ///< Over current in discharge fault event indicator (latched until cleared by host)
        unsigned char SCD           : 1; ///< Short circuit in discharge fault event indicator (latched until cleared by host)
        unsigned char OV            : 1; ///< Over voltage fault event indicator (latched until cleared by host)
        unsigned char UV            : 1; ///< Under voltage fault event indicator (latched until cleared by host)
        unsigned char OVRD_ALERT    : 1; ///< External pull-up on the ALERT pin indiciator. Only active when ALERT poin is not already being driven high by the AFE. 
        unsigned char DEVICE_XREADY : 1; ///< Internal chip fault indicator. When set, it should be cleared by host. 
        unsigned char               : 1;
        unsigned char CC_READY      : 1; ///< Indicates that a fresh coulomb counter reading is available. Must be cleared by host/
    };

} T_BQ769x0_SYS_STAT;

typedef union
{
    uint8_t u8;
    struct
    {
        unsigned char CB1           : 1;
        unsigned char CB2           : 1;
        unsigned char CB3           : 1;
        unsigned char CB4           : 1;
        unsigned char CB5           : 1;
        unsigned char               : 3;
    };

} T_BQ769x0_CELLBAL1;

typedef union
{
    uint8_t u8;
    struct
    {
        unsigned char CB6           : 1;
        unsigned char CB7           : 1;
        unsigned char CB8           : 1;
        unsigned char CB9           : 1;
        unsigned char CB10          : 1;
        unsigned char               : 3;
    };

} T_BQ769x0_CELLBAL2;



typedef union
{
    uint8_t u8;
    struct
    {
        unsigned char CB11          : 1;
        unsigned char CB12          : 1;
        unsigned char CB13          : 1;
        unsigned char CB14          : 1;
        unsigned char CB15          : 1;
        unsigned char               : 3;
    };

} T_BQ769x0_CELLBAL3;


typedef union
{
    uint8_t u8;
    struct 
    {
        unsigned char SHUT_B        : 1; ///< Shutodwn command from host, must be written in specific order.
        unsigned char SHUT_A        : 1; ///< Shutodwn command from host, must be written in specific order.
        unsigned char               : 1; ///< Reserved. Do not set to 1.
        unsigned char TEMP_SEL      : 1; ///< TSx_HI and TSx_LO temperature source (0= store intenral die temp, 1=store thermistor reading)
        unsigned char ADC_EN        : 1; ///< ADC enable command. Also enales/disables OV protection
        unsigned char               : 2; 
        unsigned char LOAD_PRESENT  : 1; ///< Valid only when [CHG_ON]=0. Is high if CHG pin is detected to exceed VLOAD_DETECT. Read only.
    };

} T_BQ769x0_SYS_CTRL1;

typedef union
{
    uint8_t u8;
    struct
    {
        unsigned char CHG_ON        : 1; ///< Charge signal control
        unsigned char DSG_ON        : 1; ///< Discharge signal control
        unsigned char               : 3; 
        unsigned char CC_ONESHOT    : 1; ///< Coulomb counter single 250ms reading trigger commnand. 
        unsigned char CC_EN         : 1; ///< Coulomb counter continuous operation enbale. If set, [CC_ONESHOT] is ignored.
        unsigned char DELAY_DIS     : 1; ///< Disable OV, UV, OCD, SCD delays for faster production testing.
    };

} T_BQ769x0_SYS_CTRL2;


typedef union 
{
    uint8_t u8;
    struct
    {
        unsigned char SCD_THRESH    : 3; ///< Short circuit in discharge threshold sertting 
        unsigned char SCD_DELAY     : 2; ///< Short circuit in discharge delay setting.
        unsigned char               : 2; 
        unsigned char RSNS          : 1; ///<Allows for doubling the OCD and SCD thresholds simultaneously
    };
} T_BQ769x0_PROTECT1;


typedef union 
{
    uint8_t u8;
    struct
    {
        unsigned char OCD_THRESH    : 4; ///< Overcurrent in discharge threshold setting
        unsigned char OCD_DELAY     : 3; ///< Overcurrent in discharge delay setting
        unsigned char               : 1;
    };

} T_BQ769x0_PROTECT2;

typedef union 
{
    uint8_t u8;
    struct
    {    
        unsigned char               : 4;
        unsigned char OV_DELAY      : 2; ///< Overvoltage delay setting
        unsigned char UV_DELAY      : 2; ///< Undervoltage delay setting
    };

} T_BQ769x0_PROTECT3;


/* CC_CFG is required to be set to 0x19 */
#   define CC_CFG_KEY_VAL      (0x19)
typedef union 
{
    uint8_t u8;
    struct
    {
        unsigned char key           : 6; ///<For "optimal performance", these bits should be set to 0x19 on boot.
        unsigned char               : 2;
    };
} T_BQ769x0_CC_CFG;


typedef union 
{
    uint8_t u8;
    struct
    {
        unsigned char               : 2;
        unsigned char ADCGAIN_4_3   : 2; ///< ADC GAIN offset up 2 LSB 
        unsigned char               : 4;
    };  
} T_BQ769x0_ADCGAIN1;


#define BQ76940_MIN_ADCGAIN__uV         (365)
typedef union 
{
    uint8_t u8;
    struct
    {
        unsigned char               : 5;
        unsigned char ADCGAIN_2_0   : 3; ///<ADC GAIN offset lower 3 LSB
    };
} T_BQ769x0_ADCGAIN2;



/* SCD threshold values: LO for RSNS=0, HI for RSNS=1 */
#define BQ769x0_SCD_THRESH_LO__22mV      (0x0)
#define BQ769x0_SCD_THRESH_LO__33mV      (0x1)
#define BQ769x0_SCD_THRESH_LO__44mV      (0x2)
#define BQ769x0_SCD_THRESH_LO__44mV      (0x2)
#define BQ769x0_SCD_THRESH_LO__56mV      (0x3)
#define BQ769x0_SCD_THRESH_LO__67mV      (0x4)
#define BQ769x0_SCD_THRESH_LO__78mV      (0x5)
#define BQ769x0_SCD_THRESH_LO__89mV      (0x6)
#define BQ769x0_SCD_THRESH_LO__100mV     (0x7)

#define BQ769x0_SCD_THRESH_HI__44mV      (0x0)
#define BQ769x0_SCD_THRESH_HI__67mV      (0x1)
#define BQ769x0_SCD_THRESH_HI__89mV      (0x2)
#define BQ769x0_SCD_THRESH_HI__111mV     (0x3)
#define BQ769x0_SCD_THRESH_HI__133mV     (0x4)
#define BQ769x0_SCD_THRESH_HI__155mV     (0x5)
#define BQ769x0_SCD_THRESH_HI__178mV     (0x6)
#define BQ769x0_SCD_THRESH_HI__200mV     (0x7)

#define BQ769x0_SCD_DELAY__70us          (0x0)
#define BQ769x0_SCD_DELAY__100us         (0x1)
#define BQ769x0_SCD_DELAY__200us         (0x2)
#define BQ769x0_SCD_DELAY__300us         (0x3)


/* OCD threshold values: LO for RSNS=0, HI for RSNS=1 */
#define BQ769x0_OCD_THRESH_LO__8mV       (0x0)
#define BQ769x0_OCD_THRESH_LO__11mV      (0x1)
#define BQ769x0_OCD_THRESH_LO__14mV      (0x2)
#define BQ769x0_OCD_THRESH_LO__17mV      (0x3)
#define BQ769x0_OCD_THRESH_LO__19mV      (0x4)
#define BQ769x0_OCD_THRESH_LO__22mV      (0x5)
#define BQ769x0_OCD_THRESH_LO__25mV      (0x6)
#define BQ769x0_OCD_THRESH_LO__28mV      (0x7)
#define BQ769x0_OCD_THRESH_LO__31mV      (0x8)
#define BQ769x0_OCD_THRESH_LO__33mV      (0x9)
#define BQ769x0_OCD_THRESH_LO__36mV      (0xA)
#define BQ769x0_OCD_THRESH_LO__39mV      (0xB)
#define BQ769x0_OCD_THRESH_LO__42mV      (0xC)
#define BQ769x0_OCD_THRESH_LO__44mV      (0xD)
#define BQ769x0_OCD_THRESH_LO__47mV      (0xE)
#define BQ769x0_OCD_THRESH_LO__50mV      (0xF)

#define BQ769x0_OCD_THRESH_HI__17mV      (0x0)
#define BQ769x0_OCD_THRESH_HI__22mV      (0x1)
#define BQ769x0_OCD_THRESH_HI__28mV      (0x2)
#define BQ769x0_OCD_THRESH_HI__33mV      (0x3)
#define BQ769x0_OCD_THRESH_HI__39mV      (0x4)
#define BQ769x0_OCD_THRESH_HI__44mV      (0x5)
#define BQ769x0_OCD_THRESH_HI__50mV      (0x6)
#define BQ769x0_OCD_THRESH_HI__56mV      (0x7)
#define BQ769x0_OCD_THRESH_HI__61mV      (0x8)
#define BQ769x0_OCD_THRESH_HI__67mV      (0x9)
#define BQ769x0_OCD_THRESH_HI__72mV      (0xA)
#define BQ769x0_OCD_THRESH_HI__78mV      (0xB)
#define BQ769x0_OCD_THRESH_HI__83mV      (0xC)
#define BQ769x0_OCD_THRESH_HI__89mV      (0xD)
#define BQ769x0_OCD_THRESH_HI__94mV      (0xE)
#define BQ769x0_OCD_THRESH_HI__100mV     (0xF)

#define BQ769x0_OCD_DELAY__8ms           (0x0)
#define BQ769x0_OCD_DELAY__20ms          (0x1)
#define BQ769x0_OCD_DELAY__40ms          (0x2)
#define BQ769x0_OCD_DELAY__80ms          (0x3)
#define BQ769x0_OCD_DELAY__160ms         (0x4)
#define BQ769x0_OCD_DELAY__320ms         (0x5)
#define BQ769x0_OCD_DELAY__640ms         (0x6)
#define BQ769x0_OCD_DELAY__1280ms        (0x7)

/* Undervoltage Delay settings */
#define BQ769x0_UV_DELAY__1s             (0x0)
#define BQ769x0_UV_DELAY__4s             (0x1)
#define BQ769x0_UV_DELAY__8s             (0x2)
#define BQ769x0_UV_DELAY__16s            (0x3)

/* Overvoltage Delay settings */
#define BQ769x0_OV_DELAY__1s             (0x0)
#define BQ769x0_OV_DELAY__2s             (0x1)
#define BQ769x0_OV_DELAY__4s             (0x2)
#define BQ769x0_OV_DELAY__8s             (0x3)

/* Controls whether to add the CRC byte after each data byte when creating
 * register maps
 */
#ifndef BQ769x0_USE_CRC
#   error "Must explicitly define BQ76940_USE_CRC as 1 (enabled) or 0 (disabled)"
#endif

#if BQ769x0_USE_CRC
#   define CRC_BYTE(name)               uint8_t name##_crc
#   define CRC_PADDING(name, length)    uint8_t name##_crc_padding[length]
#else
#   define CRC_BYTE(name)
#   define CRC_PADDING(name, length)
#endif

/** Macro to build a bitfield register; raw access, bitfield access, and a subsequent CRC byte if enabled */
#define BQ769x0_BITFIELD_FACTORY(name)    \
    T_BQ769x0_##name  name;              \
    CRC_BYTE(name)                        

/** Macro to build a normal register; raw access and a subsequent CRC byte if enabled*/
#define BQ769x0_REGISTER_FACTORY(name)      \
    uint8_t name;                           \
    CRC_BYTE(name)

#define BQ769x0_REGISTER_PADDING(name, length)      \
    uint8_t padding_after_##name [length];          \
    CRC_PADDING(name, length)

/** Full BQ76940 Register Map
 * This struct contains CRC data bytes if the driver has been set to use CRC
 * bytes. This allows data to be written directly to this register using the
 * bitfield access (where appropriate) */
typedef union
{
    uint8_t raw_bytes[BQ769x0_REGISTER_MAP_LEN];
    struct
    {
        BQ769x0_BITFIELD_FACTORY(SYS_STAT);
        BQ769x0_BITFIELD_FACTORY(CELLBAL1);

        BQ769x0_BITFIELD_FACTORY(CELLBAL2);//Only valid for 30, 40
        BQ769x0_BITFIELD_FACTORY(CELLBAL3);//Only valid for 40

        BQ769x0_BITFIELD_FACTORY(SYS_CTRL1);
        BQ769x0_BITFIELD_FACTORY(SYS_CTRL2);
        BQ769x0_BITFIELD_FACTORY(PROTECT1);
        BQ769x0_BITFIELD_FACTORY(PROTECT2);
        BQ769x0_BITFIELD_FACTORY(PROTECT3);
        BQ769x0_REGISTER_FACTORY(OV_TRIP);
        BQ769x0_REGISTER_FACTORY(UV_TRIP);
        BQ769x0_BITFIELD_FACTORY(CC_CFG);

        BQ769x0_REGISTER_FACTORY(VC1_HI);
        BQ769x0_REGISTER_FACTORY(VC1_LO);
        BQ769x0_REGISTER_FACTORY(VC2_HI);
        BQ769x0_REGISTER_FACTORY(VC2_LO);
        BQ769x0_REGISTER_FACTORY(VC3_HI);
        BQ769x0_REGISTER_FACTORY(VC3_LO);
        BQ769x0_REGISTER_FACTORY(VC4_HI);
        BQ769x0_REGISTER_FACTORY(VC4_LO);
        BQ769x0_REGISTER_FACTORY(VC5_HI);
        BQ769x0_REGISTER_FACTORY(VC5_LO);

        //This block only valid for '30, '40
        BQ769x0_REGISTER_FACTORY(VC6_HI);
        BQ769x0_REGISTER_FACTORY(VC6_LO);
        BQ769x0_REGISTER_FACTORY(VC7_HI);
        BQ769x0_REGISTER_FACTORY(VC7_LO);
        BQ769x0_REGISTER_FACTORY(VC8_HI);
        BQ769x0_REGISTER_FACTORY(VC8_LO);
        BQ769x0_REGISTER_FACTORY(VC9_HI);
        BQ769x0_REGISTER_FACTORY(VC9_LO);
        BQ769x0_REGISTER_FACTORY(VC10_HI);
        BQ769x0_REGISTER_FACTORY(VC10_LO);


        //This block only valid for '40
        BQ769x0_REGISTER_FACTORY(VC11_HI);
		BQ769x0_REGISTER_FACTORY(VC11_LO);
        BQ769x0_REGISTER_FACTORY(VC12_HI);
        BQ769x0_REGISTER_FACTORY(VC12_LO);
        BQ769x0_REGISTER_FACTORY(VC13_HI);
        BQ769x0_REGISTER_FACTORY(VC13_LO);
        BQ769x0_REGISTER_FACTORY(VC14_HI);
        BQ769x0_REGISTER_FACTORY(VC14_LO);
        BQ769x0_REGISTER_FACTORY(VC15_HI);
        BQ769x0_REGISTER_FACTORY(VC15_LO);


        BQ769x0_REGISTER_FACTORY(BAT_HI);
        BQ769x0_REGISTER_FACTORY(BAT_LO);
        BQ769x0_REGISTER_FACTORY(TS1_HI);
        BQ769x0_REGISTER_FACTORY(TS1_LO);

        //This block only valid for '30, '40
        BQ769x0_REGISTER_FACTORY(TS2_HI);
        BQ769x0_REGISTER_FACTORY(TS2_LO);

        //This block only valid for '40
        BQ769x0_REGISTER_FACTORY(TS3_HI);
        BQ769x0_REGISTER_FACTORY(TS3_LO);

        BQ769x0_REGISTER_FACTORY(CC_HI);
        BQ769x0_REGISTER_FACTORY(CC_LO);

        //Need 28 bytes of padding
        BQ769x0_REGISTER_PADDING(CC_LO, 0x1C);

        BQ769x0_BITFIELD_FACTORY(ADCGAIN1);
        BQ769x0_REGISTER_FACTORY(ADCOFFSET);

        BQ769x0_REGISTER_PADDING(ADCOFFSET, 0x07);

        BQ769x0_BITFIELD_FACTORY(ADCGAIN2);
    };

} T_BQ769x0_RegisterMap;


/* Assure there's no packing issues by checking the exact length of the struct */
static_assert(sizeof(T_BQ769x0_RegisterMap) == BQ769x0_REGISTER_MAP_LEN, "BQ769x0 register map length=" STRINGIZE_VALUE_OF(BQ769x0_REGISTER_MAP_LEN));


///###Prototypes############################

const char* BQ769x0_Addr2String(T_BQ769x0_RegisterAddress addr);
void BQ769x0_PrintRegister(T_BQ769x0_RegisterAddress addr, T_BQ769x0_RegisterMap* map);

#endif /* BQ76920_H_ */
