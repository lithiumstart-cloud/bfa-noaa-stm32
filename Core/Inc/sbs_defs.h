/**
 * @file sbs.h
 * @brief Implements the Smart Battery Specification (SBS) software layer.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-04-16
 *
**/

#ifndef SBS_DEFS_H
#define SBS_DEFS_H


//These are all the message types! Yikes. 
#define SBS_REMAINING_CAP_ALARM_THRESH      (0x01)    ///< Sets or gets the low capacity alarm thresh value
#define SBS_REMAINING_TIME_ALARM_THRESH     (0x02)    ///< Sets or gets the remaining time alarm thresh value
#define SBS_BATTERY_MODE                    (0x03)    ///< Sets or gets the battery mode word

//at rate is special case, revist
#define SBS_AT_RATE                 (0x04)
#define SBS_AT_RATE_TIME_TO_FULL    (0x05)
#define SBS_AT_RATE_TIME_TO_EMPTY   (0x06)
#define SBS_AT_RATE_OK              (0x07)

#define SBS_TEMPERATURE             (0x08) ///< Get the cell pack temp in K
#define SBS_VOLTAGE                 (0x09) ///< Get the pack voltage in mV
#define SBS_CURRENT                 (0x0A) ///< Get the pack current in mA
#define SBS_AVG_CURRENT             (0x0B) ///< Get average current over 1 min
#define SBS_MAX_ERROR               (0x0C) ///< Expected margin of error in SoC calc
#define SBS_REL_SOC                 (0x0D) ///< Get predicated SoC relative to FullChargeCap
#define SBS_ABS_SOC                 (0x0E) ///< Get predicated SoC relative to DesignCap
#define SBS_REMAINING_CAP           (0x0F) ///< Remaining capacity (units depend on CAPACITY MODE)
#define SBS_FULL_CHARGE_CAP         (0x10) ///< Predicted max capacity (units depend)
#define SBS_RUN_TIME_TO_EMPTY       (0x11) ///< Predicted time left at current rate of discharge (min)
#define SBS_AVG_TIME_TO_EMPTY       (0x12) ///< Average run time to empty over one-minute
#define SBS_AVG_TIME_TO_FULL        (0x13) ///< Average run time to full over one-minute
#define SBS_CHARGING_CURRENT        (0x14) ///< Desired charging current - n/a
#define SBS_CHARGING_VOLTAGE        (0x15) ///< Desired charging voltage - n/a
#define SBS_BATTERY_STATUS          (0x16) ///< Get battery status regsiter.
#define SBS_CYCLE_COUNT             (0x17) ///< Get number of cycles. 
#define SBS_DESIGN_CAPACITY         (0x18) ///< Get nominal capacity of pack.
#define SBS_DESIGN_VOLTAGE          (0x19) ///< Get nominal voltage of pack.
#define SBS_SPECIFICATION_INFO      (0x1A) ///< Get packed uint desicribing version of SBS supported.
#define SBS_MANUFACTURE_DATE        (0x1B) ///< Get packed manufacutring date.
#define SBS_SERIAL_NUMBER           (0x1C) ///< Get manufacturer's serieal number.
#define SBS_MANUFACTURER_NAME       (0x20) ///< Get manufacturer's name (string)
#define SBS_DEVICE_NAME             (0x21) ///< Get device name (string)
#define SBS_DEVICE_CHEMISTRY        (0x22) ///< Get device chemsitry (string)
#define SBS_MANUFACTURER_DATA       (0x23) ///< Get/set custom data field

#define SBS_OPTIONAL_MFG_5          (0x2F) ///< block r/w (used for special requests)
#define SBS_OPTIONAL_MFG_4          (0x3C) ///< word r/w
#define SBS_OPTIONAL_MFG_3          (0x3D) ///< word r/w 
#define SBS_OPTIONAL_MFG_2          (0x3E) ///< word r/w 
#define SBS_OPTIONAL_MFG_1          (0x3F) ///< word r/w 

#define SBS_RELAY_STATES_CONDENSED  (SBS_OPTIONAL_MFG_1) ///<get the relay states
#define SBS_REQUEST_RESTART         (SBS_OPTIONAL_MFG_2) ///<request a system reset
#define SBS_BALANCING_STATUS        (SBS_OPTIONAL_MFG_3) ///<get state of balancing

#define SBS_MANUFACTURER_ACCESS     (0x00)    ///< Used to load special request code.

///These are the "special request codes" used to load blocks of diagnostic data.
///They should be used as the parameter to a write word command, using cmd 0x00.
#define SBS_SPECIAL_CELL_STATS          (0x00) ///< Get block of min, max cell voltages and alarm.
#define SBS_SPECIAL_TEMPERATURE_DATA    (0x01) ///< Get all temperatures and alarms.
#define SBS_SPECIAL_UPTIME              (0x02) ///< Get elapsed time since system reset.
#define SBS_SPECIAL_ALARMS              (0x03) ///< Get master alarm, current alarm, and psFaultAlarm
#define SBS_SPECIAL_CELL_DETAIL         (0x04) ///< Get all four cell voltages
#define SBS_SPECIAL_VERSION_ID          (0x05) ///< Get git hash version string
#define SBS_SPECIAL_RELAY_STATES_DETAIL (0x06) ///< Get relay states as enums

#define SBS_SPECIAL_BLOCK           (SBS_OPTIONAL_MFG_5) ///< This command is used to retrieve "special" blocks

#define SBS_ALARM_WARNING           (SBS_BATTERY_STATUS)

//These are the Error Codes as defined in appendic C of the SBS spec.
enum
{
    SBS_ERR_OK = 0,
    SBS_ERR_BUSY,
    SBS_ERR_RESERVED,
    SBS_ERR_UNSUPPORTED,
    SBS_ERR_ACCESS_DENIED,
    SBS_ERR_OVERFLOW,
    SBS_ERR_BAD_SIZE,
    SBS_ERR_UNKNOWN
};



/**
 *  A union of bit fields and raw word to represent BatteryStatus register as
 *  defined in SBS specification (pg 29).
 */
typedef union
{
   uint16_t Raw; ///< Access all 16 bits of BatteryStatus register.
   struct
   {   unsigned ErrorCode               :4;///< See Appendix C of SBS spec
       unsigned FullyDischarged         :1;///< Battery capacity is depleted (< 20%)
       unsigned FullyCharged            :1;///< Battery is full and further charge is not required.
       unsigned Discharging             :1;///< Battery is discharging. Include self-discharges.
       unsigned Initialized             :1;///< 1- battery calibrated; 0- calib. info lost.
       unsigned RemainingTimeAlarm      :1;///< AverageTimeToEmpty() < RemainingTimeAlarmThresh()
       unsigned RemainingCapacityAlarm  :1;///< RemainingCapacity() < RemainingCapacityAlarmThresh()
       unsigned                         :1;///< Reserved
       unsigned TerminateDischargeAlarm :1;///< Battery capacity is depleted; cease discharge ASAP
       unsigned OverTempAlarm           :1;///< Temperature is above preset limit. Stop discharge/charge.
       unsigned                         :1;///< Reserved
       unsigned TerminateChargeAlarm    :1;///< Charging should be suspended temporarily
       unsigned OverChargedAlarm        :1;///< Battery is fully charged - stop charging.
   };
 } T_SBS_BatteryStatus;

 /**
  * A union of bit fields and a raw word to reprsent BatteryMode register as
  * defined in SBS specification (pg 15). Read only from host.
  */
typedef union
{
   uint16_t Raw;  ///< Access all 16 bits of BatteryMode register.

   struct
   {
       uint8_t Mode_LSB; ///< Lower byte of mode register. Read only from host.
       uint8_t Mode_MSB; ///< Upper byte of mode regsiter. Read/write from host.
   };

   struct
   {
       unsigned InternalChargeController:1;///< Set if internal charge controller supported.
       unsigned PrimaryBatterySupport   :1;///< Set if primary or secondary battery support exists.
       unsigned                         :5;///< Reserved
       unsigned ConditionFlag           :1;///< Set if battery conditioning cycle required. Clear when OK.
       unsigned ChargeControllerEnabled :1;///< Set when internal charge control enabled.
       unsigned PrimaryBattery          :1;///< Set when battery is operating in primary mode.
       unsigned                         :3;///< Reserved
       unsigned AlarmMode               :1;///< Set when alarm warning broadcasts to host are disabled. Default low.
       unsigned ChargerMode             :1;///< Set when charge info broadcasts are disabled. (Against spec, default high).
       unsigned CapacityMode            :1;///< 0- Report in mA/mAh (default); 1 - Report in 10mW/10mWh
    };
 } T_SBS_BatteryMode;
#endif
