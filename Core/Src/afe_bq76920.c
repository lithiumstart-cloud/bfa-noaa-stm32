/**
 * @file afe_BQ76920.h
 * @brief AFE driver for TI BQ769x0
 * @copyright 2017 EP Lithiumstart Division
 * @author A. Kessler
 * @date   2017-05-30
 *
**/
//############################## INCLUDES ######################################

#include "afe_bq76920.h"
#include "bluflex_core.h"


#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_iwdg.h"

#include "bq769x0.h"



static void MX_I2C2_Init(void);

//############################## DEFINES ######################################

#define AFE_TXRX_TIMEOUT	(10u)			//10ms
#define AFE_NUM_CELLS 		(3)
#define CRC_KEY 	  		(7)
#define CC_LSB_VALUE_uV_x1000	(8440)
#define CC_SENSE_RESISTOR_mOhm	(10)	// R3 and R31 in parallel

//############################## TYPEDEFS #####################################

typedef enum
{
	ST_AFE_RESETTING,
	ST_AFE_WAIT_TO_GO_OUT_RESET,
	ST_WAIT_SOFT_RESET,
	ST_AFE_BOOTING,
    ST_AFE_RUNNING,
    ST_AFE_ERROR,
} T_State;



//############################## VARIABLES ####################################
I2C_HandleTypeDef hi2c2;

static T_AFE_BQ76920_Sigs afeSigs;

static T_Timer afeInitTimer;
static T_State afeCurrentState =  ST_AFE_BOOTING;

static T_BQ769x0_RegisterMap bqMap;

/* Converted */
static struct
{
    uint16_t adcGain_uV;
    int8_t   cellOffset_mV;
    int16_t  packOffset_mV;
} adcCalib;

static struct
{
	uint32_t cellVltgs_raw[AFE_NUM_CELLS];
	uint32_t cellVltgs_mV[AFE_NUM_CELLS];

	uint32_t packVltg_raw;
	uint32_t packVltg_mV;

	uint16_t dieTemp_raw;
	int16_t  dieTemp_dC;

	uint16_t current_raw;
	int32_t  current_mA;

} latestMeasurements;

/*
 * This signals that the AFE Alert pin has been detected by the interrupt
 */
bool afeAlert = false;


//############################### PROTOTYPES ####################################

static void 				unpackCalibration(void);
static void 				printCalibration(void);

//static HAL_StatusTypeDef 	requestCalibration(void);
//static HAL_StatusTypeDef 	requestCellVoltage(uint8_t cellInx);


static uint8_t 				cellInx2ConnectionInx(uint8_t cellInx);
static bool 				unpackCellVoltage_mV(uint8_t cellInx);
static bool 				unpackPackVoltage_mV(void);
static bool 				unpackDieTemp_dC(void);
static bool					unpackCurent(void);

static HAL_StatusTypeDef 	bq769x0_Config(void);

static HAL_StatusTypeDef 	bq769x0_RequestReadRegister_U8(T_BQ769x0_RegisterAddress addr);
static HAL_StatusTypeDef 	bq769x0_RequestReadRegister_U16(T_BQ769x0_RegisterAddress addr);
static HAL_StatusTypeDef 	bq769x0_RequestWriteRegister_U8(T_BQ769x0_RegisterAddress addr, uint8_t data);

static void 				bq769x0_WriteBalancing(void);
uint8_t 					bq769x0_GetBalancingMask(void);
void    					bq769x0_SetBalancingMask(uint8_t mask);

static uint8_t CRC8(uint8_t *ptr, uint8_t len, uint8_t key);
static HAL_StatusTypeDef GetADCGainOffset(void);


//############################## HAL PROTOTYPES ##################################
void AFE_HW_DisableAfePower( void );
void AFE_HW_EnableAfePower( void );


//############################### CODE #########################################

void AFE_Init(T_AFE_BQ76920_Sigs* pSigs)
{
    if (pSigs)
    {
        afeSigs = *pSigs;
    }

	memset((void*) &bqMap, 0x00, sizeof(bqMap));
	TMR_StartOneshot(&afeInitTimer, 1000 );

	afeCurrentState = ST_AFE_RESETTING;
    return;
}



bool AFE_Update(void)
{
	switch (afeCurrentState)
	{
		case ST_AFE_RESETTING:
			if ( TMR_HasTripped_ms(&afeInitTimer) )
			{
				AFE_HW_EnableAfePower();
				TMR_StartOneshot(&afeInitTimer, 1000 );
				afeCurrentState = ST_AFE_WAIT_TO_GO_OUT_RESET;
			}

			/* Don't allow the system to sleep or the timer won't work */
			return (false);
			break;

		case ST_AFE_WAIT_TO_GO_OUT_RESET:
			if ( TMR_HasTripped_ms(&afeInitTimer) )
			{
				AFE_TriggerReset();
				TMR_StartOneshot(&afeInitTimer, BQ769x0_BOOT_HOLD_TIME );
				afeCurrentState = ST_WAIT_SOFT_RESET;
			}

			/* Don't allow the system to sleep or the timer won't work */
			return (false);
			break;

		case ST_WAIT_SOFT_RESET:
			if ( TMR_HasTripped_ms(&afeInitTimer) )
			{
				AFE_ReleaseReset();
				MX_I2C2_Init();
				TMR_StartOneshot(&afeInitTimer, BQ769x0_BOOT_WAIT_TIME );
				afeCurrentState = ST_AFE_BOOTING;
			}

			/* Don't allow the system to sleep or the timer won't work */
			return (false);
			break;


		case ST_AFE_BOOTING:
			if (TMR_HasTripped_ms(&afeInitTimer))
			{
				if (GetADCGainOffset() != HAL_OK)
				{
					afeCurrentState = ST_AFE_ERROR;
					return (true);
				}

				unpackCalibration();
				printCalibration();

				if (bq769x0_Config() != HAL_OK)
				{
					afeCurrentState = ST_AFE_ERROR;
					return (true);
				}
				else
				{
					afeCurrentState = ST_AFE_RUNNING;
				}
			}
			else
			{
				/* Don't allow the system to sleep or the timer won't work */
				return (false);
			}
			break;


		case ST_AFE_RUNNING:
			if (afeAlert)
			{
				afeAlert = false;

				bq769x0_RequestReadRegister_U16(BQ769x0_ADDR__VC1_HI);
				bq769x0_RequestReadRegister_U16(BQ769x0_ADDR__VC2_HI);
				bq769x0_RequestReadRegister_U16(BQ769x0_ADDR__VC5_HI);
				bq769x0_RequestReadRegister_U16(BQ769x0_ADDR__BAT_HI);
				bq769x0_RequestReadRegister_U16(BQ769x0_ADDR__TS1_HI);
				bq769x0_RequestReadRegister_U16(BQ769x0_ADDR__CC_HI);

				if ((bqMap.SYS_STAT.OCD == 1) ||			// Over current in discharge fault event indicator
					(bqMap.SYS_STAT.SCD == 1) ||			// Short circuit in discharge fault event indicator
					(bqMap.SYS_STAT.UV  == 1) ||			// Under voltage fault event indicator
					(bqMap.SYS_STAT.DEVICE_XREADY == 1))	// Internal chip fault indicator
				{
					bqMap.SYS_CTRL2.DSG_ON = 0;
					bq769x0_RequestWriteRegister_U8(BQ769x0_ADDR__SYS_CTRL2, bqMap.SYS_CTRL2.u8);
				}
				else
				{
					if (RLY_IsClosed(afeSigs.dischargeRelay))
					{
						bqMap.SYS_CTRL2.DSG_ON = 1;
						bq769x0_RequestWriteRegister_U8(BQ769x0_ADDR__SYS_CTRL2, bqMap.SYS_CTRL2.u8);
					}
				}

				/*
				 *  Over voltage fault event indicator
				 */
				if ( bqMap.SYS_STAT.OV == 1 )
				{
				}

				/*
				 *  Indicates that a fresh coulomb counter reading is available
				 */
				if ( bqMap.SYS_STAT.CC_READY == 1 )
				{
					for (uint8_t ii = 0; ii < AFE_NUM_CELLS; ii++)
					{
						unpackCellVoltage_mV(ii);
					}

					unpackPackVoltage_mV();
					unpackDieTemp_dC();
					unpackCurent();

					SIG_WriteToChildren_U32( afeSigs.cellVoltageRoot, &(latestMeasurements.cellVltgs_mV[0]), AFE_NUM_CELLS );

					/* The mask is setup elsewhere.  This just transfers it to the BQ */
					bq769x0_WriteBalancing();
				}

				bq769x0_ClearAlerts();
			}
			break;


		/*
		 * If there was a problem communicating, reboot AFE chip and start over
		 * There may be more sophisticated solutions, but this should never even happen
		 */
		case ST_AFE_ERROR:
			puts("AFE NOT RESPONDING!!!");
			puts("AFE restarting...");
			AFE_Init(&afeSigs);

			/*
			 * Don't allow the system to sleep or the timer won't work
			 */
			return (false);
			break;

		/*
		 * If the SM gets here, something is really wrong
		 */
		default:
			printf("AFE: Unknown state: %u\r\n", afeCurrentState);
			afeCurrentState = ST_AFE_ERROR;

			/*
			 * Don't allow the system to sleep or the timer won't work
			 */
			return (false);
			break;
	}

	return (true);
}


void AFE_ProcessInput(const char* tokens[], uint8_t nArgs)
{
    if (nArgs == 0 || tokens[0][0] == '?')
    {
    	puts("kill");
    	puts("calib");
    	puts("state");
    	puts("dcdc");
    	puts("size");
    	puts("bal");
    }
    else if (stricmp(tokens[0], "size") == 0)
	{
    	printf("sizeof(T_BQ769x0_RegisterMap)=%u\r\n", sizeof(T_BQ769x0_RegisterMap));
	}
    else if (stricmp(tokens[0], "kill") == 0)
    {
    	puts("Cutting power to AFE...");
    	AFE_HW_DisableAfePower();
    }
    else if (stricmp(tokens[0], "calib") == 0)
    {
    	printCalibration();
    }
    else if(stricmp(tokens[0], "bal") == 0)
    {
    	if (nArgs >= 2)
        {
            if (stricmp(tokens[1], "clear") == 0 )
            {
            	bq769x0_SetBalancingMask(0x00);
            	bq769x0_WriteBalancing();
            	puts("Balancing cleared");
            }
            else if (stricmp(tokens[1],"cell") == 0 )
            {
            	if (tokens[2][0] > '0' && tokens[2][0] < '6')
            	{
					uint8_t n = tokens[2][0] - '0';
					uint8_t mask = bq769x0_GetBalancingMask();
					printf("Balancing cell %u\r\n", n);
					mask |= (1 << (n - 1));
					bq769x0_SetBalancingMask(mask);
					bq769x0_WriteBalancing();
				}
				else
				{
					puts("bal cell {1, 2 or 3]");
				}
            }
        }
        else
        {
        	puts("clear");
			puts("cell <1 - 5>");
        }
    }
    else
    {
    	puts("?");
    }
}




void AFE_SetBalanceMask(uint8_t balMask)
{
     bq769x0_SetBalancingMask(balMask);
}

uint8_t AFE_GetBalanceMask()
{
     return bq769x0_GetBalancingMask();
}


//####################### PRIVATE CODE ########################################


//######################### DRAWING FUNCTIONS ##################################
void AFE_DrawInit()
{

    puts("\t\t\tBQ769x0 DEBUG\n\n");

    uint8_t ii;
    VT100_PrintUnderline(" Chn      ADC    Engr  \r\n");
    for (ii = 1 ; ii <= AFE_NUM_CELLS; ii++)
    {
    	printf("Cell%u\r\n", ii);
    }

    puts("PackV");
    puts("DieT");
    puts("Curr");
}

void AFE_DrawUpdate()
{
	uint8_t ii;
	puts("\n\n\n");

	for (ii = 0 ; ii < AFE_NUM_CELLS; ii++)
	{
		printf("\t0x%04X\t%5u mV\r\n", latestMeasurements.cellVltgs_raw[ii], latestMeasurements.cellVltgs_mV[ii]);
	}

	printf("\t0x%04X\t%5lu mV\r\n", latestMeasurements.packVltg_raw, latestMeasurements.packVltg_mV);
	printf("\t0x%04X\t%5d dC\r\n", latestMeasurements.dieTemp_raw, latestMeasurements.dieTemp_dC);
	printf("\t0x%04X\t%5ld mA\r\n", latestMeasurements.current_raw, latestMeasurements.current_mA);
}




///####### PRIVATE FUNCTIONS ################################################

/**
 * The index of a cell does not necessarily correspond to how they are hooked up
 * to AFE.
 */
static uint8_t cellInx2ConnectionInx(uint8_t cellInx)
{

	//This is the 3 cell configuration
	switch (cellInx)
	{
		case 0: return 0; //VC1
		case 1: return 1; //VC2
		case 2: return 4; //VC5

		default:
			printf("Bad cell index %u!\r\n", cellInx);
		break;
	}

	return 0;
}
/**
 * Convert a raw cell voltage ADC reading to mV
 *
 * The BQ76940 register map stores 16bit ADC readings as two 8bit values, big-
 * endian. Given the cell index, this function will correctly assemble the data
 * regardless of processor endianess and apply the BQ's gain and offset to convert to mV.
 *
 * Stores in latestMeasurements struct
 *
 * @param cellInx cell index (not connection index)
 * @return Success
 */
static bool unpackCellVoltage_mV(uint8_t cellInx)
{
    uint8_t cnx = cellInx2ConnectionInx(cellInx);

    /* Get the cell voltage high register from the index */
    uint32_t VCx_HI = *(&(bqMap.VC1_HI) + BQ769x0_CONVERSION_STEP*cnx);

    uint32_t VCx_LO = *(&(bqMap.VC1_HI) + BQ769x0_CONVERSION_STEP*cnx + BQ769x0_REGISTER_STEP);

    latestMeasurements.cellVltgs_raw[cellInx] = (VCx_HI << 8) + VCx_LO;

    uint32_t temp = latestMeasurements.cellVltgs_raw[cellInx];

    temp *= adcCalib.adcGain_uV;
    temp /= 1000;
    temp += adcCalib.cellOffset_mV;

    latestMeasurements.cellVltgs_mV[cellInx] = temp;

    return true;
}

/**
 * Convert the full battery voltage ADC reading to mV,
 * store in latestMeasurements struct
 *
 * Formula from BQ76940 datasheet:
 * V_batt = 4 * ADC * GAIN + (OFFSET * AFE_NUM_CELLS)
 *
 * @return Success
 */
static bool unpackPackVoltage_mV(void)
{

    latestMeasurements.packVltg_raw = (bqMap.BAT_HI << 8) + (bqMap.BAT_LO);

    uint32_t temp = latestMeasurements.packVltg_raw;

    temp *= adcCalib.adcGain_uV;
    temp *= 4;
    temp /= 1000;
    temp += adcCalib.packOffset_mV;

    latestMeasurements.packVltg_mV = temp;

    return true;
}

static bool unpackDieTemp_dC(void)
{
    latestMeasurements.dieTemp_raw = (bqMap.TS1_HI << 8) + bqMap.TS1_LO;
    uint16_t temp_mV = (latestMeasurements.dieTemp_raw*382)/1000; //this puts the reading in mV
    int32_t deltaV = ((int32_t) temp_mV) - 1200;
    int32_t deltaC = deltaV/42;
    int32_t dC = 25 - deltaC;
    latestMeasurements.dieTemp_dC = dC;
    return true;
}

static void printCalibration()
{
	printf("Gain = %u uV\r\n", adcCalib.adcGain_uV);
	printf("CellOffset = %d mV\r\n", adcCalib.cellOffset_mV);
	printf("PackOffset = %d mV\r\n", adcCalib.packOffset_mV);
}

static void unpackCalibration()
{

    /* Since ADCGAIN is broken up across two registers, we'll
     * combine it now for simplicity. The formula is
     * GAIN = (ADCGAIN + 365)*1uV/tick
     */
    adcCalib.adcGain_uV =
    		(bqMap.ADCGAIN1.ADCGAIN_4_3 << 3) +
            (bqMap.ADCGAIN2.ADCGAIN_2_0) +
            BQ76940_MIN_ADCGAIN__uV;

    adcCalib.cellOffset_mV = (int8_t) bqMap.ADCOFFSET;

    /* Also pre-compute the full battery offset to avoid the
     * multiplication every time we read the battery voltage */
    adcCalib.packOffset_mV = AFE_NUM_CELLS * adcCalib.cellOffset_mV;
}


static HAL_StatusTypeDef bq769x0_Config(void)
{
	HAL_StatusTypeDef s = HAL_OK;

	bqMap.SYS_STAT.u8 			= 0xFF; 							//Clear all pending alerts
	bqMap.SYS_CTRL1.ADC_EN 		= 1; 								//turn on ADC
	bqMap.SYS_CTRL1.TEMP_SEL 	= 0; 								//use internal temp

	bqMap.SYS_CTRL2.CC_EN 		= 1; 								//continous CC

	bqMap.PROTECT1.RSNS 		= 1;        						// higher Rsns voltage readings
	bqMap.PROTECT1.SCD_THRESH 	= BQ769x0_SCD_THRESH_HI__200mV;    	// 100A w/ 2mOhm
	bqMap.PROTECT1.SCD_DELAY 	= BQ769x0_SCD_DELAY__70us;

	bqMap.PROTECT2.OCD_THRESH 	= BQ769x0_OCD_THRESH_HI__100mV;    	// 50A w/ 2mOhm
	bqMap.PROTECT2.OCD_DELAY 	= BQ769x0_OCD_DELAY__1280ms;

	bqMap.PROTECT3.OV_DELAY 	= BQ769x0_OV_DELAY__4s;
	bqMap.PROTECT3.UV_DELAY 	= BQ769x0_UV_DELAY__4s;

	//bqMap.OV_TRIP = CalculateVoltageLimitRegister(AOV__mV);	//<SRB>### DETERMINE VALUES TO BE USED ###
	//bqMap.UV_TRIP = CalculateVoltageLimitRegister(AUV__mV);	//<SRB>### DETERMINE VALUES TO BE USED ###
	bqMap.OV_TRIP = 0xAC;										//<SRB>### USE DEFAULT VALUES FOR NOW ###
	bqMap.UV_TRIP = 0x97;										//<SRB>### USE DEFAULT VALUES FOR NOW ###

	bqMap.CC_CFG.u8 = (uint8_t) CC_CFG_KEY_VAL; //this is what it is


	if (( s=bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__SYS_STAT,  bqMap.SYS_STAT.u8  )) != HAL_OK ) return(s);
	if (( s=bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__SYS_CTRL1, bqMap.SYS_CTRL1.u8 )) != HAL_OK ) return(s);
	if (( s=bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__SYS_CTRL2, bqMap.SYS_CTRL2.u8 )) != HAL_OK ) return(s);
	if (( s=bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__PROTECT1,  bqMap.PROTECT1.u8  )) != HAL_OK ) return(s);
	if (( s=bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__PROTECT2,  bqMap.PROTECT2.u8  )) != HAL_OK ) return(s);
	if (( s=bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__PROTECT3,  bqMap.PROTECT3.u8  )) != HAL_OK ) return(s);
	if (( s=bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__OV_TRIP,   bqMap.OV_TRIP      )) != HAL_OK ) return(s);
	if (( s=bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__UV_TRIP,   bqMap.UV_TRIP      )) != HAL_OK ) return(s);
	if (( s=bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__CC_CFG,    bqMap.CC_CFG.u8    )) != HAL_OK ) return(s);

	return(s);
}


HAL_StatusTypeDef bq769x0_ClearAlerts(void)
{
	bq769x0_RequestReadRegister_U8(BQ769x0_ADDR__SYS_STAT);

	return( bq769x0_RequestWriteRegister_U8( BQ769x0_ADDR__SYS_STAT,  0xFF  ));  // Clear pending alerts
}


static void bq769x0_WriteBalancing(void)
{
	bq769x0_RequestWriteRegister_U8(BQ769x0_ADDR__CELLBAL1, bqMap.CELLBAL1.u8);
}


uint8_t bq769x0_GetBalancingMask(void)
{
	return (bqMap.CELLBAL1.u8);
}

void bq769x0_SetBalancingMask(uint8_t mask)
{
	bqMap.CELLBAL1.u8 = mask;
}


static HAL_StatusTypeDef bq769x0_RequestReadRegister_U8(T_BQ769x0_RegisterAddress addr)
{
	HAL_StatusTypeDef Status;
	uint8_t CRCInput[2];
	uint8_t ReadData[2];
	uint8_t crc = 0;

	Status = I2C_ReadMem(addr, ReadData, 2, AFE_TXRX_TIMEOUT);

	CRCInput[0] = (BQ769x0_I2C_ADDR7 << 1) + 1;	// This +1 is for 1 in R/W bit
	CRCInput[1] = ReadData[0];

	crc = CRC8(CRCInput, 2, CRC_KEY);

	if (crc != ReadData[1])
	{
		Status = HAL_ERROR;
	}
	else
	{
		bqMap.raw_bytes[addr] = ReadData[0];
	}

	return (Status);
}

static HAL_StatusTypeDef bq769x0_RequestReadRegister_U16(T_BQ769x0_RegisterAddress addr)
{
	HAL_StatusTypeDef Status;
	uint8_t CRCInput1[2];
	uint8_t CRCInput2;
	uint8_t ReadData[4];
	uint8_t crc1 = 0;
	uint8_t crc2 = 0;

	Status = I2C_ReadMem(addr, ReadData, 4, AFE_TXRX_TIMEOUT);

	CRCInput1[0] = (BQ769x0_I2C_ADDR7 << 1) + 1;	// This +1 is for 1 in R/W bit
	CRCInput1[1] = ReadData[0];
	crc1 = CRC8(CRCInput1, 2, CRC_KEY);

	CRCInput2 = ReadData[2];
	crc2 = CRC8(&CRCInput2, 1, CRC_KEY);

	if ((crc1 != ReadData[1]) || (crc2 != ReadData[3]))
	{
		Status = HAL_ERROR;
	}
	else
	{
		bqMap.raw_bytes[addr] = ReadData[0];
		bqMap.raw_bytes[++addr] = ReadData[2];
	}

	return (Status);
}


static HAL_StatusTypeDef bq769x0_RequestWriteRegister_U8(T_BQ769x0_RegisterAddress addr, uint8_t data)
{
	uint8_t CrcBuffer[3];
	CrcBuffer[0] = BQ769x0_I2C_ADDR7 << 1;
	CrcBuffer[1] = addr;
	CrcBuffer[2] = data;

	uint8_t DataBuffer[2];
	DataBuffer[0] = data;
	DataBuffer[1] = CRC8(CrcBuffer, 3, CRC_KEY);

	return (I2C_WriteMem(addr, DataBuffer, 2, AFE_TXRX_TIMEOUT));
}


static uint8_t CRC8(uint8_t *ptr, uint8_t len, uint8_t key)
{
	uint8_t i;
	uint8_t crc=0;

	while(len--!=0)
	{
		for(i=0x80; i!=0; i/=2)
		{
			if((crc & 0x80) != 0)
			{
				crc *= 2;
				crc ^= key;
			}
			else
				crc *= 2;

			if((*ptr & i)!=0)
				crc ^= key;
		}
		ptr++;
	}

	return(crc);
}


static HAL_StatusTypeDef GetADCGainOffset(void)
{
	HAL_StatusTypeDef Status;

	Status  = bq769x0_RequestReadRegister_U8(BQ769x0_ADDR__ADCGAIN1);
	Status |= bq769x0_RequestReadRegister_U8(BQ769x0_ADDR__ADCGAIN2);
	Status |= bq769x0_RequestReadRegister_U8(BQ769x0_ADDR__ADCOFFSET);

	return (Status);
}


/**
 * Calculate value of the current across sense resistor.
 * Reference BQ76920 Datasheet, SLUSBK2I – OCTOBER 2013 – REVISED MARCH 2022
 */
static bool unpackCurent(void)
{
	int16_t CC_HI = bqMap.CC_HI;
	int16_t CC_LO = bqMap.CC_LO;
	int16_t CC = (CC_HI << 8) + CC_LO;
	int32_t CC_Voltage = CC * CC_LSB_VALUE_uV_x1000;
	latestMeasurements.current_raw = CC;

	latestMeasurements.current_mA = CC_Voltage / CC_SENSE_RESISTOR_mOhm;
	latestMeasurements.current_mA /= 1000; // convert uA to mA

	/* Current direction is opposite of what AFE gets, so need to multiply with -1 */
	latestMeasurements.current_mA *= -1;

	T_SignalDatum datum = {.i32 = latestMeasurements.current_mA};
	SIG_WriteSignal(afeSigs.currentRoot, datum);
}

//<SRB>##########################################################################################################
//<SRB>### MOVE TO BSP DIRECTORY ################################################################################
//<SRB>##########################################################################################################

#include "main.h"

//################ HAL IMPLEMENTATIONS #########################


//<SRB>###  VERIFY THE POLARITY #################
//<SRB>###  WHERE SHOULD THIS BE CALLED FROM? ###
void AFE_HW_DisableAfePower( void )
{
	HAL_GPIO_WritePin(AFE_Power_Kill_GPIO_Port, AFE_Power_Kill_Pin, GPIO_PIN_SET);
}

void AFE_HW_EnableAfePower( void )
{
	HAL_GPIO_WritePin(AFE_Power_Kill_GPIO_Port, AFE_Power_Kill_Pin, GPIO_PIN_RESET);
}


static void MX_I2C2_Init(void)
{
	hi2c2.Instance = I2C2;
	hi2c2.Init.Timing = 0x00707CBB;
	hi2c2.Init.OwnAddress1 = 0;
	hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c2.Init.OwnAddress2 = 0;
	hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

	if (HAL_I2C_Init(&hi2c2) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure Analogue filter
	*/

	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
	{
		Error_Handler();
	}

	/** Configure Digital filter
	*/
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
	{
		Error_Handler();
	}
}

