// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//                   Copyright 2021 EaglePicher Technologies
//
//             This file is the property of EaglePicher Technologies
//               Any unauthorized use or duplication is prohibited
//
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */


#include "noaa_bms.h"
#include "bluflex_core.h"
#include "afe_bq76920.h"
#include <setup.h>
#include "adc.h"
#include "sys.h"
#include "sbs.h"
#include "sig.h"

//char debugbfr[100];



/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */



const T_AppConfigValues appConfigValues =
{
	.consoleUartPort 					= 0, //ignore for now, driver defauls to only one port
	.consoleUartBaud 					= 115200,
//<SRB>### OUT FOR TESTING ###>	.consoleUartTxQueueLength 			= 1024,
	.consoleUartTxQueueLength 			= 1024,  //<SRB>### IN FOR TESTING ###
	.consoleUartRxQueueLength 			= 32,

	.primaryCanPort 					= (0),
	.primaryCanBaud 					= 250,

	.sysClock_Hz 						= 80e6,
	.defaultOscillatorId 				= 3,
	.isSleepEnabled 					= false,  //true,
	.clearPicWdtOnGeneralExcepetion 	= true, //we have an external WDT, so this is OK

	.extraLibConfigSlot1				= NULL,
	.extraLibConfigSlot2 				= NULL,
};



CORE_SET_APP_NAME("NOAA STM32");



/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */


#define huart_dbg  huart4
#define huart_cli  huart3


/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

SMBUS_HandleTypeDef hsmbus1;

IWDG_HandleTypeDef hiwdg;

LPTIM_HandleTypeDef hlptim2;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart4;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_SMBUS_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_UART4_Init(void);
static void MX_RTC_Init(void);
static void MX_DMA_Init(void);
static void MX_IWDG_Init(void);
static void MX_LPTIM2_Init(void);
/* USER CODE BEGIN PFP */



static bool initSignals(void);



/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */



bool dbg_printf( const char*, ... );




//##############################################################################
//##############################################################################
//##############################################################################
//####                            MAIN                                       ###
//##############################################################################
//##############################################################################
//##############################################################################

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */


//<SRB>###########################################################################
//<SRB>### THE INITIALIZATION CODE BELOW NEEDS TO MOVE TO CORE_Init() FUNCTION ###
//<SRB>###########################################################################
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_I2C1_SMBUS_Init();
  MX_SPI1_Init();
  MX_USART3_UART_Init();
  MX_UART4_Init();
  MX_RTC_Init();
  MX_DMA_Init();
  MX_IWDG_Init();
  MX_LPTIM2_Init();
  /* USER CODE BEGIN 2 */



	//############################ INIT TASKS ##################################
	CORE_Init_And_Schedule();
	puts("Core_Init_And_Schedule() complete\n\r");

	SMBUS_Init();
	SIG_Init();

	initSignals(); 		//set up the signal tree and relays

	ACC_Init(); 		//ACC init must be called AFTER SOC init

	MCU_HW_ClearWDT();	// If everything is good, poke the WDOG


	//######################### CLI CALLBACKS ###################################
    CLI_ADD_INPUT_CALLBACK(BMS);
	CLI_ADD_INPUT_CALLBACK(LED);
	CLI_ADD_INPUT_CALLBACK(LOG);
	CLI_ADD_INPUT_CALLBACK(SYS);
	CLI_ADD_INPUT_CALLBACK(SCH);
    CLI_ADD_INPUT_CALLBACK(Alarm);
	CLI_ADD_INPUT_CALLBACK(GPIO);
	CLI_ADD_INPUT_CALLBACK(RLY);
	CLI_ADD_INPUT_CALLBACK(ADC);
	CLI_ADD_INPUT_CALLBACK(AFE);
    CLI_ADD_INPUT_CALLBACK(SBS);
    CLI_ADD_INPUT_CALLBACK(ACC);
    CLI_ADD_INPUT_CALLBACK(SIG);
    CLI_ADD_INPUT_CALLBACK(RCD);
    CLI_ADD_INPUT_CALLBACK(AFE);
    CLI_ADD_INPUT_CALLBACK(HTR);
    CLI_ADD_INPUT_CALLBACK(SETUP);
    CLI_ADD_INPUT_CALLBACK(SOC);

	CLI_ADD_FULLSCREEN_MODE(AFE);
	CLI_ADD_FULLSCREEN_MODE(BMS);
	CLI_ADD_FULLSCREEN_MODE(SIG);
	CLI_ADD_FULLSCREEN_MODE(SOC);
	CLI_ADD_FULLSCREEN_MODE(ADC);

	CLI_ADD_FULLSCREEN_MODE_WITH_INPUT(HTR);
	CLI_ADD_FULLSCREEN_MODE_WITH_INPUT(RLY);

	puts("CLI Init complete");

	MCU_HW_ClearWDT();

	//########################### SCHEDULE TASKS ###############################
	bool s = true;

	s &= SCH_CREATE_TASK(AFE);
	s &= SCH_CREATE_TASK(ADC);
	s &= SCH_CREATE_TASK(BMS);
	s &= SCH_CREATE_TASK(SIG);
	s &= SCH_CREATE_TASK(RLY);
	s &= SCH_CREATE_TASK(SMBUS);
	s &= SCH_CREATE_TASK(SOC);
	s &= SCH_CREATE_TASK(ACC);
	s &= SCH_CREATE_TASK(HTR);

    if (s)
    {
        puts("Task Init complete");
    }
    else
    {
        puts("Task init error!");
        while(1) {;}
    }

	MCU_HW_ClearWDT();

    //########################## LEDS ########################################

    puts("Setting up LEDs...");
    T_LED* led_blu = LED_Create("blu",  (T_Pin){LED2_Blue_GPIO_Port,   LED2_Blue_Pin   }, ACTIVE_HIGH);		//<SRB>### ABSTRACT THIS ###
    T_LED* led_org = LED_Create("org",  (T_Pin){LED3_Orange_GPIO_Port, LED3_Orange_Pin }, ACTIVE_HIGH);		//<SRB>### ABSTRACT THIS ###
    T_LED* led_red = LED_Create("red",  (T_Pin){LED4_Red_GPIO_Port,    LED4_Red_Pin    }, ACTIVE_HIGH);		//<SRB>### ABSTRACT THIS ###
    T_LED* led_grn = LED_Create("grn",  (T_Pin){LED1_Green_GPIO_Port,  LED1_Green_Pin  }, ACTIVE_HIGH);		//<SRB>### ABSTRACT THIS ###

    if (led_org && led_blu && led_red && led_grn)
    {
        puts("success!");
    }
    else
    {
        puts("FAILED.");
    }

    SMB_AssignLED(led_org);
    BMS_SetRelayStatusLed(led_blu);
    BMS_AssignLowPowerLed(led_grn);
    SIG_AssignMasterAlarmLed(led_red);

    //######################## FINALIZING #######################################
    if (s)
    {
         LED_SetMode(led_grn, LMODE_BLINK_SLOW);
    }
    else
    {
        LED_SetMode(led_grn, LMODE_BLINK_FAST);
        puts("Task init error!");
    }


    HAL_GPIO_WritePin(LED1_Green_GPIO_Port,  LED1_Green_Pin,  0);		//<SRB>### ABSTRACT THIS ###
    HAL_GPIO_WritePin(LED2_Blue_GPIO_Port,   LED2_Blue_Pin,   0);		//<SRB>### ABSTRACT THIS ###
    HAL_GPIO_WritePin(LED3_Orange_GPIO_Port, LED3_Orange_Pin, 0);		//<SRB>### ABSTRACT THIS ###
    HAL_GPIO_WritePin(LED4_Red_GPIO_Port,    LED4_Red_Pin,    0);		//<SRB>### ABSTRACT THIS ###

	MCU_HW_ClearWDT();

    //Change some of the default Core timing parameters
    SIG_SetTreeUpdatePeriod(100, 0);
    SCH_SetMaxAllowedTaskDelay(15, 0);
    LED_SetVersionBitPeriod_ms(30, 0);
    LED_SetNormalBitPeriod_ms(30, 0);
    LED_DisplayVersion(0);

    CLI_EndStartupMode();

	MCU_HW_ClearWDT();

	puts("Enter Main Loop");

#if HAS_EXTERNAL_WDT
  SCH_AssignHeartbeatPin( (T_Pin){PGEC_GPIO_Port,PGEC_Pin} );
#endif

  	SCH_SetSleepEnable(true);

    while (1)
    {
    	SCH_Run();
		MCU_HW_ClearWDT();
    }
}


void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		Error_Handler();
	}

	HAL_PWR_EnableBkUpAccess();
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE
							  |RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 8;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;

	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
							  |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	{
		Error_Handler();
	}
}


static void MX_ADC1_Init(void)
{
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc1.Init.LowPowerAutoWait = DISABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.NbrOfConversion = 1;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.NbrOfDiscConversion = 1;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc1.Init.DMAContinuousRequests = DISABLE;
	hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	hadc1.Init.OversamplingMode = DISABLE;

	if (HAL_ADC_Init(&hadc1) != HAL_OK)
	{
		puts("HAL_ADC_Init() failed.");
	}
}



static void MX_I2C1_SMBUS_Init(void)
{
	hsmbus1.Instance = I2C1;
	hsmbus1.Init.Timing = 0x00707CBB;
	hsmbus1.Init.AnalogFilter = SMBUS_ANALOGFILTER_ENABLE;
	hsmbus1.Init.OwnAddress1 = 22;
	hsmbus1.Init.AddressingMode = SMBUS_ADDRESSINGMODE_7BIT;
	hsmbus1.Init.DualAddressMode = SMBUS_DUALADDRESS_DISABLE;
	hsmbus1.Init.OwnAddress2 = 0;
	hsmbus1.Init.OwnAddress2Masks = SMBUS_OA2_NOMASK;
	hsmbus1.Init.GeneralCallMode = SMBUS_GENERALCALL_ENABLE;
	hsmbus1.Init.NoStretchMode = SMBUS_NOSTRETCH_DISABLE;
	hsmbus1.Init.PacketErrorCheckMode = SMBUS_PEC_ENABLE;
	hsmbus1.Init.PeripheralMode = SMBUS_PERIPHERAL_MODE_SMBUS_SLAVE;
	hsmbus1.Init.SMBusTimeout = 0x81868186;

	if (HAL_SMBUS_Init(&hsmbus1) != HAL_OK)
	{
		Error_Handler();
	}
}


static void MX_IWDG_Init(void)
{
	hiwdg.Instance = IWDG;
	hiwdg.Init.Prescaler = IWDG_PRESCALER_4;
	hiwdg.Init.Window = 4095;
	hiwdg.Init.Reload = 4095;

	if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
	{
		Error_Handler();
	}
}


/* LPTIM register bits definitions */
#define CFGR_CKSEL		0x00000001
#define CFGR_TRIGEN_0	0x00020000
#define CFGR_TRIGEN_1	0x00040000
#define CFGR_WAVE		0x00100000
#define CFGR_COUNT_MODE	0x00800000

#define CR_ENABLE		0x00000001
#define CR_CNTSTRT		0x00000004

/*
 * Initialize LPTIM to generate PWD signal on pin 60 (D13)
 * PWM is set to 50% duty cycle, freq ~4KHz, 0.256mS period
 */
static void MX_LPTIM2_Init(void)
{
	hlptim2.Instance = LPTIM2;
	hlptim2.Init.Clock.Source = LPTIM_CLOCKSOURCE_ULPTIM;
	hlptim2.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV1;
	hlptim2.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;
	hlptim2.Init.Trigger.ActiveEdge = LPTIM_ACTIVEEDGE_RISING;
	hlptim2.Init.Trigger.SampleTime = LPTIM_TRIGSAMPLETIME_2TRANSITIONS;
	hlptim2.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_LOW;
	hlptim2.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;
	hlptim2.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;
	hlptim2.Init.Input1Source = LPTIM_INPUT1SOURCE_COMP2;
	hlptim2.Init.Input2Source = LPTIM_INPUT1SOURCE_COMP1_COMP2;

	if (HAL_LPTIM_Init(&hlptim2) != HAL_OK)
	{
		Error_Handler();
	}

	/* Refer to Reference Manual RM0351,
	 * pg. 1186, Chapter 34 Low-power timer (LPTIM) */
  	CLEAR_BIT(hlptim2.Instance->CFGR, CFGR_CKSEL);
  	CLEAR_BIT(hlptim2.Instance->CFGR, CFGR_WAVE);
  	CLEAR_BIT(hlptim2.Instance->CFGR, CFGR_TRIGEN_0);
  	CLEAR_BIT(hlptim2.Instance->CFGR, CFGR_TRIGEN_1);
  	CLEAR_BIT(hlptim2.Instance->CFGR, CFGR_COUNT_MODE);

  	SET_BIT(hlptim2.Instance->CR, CR_ENABLE);
  	SET_BIT(hlptim2.Instance->CR, CR_CNTSTRT);

  	WRITE_REG(hlptim2.Instance->CMP, 0x0FFF);
  	WRITE_REG(hlptim2.Instance->ARR, 0x1FFF);
}


static void MX_RTC_Init(void)
{
	RTC_TimeTypeDef sTime = {0};
	RTC_DateTypeDef sDate = {0};

	hrtc.Instance = RTC;
	hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv = 127;
	hrtc.Init.SynchPrediv = 255;
	hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
	hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;

	if (HAL_RTC_Init(&hrtc) != HAL_OK)
	{
		Error_Handler();
	}

	sTime.Hours = 0x0;
	sTime.Minutes = 0x0;
	sTime.Seconds = 0x0;
	sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sTime.StoreOperation = RTC_STOREOPERATION_RESET;

	if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
	{
		Error_Handler();
	}

	sDate.WeekDay = RTC_WEEKDAY_MONDAY;
	sDate.Month = RTC_MONTH_JANUARY;
	sDate.Date = 0x1;
	sDate.Year = 0x0;

	if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
	{
		Error_Handler();
	}
}



static void MX_SPI1_Init(void)
{
	hspi1.Instance = SPI1;
	hspi1.Init.Mode = SPI_MODE_MASTER;
	hspi1.Init.Direction = SPI_DIRECTION_2LINES;
	hspi1.Init.DataSize = SPI_DATASIZE_4BIT;
	hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi1.Init.NSS = SPI_NSS_SOFT;
	hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
	hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi1.Init.CRCPolynomial = 7;
	hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
	hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;

	if (HAL_SPI_Init(&hspi1) != HAL_OK)
	{
		Error_Handler();
	}
}



static void MX_UART4_Init(void)
{
	huart4.Instance = UART4;
	huart4.Init.BaudRate = 115200;
	huart4.Init.WordLength = UART_WORDLENGTH_8B;
	huart4.Init.StopBits = UART_STOPBITS_1;
	huart4.Init.Parity = UART_PARITY_NONE;
	huart4.Init.Mode = UART_MODE_TX_RX;
	huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart4.Init.OverSampling = UART_OVERSAMPLING_16;
	huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

	if (HAL_UART_Init(&huart4) != HAL_OK)
	{
		Error_Handler();
	}
}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{
	huart3.Instance = USART3;
	huart3.Init.BaudRate = 115200;
	huart3.Init.WordLength = UART_WORDLENGTH_8B;
	huart3.Init.StopBits = UART_STOPBITS_1;
	huart3.Init.Parity = UART_PARITY_NONE;
	huart3.Init.Mode = UART_MODE_TX_RX;
	huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart3.Init.OverSampling = UART_OVERSAMPLING_16;
	huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

	if (HAL_UART_Init(&huart3) != HAL_OK)
	{
		Error_Handler();
	}
}


static void MX_DMA_Init(void)
{
	__HAL_RCC_DMA1_CLK_ENABLE();

	HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}



static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOE, PowerOff_Serial_Pin|Heater_B_Pin|AFE_Boot_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(SDPwrOff_GPIO_Port, SDPwrOff_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, LED1_Green_Pin|LED2_Blue_Pin|LED3_Orange_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LED4_Red_GPIO_Port, LED4_Red_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOD, Heater_A_Pin|Debug5_Pin|Debug6_Pin
						  |Debug4_Pin|Debug3_Pin|Debug2_Pin|Debug1_Pin
						  |SDC_CS_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, Discharge_Enable_Pin|Charge_Enable_Pin|Allow_Heat_From_Battery_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pins : AFE_Power_Kill_Pin PowerOff_Serial_Pin LED4_Red_Pin Heater_B_Pin
						   AFE_Boot_Pin */
	GPIO_InitStruct.Pin = AFE_Power_Kill_Pin|PowerOff_Serial_Pin|LED4_Red_Pin|Heater_B_Pin
						  |AFE_Boot_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	/*Configure GPIO pin : DCDC_Power_Good_Pin */
	GPIO_InitStruct.Pin = DCDC_Power_Good_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(DCDC_Power_Good_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : AFE_Alert_Pin */
	GPIO_InitStruct.Pin = AFE_Alert_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(AFE_Alert_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : SD_Inserted_Pin */
	GPIO_InitStruct.Pin = SD_Inserted_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SD_Inserted_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : SDPwrOff_Pin */
	GPIO_InitStruct.Pin = SDPwrOff_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(SDPwrOff_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : LED1_Green_Pin LED2_Blue_Pin LED3_Orange_Pin */
	GPIO_InitStruct.Pin = AFE_Clock_Pin | AFE_Data_Pin | LED1_Green_Pin|LED2_Blue_Pin|LED3_Orange_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : Button1_Pin Button2_Pin */
	GPIO_InitStruct.Pin = Button1_Pin|Button2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	/*Configure GPIO pins : Heater_A_Pin Debug5_Pin Debug6_Pin Debug4_Pin
						   Debug3_Pin Debug2_Pin Debug1_Pin SDC_CS_Pin */
	GPIO_InitStruct.Pin = Heater_A_Pin|Debug5_Pin|Debug6_Pin|Debug4_Pin
						  |Debug3_Pin|Debug2_Pin|Debug1_Pin|SDC_CS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);


	/*Configure GPIO pins : Discharge_Enable_Pin Charge_Enable_Pin Allow_Heat_From_Battery_Pin */
	GPIO_InitStruct.Pin = Discharge_Enable_Pin|Charge_Enable_Pin|Allow_Heat_From_Battery_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

	HAL_GPIO_WritePin(GPIOE, AFE_Power_Kill_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOE, AFE_Boot_Pin,       GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, AFE_Clock_Pin | AFE_Data_Pin, GPIO_PIN_RESET);
}



static bool initSignals(void)
{
	SETUP_Init(); //load any saved config data from NVRAM
	bool isHtrHardwareExpected = SETUP_IsHeaterHwExpected();
	printf("Heater HW expected... %s.\r\n", (isHtrHardwareExpected ? "YES" : "NO"));


	//############################## ALARM TEMPLATES ############################
	//           Name
	//           ClrLo,  ClrHi
	//           WarnLo, WarnHi
	//           CritLo, CritHi
	//
	T_AlarmTemplate* currentAlarmTemplate = Alarm_CreateTemplate_I32(
		   "Currnt",
		   UNIT_CURRENT_MA,
		   -12000, 15000,
		   -14000, 18000,
		   -16000, 20000 );

	T_AlarmTemplate* cellVoltageAlarmTemplate = Alarm_CreateTemplate_U32(
		   "ClVltg",
		   UNIT_VOLTAGE_MV,
		   3300, 4100,
		   3250, 4125,
		   3200, 4150 );

	T_AlarmTemplate* hotTempAlarmTemplate = Alarm_CreateTemplate_I32(
		   "HotTemp",
		   UNIT_TEMP_DC,
		   -10, 70,
		   -15, 75,
		   -20, 80 );

	T_AlarmTemplate* regTempAlarmTemplate = Alarm_CreateTemplate_I32(
		   "RegTemp",
		   UNIT_TEMP_DC,
		   -10, 50,
		   -15, 55,
		   -20, 60 );

	if (!currentAlarmTemplate || !cellVoltageAlarmTemplate || !hotTempAlarmTemplate || !regTempAlarmTemplate)
	{
	   puts("Alarm created failed.");
	   return false;
	}

	//############################### SIGNALS ###################################

	//Create the signal tree, and inform the appropriate modules.

	//Create all the nodes (order doesn't matter)
	T_CoreSignals coreSigs = {NULL};

	coreSigs.nRoot       = SIG_CreateNode("Root",    UNIT_NONE,          INHERIT_ALARM);
	coreSigs.nTempRoot   = SIG_CreateNode("All_Ts",  UNIT_TEMP_DC,       INHERIT_ALARM);
	coreSigs.nCurrRoot   = SIG_CreateNode("PakCur",  UNIT_CURRENT_MA,    currentAlarmTemplate);
	coreSigs.nVltgRoot   = SIG_CreateNode("cvs",     UNIT_VOLTAGE_MV,    cellVoltageAlarmTemplate);
	coreSigs.nSoc        = SIG_CreateNode("SoC",     UNIT_PERCENTAGE,    NO_ALARM);


	bool makeSuccess = true;

	/*Create/Link the Cell Voltage nodes into tree structure - these must be done in cell order.*/
	makeSuccess &= SIG_MakeNodeChildOf(SIG_CreateNode("cv1", UNIT_VOLTAGE_MV, NO_ALARM), coreSigs.nVltgRoot);
	makeSuccess &= SIG_MakeNodeChildOf(SIG_CreateNode("cv2", UNIT_VOLTAGE_MV, NO_ALARM), coreSigs.nVltgRoot);
	makeSuccess &= SIG_MakeNodeChildOf(SIG_CreateNode("cv3", UNIT_VOLTAGE_MV, NO_ALARM), coreSigs.nVltgRoot);

	/*          +---[AllTs]--------+
	*          |                  |
	*       [OnboardT]         [CellT]
	*          |                  |
	* +--------+--------+         +--------+
	* |        |        |         |        |
	* [rt1]  [rt2]  [rt3]    [BaseT]   [HrtTs]
	*                                      |
	*              +-------+------+--------+
	*              |       |      |        |
	*           [HtrT1] [HtrT2] [HtrT3] [HtrT4]
	*/

	/*Create/link the Temperature nodes into tree structure - these must be done in pin order. */

	T_SignalNode* onboardTs  = SIG_CreateNode("BrdTs", UNIT_TEMP_DC, INHERIT_ALARM);
	makeSuccess &= SIG_MakeNodeChildOf(onboardTs, coreSigs.nTempRoot);

	T_SignalNode* rt1       = SIG_CreateNode("rt1", UNIT_TEMP_DC, hotTempAlarmTemplate);
	makeSuccess &= SIG_MakeNodeChildOf(rt1, onboardTs);

	T_SignalNode* rt2       = SIG_CreateNode("rt2", UNIT_TEMP_DC, hotTempAlarmTemplate);
	makeSuccess &= SIG_MakeNodeChildOf(rt2, onboardTs);

	T_SignalNode* rt3       = SIG_CreateNode("rt3", UNIT_TEMP_DC, hotTempAlarmTemplate);
	makeSuccess &= SIG_MakeNodeChildOf(rt3, onboardTs);

	T_SignalNode* cellTs     = SIG_CreateNode("CellT", UNIT_TEMP_DC, INHERIT_ALARM);
	makeSuccess &= SIG_MakeNodeChildOf(cellTs, coreSigs.nTempRoot);

	T_SignalNode* baseboardT = SIG_CreateNode("BaseT", UNIT_TEMP_DC, hotTempAlarmTemplate);
	makeSuccess &= SIG_MakeNodeChildOf(baseboardT, cellTs);

	T_SignalNode* heaterTs = NULL;

	if (isHtrHardwareExpected)
	{
		heaterTs = SIG_CreateNode("HtrT", UNIT_TEMP_DC, hotTempAlarmTemplate);
		makeSuccess &= SIG_MakeNodeChildOf(heaterTs, cellTs);

		for (uint8_t ii = 0; ii < NUM_HEATER_THERMISTORS; ii++)	//<SRB>### REPLACE 4 WITH CONSTANT ###
		{
			 makeSuccess &= SIG_MakeNodeChildOf(SIG_CreateNode(ANONYMOUS, UNIT_TEMP_DC, NO_ALARM), heaterTs);
		}
	}

	makeSuccess &= SIG_MakeNodeChildOf(coreSigs.nVltgRoot, coreSigs.nRoot);
	makeSuccess &= SIG_MakeNodeChildOf(coreSigs.nTempRoot, coreSigs.nRoot);
	makeSuccess &= SIG_MakeNodeChildOf(coreSigs.nCurrRoot, coreSigs.nRoot);
	makeSuccess &= SIG_MakeNodeChildOf(coreSigs.nSoc, 	  coreSigs.nRoot);

	if (!makeSuccess)
	{
	   puts("Failed making parent/child.");
	   return false;
	}

	//After all the nodes are created and attached, need to validate the tree.
	if (!SIG_ValidateTree(coreSigs.nRoot))
	{
	   puts("Tree validation failed!");
	   return false;
	}


	//########################### RELAY SETUP ###################################
	//HiSide (CHARGE CONTROL) <--> C3
	//LoSide (DISCHARGE CONTROL) <--> C2
	//Writing a HI to the discharge relay controller is open!

	puts("Initializing RLY module...\n\r");
	T_RLY_Config rlyConfig =
	{
	.mechContactorMaxCloseTime_ms 	= 0xFFFF,  //<SRB>### FIX THIS -IF NEEDED, GET REAL VALUES ###
	.mechContactorMaxOpenTime_ms  	= 0xFFFF,  //<SRB>### FIX THIS -IF NEEDED, GET REAL VALUES ###
	.prechargeMaxCloseTime_ms 		= 0xFFFF,  //<SRB>### FIX THIS -IF NEEDED, GET REAL VALUES ###
	.prechargeMaxOpenTime_ms 		= 0xFFFF,  //<SRB>### FIX THIS -IF NEEDED, GET REAL VALUES ###
	};
	RLY_Init( &rlyConfig);

	puts("Initializing Relays...");
	T_Relay* chg =  RLY_CreateSimpleSwitch("Chg",  (T_Pin){Charge_Enable_GPIO_Port,Charge_Enable_Pin});
	T_Relay* dis =  RLY_CreateSimpleSwitch("Dis",  (T_Pin){Discharge_Enable_GPIO_Port,Discharge_Enable_Pin});
	T_Relay* htrA = RLY_CreateSimpleSwitch("HtrA", (T_Pin){Heater_A_GPIO_Port,Heater_A_Pin});
	T_Relay* htrB = RLY_CreateSimpleSwitch("HtrB", (T_Pin){Heater_B_GPIO_Port,Heater_B_Pin});
	T_Relay* hpe =  RLY_CreateSimpleSwitch("HPE",  (T_Pin){Allow_Heat_From_Battery_GPIO_Port,Allow_Heat_From_Battery_Pin});


	puts("Initializing HTR module...");

	T_HTR_Config htrConfig =
	{
		.heaterHardwareExpected = isHtrHardwareExpected,
		.htrA_Relay = htrA,
		.htrB_Relay = htrB,
		.hbe_Relay = hpe,
		.chg_Relay = chg,
		.nCellTempRoot = cellTs,
	};

	HTR_Init(&htrConfig, &coreSigs);


	puts("Initializing BMS module...");

	T_BMS_Config bmsConfig =
	{
		.nCellTemps = cellTs,
		.pChg = chg,
		.pDis = dis,
	};

	BMS_Init(&coreSigs, &bmsConfig);


	//### AFE ##################################################################
	puts("Initializing AFE module...");

	T_AFE_BQ76920_Sigs afeSigs =
	{
		.cellVoltageRoot 	= coreSigs.nVltgRoot,
		.currentRoot 		= coreSigs.nCurrRoot,
		.dischargeRelay     = dis,
	};

	AFE_Init(&afeSigs);

	//### ADC ###################################################################
	T_ADC_Sigs adcSigs =
	{
		.onboardTempRoot = onboardTs,
		.heaterTempRoot  = heaterTs,
		.baseboardTemp   = baseboardT,
	};

	ADC_Init(&adcSigs);


	//### CELL #################################################################
	puts("Selecting Cell Profile...");
	CELL_SelectProfile(CELL_TYPE_PANASONIC_NCR18650, CELL_USAGE_BASIC);

	//### SOC ##################################################################
	puts("Initializing SoC module...");
	T_SOC_Config socConfig =
	{
	.runPeriod_ms = 200,
	.designCapacity_mAh = 120000,

	.ccCurrThreshP_mA = 1500,
	.ccCurrThreshN_mA = -1500,
	.absSocLookupCurr_mA = 2000,

	.loSocSnapThresh_mV = 3000,
	.hiSocSnapThresh_mV = 4150,

	.minTimeForActiveCcOut_ms = 1000,
	.minTimeForActiveCcIn_ms = 1000,
	.minTimeForActiveLkp_ms = 30*1000,

	.maxSocDeltaPerMin_p01pct = 60, //.60%/min = .01%/sec
	.minSocDeltaPerLoop_p01pct = 100, //Must be >1% delta between lookup and actual
	.cycleUpper_pct_SoC = 95,
	.cycleLower_pct_SoC = 20,

	};
	SOC_Init(&coreSigs, &socConfig);


	T_SignalDatum d = {.u32 = 0};
	SIG_WriteSignal(coreSigs.nSoc, d);//Set SoC node as init'd, so it doesn't hold up SIG_IsReady() TODO: separate measured/calculated signals, etc.

	//### SMBUS #################################################################
	T_SBS_Sigs sbsSigs =
	{
	   .ambientTemp = rt1,
	   .balanceTemp = rt2,
	   .boardTemp = rt3,
	   .cellTemp = cellTs,
	};

	SBS_Init(&coreSigs, &sbsSigs);


	return true;
}


/**
  * @brief  AFE Alert detection callback.
  * @param  None
  * @retval None
  */
extern bool afeAlert;



static void AFE_Alert_IRQ_Callback(void)
{
	afeAlert = true;
	bq769x0_ClearAlerts();
	return;
}



static void LP_Button_IRQ_Callback(void)
{
	BMS_ProcessLowPowerButtonPush();
}



static void Reset_Button_IRQ_Callback(void)
{
    SYS_Restart();
}


/**
  * @brief  EXTI line detection callback.
  * @param  GPIO_Pin Specifies the port pin connected to corresponding EXTI line.
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin)
	{
		case GPIO_PIN_5:
			AFE_Alert_IRQ_Callback();
			break;

		case GPIO_PIN_11:
			LP_Button_IRQ_Callback();
			break;

		case GPIO_PIN_12:
			Reset_Button_IRQ_Callback();
			break;

		default:
			//<SRB>### DOES ANYTHING NEED TO BE DONE HERE? ###
			break;

	}

	return;
}


void AFE_TriggerReset(void)
{
	HAL_GPIO_WritePin(AFE_Boot_GPIO_Port, AFE_Boot_Pin, GPIO_PIN_RESET);
}

void AFE_ReleaseReset(void)
{
	HAL_GPIO_WritePin(AFE_Boot_GPIO_Port, AFE_Boot_Pin, GPIO_PIN_SET);
}



void DisableRS232(void)
{
	//<SRB>### SHOULD I ALSO TURN OFF THE PORT INTERNALLY? ###
	HAL_GPIO_WritePin(PowerOff_Serial_GPIO_Port, PowerOff_Serial_Pin, GPIO_PIN_SET);
}


void EnableRS232(void)
{
	//<SRB>### SHOULD I ALSO TURN ON THE PORT INTERNALLY? ###
	HAL_GPIO_WritePin(PowerOff_Serial_GPIO_Port, PowerOff_Serial_Pin, GPIO_PIN_RESET);
}





//char cliTxBfr[100];
//void cli_printf( const char* format, ... )
//{
//	__VALIST va;
//	sprintf(cliTxBfr,format,va);
//
//	char* txPtr = cliTxBfr;
//
//	if ( UART_CheckIdleState(&huart3) != HAL_OK )
//		return -1;
//
//	HAL_UART_Transmit_IT(&huart3, txPtr, strlen(txPtr));
//
////	while ( *txPtr != '\0' )
////	{
////	  HAL_UART_Transmit(&huart4, (uint8_t *)txPtr, 1, 0xFFFF);
////	  txPtr++;
////	}
//}

//void cli_puts( const char* dataPtr )
//{
//	char* txPtr = dataPtr;
//
//	if ( UART_CheckIdleState(&huart3) != HAL_OK )
//		return -1;
//
//	HAL_UART_Transmit_IT(&huart3, txPtr, strlen(txPtr));
//
//	//	while ( *txPtr != '\0' )
////	{
////	  HAL_UART_Transmit(&huart4, (uint8_t *)txPtr, 1, 0xFFFF);
////	  txPtr++;
////	}
//}



char dbgTxBfr[100];
bool dbg_printf( const char* format, ... )
{
	__VALIST va;
	sprintf(dbgTxBfr,format,va);

	char* txPtr = dbgTxBfr;

	if ( UART_CheckIdleState(&huart4) != HAL_OK )
		return -1;

	HAL_UART_Transmit_IT(&huart4, txPtr, strlen(txPtr));

//	while ( *txPtr != '\0' )
//	{
//	  HAL_UART_Transmit_IT(&huart4, (uint8_t *)txPtr, 1);
//	  txPtr++;
//	}
	return(0);
}

void dbg_puts( const char* dataPtr )
{
	char* txPtr = dataPtr;

	while ( *txPtr != '\0' )
	{
	  HAL_UART_Transmit_IT(&huart4, (uint8_t *)txPtr, 1);
	  txPtr++;
	}
}

bool SMBUS_StartListening(void)
{
	return (HAL_SMBUS_EnableListen_IT(&hsmbus1) == HAL_OK);
}




/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */


	/* User can add his own implementation to report the HAL error return state */
  __disable_irq();

  while (1)
  {
  }


  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
