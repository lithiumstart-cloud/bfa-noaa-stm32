/**
 * @file adc.c
 * @brief ADC Driver for STM32 Daughter card
 * @copyright 2017 EaglePicher, Lithiumstart Division
 * @author A. Kessler, Modified: Miroslav Grubic
 * @date   2017-05-29
 *
**/
//############################## INCLUDES ######################################

#include "adc.h"
#include <stdint.h>
#include <stdbool.h>
#include <bluflex_core.h>
#include "stm32l4xx_hal.h"


#define NUM_BOARD_THERMISTORS 			(4)
#define NUM_THERMISTORS					(NUM_BOARD_THERMISTORS + NUM_HEATER_THERMISTORS)
#define NUM_BASEBOARD_THERMISTORS		(1)
#define ADC_THERMISTOR_BOOTUP_TIME_mS	(1000)

//############################## DEFINES ######################################

typedef enum
{
	ST_ADC_BOOTING,
    ST_ADC_SETUP_BOARD_TEMPERATURES,
    ST_ADC_READING_BOARD_TEMPERATURES,
	ST_ADC_SETUP_HEATER_TEMPERATURES,
	ST_ADC_READING_HEATER_TEMPERATURES,
	ST_ADC_ERROR
} T_State;


//########################### CONFIG VALUES ####################################

static struct
{
       uint32_t dummy;
} configValues =
{
    .dummy = 0xFFFF,
};


#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))


static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "dummy",
};


//############################## VARIABLES ####################################

static T_Timer adcTimer;
static T_ADC_Sigs adcSigs;
static T_State adcCurrentState = ST_ADC_SETUP_BOARD_TEMPERATURES;
static uint8_t BoardTemperaturesIndex = 0;
static uint8_t HeaterTemperaturesIndex = 0;


static struct
{
    uint32_t raw[NUM_BOARD_THERMISTORS];
    int32_t  temp_dC[NUM_BOARD_THERMISTORS];
} latestBoardTemperatures;


static struct
{
    uint32_t raw[NUM_HEATER_THERMISTORS];
    int32_t  temp_dC[NUM_HEATER_THERMISTORS];
} latestHeaterTemperatures;


static uint32_t adcChannelMaskBoardTemperatures[NUM_BOARD_THERMISTORS] =
{
	ADC_CHANNEL_9,
	ADC_CHANNEL_10,
	ADC_CHANNEL_11,
	ADC_CHANNEL_12
};


static uint32_t adcChannelMaskHeaterTemperatures[NUM_HEATER_THERMISTORS] =
{
	ADC_CHANNEL_5,
	ADC_CHANNEL_6,
	ADC_CHANNEL_7,
	ADC_CHANNEL_8,
};


//############################### PUBLIC DATA ##################################

extern ADC_HandleTypeDef hadc1;

//############################### PROTOTYPES####################################

static void ADC_HW_StartSampling(void);
static bool ADC_HW_IsSamplingDone(void);
static void ADC_HW_StartSampling(void);
static uint16_t ADC_HW_GetRaw(void);
static void ADC_HW_SetupForBoardTemp(uint8_t inx);
static void ADC_HW_SetupForHeaterTemp(uint8_t inx);
static void postBoardTemperatures(void);
static void postHeaterTemperatures(void);


//########################### PUBLIC CODE ######################################

void ADC_Init(const T_ADC_Sigs* pAdcSigs)
{
	if (pAdcSigs)
	{
		adcSigs =  *pAdcSigs;
	}

    TMR_StartOneshot(&adcTimer, ADC_THERMISTOR_BOOTUP_TIME_mS);
	adcCurrentState = ST_ADC_BOOTING;
}



bool ADC_Update()
{
	bool returnStatus = false;

	switch (adcCurrentState)
	{
		case ST_ADC_BOOTING:
			if (TMR_HasTripped_ms(&adcTimer))
			{
				adcCurrentState = ST_ADC_SETUP_BOARD_TEMPERATURES;
			}
			break;

		case ST_ADC_SETUP_BOARD_TEMPERATURES:
			BoardTemperaturesIndex = 0;
			ADC_HW_SetupForBoardTemp(BoardTemperaturesIndex);
			ADC_HW_StartSampling();
			adcCurrentState = ST_ADC_READING_BOARD_TEMPERATURES;
			break;

		case ST_ADC_READING_BOARD_TEMPERATURES:
			if (ADC_HW_IsSamplingDone())
			{
				latestBoardTemperatures.raw[BoardTemperaturesIndex] = ADC_HW_GetRaw();
				latestBoardTemperatures.temp_dC[BoardTemperaturesIndex] =
						THRM_LookupTemp__dC(
								(latestBoardTemperatures.raw[BoardTemperaturesIndex] >> 4),
								THRM_TYPE__B4500);
				BoardTemperaturesIndex++;

				if (BoardTemperaturesIndex < NUM_BOARD_THERMISTORS)
				{
					ADC_HW_SetupForBoardTemp(BoardTemperaturesIndex);
					ADC_HW_StartSampling();

				}
				else
				{
					adcCurrentState = ST_ADC_SETUP_HEATER_TEMPERATURES;
					postBoardTemperatures();
				}
			}
			break;

		case ST_ADC_SETUP_HEATER_TEMPERATURES:
			HeaterTemperaturesIndex= 0;
			ADC_HW_SetupForHeaterTemp(HeaterTemperaturesIndex);
			ADC_HW_StartSampling();
			adcCurrentState = ST_ADC_READING_HEATER_TEMPERATURES;
			break;

		case ST_ADC_READING_HEATER_TEMPERATURES:
			if (ADC_HW_IsSamplingDone())
			{
				latestHeaterTemperatures.raw[HeaterTemperaturesIndex] = ADC_HW_GetRaw();
				latestHeaterTemperatures.temp_dC[HeaterTemperaturesIndex] =
						THRM_LookupTemp__dC(
								(latestHeaterTemperatures.raw[HeaterTemperaturesIndex] >> 4),
								THRM_TYPE__B4500);
				HeaterTemperaturesIndex++;

				if (HeaterTemperaturesIndex < NUM_HEATER_THERMISTORS)
				{
					ADC_HW_SetupForHeaterTemp(HeaterTemperaturesIndex);
					ADC_HW_StartSampling();

				}
				else
				{
					adcCurrentState = ST_ADC_SETUP_BOARD_TEMPERATURES;
					postHeaterTemperatures();
					returnStatus = true;
				}
			}
			break;

		case ST_ADC_ERROR:
			break;

		default:
			printf("ADC: Unknown state: %u\r\n", adcCurrentState);
			adcCurrentState = ST_ADC_ERROR;
			break;
	}

	/*  Don't sleep unless all conversions are done */
    return (returnStatus);
}


//########################### DRAWING FUNCTIONS ################################

void ADC_DrawInit()
{
	putstr("\t\tADC DEBUG\r\n\nChn\tRaw\tEngr\r\n");

	for (uint8_t ii = 0; ii < NUM_THERMISTORS; ii++)
	{
	   printf("T%u\r\n", ii + 1);
	}
}


void ADC_DrawUpdate()
{
    puts("\n\n");

    for (uint32_t ii = 0; ii < NUM_BOARD_THERMISTORS; ii++)
    {
       printf("\t%04u\t%03d\r\n", latestBoardTemperatures.raw[ii],
    		   latestBoardTemperatures.temp_dC[ii]);
    }

    for (uint32_t ii = 0; ii < NUM_HEATER_THERMISTORS; ii++)
	{
	   printf("\t%04u\t%03d\r\n", latestHeaterTemperatures.raw[ii],
			   latestHeaterTemperatures.temp_dC[ii]);
	}

    puts("\n");
    CLI_DrawProgressBar();
}


/**
 * Handle command line inputs that had first token 'HVFE'.
 * @param[in] buf   Command line data.
 * @param[in] nArgs Number of tokens AFTER module code.
 */
 void ADC_ProcessInput(const char* tokens[], uint8_t nArgs)
{
    
    if (nArgs == 0 || tokens[0][0] == '?')
    {
        puts("info");
        puts("config");
    }
    else if (stricmp(tokens[0], "info") == 0)
    {
        NEW_LINE();
		puts("Use full adc command to see temperature values");
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
      CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues,
    		  NUM_CONFIG_VALUES, tokens, nArgs);
    }
    else
    {
        putchar('?');
    }
}


/* With new implementation, this function is not doing anything. */
void ADC_StartConversion(void)
{

}


static void ADC_HW_SetupForBoardTemp(uint8_t inx)
{
	static ADC_ChannelConfTypeDef sConfig;

	sConfig.Channel = adcChannelMaskBoardTemperatures[inx];
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;

	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		puts("Failed to configure ADC.");
	}
}


static void ADC_HW_SetupForHeaterTemp(uint8_t inx)
{
	static ADC_ChannelConfTypeDef sConfig;

	sConfig.Channel = adcChannelMaskHeaterTemperatures[inx];
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;

	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		puts("Failed to configure ADC.");
	}
}


static void postBoardTemperatures(void)
{
    T_SignalDatum d;
    T_SignalNode* tempNode = SIG_GetFirstChild(adcSigs.onboardTempRoot);

    for (uint8_t ii = 0; ii < NUM_BOARD_THERMISTORS - NUM_BASEBOARD_THERMISTORS; ii++)
    {
        d.i32 = latestBoardTemperatures.temp_dC[ii];
        SIG_WriteSignal(tempNode, d);
        tempNode = SIG_GetNextSibling(tempNode);
    }

    /* fourth board node is connector J-RT and it goes to baseboard */
    d.i32 = latestBoardTemperatures.temp_dC[NUM_BOARD_THERMISTORS - NUM_BASEBOARD_THERMISTORS];
    SIG_WriteSignal(adcSigs.baseboardTemp, d);
}


static void postHeaterTemperatures(void)
{
    T_SignalDatum d;
    T_SignalNode* tempNode = SIG_GetFirstChild(adcSigs.heaterTempRoot);

    for (uint8_t ii = 0; ii < NUM_HEATER_THERMISTORS; ii++)
    {
        d.i32 = latestHeaterTemperatures.temp_dC[ii];
        SIG_WriteSignal(tempNode, d);
        tempNode = SIG_GetNextSibling(tempNode);
    }
}


static uint16_t ADC_HW_GetRaw(void)
{
    uint32_t raw = 0;

	raw = HAL_ADC_GetValue(&hadc1);
    HAL_ADC_Stop(&hadc1);

    return (uint16_t) raw;
}



static void ADC_HW_StartSampling()
{
	if (HAL_ADC_Start(&hadc1) != HAL_OK)
	{
		puts("Failed to start ADC sampling!");
	}
}


static bool ADC_HW_IsSamplingDone()
{
	return (HAL_ADC_PollForConversion(&hadc1, 0) == HAL_OK);
}
