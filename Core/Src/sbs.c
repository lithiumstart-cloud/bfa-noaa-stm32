/**
 * @file sbs.c
 * @brief Implements the Smart Battery Specification (SBS) software layer.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-04-16
 *
 * Notes:
 *    - All AtRate and TimeRemaining tests and calculations are disabled
 *    - The BatteryMode cannot be changed by the Host, so:
 *          - Alarm Messages are never sent unprompted - the host must poll.
 *          - The CapacityMode setting cannot be changed from mAh.
 *    - Calculations based on SoH are disabled, so:
 *          - RelativeStateOfCharge := AbsoluteStateOfCharge
 *          - FullChargeCapacity := DesignCapacity
 *          - The battery will never request a conditioning cycle.
 *          - MaxError := 0%
**/

//############################# MODULE INCLUDES ##############################

#include "sbs.h"
#include "acc.h"
#include "smbus.h"
#include "soc.h"
#include <bluflex_core.h>

extern void AFE_HW_DisableAfePower(void);

//############################ MODULE DEFINES #################################
//Set bits are reserved and cannot be written to. See pg14 of SBS spec.
#define SBS_BAT_MOD_RES_MASK_MSB (0b00011100)

#define RESET_KEY   (0xDEAD)

//TODO: make these dynamic!
#define DESIRED_CHARGING_VOLTAGE_MV (12500)
#define DESIRED_CHARGING_CURRENT_MA (10000)
//########################### MODULE TYPEDEFS ################################

static T_SBS_BatteryStatus batteryStatus;
static T_SBS_BatteryMode batteryMode;


uint8_t generatePEC(uint8_t const msg[], uint8_t nBytes, uint8_t init);
//########################## MODULE VARIABLES ################################

static uint8_t lastSpecialRequest;

static uint16_t RemainingCapacityAlarmThresh; ///< Value below which RemainingCapacityAlarm flag is set. Stored in scaled mAh
static uint16_t RemainingTimeAlarmThresh; ///< Value below which RemainingTimeAlarm flag is set. (minutes)

static T_SBS_Sigs sbsSigs;
static T_CoreSignals coreSigs;

static union
{
 int16_t i16;
 uint16_t u16;
} atRate;

static const char* ManufacturerName = "LSCorp";
static const char* DeviceChemistry = "LION";
static const char* DeviceName = "BluFlex";

const uint16_t DesignVoltage =  12*1000;    //mV (nominal)

#define SBS_MAJOR_VERSION_NUMBER (0b0011) ///< Version 1.1 w/ optional PEC
#define SBS_MINOR_REVISION_NUMBER (0b0001) ///< Version 1.*
#define SBS_VOLTAGE_SCALING (0) ///< Scale voltage by 10^0 = 1
#define SBS_CURRENT_SCALING (1) ///< Scale currents and capacities by 10^1 = 10
#define SBS_CURRENT_FACTOR   (10) //(10**SBS_CURRENT_SCALING)
#define AH_TO_MAH_SCALED (100) // 1000/SBS_CURRENT_FACTOR
#define dAH_TO_MAH_SCALED (AH_TO_MAH_SCALED/10)

const static uint16_t SpecificationInfo = (0x10*SBS_MAJOR_VERSION_NUMBER + SBS_MINOR_REVISION_NUMBER)  + 0x100  *(SBS_VOLTAGE_SCALING + 0x10*SBS_CURRENT_SCALING);

static uint16_t ManufactureDate;
static uint16_t DesignCapacity;
static uint16_t SoftwareVersion;

static T_Version appVersion;
static T_Date appDate;


//############################ MODULE CODE ###################################

void SBS_Init(const T_CoreSignals* pCoreSigs, T_SBS_Sigs* pSbsSigs)
{
    DesignCapacity = 1000;//appConfigValues.packCapacity_Ah*AH_TO_MAH_SCALED; //mAh, scaled

    //Parse the version string here...
    
    CORE_GetParsedAppVersion(&appVersion);
    SoftwareVersion = (appVersion.x << 8) + (appVersion.y << 4) + (appVersion.z);

    CORE_GetParsedAppDate(&appDate);
    ManufactureDate = ((appDate.year) - 1980)*512 + (appDate.month)*32 + (appDate.day); ///< Packed uint; see pg 34 of SBS spec
    
    RemainingCapacityAlarmThresh 	= DesignCapacity/10; //SBS says to init at 10% of DesignCapacity
    RemainingTimeAlarmThresh 		= 10; //SBS says to init at 10 min

    batteryMode.Raw 						= 0;
    batteryMode.InternalChargeController 	= 1; //Internal Charge Controller supported
    batteryMode.PrimaryBatterySupport 		= 0; //Secondary Battery Unsupported
    batteryMode.InternalChargeController 	= 1; // Internal Charge Control is always enabled...
    batteryMode.PrimaryBattery 				= 1; //Battery is operating in primary role
    batteryMode.ChargerMode 				= 1; //Disables broadcast of ChargingVoltage/ChargingCurrent (default is 0_)

    batteryStatus.Raw 	= 0;
    lastSpecialRequest 	= 0;
    atRate.u16 			= 0;

    if (pCoreSigs && pSbsSigs)
    {
        coreSigs = *pCoreSigs;
        sbsSigs = *pSbsSigs;
    }
    else
    {
        puts("SBS: Core sigs null");
    }
}


/**
 * Fill the packet with the given string; set numBytes and first length
 * byte of block as needed.
 * @param[out] pkt Packet to fill.
 * @param[in]  str String to fill with. Must be null terminated.
 */
void fillData_str(T_SMBUS_Packet *pkt, const char * str)
{
    uint8_t ii = 0;

   while (str[ii] && (ii+1) < SMBUS_MAX_BLOCK_SIZE)
   {
       pkt->data[ii+1] = str[ii];
       ii++;
  }

    //throwing error
    pkt->numBytes= ii+2; //number bytes in the block  = len(str) + 1 for Length + 1 for PEC
    pkt->data[0] = ii; //num bytes in the string
    pkt->data[ii+1] = UTIL_AddBlockToCRC8((0x07), pkt->crc, pkt->data, ii+1);
    
}

/**
 * Fill the packet with the given word and set numBytes appropriately.
 * @param[out] pkt Packet to fill.
 * @param[in]  word Data to fill with.
 */
void fillData_u16(T_SMBUS_Packet *pkt, uint16_t word)
{
    pkt->numBytes = 3;
    pkt->data[0] = (uint8_t)(word & 0xFF);
    pkt->data[1] = (uint8_t)((word >> 8) & 0xFF);
    
    pkt->data[2] = UTIL_AddBlockToCRC8((0x07), pkt->crc, pkt->data, 2);

}

void fillData_Special(T_SMBUS_Packet *pkt)
{
    uint16_t word;
    uint8_t u8;
    T_SignalNode* child;
    T_TimeDelta uptime;
    uint8_t inx = 1;
    union
    {
        uint8_t u8;
        int8_t i8;
    } byte;

    switch (lastSpecialRequest)
    {
        case SBS_SPECIAL_CELL_STATS:

            word = (uint16_t)SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MAX);
            pkt->data[inx++] = word & 0xFF;
            pkt->data[inx++] = (word >> 8) & 0xFF;

            word = (uint16_t) SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN);
            pkt->data[inx++] = word & 0xFF;
            pkt->data[inx++] = (word >> 8) & 0xFF;

            word = (uint16_t) SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_AVG);
            pkt->data[inx++] = word & 0xFF;
            pkt->data[inx++] = (word >> 8) & 0xFF;

            pkt->data[inx++] = coreSigs.nVltgRoot? SIG_GetAlarmNow(coreSigs.nVltgRoot) : 0;

        break;

        case SBS_SPECIAL_TEMPERATURE_DATA:

            //Ambient Temp
            byte.i8 = (int8_t) SIG_GetData_I32(sbsSigs.ambientTemp, SIG_RQST_AVG);
            pkt->data[inx++] = byte.u8;
            pkt->data[inx++] = SIG_GetAlarmNow(sbsSigs.ambientTemp);
            
            //Balance Temp
            byte.i8 = (int8_t) SIG_GetData_I32(sbsSigs.balanceTemp, SIG_RQST_AVG);
            pkt->data[inx++] = byte.u8;
            pkt->data[inx++] = SIG_GetAlarmNow(sbsSigs.balanceTemp);
            
            //Board Temp
            byte.i8 = (int8_t) SIG_GetData_I32(sbsSigs.boardTemp, SIG_RQST_AVG);
            pkt->data[inx++] = byte.u8;
            pkt->data[inx++] = SIG_GetAlarmNow(sbsSigs.boardTemp);
            
            //Cell Temp
            byte.i8 = (int8_t) SIG_GetData_I32(sbsSigs.cellTemp, SIG_RQST_MAX);
            pkt->data[inx++] = byte.u8;

            byte.i8 = (int8_t) SIG_GetData_I32(sbsSigs.cellTemp, SIG_RQST_MIN);
            pkt->data[inx++] = byte.u8;
            
            byte.i8 = (int8_t) SIG_GetData_I32(sbsSigs.cellTemp, SIG_RQST_AVG);
            pkt->data[inx++] = byte.u8;
                        
            pkt->data[inx++] = SIG_GetAlarmNow(sbsSigs.cellTemp);
            
        break;

        case SBS_SPECIAL_UPTIME:

        	SYS_GetUpTime(&uptime);
                
            pkt->data[inx++] =  uptime.days & 0xFF;
            pkt->data[inx++] = (uptime.days >> 8) & 0xFF;
            pkt->data[inx++] = uptime.hours;
            pkt->data[inx++] = uptime.mins;
            pkt->data[inx++] = uptime.secs;
        break;

        case SBS_SPECIAL_ALARMS:

            pkt->data[inx++] =  SIG_GetAlarmNow(coreSigs.nRoot);
            pkt->data[inx++] =  SIG_GetAlarmNow(coreSigs.nCurrRoot);
            //TODO: pkt->data[inx++] =  SYS_IsPowerSupplyFaulted() ? ALARM_LVL_STOP : ALARM_LVL_CLEAR;
            pkt->data[inx++] =  ALARM_LVL_CLEAR;

        break;

         case SBS_SPECIAL_CELL_DETAIL:

            child = SIG_GetFirstChild(coreSigs.nVltgRoot);

            while (child)
            {
                word = (uint16_t) SIG_GetAvg_U32(child);
                pkt->data[inx++] = word & 0xFF;
                pkt->data[inx++] = (word >> 8) & 0xFF;
                child = SIG_GetNextSibling(child);
            }

        break;

        case SBS_SPECIAL_VERSION_ID:

            fillData_str(pkt, STRINGIZE_VALUE_OF(GHASH_STR));
        return; //the function does the CRC for us!
        //break;

        case SBS_SPECIAL_RELAY_STATES_DETAIL:
            
            //TODO: make relay a signal node....
            u8 = RLY_GetRelayMask();
            pkt->data[inx++] = u8 & 0x01; //chg
            
            u8 >>= 1;
            pkt->data[inx++] = u8 & 0x01; //dis

            u8 >>= 1;
            pkt->data[inx++] = u8 & 0x01; //hpe
            
            u8 >>= 1;
            pkt->data[inx++] = u8 & 0x01; //htrA
            
            u8 >>= 1;
            pkt->data[inx++] = u8 & 0x01; //htrB
            
            pkt->data[inx++] = 0; //todo: relay alarm level

        break;

        default:
            LOG_Report(LOG_MOD_SBS, LOG_LVL_FATAL, LOG_MSG_NOT_SUPPORTED, 0);
        break;
    }

    //No matter what, fill in the packet length & CRC.

    //Just do some checks to make sure things don't rollover
    pkt->data[0] = (inx == 0) ? 0 : inx - 1;
    pkt->numBytes = (inx == UINT8_MAX) ? UINT8_MAX : inx + 1;
    pkt->data[inx] = UTIL_AddBlockToCRC8((0x07), pkt->crc, pkt->data, inx);

    //TODO: None of these loops check for index out of bound errors
   

}


/**
 * This is called by the slave SM, when the master requests data.
 * @pararm[in,out] thePacket The packet to fill with data based on command.
 * @return bool - Whether or not we have a response for this packet
 */
bool SBS_GetRequestedData(T_SMBUS_Packet *thePacket)
{
    if (!thePacket)
    {
        LOG_Report(LOG_MOD_SBS, LOG_LVL_FATAL, LOG_MSG_NULL_PTR_REF, 0);
        return false;
    }
    //uint8_t result;
    int16_t  buf_i16;
    uint8_t u8;
    
    uint16_t buf_u16;
    //uint32_t buf_u32;
    switch (thePacket->cmd)
    {

       case SBS_MANUFACTURER_ACCESS:
            thePacket->numBytes = 3;
            thePacket->data[0] = lastSpecialRequest;
            thePacket->data[1] = 0x00;
            thePacket->data[2] = UTIL_AddBlockToCRC8((0x07), thePacket->crc, thePacket->data, 2);
        break;

        case SBS_REMAINING_CAP_ALARM_THRESH:
            fillData_u16(thePacket, RemainingCapacityAlarmThresh);
        break;

        case SBS_REMAINING_TIME_ALARM_THRESH:
           fillData_u16(thePacket, RemainingTimeAlarmThresh);
        break;

        case SBS_BATTERY_MODE:
            fillData_u16(thePacket, batteryMode.Raw);
        break;

        case SBS_AT_RATE:
            fillData_u16(thePacket, atRate.u16); //just return at rate back
        break;

        case SBS_AT_RATE_TIME_TO_EMPTY:
            fillData_u16(thePacket, SOC_GetTimeToEmptyAtRate_m(atRate.i16));
        break;

        case SBS_AT_RATE_TIME_TO_FULL:
            fillData_u16(thePacket, SOC_GetTimeToFullAtRate_m(atRate.i16));
        break;

        /// Returns a Boolean indicating whether or not the battery can deliver
        /// the previously written AtRate value of additional energy for 10s.
        /// If the AtRate value is zero or positive, the AtRateOK() function
        /// will ALWAYS return true.
        case SBS_AT_RATE_OK:
#if 0
            if (atRate.i16 < 0)
            {
                buf_i16 = BMS_GetCurrent();
                if (buf_i16 > 0)
                {
                    //If charging
                    net = atRate.i16 + buf_i16;
                    if (net > 0)
                    {
                        result = 1;
                    }
                    else
                    {
                        //Net discharging. Can this be supported for 10sec?
                        buf_u16 = (uint16_t) -1*net;
                        SOC_GetCharge(&buf_u32);

                        if ((buf_u32/10/buf_u16) > 10)
                        {
                            result  = 1; //yes!
                        }
                        else
                        {
                            result = 0; //no.
                        }
                    }
                }
                else
                {
                    //If discharging...
                    buf_u16 = (-buf_i16) + (-atRate.i16); //net discharge. can support?
                    SOC_GetCharge(&buf_u32);
                    if ((buf_u32/10/buf_u16) > 10)
                    {
                        result  = 1; //yes!
                    }
                    else
                    {
                        result = 0; //no.
                    }
                }

                fillData_u16(thePacket, result);
            }
            else
            {
                //If the atRate is charging, we're supposed to return 0xFFFF.
                 fillData_u16(thePacket, 0xFFFF);
            }
#endif
            fillData_u16(thePacket, 1); //just say "TRUE"
        break;

        case SBS_TEMPERATURE:
            //Returns average all ALL temps in units of .1K (deciKelvin)
            //Temp_c will be at minimum -128, so 273+t_C is guaranteed to be >0.
            //Temp_c will be at maximum +127, so 273+t_C is guaranteed to be <400
            buf_i16 = SIG_GetData_I32(coreSigs.nTempRoot, SIG_RQST_AVG) + 273;
            buf_u16 = (buf_i16 > 0) ? ((uint16_t) buf_i16) : 0 ;
            fillData_u16(thePacket, coreSigs.nTempRoot ? 10*(buf_u16) : 0xFFFF);
        break;

        case SBS_VOLTAGE:
            //TODO: implemented summed nodes; change to sum
            fillData_u16(thePacket, SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_SUM));
        break;

        case SBS_CURRENT:
            //We need to make sure we don't change the actual bits...
            buf_i16 = SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_AVG)/SBS_CURRENT_FACTOR;
            fillData_u16(thePacket, ( * ((uint16_t *) &buf_i16) ) );
          //  UART_SendHex_U8(thePacket->data[0]);
          //  putstr(", ");
          //  UART_SendHex_U8(thePacket->data[1]);
          //  NEW_LINE();
        break;

       case SBS_AVG_CURRENT:
            //We need to make sure we don't change the actual bits...
            buf_i16 = SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_AVG)/SBS_CURRENT_FACTOR;
            fillData_u16(thePacket, ( * ((uint16_t *) &buf_i16) ) );
       break;

        case SBS_MAX_ERROR:
            //TODO: max error
            fillData_u16(thePacket, 0);
        break;

        //For now, these are the same...
        case SBS_REL_SOC:
        case SBS_ABS_SOC:
            fillData_u16(thePacket, SIG_GetAvg_U32(coreSigs.nSoc));
        break;

        case SBS_REMAINING_CAP:
            //We want it in mAh, scaled by 10...
             fillData_u16(thePacket, SOC_GetRemainingCharge_dAh()*dAH_TO_MAH_SCALED);
        break;

        case SBS_FULL_CHARGE_CAP:
//TODO:            fillData_u16(thePacket, appConfigValues.packCapacity_Ah *(AH_TO_MAH_SCALED));
            fillData_u16(thePacket, 0);
            break;

        case SBS_RUN_TIME_TO_EMPTY:
            fillData_u16(thePacket, SOC_GetInsTimeToEmpty_m());
        break;

        case SBS_AVG_TIME_TO_EMPTY:
            fillData_u16(thePacket, SOC_GetAveTimeToEmpty_m());
        break;

        case SBS_AVG_TIME_TO_FULL:
            fillData_u16(thePacket, SOC_GetAveTimeToFull_m());
        break;

        case SBS_CHARGING_VOLTAGE:
            //TODO: linke to particular relay
            fillData_u16(thePacket,  (RLY_GetRelayMask() & 0x1) ? DESIRED_CHARGING_VOLTAGE_MV : 0);
        break;

        case SBS_CHARGING_CURRENT:
            //TODO: linke to particular relay
            fillData_u16(thePacket,  (RLY_GetRelayMask() & 0x1) ? DESIRED_CHARGING_CURRENT_MA/SBS_CURRENT_FACTOR : 0);
        break;

        case SBS_BATTERY_STATUS:
            u8 = SIG_GetAvg_U32(coreSigs.nSoc);
            batteryStatus.FullyCharged =  u8 > 99;
            batteryStatus.FullyDischarged = u8 < 5;
            batteryStatus.OverTempAlarm = (SIG_GetAlarmNow(coreSigs.nTempRoot) == ALARM_LVL_STOP);
            batteryStatus.TerminateChargeAlarm = batteryStatus.FullyCharged;
            batteryStatus.TerminateDischargeAlarm =batteryStatus.FullyDischarged;

            batteryStatus.RemainingTimeAlarm = 0; //TODO: implement remaining time calculations
            batteryStatus.RemainingCapacityAlarm = (SOC_GetRemainingCharge_dAh()*dAH_TO_MAH_SCALED) < RemainingCapacityAlarmThresh ? 1 : 0;
            fillData_u16(thePacket, batteryStatus.Raw);

            batteryStatus.ErrorCode = SBS_ERR_OK; //clear the error code after it has been loaded up
        break;

        case SBS_CYCLE_COUNT:
            fillData_u16(thePacket, ACC_GetNumberCycles());
            break;

        case SBS_DESIGN_CAPACITY:
            ///TODO:fillData_u16(thePacket, appConfigValues.packCapacity_Ah *AH_TO_MAH_SCALED);
            fillData_u16(thePacket,0);
        break;

        case SBS_DESIGN_VOLTAGE:
            fillData_u16(thePacket,DesignVoltage);
        break;


        case SBS_SPECIFICATION_INFO:
            fillData_u16(thePacket, SpecificationInfo);
        break;

        case SBS_MANUFACTURE_DATE:
            fillData_u16(thePacket, ManufactureDate);
        break;

        case SBS_SERIAL_NUMBER:
            fillData_u16(thePacket, SYS_GetSerialNumber_LSW());
        break;

        case SBS_MANUFACTURER_NAME:
            fillData_str(thePacket, ManufacturerName);
        break;

        case SBS_DEVICE_NAME:
            fillData_str(thePacket, DeviceName);
        break;

       case SBS_DEVICE_CHEMISTRY:
            fillData_str(thePacket, DeviceChemistry);
        break;


        //Optional codes are below.

        case SBS_RELAY_STATES_CONDENSED:
            fillData_u16(thePacket, RLY_GetRelayMask());
        break;

        case SBS_BALANCING_STATUS:
            fillData_u16(thePacket, 0xFFFF); //todo:reimplement balancing state
        break;

        case SBS_SPECIAL_BLOCK:
            fillData_Special(thePacket);
        break;

        default:
            //Unsupported - leave packet unmodified for inspection.
            LOG_Report(LOG_MOD_SBS, LOG_LVL_FATAL,  LOG_MSG_NOT_SUPPORTED, thePacket->cmd);
            batteryStatus.ErrorCode = SBS_ERR_UNSUPPORTED;
            return false;
    }

    return true; //If we got down here, packet was OK. 
}



/**
 * Take the data that the master wrote to us, and process it.
 * @param[in] thePacket Packet to process.
 * @return bool - Whether or not this was a valid packet
 */
bool SBS_ProcessWrittenData(T_SMBUS_Packet *thePacket)
{
    //Clear the errors at the start...
    batteryStatus.ErrorCode = SBS_ERR_OK;
    
    switch (thePacket->cmd)
    {
        case SBS_MANUFACTURER_ACCESS:
            lastSpecialRequest = thePacket->data[0];
        break;

        case SBS_REMAINING_CAP_ALARM_THRESH:
            if (thePacket->numBytes >= 2)
            {
                RemainingCapacityAlarmThresh = thePacket->data[0] + (thePacket->data[1] << 8);
            }
        break;

        case SBS_REMAINING_TIME_ALARM_THRESH:
            if (thePacket->numBytes >= 2)
            {
                RemainingTimeAlarmThresh = thePacket->data[0] + (thePacket->data[1] << 8);
            }
        break;

        case SBS_BATTERY_MODE:
            if (thePacket->numBytes >= 2)
            {
                #if 0
                batteryMode.Mode_MSB = thePacket->data[1] & ~SBS_BAT_MOD_RES_MASK_MSB;
                #endif
                //Note: we do not accept any changes to BatteryMode right now.
                //Writes to BatteryMode are not errors (if well formed), but
                //we do not record or act on these changes. 
            }
        break;

        case SBS_AT_RATE:
            atRate.u16 = thePacket->data[0] + (thePacket->data[1] << 8);
        break;
        

        //We only want to reset if we get the right "key."
        case SBS_REQUEST_RESTART:
            if (thePacket->numBytes >=2)
            {
                if ((thePacket->data[0] == (RESET_KEY & 0xFF)) && (thePacket->data[1] == (RESET_KEY >> 8)))
                {
                	AFE_HW_DisableAfePower();
                    SYS_Restart();
                }
            }
        break;

        default:
            LOG_Report(LOG_MOD_SBS, LOG_LVL_FATAL,  LOG_MSG_NOT_SUPPORTED, thePacket->cmd);
            batteryStatus.ErrorCode = SBS_ERR_UNSUPPORTED;
            return false;
         
    }

    return true;
}

/**
 * Push a battery status message onto the TX queue.
 */
//T_PID SBS_SendBatteryStatus()
//{
//    T_SMBUS_Packet pkt;
//
//    pkt.addr = SMBUS_HOST_ADDRESS;
//    pkt.cmd = SBS_ALARM_WARNING;
//    pkt.cmdType = SMBUS_TYP_WRITE;
//    fillData_u16(&pkt, batteryStatus.Raw | 0xF); //all error bits must be set high
//    return SMB_PushForTx(&pkt);
//
//}


//### Command Line Interface ###################################################
//
/**
 * Handle command line inputs that had first token 'sbs'.
 * @param[in] buf   Command line data.
 * @param[in] nArgs Number of tokens AFTER module code.
 */
void SBS_ProcessInput(const char* tokens[], uint8_t nTokens)
{

    if (nTokens == 0 || tokens[0][0] == '?')
    {
    	puts("mode\tShow SBS mode register.");
    	puts("status\tShow SBS status register.");
    }
    else if (stricmp(tokens[0], STR_mode) == 0)
    {
    	printf("0x%04X\r\n", batteryMode.Raw);
    }
    else if (stricmp(tokens[0], STR_status) == 0)
    {
    	printf("0x%04X\r\n", batteryStatus.Raw);
    }
    else
    {
        //Print error string.
    	puts("?");
    }
}

