/* 
 * File:   noaa_bms.h
 * Author: sbickham
 *
 * Created on March 11, 2016, 11:53 AM
 */

#ifndef NOAA_BMS_H
#define	NOAA_BMS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <bluflex_core.h>
    
extern const T_ConfigInfo bmsLibConfigInfo;

#include "inc/acc.h"
#include "inc/cell.h"
#include "inc/htr.h"
#include "inc/smbus.h"
#include "inc/bms.h"
#include "inc/rcd.h"
#include "inc/soc.h"
#include "inc/soh.h"

#ifdef	__cplusplus
}
#endif

#endif	/* NOAA_BMS_H */

