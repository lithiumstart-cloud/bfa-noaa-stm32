/**
 * @file soh.c
 * @brief State of Health (SoH) tracker
 * @copyright 2015 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2015-07-16
 *
 * 
 * 
 **/



#include "soh.h"

#include <bluflex_core.h>

//#include "bal.h"


//####################### DEFINES ##############################################
#define ABS(x) ((x >= 0) ? (x) : (-x))

//########################### CONFIG VALUES ####################################

//Unlike other modules, the config struct is passed into this function.
static T_SOH_Config configValues;
static const uint32_t configSignMask = 0x840C;

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
   "runPeriod (ms)",
   "#CellsIn||",
   "sohP:currStart (mA)",
   "sohP:currStop (mA)",
   "sohP:dResMin (uOhm)", 
   "sohP:dResMax (uOhm)", 
  
   "sohP:UpperSoc (%)", 
   "sohP:LowerSoc (%)", 
  
   "sohP:RisingTau (sec)", 
   "sohP:FallingTau (sec)",
  
   "scf:MaxAllowedTemp (dC)",
   "scf:NeededSoc (%)", 
   "sohC:NeededSoc (%)",
   "sohC:maxImbalance (mV)", 
   "sohC:OcvRecoveryTime (ms)",
   "sohC:absNoCurrLimit (mA)", 
};

//############################## TYPES #########################################

///Module level states
typedef enum
{
    ST_WAITING_FOR_BOOT,
    ST_RUNNING,
} T_State;

static struct
{
    uint32_t minCell_mV;
    uint32_t avgCell_mV;
    uint32_t maxCell_mV;
    uint32_t soc_prct;
    int32_t curr_mA;
} shared_vars;

//############################# VARIABLES ######################################

static T_CoreSignals coreSigs;
static T_SOH_Signals sohSigs;
static T_State currentState;
static T_Timer tmrUpdate;

static T_MemAddr vAddr_SohP;

//############################# FUNCTIONS ######################################
static void updatePower(void);
static void filterAndPostSohP(void);
static void filerAndPostSohC(void);
static void updateSohCapacity(void);
static void updateSharedVars(void);
static void crunchSingleCellFailure(void);
static void crunchSohCapacity(void);
static void crunchSohPFilterCoefficents(void);

//################################# PUBLIC CODE ################################

void SOH_Init(T_CoreSignals* pCoreSigs,T_SOH_Signals* pSohSigs,  T_SOH_Config* pSohConfig)
{

    if (pCoreSigs)
    {
        coreSigs = *pCoreSigs;
    }

    if (pSohConfig)
    {
        configValues = *pSohConfig;
    }
    
    if (pSohSigs)
    {
        sohSigs = *pSohSigs;
        if (sohSigs.nCellTemps == NULL)
        {
            puts("SOH: Cannot run without cell temps!");
        }
    }
    
    
           
   
    
    T_SignalDatum d = {.u32 = 100};
    SIG_WriteSignal(sohSigs.soh_capacity, d);
    SIG_WriteSignal(sohSigs.soh_power, d);
    
    crunchSohPFilterCoefficents();
    
    
    //Attempting to load SoH_P...
    uint32_t sohp = 1000;
    vAddr_SohP = MEM_AllocateWord();
    if (!vAddr_SohP)
    {
        puts("Unable to allocate NVRAM space for SohP!");
    }
    else
    {
        MEM_LoadDefaultData("SohP", vAddr_SohP, &sohp, 1);
        if (sohp > 100)
        {
            puts("No valid SohP data found in NVRAM!");
        }
        else
        {
            //Data is good. Use it. 
            d.u32 = sohp;
            SIG_WriteSignal(sohSigs.soh_power, d);
            printf("Loaded SoH_P (%u%%) from NVRAM\r\n", sohp);
            //Write bad data to NVRAM space so we don't use this again without saving first
            MEM_WriteWordFromRamToNvm(vAddr_SohP, 0xFFFF);
        }
    }
    
} 




bool SOH_Update()
{
    switch (currentState)
    {
        case ST_WAITING_FOR_BOOT:
            if (SIG_IsReady() && (sohSigs.nCellTemps != NULL))
            {
                filerAndPostSohC(); //make sure we're consistent with anything loaded from NVRAM in SOC
                
                currentState = ST_RUNNING;
                TMR_StartRepeating(&tmrUpdate, configValues.runPeriod_ms);
            }
        break;

        
        case ST_RUNNING:
            if (TMR_HasTripped_ms(&tmrUpdate))
            {
                updateSharedVars();
                
                updatePower();
                updateSohCapacity();
            }         
        break;
    }
   

    return true; //Always ready to SLEEP
}

static void updateSharedVars(void)
{
    shared_vars.avgCell_mV = SIG_GetAvg_U32(coreSigs.nVltgRoot);
    shared_vars.minCell_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN);
    shared_vars.maxCell_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MAX);
    
    shared_vars.curr_mA = SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_AVG);
    shared_vars.soc_prct = SIG_GetData_U32(coreSigs.nSoc, SIG_RQST_AVG);
}




typedef enum
{
    SOHP_PINIT =0,
    SOHP_INACTIVE_TOP_OF_CHARGE,
    SOHP_ACTIVE,
    SOHP_INACTIVE_BOTTOM_OF_CHARGE,
} T_SohP_State;

static const char* sohp_state2Str(T_SohP_State state)
{
    switch (state)
    {
        case SOHP_PINIT:                        return "PInit";
        case SOHP_INACTIVE_TOP_OF_CHARGE:       return "Inactive (Top)";
        case SOHP_ACTIVE:                       return "Active";
        case SOHP_INACTIVE_BOTTOM_OF_CHARGE:    return "Inactive (Bottom)";
    }
    
    return "???";
}

static struct
{
    T_SohP_State state;
    uint32_t deltaV_mV;
    uint32_t deltaR_uOhm;
    uint32_t sohp_raw_x10prct; 
    uint32_t riseBeta;
    uint32_t fallBeta;
} sohp_vars = 
{
    .state = SOHP_PINIT,    
};

static void crunchSohPFilterCoefficents()
{
    if (configValues.runPeriod_ms > 0)
    {   
        sohp_vars.riseBeta = (configValues.sohP_RisingTau_sec*1000ULL)/(configValues.runPeriod_ms);
        sohp_vars.fallBeta = (configValues.sohP_FallingTau_sec*1000ULL)/(configValues.runPeriod_ms);
    }
    else
    {
       LOG_Event(LOG_LVL_FATAL, "Can't crunch SohP filter coeffs!");
    }
}
static void updatePower()
{
    /*
     * SOH - Power. System will watch for SoC to fall within designated window. 
     * When this occurs, algorithm will watch for signficant deviations
     * in cell resistance. Output SoH is mapped as function of |Ravg - Rmin|.
     *         
     *
     *  Actual outward facing SoH is unsymmetrically filtered such that:
     *      1) There are no instantaneous changes in SOH
     *      2) SoH is slower to rise than fall 
     */
    
    
    
    switch (sohp_vars.state)
    {
        case SOHP_PINIT:
            sohp_vars.state = SOHP_INACTIVE_TOP_OF_CHARGE;
        break;
        
        case SOHP_INACTIVE_TOP_OF_CHARGE:
            if (shared_vars.soc_prct <= configValues.sohP_UpperSoc_Prct && shared_vars.curr_mA < configValues.sohP_currStart_mA)
            {
                LOG_Event(LOG_LVL_INFO, "Starting SohP monitoring!");
                sohp_vars.state = SOHP_ACTIVE;
            }
            else if (shared_vars.soc_prct < configValues.sohP_LowerSoc_Prct)
            {
                //looks like there was no high power discharge
                sohp_vars.state = SOHP_INACTIVE_BOTTOM_OF_CHARGE;
            }
        break;
        
        
        case SOHP_ACTIVE:
           if (shared_vars.soc_prct < configValues.sohP_LowerSoc_Prct)
           {
               sohp_vars.state = SOHP_INACTIVE_BOTTOM_OF_CHARGE;
               LOG_Event(LOG_LVL_INFO, "Stopping SoH_P monitoring (SoC too low).");
           }
           else if (shared_vars.curr_mA > configValues.sohP_currStop_mA)
           {
               //We're in the SoC range, but this isn't a high power discharge anymore.
               sohp_vars.state = SOHP_INACTIVE_TOP_OF_CHARGE;
               LOG_Event(LOG_LVL_INFO, "Stopping SoH_P monitoring (current to low).");
           }
           else
           {
               //Do some math!
               
               //Get the difference in voltage form the avg cell to the min cell....
               sohp_vars.deltaV_mV = shared_vars.avgCell_mV - shared_vars.minCell_mV;
               
               //Convert the difference in voltage to a differnce in resitance...
               uint64_t dV_nV = sohp_vars.deltaV_mV * 1e6;
               uint32_t uCurr_mA = (shared_vars.curr_mA < 0) ? -shared_vars.curr_mA : shared_vars.curr_mA;
               sohp_vars.deltaR_uOhm = dV_nV/(uCurr_mA);
               
               if (sohp_vars.deltaR_uOhm <= configValues.sohP_dResMin_uOhm)
               {
                   sohp_vars.sohp_raw_x10prct = 1000;
               }
               else if (sohp_vars.deltaR_uOhm >= configValues.sohP_dResMax_uOhm)
               {
                   sohp_vars.sohp_raw_x10prct = 0;
               }
               else
               {
                   //Interpolate...
                   uint32_t dR = (configValues.sohP_dResMax_uOhm - configValues.sohP_dResMin_uOhm);
                   if (dR > 0)
                   {
                        sohp_vars.sohp_raw_x10prct = 1000ULL - 1000ULL*(sohp_vars.deltaR_uOhm - configValues.sohP_dResMin_uOhm)/dR;
                   }
                   else
                   {
                       //We shouldn't get here, since we catch this in the above condition
                        LOG_Event(LOG_LVL_FATAL, "SOH: dR is 0! Can't divide.");
                        sohp_vars.sohp_raw_x10prct = 1000;
                   }
               }
               
               filterAndPostSohP();
               
           }
           
        break;
        
        case SOHP_INACTIVE_BOTTOM_OF_CHARGE:
            if (shared_vars.soc_prct > configValues.sohP_UpperSoc_Prct)
            {
                //back to the top!
                sohp_vars.state = SOHP_INACTIVE_TOP_OF_CHARGE;
            }
        break;
        
    }
}

//### Capacity (SohC) /Single Cell Failure (SCF)
typedef enum
{
    SOHC_PINIT = 0,
    SOHC_FULLY_CHARGED_BALANCED,
    SOHC_FULLY_CHARGED_UNBALANCED,
    SOHC_DISCHARGING,
    SOHC_WAITING_FOR_DISCHARGE_TO_STOP,
    SOHC_WAITING_FOR_OCV,
    SOHC_WAITING_FOR_FULL_CHARGE,
           
} T_SOHC_State;

static const char* sohc_state2str(T_SOHC_State state)
{
    switch (state)
    {
        case SOHC_PINIT: return "Pinit";
        case SOHC_FULLY_CHARGED_BALANCED: return "Charged (Balanced)";
        case SOHC_FULLY_CHARGED_UNBALANCED: return "Charged (Unbalanced)";
        case SOHC_DISCHARGING: return "Discharging";
        case SOHC_WAITING_FOR_DISCHARGE_TO_STOP: return "WaitingFor0A";
        case SOHC_WAITING_FOR_OCV: return "WaitingForOcv";
        case SOHC_WAITING_FOR_FULL_CHARGE: return "WaitingForFullCharge";
    }
    
    return "???";
}

static struct
{
    T_SOHC_State state;
    uint32_t estNumFailedCells_x10;
    bool hotFlag;
    uint8_t relativeCapacity_Prct;
} sohc_vars;

static bool checkForChargingOrSnapToZeroSoc(void)
{
    if (shared_vars.curr_mA > configValues.sohC_absNoCurrLimit_mA)
    {
        LOG_Event(LOG_LVL_INFO, "SOHC: Discharge cycle interrupted!");
        return true;
    }
    if (shared_vars.soc_prct == 0)
    {
        LOG_Event(LOG_LVL_INFO, "SOHC: SoC snapped to 0% - ineligble cycle.");
        return true;
    }
    
    return false;
}

static void updateSohCapacity(void)
{
    uint32_t dV_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MAX) - SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN);
    
    //No matter what state we're in, update the hotFlag
    if (!sohc_vars.hotFlag && (SIG_GetData_I32(sohSigs.nCellTemps, SIG_RQST_MAX) > configValues.scf_MaxAllowedTemp_dC))
    {
        LOG_Event(LOG_LVL_INFO, "SOH: Cell(s) too warm for SCF analysis.");
        sohc_vars.hotFlag = true;
    }
    
    
    static T_Timer tmr;
    switch (sohc_vars.state)
    {
        case SOHC_PINIT:
            if (shared_vars.soc_prct == 100)
            {
                sohc_vars.state = SOHC_FULLY_CHARGED_BALANCED;
   
            }
            else
            {
                sohc_vars.state = SOHC_WAITING_FOR_FULL_CHARGE;
            }
            
            sohc_vars.hotFlag = false;
        break;
        
        case SOHC_FULLY_CHARGED_BALANCED:
            if (shared_vars.soc_prct < 100)
            {
                if (shared_vars.curr_mA < -configValues.sohC_absNoCurrLimit_mA)
                {
                    //Starting a discharge!
                    sohc_vars.state = SOHC_DISCHARGING;
                    LOG_Event(LOG_LVL_INFO, "SOHC: Discharge started!");
                }
                else
                { 
                    //The SoC probably snapped, so this isn't viable.
                    sohc_vars.state = SOHC_WAITING_FOR_FULL_CHARGE;
                    LOG_Event(LOG_LVL_INFO, "SOHC: Discharge w/o current!");
                }
            }
            else if (dV_mV > configValues.sohC_maxImbalance_mV)
            {
                sohc_vars.state = SOHC_FULLY_CHARGED_UNBALANCED;
                LOG_Event(LOG_LVL_INFO, "SOHC: Pack too unbalanced.");
            }
        
        break;
        
        
        case SOHC_FULLY_CHARGED_UNBALANCED:
            if (shared_vars.soc_prct < 100)
            {
                sohc_vars.state = SOHC_WAITING_FOR_FULL_CHARGE;
            }
            else if (dV_mV < configValues.sohC_maxImbalance_mV)
            {
                sohc_vars.state = SOHC_FULLY_CHARGED_BALANCED;
                LOG_Event(LOG_LVL_INFO, "SOHC: Pack sufficently balanced.");
            }
        break;
        
        case SOHC_DISCHARGING:
            if (checkForChargingOrSnapToZeroSoc())
            {
                sohc_vars.state = SOHC_WAITING_FOR_FULL_CHARGE;
            }
            else if (shared_vars.soc_prct <= configValues.sohC_NeededSoc_Prct)
            {
                sohc_vars.state = SOHC_WAITING_FOR_DISCHARGE_TO_STOP;
                LOG_Event(LOG_LVL_INFO, "SOHC: Discharge sufficent- waiting for 0A.");
            }
        break;
        
        case SOHC_WAITING_FOR_DISCHARGE_TO_STOP:
             if (checkForChargingOrSnapToZeroSoc())
            {
                sohc_vars.state = SOHC_WAITING_FOR_FULL_CHARGE;
            }
            else if (shared_vars.curr_mA > -configValues.sohC_absNoCurrLimit_mA)
            {
                LOG_Event(LOG_LVL_INFO, "SOHC: Discharge done! Waiting for OCV.");
                sohc_vars.state = SOHC_WAITING_FOR_OCV;
                TMR_StartOneshot(&tmr, configValues.sohC_OcvRecoveryTime_ms);
            }
        break;
        
        case SOHC_WAITING_FOR_OCV:
            if (checkForChargingOrSnapToZeroSoc())
            {
                sohc_vars.state = SOHC_WAITING_FOR_FULL_CHARGE;
            }
             else if (shared_vars.curr_mA < -configValues.sohC_absNoCurrLimit_mA)
            {
                LOG_Event(LOG_LVL_INFO, "SOHC: Discharge started again!");
                sohc_vars.state = SOHC_WAITING_FOR_DISCHARGE_TO_STOP;
            }
            else if (TMR_HasTripped_ms(&tmr))
            {
                //ocv reached!
                crunchSohCapacity();
                
                if (sohc_vars.hotFlag == false && shared_vars.soc_prct <= configValues.scf_NeededSoc_Prct)
                {
                    crunchSingleCellFailure();  
                }
                
                sohc_vars.state = SOHC_WAITING_FOR_FULL_CHARGE;
            }
        break;
     
        case SOHC_WAITING_FOR_FULL_CHARGE:
            if (shared_vars.soc_prct == 100)
            {
                sohc_vars.state = SOHC_FULLY_CHARGED_BALANCED;
                sohc_vars.hotFlag = false;
                LOG_Event(LOG_LVL_INFO, "SOHC: Pack fully charged!");
            }
        
        break;
    }
    
}

static void filerAndPostSohC(void)
{
    /* Update SoH_Capacity based on values from SOC*/
    sohc_vars.relativeCapacity_Prct = SOC_GetRelativeCapacity_Prct();
    T_SignalDatum d = {.u32 = ( sohc_vars.relativeCapacity_Prct > 95) ? 100 :  sohc_vars.relativeCapacity_Prct}; 
    SIG_WriteSignal(sohSigs.soh_capacity, d);
}
static void crunchSohCapacity(void)
{
    uint32_t minCellSoc_Prct_x10 = CELL_LookupDischargeSocFromVltg_Prct_x10(shared_vars.minCell_mV);
    uint32_t fullChargeCap_dAh = SOC_GetFullChargeCapacity_dAh();
    
    uint32_t actualRemainingCharge_dAh = ((minCellSoc_Prct_x10 * fullChargeCap_dAh) + 5)/1000;
    uint32_t socNow_dAh = SOC_GetRemainingCharge_dAh();
    
    puts("------SoH Capacity------");
    
    printf("Full Charge Capacity = %u dAh\r\n", fullChargeCap_dAh);
    printf("Exp. Remaining Charge = %u%% --> %u dAh\r\n",shared_vars.soc_prct, socNow_dAh);
    
    printf("MinCell = %u mV --> ", shared_vars.minCell_mV);
    PrintAs_Scaled_U32(minCellSoc_Prct_x10, 1);
    printf("%% = %u dAh\r\n", actualRemainingCharge_dAh);
      
       
    //How much fuller do we think we are? 
    int32_t dCapacity_dAh = socNow_dAh - actualRemainingCharge_dAh;
    
    if (dCapacity_dAh > 0)
    {
        //We think we're fuller than we actually are.
        printf("Pack is smaller than expected by %d dAh\r\n", dCapacity_dAh);
    }
    else if (dCapacity_dAh < 0)
    {
        //We think we're emptier than we actualla are.
        printf("Pack is larger than expected by %d dAh\r\n", -1*dCapacity_dAh);
    }
    else
    {
        puts("Pack capacity is accurate!");
    }
    
    int32_t adj_dAh = dCapacity_dAh/4;
    printf("Decreasing the pack by %d dAh.\r\n\n", adj_dAh/4);
    SOC_AdjustFullChargeCapacity_dAh(adj_dAh/4);
    filerAndPostSohC();
    
    printf("New FCC = %u dAh\r\n", SOC_GetFullChargeCapacity_dAh());
    printf("New FCC/DC = %u%%\r\n", SOC_GetRelativeCapacity_Prct());
    
}

static void crunchSingleCellFailure(void)
{

    uint32_t minCellSoc_Prct_x10 = CELL_LookupDischargeSocFromVltg_Prct_x10(shared_vars.minCell_mV);
    uint32_t avgCellSoc_Prct_x10 = CELL_LookupDischargeSocFromVltg_Prct_x10(shared_vars.avgCell_mV);
    
    puts("------Single Cell Failure------");
    printf("Min Cell = %umV, ", shared_vars.minCell_mV);
    PrintAs_Scaled_U32(minCellSoc_Prct_x10, 1);
    puts("%");
    
    printf("Avg Cell = %umV, ", shared_vars.avgCell_mV);
    PrintAs_Scaled_U32(avgCellSoc_Prct_x10, 1);
    puts("%");
    
    uint32_t delta_soc_x10 = avgCellSoc_Prct_x10 - minCellSoc_Prct_x10;
    
    putstr("dSoC = ");
    PrintAs_Scaled_U32(delta_soc_x10, 1);
    puts("%");
    

    sohc_vars.estNumFailedCells_x10 = (delta_soc_x10 * configValues.numCellsInParallel)/100;
    
    putstr("Est. # failed cells = ");
    PrintAs_Scaled_U32(sohc_vars.estNumFailedCells_x10, 1);
    NEW_LINE();   
}




void SOH_PrepareForShutdown()
{
    if (MEM_WriteWordFromRamToNvm(vAddr_SohP, SIG_GetAvg_U32(sohSigs.soh_power)))
    {
        puts("Saved SoH Power to NVRAM!");
    }
    else
    {
        puts("Failed to save SoH Power to NVRAM.");
    }

}
void SOH_FillNumFailedCells(uint8_t* dst, uint8_t nBytes)
{
    if (nBytes)
    {
        dst[0] = (sohc_vars.estNumFailedCells_x10 > UINT8_MAX) ? UINT8_MAX : sohc_vars.estNumFailedCells_x10;
    }
}

static void filterAndPostSohP()
{
    /*
     * s[k] = alpha*x[k] + (1 - alpha)*s[k-1]
     *      = (x[k] + (beta - 1)*s[k-1])/beta
     */
        
    //the filter is differnet depending on direction...
    uint32_t last_soh_x10 = SIG_GetAvg_U32(sohSigs.soh_power)*10; //s[k-1]
    uint32_t beta, new_soh_x10;
    

    
    int32_t dSohP = sohp_vars.sohp_raw_x10prct - last_soh_x10;
    
    new_soh_x10 = last_soh_x10;
    
    if (dSohP != 0)
    {
        //Determine which beta to use depending on whether we're rising or falling
        beta = (dSohP > 0) ? sohp_vars.riseBeta : sohp_vars.fallBeta;
        
        if (beta > 0)
        {
           new_soh_x10 = (sohp_vars.sohp_raw_x10prct + (beta - 1)*last_soh_x10)/beta;
        }
        else
        {
            LOG_Event(LOG_LVL_FATAL, "Bad SohP beta!");
        }
        
    }
     
    //Now post!
    T_SignalDatum d;
    d.u32 = (new_soh_x10 + 5)/10;
    SIG_WriteSignal(sohSigs.soh_power, d);

}

static T_SignalNode* char2sig(char x)
{
    switch (x)
    {
        case 'c': 
            return sohSigs.soh_capacity;
        case 'p': 
            return sohSigs.soh_power;
        case 's': 
        case 'b':
            return sohSigs.soh_selfDischarge;    
    }
    
    return NULL;
}

void SOH_ProcessInput(const char *tokens[], uint8_t nTokens)
{

    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("config\tView/modify module configuration.");
        puts("set [c,p,s] [%]\tOverride a particular SoH output.");
        puts("crunch-scf\tForce a single cell failure calculation.");
        puts("crunch-sohc\tForce a SOH Capacity calculation.");
        puts("toggle-hot\tManually toggle the SCF \"hot-flag\".");
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
        CLI_ProcessSignedConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, configSignMask, tokens, nTokens);
    }
    else if (stricmp(tokens[0], "set") == 0)
    {
        T_SignalDatum d;
        int32_t i32;
        if (nTokens >= 3)
        {
            T_SignalNode* node = char2sig(tokens[1][0]);
            if (node)
            {
                i32 = atoi(tokens[2]);
                d.i32 = (i32 < 0) ? 0 : ((i32 > 100) ? 100 : i32);
                SIG_WriteSignal(node, d);
                printf("%s <-- %u", SIG_GetName(node), d.u32);
   
            }
            else
            {
                puts("Bad input.");
            }
                    
        }
        else
        {
            puts("set [c,p,s] [%]");
        }
    }
    else if (stricmp(tokens[0], "crunch-scf") == 0)
    {
        updateSharedVars();
        crunchSingleCellFailure();
    }
    else if (stricmp(tokens[0], "crunch-sohc") == 0)
    {
        updateSharedVars();
        crunchSohCapacity();
        updateSohCapacity();
    }
    else if (stricmp(tokens[0], "toggle-hot") == 0)
    {
        sohc_vars.hotFlag = !sohc_vars.hotFlag;
    }
    else
    {
        putchar('?');
    }

}

void SOH_DrawInit()
{
     //Print title
    
    putstr("\t\tSOH DEBUG\r\n\n");
    VT100_PrintBold("Generic Stats");
    puts("\r\nSoC\r\nCurr\r\nMinCell\r\nAvgCell\r\nMaxCell\r\n");
    
    VT100_PrintBold("Power");
    putstr("\r\nState\r\ndV\r\ndRes\r\nRaw\r\nOut\r\n\n");
    
    VT100_PrintBold("Capacity/Single Cell Failure");
    putstr("\r\nState\r\nSohC\r\nHotFlag\r\nSCF\r\n");
}


void SOH_DrawUpdate()
{
    putstr("\n\n\n\t");
    VT100_CLEAR_LINE();
    SIG_PrintData(coreSigs.nSoc, SIG_RQST_AVG);
    putstr("%\r\n\t");
    VT100_CLEAR_LINE();
    PrintAs_Millis_I32_f2(shared_vars.curr_mA);
    putstr(" A\r\n\t");
    VT100_CLEAR_LINE();
    PrintAs_Millis_U32_f2(shared_vars.minCell_mV);
    putstr(" mV\r\n\t");
    VT100_CLEAR_LINE();
    PrintAs_Millis_U32_f2(shared_vars.avgCell_mV);
    putstr(" mV\r\n\t");
    VT100_CLEAR_LINE();
    PrintAs_Millis_U32_f2(shared_vars.maxCell_mV);
    puts(" mV");
     
    putstr("\r\n\n\t");
    VT100_CLEAR_LINE();
    putstr(sohp_state2Str(sohp_vars.state));
    NEW_LINE_TAB();
    VT100_CLEAR_LINE();
    PrintAs_Dec_U32(sohp_vars.deltaV_mV, 0);
    putstr(" mV\r\n\t");
    
    VT100_CLEAR_LINE();
    PrintAs_Dec_U32(sohp_vars.deltaR_uOhm, 0);
    putstr(" uOhm\r\n\t");
    VT100_CLEAR_LINE();
    PrintAs_Scaled_U32(sohp_vars.sohp_raw_x10prct, 1);
    putstr("%\r\n\t");
    VT100_CLEAR_LINE();
    SIG_PrintData(sohSigs.soh_power, SIG_RQST_AVG);
    putstr("%\r\n\n\n\t");
    
    VT100_CLEAR_LINE();
    putstr(sohc_state2str(sohc_vars.state));
    NEW_LINE_TAB();
    VT100_CLEAR_LINE();
    printf("%u%%\t", sohc_vars.relativeCapacity_Prct);
    SIG_PrintData(sohSigs.soh_capacity, SIG_RQST_AVG);
    putstr("%\r\n\t");
    VT100_CLEAR_LINE();
    puts(sohc_vars.hotFlag ? "Triggered" : "OK");
    SEND_TAB();
    VT100_CLEAR_LINE();
    PrintAs_Scaled_U32(sohc_vars.estNumFailedCells_x10,1);
    puts(" cells\r\n\n");

    CLI_DrawProgressBar();
    NEW_LINE();
}


