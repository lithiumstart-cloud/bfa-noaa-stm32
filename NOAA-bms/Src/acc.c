/**
 * @file acc.c
 * @brief Lifetime charge accumulator
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-08-18
 *
**/

#include "acc.h"
#include <bluflex_core.h>
//####################### DEFINES ##############################################


typedef union
{
    uint32_t arrayU32[2];

    struct
    {
        uint32_t workingCycle_ccu; ///< Stores charges less than pack capacity, CCU
        uint32_t accumulated_Ah; ///< The charge accumulated over time, in Ah
    };
}T_Acc;
#define NVM_TACC_WORDS  (2)
#define NVM_SYNC_PERIOD_MS (1000ULL*60*60*24) ///< Write to NVRAM once/day
//############################# VARIABLES ######################################
enum
{
    SINCE_RESET =0,
    ALL_TIME,
    NUM_CHANNELS,
};

static T_Acc accumulators[ACC_NUM_ACCUMULATORS][NUM_CHANNELS];
static T_MemAddr allTimeAccumulator_vAddr[ACC_NUM_ACCUMULATORS];
static uint32_t CCU_per_Ah;
static T_Timer tmrSync;
//############################# FUNCTIONS ######################################
static void syncNvmStorage(void);

static const char * acc2Str(T_AccId id);

//############################## CODE ##########################################

void ACC_Init()
{
    uint8_t ii,jj;
    for (ii = 0; ii < ACC_NUM_ACCUMULATORS; ii++)
    {
        for (jj = 0; jj < NUM_CHANNELS; jj++)
        {
            accumulators[ii][jj].workingCycle_ccu = 0;
            accumulators[ii][jj].accumulated_Ah = 0;
        }

        //Allocate space in NVR
        allTimeAccumulator_vAddr[ii] = MEM_AllocateData(2); //2 U32's in T_Acc
       
        //Read in any existing data
        if (allTimeAccumulator_vAddr[ii])
        {
            if (MEM_WasJustProgrammed())
            {
                //If we were just programmed, write zeros to NVRM.
                MEM_WriteDataFromRamToNvm(allTimeAccumulator_vAddr[ii], &accumulators[ii][ALL_TIME].arrayU32[0], NVM_TACC_WORDS);
            }
            else
            {
                //Otherwise, load whatever was saved there.
               MEM_ReadDataFromNvmToRam(allTimeAccumulator_vAddr[ii], &accumulators[ii][ALL_TIME].arrayU32[0], NVM_TACC_WORDS);
            }
        }
        else
        {
            putstr("Could not init MEM space for all-time ACC!\r\n");
        }
    }

    //Grab CCUs-per-Ah constant from SOC module:
    CCU_per_Ah = SOC_GetCcuPerAh();
    if (CCU_per_Ah == 0)
    {
        putstr("ERROR! ACC init'd before SOC\r\n");
    }
    
    TMR_StartRepeating(&tmrSync, NVM_SYNC_PERIOD_MS);

}

bool ACC_Update()
{

    if (TMR_HasTripped_ms(&tmrSync))
    {
        syncNvmStorage();
        
    }

    return true;
}
void ACC_AddDeltaTo(T_AccId id, uint32_t dCCU)
{
    if (CCU_per_Ah == 0)
    {
        //Did you call Acc_Init before SoC?
        LOG_Report(LOG_MOD_ACC, LOG_LVL_FATAL,  LOG_MSG_ASSERTION_FAILED, __LINE__);
        return;
    }

    if (id < ACC_NUM_ACCUMULATORS)
    {
        uint8_t jj;
        for (jj = 0; jj < NUM_CHANNELS; jj++)
        {
            uint32_t old_working = accumulators[id][jj].workingCycle_ccu;
            accumulators[id][jj].workingCycle_ccu += dCCU;

            if (accumulators[id][jj].workingCycle_ccu < old_working)
            {
                //We overflowed. Max out, and log error. We can't handle changes this big for now.
                accumulators[id][jj].workingCycle_ccu = UINT32_MAX;
                LOG_Report(LOG_MOD_ACC, LOG_LVL_FATAL, LOG_MSG_CALC_OVERFLOW, __LINE__);

            }

            while (accumulators[id][jj].workingCycle_ccu >= CCU_per_Ah)
            {
                accumulators[id][jj].accumulated_Ah += 1;
                accumulators[id][jj].workingCycle_ccu -= CCU_per_Ah;
            }

            LOG_ASSERT(LOG_MOD_ACC, accumulators[id][jj].workingCycle_ccu < CCU_per_Ah);
        }
    }
}


void ACC_ProcessInput(const char* tokens[], uint8_t nTokens)
{
    uint8_t ii;
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        putstr(STR_now); NEW_LINE();
        putstr(STR_clear); NEW_LINE();
        putstr("import\r\n");
    }
    else if (stricmp(tokens[0],STR_now) == 0)
    {
        putstr("\tSinceOn\t\tAllTime\r\n");
        for (ii = 0; ii < ACC_NUM_ACCUMULATORS; ii++)
        {
            putstr(acc2Str(ii));
            SEND_TAB();
            PrintAs_Dec_U16(accumulators[ii][SINCE_RESET].accumulated_Ah, 0);
            uint16_t cAh = (((accumulators[ii][SINCE_RESET].workingCycle_ccu >>16 ))*1000ULL + 5)/(CCU_per_Ah >> 16);
            putchar('.');
            PrintAs_Dec_U16(cAh,3);
            
            putstr("\t\t");

            PrintAs_Dec_U16(accumulators[ii][ALL_TIME].accumulated_Ah, 0);
            cAh = (((accumulators[ii][ALL_TIME].workingCycle_ccu >>16 ))*1000ULL + 5)/(CCU_per_Ah >> 16);
            putchar('.');
            PrintAs_Dec_U16(cAh,3);
            putstr("\t Ah");
            NEW_LINE();
        }

        //TODO: account for overflow and CCU's
        uint32_t total = accumulators[ACC_IN][ALL_TIME].accumulated_Ah + accumulators[ACC_OUT][ALL_TIME].accumulated_Ah;
        putstr("Total: "); PrintAs_Dec_U16(total,0); putstr(" Ah\r\n");
        //putstr("Cycles: "); PrintAs_Dec_U16(total/(2*appConfigValues.packCapacity_Ah),0);
        NEW_LINE();
    }
    else if (stricmp(tokens[0], STR_clear) == 0)
    {
        for (ii = 0; ii < ACC_NUM_ACCUMULATORS; ii++)
        {
            accumulators[ii][SINCE_RESET].workingCycle_ccu = 0;
            accumulators[ii][SINCE_RESET].accumulated_Ah = 0;
        }
    }
    else if (stricmp(tokens[0], "import") == 0)
    {
        if (nTokens == 3)
        {
            accumulators[ACC_IN][ALL_TIME].accumulated_Ah = atol(tokens[1]);
            accumulators[ACC_OUT][ALL_TIME].accumulated_Ah = atol(tokens[2]);
            syncNvmStorage();
        }
        else
        {
            putstr("acc import <Ah in> <Ah out>");
        }
    }
    else
    {
        putchar('?');
    }

}


static const char * acc2Str(T_AccId id)
{
    switch (id)
    {
        case ACC_IN:    return "In";
        case ACC_OUT:   return "Out";
    }
    
    return "???";

}

uint16_t ACC_GetNumberCycles(void)
{
//    return (accumulators[ACC_IN][ALL_TIME].accumulated_Ah + accumulators[ACC_OUT][ALL_TIME].accumulated_Ah)/(2*appConfigValues.packCapacity_Ah);
    return 0;
}


static bool testEquality(T_Acc* a, T_Acc* b)
{
    return (a->accumulated_Ah == b->accumulated_Ah) && (a->workingCycle_ccu == b->workingCycle_ccu);
}

static void syncNvmStorage(void)
{
    T_Acc tempBuffer;
    uint8_t ii;
            //Write SN to NVRAM
     for (ii = 0; ii < ACC_NUM_ACCUMULATORS; ii++)
     {
        if (!MEM_WriteDataFromRamToNvm(allTimeAccumulator_vAddr[ii], &accumulators[ii][ALL_TIME].arrayU32[0], NVM_TACC_WORDS))
        {
            putstr("Error occured writing Acc data to NVRAM");
        }
        else if (!MEM_ReadDataFromNvmToRam(allTimeAccumulator_vAddr[ii], &tempBuffer.arrayU32[0], NVM_TACC_WORDS))
        {
            putstr("Error occured reading back Acc data from NVRAM");
        }
        else if (testEquality(&accumulators[ii][ALL_TIME], &tempBuffer))
        {
            putstr("Accumulator ");
            PrintAs_Dec_U8(ii,0);
            putstr(" updated!\r\n");
            
        }
        else
        {
            putstr("Failed to write updated ACC data to flash (mistmatch).\r\n");
        }
     }
}
