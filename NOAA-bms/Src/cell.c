 /**
 * @file cell.c
 * @brief Bundled Cell Profile Library
 * @copyright 2016 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-05-09
 *
**/
//############################## INCLUDES ######################################


#include "cell.h"
#include "soc.h"
#include <bluflex_core.h>

//############################## DEFINES ######################################
#define SOC_MAP_NUM_POINTS (11)
#define SOC_NUM_CYCLE_TYPES (2)

//########################### CONFIG VALUES ####################################



//########################### MODULE  TYPES ####################################
static struct
{
    const T_Table* pChgCycle_Tbl;
    const T_Table* pDisCycle_Tbl;
} vltgTables;



//########################## MODULE VARIABLES ##################################

static const T_Point vltg2Soc_Points[][SOC_NUM_CYCLE_TYPES][SOC_MAP_NUM_POINTS]; //forward dec
static const T_AlarmLimits cellVoltageAlarms[][CELL_USAGE_NUM_USAGE_MODES];
static const T_AlarmLimits cellTemperatureAlarms[][CELL_USAGE_NUM_USAGE_MODES];
static const T_CELL_SnapPoints socSnapVoltages[];
static const uint16_t balancingMinimums_mV[];
static const T_CELL_LIM_Settings limSettings[];

static T_Cell_Type cellType;
static T_Cell_UsageMode cellUsageMode;

//############################# PROTOTYPES #####################################


//############################ PUBLIC CODE #####################################
bool CELL_SelectProfile(T_Cell_Type _type, T_Cell_UsageMode _profile)
{
    cellUsageMode = _profile;
    cellType = _type;
    
    if (cellType < CELL_TYPE_NUM_CELL_TYPES)
    {
        vltgTables.pChgCycle_Tbl = TBL_AddTable("ChgCycle", &vltg2Soc_Points[cellType][SOC_MAP_CHG_CYCLE][0], SOC_MAP_NUM_POINTS);
        vltgTables.pDisCycle_Tbl = TBL_AddTable("DisCycle", &vltg2Soc_Points[cellType][SOC_MAP_DIS_CYCLE][0], SOC_MAP_NUM_POINTS);
    }
    else   
    {
        puts("Bad cell type!");
        return false;
    }
           
    
    if (cellUsageMode >= CELL_USAGE_NUM_USAGE_MODES)
    {
        puts("Bad cell usage mode!");
        return false;
    }
    
    
    printf("Set to \"%s (%s)\"\r\n", CELL_Type2Str(cellType),CELL_UsageMode2Str(cellUsageMode));

    return true;
    
}


uint32_t CELL_LookupDischargeSocFromVltg_Prct_x10(uint32_t cellVltg_mV)
{
    return (uint32_t) TBL_Lookup(vltgTables.pDisCycle_Tbl, cellVltg_mV);
}


uint32_t CELL_LookupChargeSocFromVltg_Prct_x10(uint32_t cellVltg_mV)
{
    return (uint32_t) TBL_Lookup(vltgTables.pChgCycle_Tbl, cellVltg_mV);
}


const char* CELL_Type2Str(T_Cell_Type c)
{
    switch (c)
    {
        case CELL_TYPE_PANASONIC_NCR18650:      return "Panasonic-NCR18650";
        case CELL_TYPE_SAMSUNG_INR18650_30Q:    return "Samsung-INR18650-30Q";
        case CELL_TYPE_A123_26650B:             return "A123-26650B";
        case CELL_TYPE_A123_26700M1:            return "A123-26700M1";
        case CELL_TYPE_LG_18650_HG2:            return "LG-18650-HG2";
        case CELL_TYPE_24V_LEAD_ACID:           return "24V-LeadAcid";
        default: break;
    }

    return "???";
}

const char* CELL_UsageMode2Str(T_Cell_UsageMode p)
{
    switch (p)
    {
        case CELL_USAGE_BASIC: return "Basic";
        case CELL_USAGE_WIDE: return "Wide";
        
        default: break;
    }
    
    return "???";
   
}

const T_AlarmLimits* CELL_GetCellVoltageAlarmLimits()
{
    if (cellType < CELL_TYPE_NUM_CELL_TYPES && cellUsageMode < CELL_USAGE_NUM_USAGE_MODES)
    {
        return &(cellVoltageAlarms[cellType][cellUsageMode]);
    }
    
    return NULL;
}

const T_AlarmLimits* CELL_GetCellTemperatureAlarmLimits()
{
    if (cellType < CELL_TYPE_NUM_CELL_TYPES && cellUsageMode < CELL_USAGE_NUM_USAGE_MODES)
    {
        return &(cellTemperatureAlarms[cellType][cellUsageMode]);
    }
    
    return NULL;
}


uint16_t CELL_GetReccomendedMinBalancingVoltage_mV()
{
    if (cellType < CELL_TYPE_NUM_CELL_TYPES)
    {
        return balancingMinimums_mV[cellType];
    }
    
    return UINT16_MAX;
}

T_CELL_SnapPoints CELL_GetReccomendedSocSnapVoltages()
{
    if (cellType < CELL_TYPE_NUM_CELL_TYPES)
    {
        return socSnapVoltages[cellType];
    }
    
    T_CELL_SnapPoints c = {4000,5000};
           
    return c;
            
}

T_CELL_LIM_Settings CELL_GetReccomendedLimSettings(void)
{
    if (cellType < CELL_TYPE_NUM_CELL_TYPES)
    {
        return limSettings[cellType];
    }
    
    T_CELL_LIM_Settings z = {0};
    return z;
}
//########################### CLI HANDLER ######################################
//Handle command line inputs that had first token 'relay'
void CELL_ProcessInput(const char* tokens[], uint8_t nArgs)
{
    if (nArgs == 0 || tokens[0][0] == '?')
    {
        puts("info\tPrint info about select cell profile.");
        
    }
    else if (stricmp(tokens[0], "info") == 0)
    {
        printf("Cell type = %s\r\n", CELL_Type2Str(cellType));
        printf("Usage mode = %s\r\n", CELL_UsageMode2Str(cellUsageMode));
    }
    else
    {
        putchar('?');
    }
}



//### LOOKUP TABLES ############################################################

//{ Minimum cell voltage (mV) , SoC (.1%)}
//                                     # cells          # different cycles        points/cell
static const T_Point vltg2Soc_Points[CELL_TYPE_NUM_CELL_TYPES][SOC_NUM_CYCLE_TYPES][SOC_MAP_NUM_POINTS] =
{
    //PANASONIC NCR18650
    {
        //Charge Cycle
        {
            {3000,   0},
            {3332, 100},
            {3442, 200},
            {3499, 300},
            {3554, 400},
            {3648, 500},
            {3703, 600},
            {3796, 700},
            {3883, 800},
            {4001, 900},
            {4185, 1000}
        },

        //Discharge Cycle 
        {
            {3000,   0},
            {3332, 100},
            {3442, 200},
            {3499, 300},
            {3554, 400},
            {3648, 500},
            {3703, 600},
            {3796, 700},
            {3883, 800},
            {4001, 900},
            {4185, 1000}
        },
    },

     //SAMSUNG INR18650-30Q
    {
        //Charge Cycle
        {
            {3000,   0},
            {3332, 100},
            {3442, 200},
            {3499, 300},
            {3554, 400},
            {3648, 500},
            {3703, 600},
            {3796, 700},
            {3883, 800},
            {4001, 900},
            {4185, 1000}
        },

        //Discharge Cycle 
        {
            {3000,   0},
            {3332, 100},
            {3442, 200},
            {3499, 300},
            {3554, 400},
            {3648, 500},
            {3703, 600},
            {3796, 700},
            {3883, 800},
            {4001, 900},
            {4185, 1000}
        },
    },
    
    //A123 26650B
    {
        //Charge Cycle
        {
            {3109,   0},
            {3223, 100},
            {3264, 200},
            {3299, 300},
            {3308, 400},
            {3310, 500},
            {3313, 600},
            {3326, 700},
            {3343, 800},
            {3344, 900},
            {3494, 1000}
        },

        //Discharge Cycle
        {
            {2546,   0},
            {3188, 100},
            {3227, 200},
            {3257, 300},
            {3285, 400},
            {3292, 500},
            {3294, 600},
            {3301, 700},
            {3332, 800},
            {3335, 900},
            {3436, 1000}
        }
    },
    
    //A123 26700M1
    {
        //Charge Cycle
        {
            {2640,   0},
            {3073, 40},
            {3205, 80},
            {3223, 100},
            {3299, 300},
            {3308, 360},
            {3314, 660},
            {3343, 760},
            {3345, 960},
            {3373, 980},
            {3540, 1000}
        },

        //Discharge Cycle
        {
            {2580,   0},
            {3045, 40},
            {3128, 60},
            {3182, 80},
            {3197, 100},
            {3287, 380},
            {3301, 700},
            {3326, 740},
            {3339, 960},
            {3367, 980},
            {3480, 1000}
        }
    },
      //LG 18650HG2
    {
        //Charge Cycle
        {
            {3142, 0},
            {3329, 100},
            {3481, 183},
            {3574, 300},
            {3668, 400},
            {3753, 500},
            {3838, 600},
            {3924, 700},
            {4005, 800},
            {4082, 900},
            {4134, 1000},
        },
        //Discharge Cycle
        {
            {3142, 0},
            {3329, 100},
            {3481, 183},
            {3574, 300},
            {3668, 400},
            {3753, 500},
            {3838, 600},
            {3924, 700},
            {4005, 800},
            {4082, 900},
            {4134, 1000},
        }
    },
        //24V Lead Acid
    {
        //Charge Cycle
        {
            {21000,   0},
            {22400, 100},
            {23000, 200},
            {23500, 300},
            {23800, 400},
            {24050, 500},
            {24250, 600},
            {24500, 700},
            {24750, 800},
            {24900, 900},
            {25500, 1000}
        },

        //Discharge Cycle
        {
            {21000,   0},
            {22400, 100},
            {23000, 200},
            {23500, 300},
            {23800, 400},
            {24050, 500},
            {24250, 600},
            {24500, 700},
            {24750, 800},
            {24900, 900},
            {25500, 1000}
        },
    }
};

static const T_AlarmLimits cellVoltageAlarms[CELL_TYPE_NUM_CELL_TYPES][CELL_USAGE_NUM_USAGE_MODES] = 
{
    //Panasonic NCR18650
    {
        {4150, 4125, 4100, 3300, 3250, 3200}, //standard
        {4300, 4250, 4100, 2200, 2100, 2000}, //wide
    },

    //Samsung INR18650-30Q
    {
        {4200, 4150, 4100, 2700, 2600, 2500}, //standard
        {4250, 4200, 4100, 2200, 2100, 2000}, //wide
    },
    
    //A123 26650B
    {
        {3800, 3750, 3700, 2600, 2500, 2200}, //standard
        {3800, 3770, 3750, 2200, 2100, 2000}, //wide
    },
    
    //A123 26700M1
    {
        {3800, 3750, 3700, 2600, 2500, 2200}, //standard
        {3900, 3800, 3700, 2000, 1900, 1800}, //wide
    },
    //LG 18650 HG2
    {
        {4200, 4150, 4100, 3300, 3250, 3200}, //standard
        {4250, 4200, 4150, 2200, 2100, 2000}, //wide
    },
    //24V Lead Acid
    {
        {26e3, 25500, 25e3, 22e3, 21e3, 20e3}, //standard
        {26e3, 25500, 25e3, 22e3, 21e3, 20e3}, //wide
    } 
};

static const T_AlarmLimits cellTemperatureAlarms[CELL_TYPE_NUM_CELL_TYPES][CELL_USAGE_NUM_USAGE_MODES] = 
{
    //Panasonic NCR18650
    {
        {60, 55, 50, 15, 10, 0}, //standard
        {80, 75, 70, 10, 5, 0}, //wide
    },
    
    //Samsung INR18650-30Q
    {
        {60, 55, 50, 15, 10, 0}, //standard
        {80, 75, 70, -20, -25, -30}, //wide
    },
    
    //A123 26650B
    {
        {60, 55, 50, 15, 10, 0}, //standard
        {80, 75, 70, 10, 5, 0}, //wide
    } ,
    
    //A123 26700M1
    {
        {60, 55, 50, 15, 10, 0}, //standard
        {90, 85, 80, 10, 0, -10}, //wide
    } ,
    //LG 18650 HG2
    {
        {60, 55, 50, 15, 10, 0}, //standard
        {90, 75, 70, 10, 5, 0}, //wide
    },
    
    //24V Lead Acid
    {
        {60, 55, 50, 15, 10, 0}, //standard
        {80, 75, 70, 10, 5, 0}, //wide
    } 
};


static const uint16_t balancingMinimums_mV[CELL_TYPE_NUM_CELL_TYPES] = 
{
    4000, //Panasonic NCR18650
    4000, //Samsung INR18650-30Q
    3400, //A123 26650B
    3390, //A123 26700M1
    4000, //LG 18650 HG2
    24e3, //24V Lead Acid
};

static const T_CELL_SnapPoints socSnapVoltages[CELL_TYPE_NUM_CELL_TYPES] = 
{
  /* 100%,  0% */
    {4150, 3300}, //Panasonic NCR18650
    {4150, 3300}, //Samsung INR18650-30Q
    {3400, 3000}, //A123 26650B
    {3400, 3000}, //A123 26700M1
    {4150, 3300}, //LG 18650 HG2
    {29000, 23000}, //24V Lead Acid
};

         
static const T_CELL_LIM_Settings limSettings[CELL_TYPE_NUM_CELL_TYPES] =
{
 // CCL Max   CCL Taper   DCL Min    DCL Taper    CVL Nom   DVL Nom
    {4150,      4000,       3000,       3300,      4000,    3100},  //Panasonic NCR18650
    {4150,      4000,       3000,       3300,      4000,    3100},  //Samsung INR18650-30Q
    {3750,      3500,       2400,       2700,      3600,    2700},  //A123 26650
    {3750,      3500,       2400,       2700,      3600,    2700},  //A123 26700M1              
    {4150,      4000,       3000,       3300,      4000,    3100},  //LG 18650 HG2
    {26e3,      25e3,      20e3,       21e3,      25500,    21500}, //24V Lead Acid
};
