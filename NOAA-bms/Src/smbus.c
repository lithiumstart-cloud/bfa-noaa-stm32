/**
 * @file noaa_smbus.c
 * @brief SMBus driver for STM32L4xx
 * @copyright 2017 EP Lithiumstart Division
 * @author A. Kessler
 * @date   2017-08-01
 *
**/
//############################## INCLUDES ######################################

#include "sbs.h"
#include "smbus.h"

#include "bluflex_core.h"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_smbus.h"



//############################## DEFINES ######################################
#define BASE_I2C_ADDRESS_8_BIT 0x16

//############################### TYPEDEFS ####################################
typedef enum
{
	ST_SMB_PINIT = 0,
	ST_SMB_WAITING_FOR_FIRST_ADDR_MATCH,
	ST_SMB_WAITING_FOR_COMMAND_BYTE,
	ST_SMB_WAITING_FOR_RSTART_OR_WRITE_BYTE,
	ST_SMB_SENDING_DATA,
	ST_SMB_RECEIVING_DATA,
	ST_SMB_ERROR,
} T_SMB_State;


typedef enum
{
	ERR_SMB__NONE = 0,
	ERR_SMB__QUICK_READ_NOT_SUPPORTED,
	ERR_SMB__REPEATED_WRITE_NOT_SUPPORTED,
} T_SMB_ErrorCode;

//############################## VARIABLES ####################################
static volatile T_SMB_State presentState;
//static SMBUS_HandleTypeDef hsmbus1;
static volatile T_SMBUS_Packet workingPacket;

static volatile uint8_t  bytesSent;
static volatile uint8_t  bytesRcvd;
static volatile uint8_t  dataRxByte;

static T_LED* pLed = NULL;

//############################## PROTOTYPES ###################################
static void changeStateTo(T_SMB_State s);

bool SMBUS_StartListening(void);



//<SRB>### FOR TESTING ########################################################
//uint32_t trace[100];		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###
//uint8_t  trIdx = 0;			//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###
//#############################################################################



//############################ PUBLIC FUNCTIONS ###############################
void SMBUS_Init(void)
{
//	SMBus_HW_Init();
	presentState = ST_SMB_PINIT;
}


bool SMBUS_Update(void)
{
	switch (presentState)
	{
		case ST_SMB_PINIT:
			changeStateTo(ST_SMB_WAITING_FOR_FIRST_ADDR_MATCH);
			SMBUS_StartListening();  //<SRB>### CHECK RETURN CODE FOR ERROR AND DO SOMETHING ###
		break;

		case ST_SMB_WAITING_FOR_FIRST_ADDR_MATCH:
			LED_SetNow(pLed, false);
            break;

		case ST_SMB_WAITING_FOR_COMMAND_BYTE:
		case ST_SMB_WAITING_FOR_RSTART_OR_WRITE_BYTE:
		case ST_SMB_SENDING_DATA:
		case ST_SMB_RECEIVING_DATA:
            LED_SetNow(pLed, true);
            break;

		case ST_SMB_ERROR:
			//Sit here until restart.
			break;


	}

	return true;
}



void SMB_AssignLED(T_LED* _pLed)
{
    pLed = _pLed;
    LED_SetMode(pLed, LMODE_OFF);
}


T_SMB_State SMB_GetState(void)
{
	return(presentState);
}


//############################## MODULE CODE ####################################
static void changeStateTo(T_SMB_State newState)
{
	presentState = newState;
}


//################################################################################################################
//################################################################################################################
//################################################################################################################
//################################################################################################################
//################################################################################################################
//################################################################################################################
//################################################################################################################




//<SRB>##########################################################################################################
//<SRB>### MOVE TO BSP DIRECTORY (MAYBE) ########################################################################
//<SRB>##########################################################################################################


/**
* @brief  Slave Address Match callback.
* @param  hsmbus Pointer to a SMBUS_HandleTypeDef structure that contains
*                the configuration information for the specified SMBUS.
* @param  TransferDirection Master request Transfer Direction (Write/Read)
* @param  AddrMatchCode Address Match Code
* @retval None
*/
void HAL_SMBUS_AddrCallback(SMBUS_HandleTypeDef *hsmbus, uint8_t TransferDirection, uint16_t AddrMatchCode)
{
  /* Prevent unused argument(s) compilation warning */
//  UNUSED(AddrMatchCode);

	T_SMB_State SMB_State = SMB_GetState();


	switch (SMB_State)
	{
		/*
		 * The device is in listener mode and this is the start of a new packet
		 */
		case ST_SMB_WAITING_FOR_FIRST_ADDR_MATCH:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###

			/*
			 *  At the beginning of the packet, the only acceptable option is a
			 *  WRITE because the command byte should follow.
			 */
			if ( TransferDirection == SMBUS_TYPE__WRITE_FROM_HOST )
			{
				HAL_SMBUS_Slave_Receive_IT(hsmbus, &workingPacket.cmd, 1, 0);
				changeStateTo(ST_SMB_WAITING_FOR_COMMAND_BYTE);
			}

			/*
			 *  A READ indicates a quick command which is not supported
			 */
			else
			{
				//<SRB>### FIGURE OUT WHAT TO DO HERE ###
//				//TODO: throw error/NACK
//				presentState = ST_SMB_ERROR;
			}
			break;


		/*
		 * If an address comes after the command byte, this is a read command
		 */
		case ST_SMB_WAITING_FOR_RSTART_OR_WRITE_BYTE:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###

			/*
			 * The host is requesting data.  Get it ready and send it
			 */
			if ( TransferDirection == SMBUS_TYPE__READ_FROM_HOST )
			{
				/*
				 * Process the command and retrieve the data
				 */
				SBS_GetRequestedData(&workingPacket);

				/*
				 * WARNING !!! Parameter SMBUS_SENDPEC_MODE should not be changed. If it is set to 0 it will mess up
				 * with data sequence being send. It enables PEC transfer.
				 */
				HAL_SMBUS_Slave_Transmit_IT(hsmbus, &(workingPacket.data[0]), workingPacket.numBytes, SMBUS_SENDPEC_MODE);
				bytesSent = workingPacket.numBytes;

				changeStateTo(ST_SMB_SENDING_DATA);
			}
			else
			{
				//<SRB>### FIGURE OUT WHAT TO DO HERE ###
//				//This would be a "quick read", which we do not support.
//				//TODO: throw error/NACK
//				presentState = ST_SMB_ERROR;
			}
			break;

		/*
		 * If an address byte comes in when the device is in any other state,
		 * we are out of sync
		 */
		default:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###

			//<SRB>### FIGURE OUT WHAT TO DO HERE ###
			//					//This would be a "quick read", which we do not support.
			//					//TODO: throw error/NACK
			//					presentState = ST_SMB_ERROR;

			break;
	}

	hsmbus->Instance->ICR = I2C_ICR_ADDRCF; //clear ADDR flag
}



/**
  * @brief  Slave Rx Transfer completed callback.
  * @param  hsmbus Pointer to a SMBUS_HandleTypeDef structure that contains
  *                the configuration information for the specified SMBUS.
  * @retval None
  */
void HAL_SMBUS_SlaveRxCpltCallback(SMBUS_HandleTypeDef *hsmbus)
{
	T_SMB_State SMB_State = SMB_GetState();		//<SRB>### FOR PORTABILITY ###

	/*
	 * A byte was received from the Master
	 * This is either a
	 */
	switch (SMB_State)
	{
		/*
		 * After a command byte, either a data byte or an address byte with a
		 * READ request will follow, so enable the receiver
		 */
		case ST_SMB_WAITING_FOR_COMMAND_BYTE:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###

			HAL_SMBUS_Slave_Receive_IT(hsmbus, &dataRxByte, 1, 0);
			changeStateTo( ST_SMB_WAITING_FOR_RSTART_OR_WRITE_BYTE );
			break;

		/*
		 * If it was a read request, the address byte would have detected above
		 * Therefore, this must be a write command
		 */
		case ST_SMB_WAITING_FOR_RSTART_OR_WRITE_BYTE:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###

			/*
			 * Transfer the byte to the data buffer and reset the data counter
			 */
			workingPacket.data[0] = dataRxByte;
			bytesRcvd = 1;

			HAL_SMBUS_Slave_Receive_IT(hsmbus,  &dataRxByte, 1, 0);
			changeStateTo( ST_SMB_RECEIVING_DATA );
			break;

		/*
		 * Keep reading data until the end of the message is reached
		 */
		case ST_SMB_RECEIVING_DATA:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###

			/*
			 * If data comes in after the max block size has been reached,
			 * there is a problem.  Until then, keep reading data
			 */
			if (bytesRcvd < SMBUS_MAX_BLOCK_SIZE)
			{
				workingPacket.data[bytesRcvd++] = dataRxByte;

				HAL_SMBUS_Slave_Receive_IT(hsmbus, &dataRxByte, 1, 0);
			}
			else
			{
				//<SRB>### WE HAVE RECEIVED TOO MUCH DATA, DO SOMETHING ###
			}

		/*
		 * Data should not be received while the device is in any other state
		 */
		default:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###

			//<SRB>### FIGURE OUT WHAT TO DO HERE ###
			break;
	}
}



/** @brief  Slave Tx Transfer completed callback.
  * @param  hsmbus Pointer to a SMBUS_HandleTypeDef structure that contains
  *                the configuration information for the specified SMBUS.
  * @retval None
  */
void HAL_SMBUS_SlaveTxCpltCallback(SMBUS_HandleTypeDef *hsmbus)
{
	/* Prevent unused argument(s) compilation warning */
	UNUSED(hsmbus);

	T_SMB_State SMB_State = SMB_GetState();		//<SRB>### FOR PORTABILITY ###

	/*
	 * In TX mode, the data is sent as a single block, so nothing needs to be
	 * done here.  We could possibly add some error checking here, but I don't
	 * know if that is necessary
	 */
	switch (SMB_State)
	{
		default:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###
			break;
	}
}



/**
  * @brief  Listen Complete callback.
  * @param  hsmbus Pointer to a SMBUS_HandleTypeDef structure that contains
  *                the configuration information for the specified SMBUS.
  * @retval None
  */
void HAL_SMBUS_ListenCpltCallback(SMBUS_HandleTypeDef *hsmbus)
{
	/* Prevent unused argument(s) compilation warning */
	UNUSED(hsmbus);

	T_SMB_State SMB_State = SMB_GetState();		//<SRB>### FOR PORTABILITY ###

	/*
	 * End of message
	 *
	 */
	switch (SMB_State)
	{
		/*
		 * After a command byte, either a data byte or an address byte with a
		 * READ request will follow, so enable the receiver
		 */
		case ST_SMB_RECEIVING_DATA:
		case ST_SMB_WAITING_FOR_RSTART_OR_WRITE_BYTE:
		case ST_SMB_WAITING_FOR_COMMAND_BYTE:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###

			SBS_ProcessWrittenData(&workingPacket);

//			changeStateTo( ST_SMB_DONE );

			changeStateTo(ST_SMB_WAITING_FOR_FIRST_ADDR_MATCH);
			SMBUS_StartListening();  //<SRB>### CHECK RETURN CODE FOR ERROR AND DO SOMETHING ###
			break;

		case ST_SMB_SENDING_DATA:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###

//			changeStateTo( ST_SMB_DONE );

			changeStateTo(ST_SMB_WAITING_FOR_FIRST_ADDR_MATCH);
			SMBUS_StartListening();  //<SRB>### CHECK RETURN CODE FOR ERROR AND DO SOMETHING ###

			break;

		case ST_SMB_WAITING_FOR_FIRST_ADDR_MATCH:
			changeStateTo(ST_SMB_WAITING_FOR_FIRST_ADDR_MATCH);
			SMBUS_StartListening();
			break;

		/*
		 * This should not happen in any other state
		 */
		default:
//			if (trIdx<100) {trace[trIdx++] = __LINE__;}		//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###
			//<SRB>### FIGURE OUT WHAT TO DO HERE ###
			break;
	}
}



/**
  * @brief  SMBUS error callback.
  * @param  hsmbus Pointer to a SMBUS_HandleTypeDef structure that contains
  *                the configuration information for the specified SMBUS.
  * @retval None
  */
void HAL_SMBUS_ErrorCallback(SMBUS_HandleTypeDef *hsmbus)
{
	/* Prevent unused argument(s) compilation warning */
//	UNUSED(hsmbus);

	if (hsmbus->ErrorCode != 32)
	{
//		if (trIdx<100) {trace[trIdx++] = __LINE__;}				//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###
//		if (trIdx<100) {trace[trIdx++] = hsmbus->ErrorCode;}	//<SRB>### FOR TESTING- TRACE EXECIUTION PATH ###
	}

  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_SMBUS_ErrorCallback() could be implemented in the user file
   */
}

//################################################################################################################
//################################################################################################################
//################################################################################################################
//################################################################################################################
//################################################################################################################
//################################################################################################################
//################################################################################################################



