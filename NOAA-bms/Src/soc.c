/**
 * @file soc.c
 * @brief State of Charge (SoC) tracker
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-02
 *
 * The State of Charge algorithm works by keeping an estimate of the instantaneous 
 * remaining charge in battery, by a combination of Coulomb Counting and slow correction
 * based on voltage at low currents. 
 *
 * The battery charge is tracking in "Coulomb Counting Units" (CCUs). One CCU is defined
 * to be [x] mAms, where [x] is the run period of the algorithm in milliseconds. With a
 * configuration of runPeriod = 200ms, one CCU is 200mAms.
 *
 * This makes Column Counting very computationally simple, as the algorithm needs only
 * add/subtract the value of instantaneous current from the charge counter each iteration 
 * through the algorithm. 
 *
 * Current limits for the three tracking modes ("CC In", "CC Out", and "Voltage Lookup")
 * should be configured such that there is no "dead zone" where none of the three modes
 * are active. 
 *
 **/



#include "soc.h"
#include "cell.h"
#include <bluflex_core.h>
#include <noaa_bms.h>


//####################### DEFINES ##############################################


//Helpful macros
#define ENFORCE_BOUNDS(x, _lower, _upper) (((x) < _lower) ? _lower : (((x) > _upper) ? _upper : (x))
#define ABS(x) ((x) < 0 ? -(x) : (x))





#define SOC_NVRAM_GOOD_DATA_KEY 0xDEAFFACE
//########################### CONFIG VALUES ####################################



#define NUM_DRIVING_CONFIG_VALUES (sizeof(T_SOC_Config)/sizeof(uint32_t))

static const uint32_t configSignBitmap = 0x1C; //0b0001 1100

static const char* drivingConfigStrs[NUM_DRIVING_CONFIG_VALUES] =
{
    "runPeriod_ms",
    "designCapacity_Ah",
    "ccCurrThreshP_mA",
    "ccCurrThreshN_mA",
    "absSocLookupCurr_mA",
    "loSocSnapThresh_mV",
    "hiSocSnapThresh_mV",
    "minTimeForActiveCcOut_ms",
    "minTimeForActiveCcIn_ms",
    "minTimeForActiveLkp_ms",
    "maxSocDelta/min (.01%)",
    "minSocDelta/loop (.01%)",
    "cycleUpper (SoC %)",
    "cycleLower (SoC %)",

};

//Derived Config values (read only)

static struct
    {
       uint32_t Ah_to_ccu; //ccu's per 1 Ah
       uint32_t designCapacity_ccu; ///< Theoretical capacity of a new pack

       uint32_t minSocDeltaPerLoop_ccu;  ///minimum difference between lookup'd SoC and SoC to qualify for adjustment
       uint32_t maxSocDeltaPerLoop_ccu;
       uint32_t maxSocDeltaPerSec_ccu;
       uint32_t chargePerLookup_ccu;
       
} derivedConfigValues;

#define NUM_DERIVED_CONFIG_VALUES (sizeof(derivedConfigValues)/sizeof(uint32_t))

static const char* derivedConfigStrs[NUM_DERIVED_CONFIG_VALUES] =
{
    "Ah_to_ccu",
    "designCapacity_ccu",
    "minSocDeltaPerLoop_ccu",
    "maxSocDeltaPerLoop_ccu",
    "maxSocDeltaPerSec_ccu",
    "chargePerLookup_ccu",
};

//############################## TYPES #########################################

///Module level states
typedef enum
{
    ST_WAITING_FOR_BOOT_WITHOUT_SOC = 0,
    ST_WAITING_FOR_BOOT_WITH_SOC,
    ST_RUNNING,
} T_State;

///Mode level states
typedef enum
{
    MODE_INACTIVE=0,
    MODE_STARTUP_WAITING,
    MODE_ACTIVE,
} T_ModeState;

typedef struct
{
    T_ModeState currentState;
    T_Timer timer;
    uint32_t activeTime_ms;
    bool (*fCurrCondition)(void); ///< This function contains the necessary conditions for CURRENT for this mode to be active
    void (*fContribute)(void); ///< This function adjusts the SoC appropriately for one active loop
} T_Mode;

static T_Mode mVltgLkp, mCcIn, mCcOut;

//############################# VARIABLES ######################################

static T_CoreSignals coreSigs;

/* These are State Variables in the sense of a Dynamical System.*/
static struct
{
    uint32_t remainingCharge_ccu; ///< Estimated remaining battery charge
    uint32_t fullChargeCapacity_ccu; ///< Estimated battery charge at max capacity
    uint32_t wasLastCycleCharge;
} stateVars;

#define NUM_STATE_VAR_WORDS (sizeof(stateVars)/sizeof(uint32_t))

/* These are Outputs in the sense of a Dynamical system- they are dependent only
 *  on the State vars.*/
static struct
{   
    uint32_t remainingCharge_dAh; ///< Estimated remaining battery charge, 0.1Ah
    uint32_t fullChargeCapacity_dAh; ///Estimated battery charge at max capacity, 0.1Ah
    
    
    uint16_t stateOfCharge_Prct_x10; ///<This is the magic number - the outward facing SoC, to one decimal point. [0-1000] 0.1%
    uint8_t stateOfCharge_Prct; ///< Outward facing SoC rounded to integer [0-100]%
    
    uint8_t relativeCapacity_Prct; ///< This is the Capacity State of Health (actual/design)
} outputVars;


static T_SOC_Config drivingConfigValues;
static bool skipLPF; ///< flag to skip the SoC low pass filter and snap to looked-up SoC on next run. self clears


static uint32_t chargeCounter; ///< A running charge counter, in CCU



static T_Timer tmrUpdate;


static uint32_t targetSoc_lkp;

static T_MemAddr vAddrStateVars; ///< NVRAM Address of State Var Data
static T_MemAddr vAddrDataKey; ///< NVRAM Addresss of GoodDataKey

static T_State currentState;


//############################# FUNCTIONS ######################################
static uint32_t convert_ccu__dAh(uint32_t ccu);
static void updateOutputs(void);
static void enforceRemainingChargeBounds(void);
static void crunchDependencies(void);

static void stepMode(T_Mode* pMode);
static void printMode(const T_Mode* pMode);
static const char* modeState2str(T_ModeState s);

static bool ccInCondition(void);
static void ccInContribute(void);

static bool ccOutCondition(void);
static void ccOutContribute(void);

static bool vltgLkpCondition(void);
static void vltgLkpContribute(void);

static void postSoc(void);

//################################# PUBLIC CODE ################################

void SOC_Init(T_CoreSignals* pCoreSigs, T_SOC_Config* pSocConfig)
{

    if (pCoreSigs)
    {
        coreSigs = *pCoreSigs;
    }

    if (pSocConfig)
    {
        drivingConfigValues = *pSocConfig;
    }
    
  
    skipLPF = false;

    chargeCounter = 0;
    
   
    //Note: Voltage Lookup starts in active state!
    mVltgLkp.currentState = MODE_ACTIVE;
    mVltgLkp.fCurrCondition = &vltgLkpCondition;
    mVltgLkp.fContribute = &vltgLkpContribute;
    mVltgLkp.activeTime_ms = drivingConfigValues.minTimeForActiveLkp_ms;

    mCcIn.currentState = MODE_INACTIVE;
    mCcIn.fCurrCondition = &ccInCondition;
    mCcIn.fContribute = &ccInContribute;
    mCcIn.activeTime_ms = drivingConfigValues.minTimeForActiveCcIn_ms;

    mCcOut.currentState = MODE_INACTIVE;
    mCcOut.fCurrCondition = &ccOutCondition;
    mCcOut.fContribute = &ccOutContribute;
    mCcOut.activeTime_ms = drivingConfigValues.minTimeForActiveCcOut_ms;

     //Take all the data from configValues, and fill rConfigValues as necessary
    crunchDependencies();

    stateVars.remainingCharge_ccu = derivedConfigValues.designCapacity_ccu;
    stateVars.fullChargeCapacity_ccu = derivedConfigValues.designCapacity_ccu;
    

    //Attempt to load good data key, and state vars
    vAddrDataKey = MEM_AllocateWord();
    vAddrStateVars = MEM_AllocateData(NUM_STATE_VAR_WORDS);
    
    
    //First, load the key to see if we have good data
    uint32_t keyValue = 0;
    MEM_LoadDefaultData("SocKey", vAddrDataKey, &keyValue, 1);
    //After we've read whatever was stored in NVRAM, zero out NVRAM.
    MEM_WriteWordFromRamToNvm(vAddrDataKey, 0x00);
    
    if (keyValue == SOC_NVRAM_GOOD_DATA_KEY)
    {
        
        //Load the rest of the data.
        if (!MEM_ReadDataFromNvmToRam(vAddrStateVars, (uint32_t*) &stateVars, NUM_STATE_VAR_WORDS))
        {
           puts("\tFailed to read saved SoC data in NVRAM, although it was expected.");
           
           currentState = ST_WAITING_FOR_BOOT_WITHOUT_SOC;
        }
        else
        {
           puts("\tSuccessfully read SoC data from NVRAM!");
           
           currentState = ST_WAITING_FOR_BOOT_WITH_SOC;
        }
    }
    else
    {
        puts("\tNo SoC data found in NVRAM!");   
        currentState = ST_WAITING_FOR_BOOT_WITHOUT_SOC;
    }
    
    updateOutputs();
           
    postSoc(); //post to SoC signal


}


bool SOC_Update()
{
    switch (currentState)
    {
        case ST_WAITING_FOR_BOOT_WITHOUT_SOC:
            if (SIG_IsReady())
            {
                //By default, we should assume last cycle was CHARGE to be
                //pessimistic about available discharge power.
                stateVars.wasLastCycleCharge = 1; 
                SOC_SnapToVoltageLookup();
                currentState = ST_RUNNING;
                TMR_StartRepeating(&tmrUpdate, drivingConfigValues.runPeriod_ms);
            }
        break;

        case ST_WAITING_FOR_BOOT_WITH_SOC:
            if (SIG_IsReady())
            {
                skipLPF = false;
                currentState = ST_RUNNING;
                TMR_StartRepeating(&tmrUpdate, drivingConfigValues.runPeriod_ms);
            }
        break;

        case ST_RUNNING:
            if (TMR_HasTripped_ms(&tmrUpdate))
            {
                stepMode(&mCcOut);
                stepMode(&mCcIn);

                if (mCcOut.currentState == MODE_ACTIVE)
                {
                    if (mCcIn.currentState == MODE_ACTIVE)
                    {
                        puts("SOC: Error! Both CcIn and CcOut are active.");
                    }
                    else
                    {
                        //CcOut Active, CcIn Inactive
                        stateVars.wasLastCycleCharge = false;
                    }
                }
                else
                {
                    if (mCcIn.currentState == MODE_ACTIVE)
                    {
                        // CcOut Inactive, CcIn Active
                        stateVars.wasLastCycleCharge = true;
                    }
                    else
                    {
                        //Both inactive - do nothing.
                    }

                }

                stepMode(&mVltgLkp);
                
                updateOutputs();
                postSoc();                
            }         
        break;
    }
   

    return true; //Always ready to SLEEP
}


#if 1

//Returns the remaining capacity as U32 in units of 100mAms.
//To calculate time remaining in minutes, do:
//               / chg * [100mAms] \     /   1 [min]    \        chg
//     t [min] =| ----------------- | * |  -----------   |  = -----------
//               \    i [mA]       /     \ 60*1000 [ms] /        i * 600

uint16_t SOC_GetInsTimeToFull_m()
{
    return 0xFFFF;
}

uint16_t SOC_GetInsTimeToEmpty_m()
{
    return 0xFFFF;
}

uint16_t SOC_GetAveTimeToFull_m()
{
    return 0xFFFF;
}

uint16_t SOC_GetAveTimeToEmpty_m()
{
    return 0xFFFF;
}

uint16_t SOC_GetTimeToEmptyAtRate_m(int16_t atRate)
{
#if 0
    if (atRate < 0)
    {
        return (remainingCharge_ccu/600/((uint16_t) -atRate));
    }
    else
    {
        return 0xFFFF;
    }
#endif
    (void) atRate;
    return 0xFFFF;
}

uint16_t SOC_GetTimeToFullAtRate_m(int16_t atRate)
{
#if 0
    if (atRate > 0)
    {
        return ((UINT32_MAX - remainingCharge_ccu)/600/((uint16_t) atRate));
    }
    else
    {
        return 0xFFFF;
    }
#endif
    (void) atRate;
    return 0xFFFF;
}
#endif

void SOC_ProcessInput(const char* tokens[], uint8_t nTokens)
{

    if (nTokens == 0 || tokens[0][0] == '?')
    {
      
      puts("\nconfig\tView/modify module configuration.");
      puts("now\tDisplay instantaneous stats.");
      puts("snap\tForce SoC to jump to lookup value.");
      puts("clear\tZero out the free running charge counter.");
      puts("count\tPrint free running charge counter.");
      puts("cell\tPrint cell type.");
      puts("mode\tPrint mode data.");
      puts("cycle <c/d>\tView/set the last cycle type.");
      puts("save-soc\tWrite current SoC to NVRAM.");
      puts("save-fcc\tWrite current FCC to NVRAM.");
      puts("set-soc [%]\tOverwrite the SoC.");
    }
    else if (stricmp(tokens[0], "now") == 0)
    {
        PrintAs_Scaled_U32(outputVars.stateOfCharge_Prct_x10, 1);
        SEND_TAB();
        PrintAs_Dec_U8(outputVars.stateOfCharge_Prct, 0);
        puts("%");
        
        PrintAs_Scaled_U32(outputVars.remainingCharge_dAh, 1);
        putstr("/");
        PrintAs_Scaled_U32(outputVars.fullChargeCapacity_dAh, 1);
        puts(" Ah");
        
        PrintAs_Hex_U32(stateVars.remainingCharge_ccu);
        putstr("/");
        PrintAs_Hex_U32(stateVars.fullChargeCapacity_ccu);
        puts(" ccu");
        
    }
    else if (stricmp(tokens[0], "snap") == 0)
    {
        SOC_SnapToVoltageLookup();
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
       MCU_HW_ClearWDT();
       if (CLI_ProcessSignedConfig(drivingConfigStrs, (uint32_t*) &drivingConfigValues,  NUM_DRIVING_CONFIG_VALUES, configSignBitmap, tokens, nTokens))
       {
           crunchDependencies();
       }

       MCU_HW_ClearWDT();
       puts("\r\nDerived:");
       CLI_ProcessConfig(derivedConfigStrs, (uint32_t*) &derivedConfigValues,  NUM_DERIVED_CONFIG_VALUES, tokens, 0);

    }
    else if (stricmp(tokens[0], "clear") == 0)
    {
        chargeCounter = 0;
    }
    else if (stricmp(tokens[0], "count")== 0)
    {
        printf("Count = 0x%08X\r\n", chargeCounter);
    }
    else if (stricmp(tokens[0], "mode") == 0)
    {
        printMode(&mCcIn);
        NEW_LINE();
        printMode(&mCcOut);
        NEW_LINE();
        printMode(&mVltgLkp);

    }
    else if (stricmp(tokens[0], "shutdown") == 0)
    {
        SOC_PrepareForShutdown();
    }
    else if (stricmp(tokens[0], "cycle") == 0)
    {
        if (nTokens > 1)
        {
            if (tokens[1][0] == 'c')
            {
                stateVars.wasLastCycleCharge = 1;
            }
            else if (tokens[1][0] == 'd')
            {
                stateVars.wasLastCycleCharge = 0;
            }
        }
        
        printf("Cycle = %s\r\n", stateVars.wasLastCycleCharge ? "Chg" : "Dis");            
    }
    else if (stricmp(tokens[0], "set-soc") == 0)
    {
        if (nTokens > 1)
        {
            uint8_t prct = atoi(tokens[1]);
            if (prct > 100)
            {
                prct = 100;
            }
            
            SOC_OverwriteRelativeStateOfCharge(prct);
            printf("Setting SoC to %d%%!\r\n", prct);
        }
        else
        {
            puts("set-soc [%]");
        }
        
        printf("Cycle = %s\r\n", stateVars.wasLastCycleCharge ? "Chg" : "Dis");            
    }
    else
    {
        putchar('?');
    }

}

void SOC_DrawInit()
{
     //Print title
    
    puts("\t\tSOC DEBUG\n");
    
    puts("Ready?");
    puts("SoC");
    puts("FCC\n");
            
    VT100_FONT_BOLD();
    putstr("TYPE");
    VT100_FONT_NORMAL();
    
    SEND_TAB();
    putstr(STR_mode);
    SEND_TAB();
    putstr(STR_count);
    SEND_TAB();
    putstr("dCCU");

    NEW_LINE();
    putstr("Lkp");
    NEW_LINE();
    putstr("ccIn");
    NEW_LINE();
    putstr("ccOut");

    LINE_FEED();
    NEW_LINE();
    putstr(STR_amps);
    NEW_LINE();
    putstr("Cycle");
    NEW_LINE();
    putstr("Trgt%");
    NEW_LINE();
    putstr("V_Min");
}


void SOC_DrawUpdate()
{
 
    printf("\n\n\t%c\r\n", currentState == ST_RUNNING ? 'Y' : 'N');
    
    VT100_CLEAR_LINE();
    SEND_TAB();
    PrintAs_Dec_U8(outputVars.stateOfCharge_Prct,0);
    putstr("%");
    SEND_TAB();
    PrintAs_Scaled_U32(outputVars.stateOfCharge_Prct_x10, 1);    
    putstr("%");
    SEND_TAB();
    PrintAs_Scaled_U32(outputVars.remainingCharge_dAh, 1);
    puts("Ah");
    SEND_TAB();
    PrintAs_Scaled_U32(outputVars.fullChargeCapacity_dAh, 1);
    puts("Ah\n\n\r");

    SEND_TAB();
    VT100_CLEAR_LINE();
    printMode(&mVltgLkp);

    NEW_LINE();
    SEND_TAB();
    VT100_CLEAR_LINE();
    printMode(&mCcIn);

    NEW_LINE();
    SEND_TAB();
    VT100_CLEAR_LINE();
    printMode(&mCcOut);


    LINE_FEED();
    NEW_LINE();
    SEND_TAB();
    VT100_CLEAR_LINE();
    PrintAs_Millis_I32_f2(SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_SUM));
    NEW_LINE();
    SEND_TAB();
    putstr(stateVars.wasLastCycleCharge ? "Chg" : "Dis");

    uint32_t V_min = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN);
    NEW_LINE();
    SEND_TAB();
    if (mVltgLkp.currentState == MODE_ACTIVE)
    {
        
        if ((V_min > drivingConfigValues.hiSocSnapThresh_mV) || (V_min < drivingConfigValues.loSocSnapThresh_mV))
        {
            putstr("Snp");
        }
        else
        {
            PrintAs_Scaled_U32(targetSoc_lkp, 1);
        }
    }
    else
    {
        puts("---");
    }

    NEW_LINE();
    SEND_TAB();
    PrintAs_Dec_U32(V_min, 4);

  puts("\n");
    CLI_DrawProgressBar();
}


uint8_t SOC_GetRelativeCapacity_Prct()
{
    return outputVars.relativeCapacity_Prct;
}

uint32_t SOC_GetCcuPerAh()
{
   return derivedConfigValues.Ah_to_ccu;
}

bool SOC_PrepareForShutdown()
{
    if (MEM_WriteDataFromRamToNvm(vAddrStateVars, (uint32_t*) &stateVars, NUM_STATE_VAR_WORDS))
    {
        
        if (MEM_WriteWordFromRamToNvm(vAddrDataKey, SOC_NVRAM_GOOD_DATA_KEY))
        {
            LOG_Event(LOG_LVL_INFO, "Wrote SoC data/key to NVRAM.");
        }
        else
        {
            LOG_Event(LOG_LVL_FATAL, "Wrote SoC data to NVRAM, but failed to write key.");
        }
        
        return true;
    }
    else
    {
        LOG_Event(LOG_LVL_FATAL, "Failed to write SoC data to NVRAM.");
    }
        
    return false;
}

void SOC_SnapToVoltageLookup()
{
    LOG_Event(LOG_LVL_INFO, "Snapping SoC to voltage lookup!");
    
    skipLPF = true;
    vltgLkpContribute();
    updateOutputs();
}

uint32_t SOC_GetFullChargeCapacity_dAh(void)
{
    return outputVars.fullChargeCapacity_dAh;
}

uint32_t SOC_GetRemainingCharge_dAh(void)
{
    return outputVars.remainingCharge_dAh;
}

void SOC_AdjustFullChargeCapacity_dAh(int32_t delta_dAh)
{
    
    uint32_t delta_dAh_u32 = (delta_dAh < 0) ? -delta_dAh : delta_dAh;
    uint32_t delta_ccu =  (((uint64_t) delta_dAh_u32)* derivedConfigValues.Ah_to_ccu + 5)/10;
    
    if (delta_dAh < 0)
    {
        //Going down...        
        if (stateVars.fullChargeCapacity_ccu > delta_ccu)
        {
            stateVars.fullChargeCapacity_ccu -= delta_ccu;
        }
        else
        {
            puts("SOC: Attempted to adjust FCC by more then FCC!\r\n");
        }           
    }
    else
    {
        //going up!
        uint32_t old = stateVars.fullChargeCapacity_ccu;
        
        stateVars.fullChargeCapacity_ccu += delta_ccu;
        if (old > stateVars.fullChargeCapacity_ccu)
        {
            puts("FCC overflowed!");
            stateVars.fullChargeCapacity_ccu = derivedConfigValues.designCapacity_ccu;
       }
        else if (stateVars.fullChargeCapacity_ccu > derivedConfigValues.designCapacity_ccu)
        {
            puts("Capping FCC at Design capacity.");
            stateVars.fullChargeCapacity_ccu = derivedConfigValues.designCapacity_ccu;
        }          
    }
}

void SOC_OverwriteRelativeStateOfCharge(uint8_t soc_prct)
{
    if (soc_prct > 100)
    {
        soc_prct = 100;
    }
    
    uint64_t num = ((uint64_t) soc_prct)*stateVars.fullChargeCapacity_ccu;
    stateVars.remainingCharge_ccu = num/100ULL;
    updateOutputs();
    postSoc();
}

//############################## HELPER FUNCTIONS ##############################

static uint32_t convert_ccu__dAh(uint32_t ccu)
{
    int64_t num = ccu*100ULL;
    
    return ((num/derivedConfigValues.Ah_to_ccu) + 5)/10;
}


static void updateOutputs()
{
    
    outputVars.fullChargeCapacity_dAh = convert_ccu__dAh(stateVars.fullChargeCapacity_ccu);
    outputVars.remainingCharge_dAh = convert_ccu__dAh(stateVars.remainingCharge_ccu);
    
    outputVars.relativeCapacity_Prct = (UTIL_CalculatePercent_x10(stateVars.fullChargeCapacity_ccu, derivedConfigValues.designCapacity_ccu) + 5)/10;
    
    
    /* The outward facing SoC should NEVER EVER EVER go above 100%. */
    if (stateVars.remainingCharge_ccu >= stateVars.fullChargeCapacity_ccu)
    {
        outputVars.stateOfCharge_Prct_x10 = 1000;
        outputVars.stateOfCharge_Prct = 100;
    }
    else
    {
        //For Relative SoC, calculate SoC relative to Full Charge Capacity
        //outputVars.stateOfCharge_Prct_x10 = UTIL_CalculatePercent_x10(stateVars.remainingCharge_ccu, stateVars.fullChargeCapacity_ccu);
        
        //For  Absolute SoC, calculate SoC relative to Design Capacity
        outputVars.stateOfCharge_Prct_x10 = UTIL_CalculatePercent_x10(stateVars.remainingCharge_ccu, derivedConfigValues.designCapacity_ccu);
        outputVars.stateOfCharge_Prct = (outputVars.stateOfCharge_Prct_x10 + 5)/10;
    }
           
    
}


static void stepMode(T_Mode* pMode)
{
    if (pMode)
    {
        /*
         * First, check if the "active condition" is met. If not, throw mode back
         * into INACTIVE state.
         */
        if (pMode->fCurrCondition == NULL || !(pMode->fCurrCondition()))
        {
            pMode->currentState = MODE_INACTIVE;
            
            //disable timer if running
            if (TMR_IsRunning(&(pMode->timer)))
            {
                TMR_Stop(&(pMode->timer));
            }
            
            return;
        }

        /*
         * Now, when we get to this switch, we can assume that conditions are
         * met for this mode to run.
         */
     switch (pMode->currentState)
        {
            case MODE_INACTIVE:
                TMR_StartOneshot(&(pMode->timer), pMode->activeTime_ms);
                pMode->currentState = MODE_STARTUP_WAITING;
            break;

            case MODE_STARTUP_WAITING:
                if (TMR_HasTripped_ms(&(pMode->timer)))
                {
                    pMode->currentState = MODE_ACTIVE;
                }
            break;

            case MODE_ACTIVE:
                if(pMode->fContribute != NULL)
                {
                    pMode->fContribute();
                }
            break;
        }

    }
}

static void crunchDependencies()
{
    if (drivingConfigValues.runPeriod_ms > 0)
    {

        derivedConfigValues.Ah_to_ccu = (1000ULL*3600ULL*1000ULL/drivingConfigValues.runPeriod_ms);
        derivedConfigValues.designCapacity_ccu = (drivingConfigValues.designCapacity_mAh * (derivedConfigValues.Ah_to_ccu/1000));

        derivedConfigValues.chargePerLookup_ccu = derivedConfigValues.designCapacity_ccu / 1000ULL; //.1%

        uint64_t imd = ((uint64_t) drivingConfigValues.maxSocDeltaPerMin_p01pct * (uint64_t) derivedConfigValues.designCapacity_ccu);
        derivedConfigValues.maxSocDeltaPerSec_ccu = imd/60ULL/100ULL/100ULL; //divide once to convert .01% to %, then again to take fraction of designCapacity
        
        imd = ((uint64_t)  derivedConfigValues.maxSocDeltaPerSec_ccu)*drivingConfigValues.runPeriod_ms;
        derivedConfigValues.maxSocDeltaPerLoop_ccu = imd/1000ULL;
        
        imd = ((uint64_t) drivingConfigValues.minSocDeltaPerLoop_p01pct) * derivedConfigValues.designCapacity_ccu;
        derivedConfigValues.minSocDeltaPerLoop_ccu = imd/100ULL/100ULL;

    }
}

static const char* modeState2str(T_ModeState s)
{
    switch (s)
    {
        case MODE_INACTIVE:         return "Off ";
        case MODE_STARTUP_WAITING:  return "Wait";
        case MODE_ACTIVE:           return "Actv";
    }

    return "???";
}


static void printMode(const T_Mode* pMode)
{

    putstr(modeState2str(pMode->currentState));
    SEND_TAB();
    switch (pMode->currentState)
    {
        case MODE_INACTIVE:
        	putstr("-------");
        break;

        case MODE_STARTUP_WAITING:
        	putstr("Waiting");
        break;

        case MODE_ACTIVE:
        	putstr("Active!");
        break;
    }
}

/**
* Convert the latest charge to SoC, and post to Sig.
*/
static void postSoc()
{
    T_SignalDatum datum = {.u32 = outputVars.stateOfCharge_Prct};
    SIG_WriteSignal(coreSigs.nSoc, datum);
}

static void enforceRemainingChargeBounds()
{
    if (stateVars.remainingCharge_ccu > derivedConfigValues.designCapacity_ccu)
    {
        stateVars.remainingCharge_ccu = derivedConfigValues.designCapacity_ccu;
    }
}

//### SOC MODE FUNCTIONS #######################################################

//### Coulomb Counting: IN (+)

static bool ccInCondition(void)
{
    if (coreSigs.nCurrRoot)
    {
        return (SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_SUM) > drivingConfigValues.ccCurrThreshP_mA);
    }

    return false;
}

static void ccInContribute(void)
{
    if (!coreSigs.nVltgRoot || !coreSigs.nCurrRoot)
    {
        return;
    }

    if (SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN) < drivingConfigValues.loSocSnapThresh_mV)
    {
        stateVars.remainingCharge_ccu = 0;
    }
    else
    {
        uint32_t curr_mA_u32 = (uint32_t)  SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_SUM);
        stateVars.remainingCharge_ccu += curr_mA_u32; //curr_mA is guaranteed to be > 0

        enforceRemainingChargeBounds();

        chargeCounter+= curr_mA_u32; //increment the free running counter

#ifndef SOC_DISABLE_ACC_INTERFACE
        ACC_AddDeltaTo(ACC_IN, curr_mA_u32);
#endif
    }
}

//### Coulomb Counting: OUT (-)
static bool ccOutCondition(void)
{
    if (coreSigs.nCurrRoot)
    {
        return (SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_SUM) < drivingConfigValues.ccCurrThreshN_mA);
    }

    return false;
}

static void ccOutContribute(void)
{
    if (!coreSigs.nVltgRoot || !coreSigs.nCurrRoot)
    {
        return;
    }

    if (SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN) > drivingConfigValues.hiSocSnapThresh_mV)
    {
       stateVars.remainingCharge_ccu = derivedConfigValues.designCapacity_ccu;
    }
    else
    {
        uint32_t curr_u32 = (uint32_t) (-1 * SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_SUM));
        
        if (stateVars.remainingCharge_ccu < curr_u32)
        {
            stateVars.remainingCharge_ccu = 0;
        }
        else
        {
            stateVars.remainingCharge_ccu -= curr_u32;
        }
        
        chargeCounter-= curr_u32; //increment the free running counter

#ifndef SOC_DISABLE_ACC_INTERFACE
        ACC_AddDeltaTo(ACC_OUT, curr_u32);
#endif
    }
}


//###  Voltage Lookup
static bool vltgLkpCondition(void)
{
    if (coreSigs.nCurrRoot)
    {
        int32_t curr_mA =  SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_SUM);
        return (ABS(curr_mA) < drivingConfigValues.absSocLookupCurr_mA);
    }

    return false;
}

static void vltgLkpContribute(void)
{
    if (!coreSigs.nVltgRoot || !coreSigs.nCurrRoot)
    {
        return;
    }

    uint32_t V_min = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN);

    if (V_min > drivingConfigValues.hiSocSnapThresh_mV)
    {
        //snap to 100% of Actual Capacity
        stateVars.remainingCharge_ccu = stateVars.fullChargeCapacity_ccu;
    }
    else if (V_min < drivingConfigValues.loSocSnapThresh_mV)
    {
        //snap to 0%
        stateVars.remainingCharge_ccu = 0;
    }
    else
    {
        //If no snap, then lookup.
        int32_t soc_lkp_i32 =  (stateVars.wasLastCycleCharge ? CELL_LookupChargeSocFromVltg_Prct_x10(V_min) : CELL_LookupDischargeSocFromVltg_Prct_x10(V_min)) ;
        
        if (soc_lkp_i32 < 0)
        {
            soc_lkp_i32 = 0;
        }
       
        targetSoc_lkp = (uint32_t) soc_lkp_i32;
        
        uint32_t targetCharge_ccu  = targetSoc_lkp*derivedConfigValues.chargePerLookup_ccu;

         uint32_t u32;
         if (skipLPF)
         {
            stateVars.remainingCharge_ccu = targetCharge_ccu;
            enforceRemainingChargeBounds();
            
            skipLPF = false;
         }
         else if (targetCharge_ccu > stateVars.remainingCharge_ccu) //we should increase SoC
         {
            u32 = targetCharge_ccu - stateVars.remainingCharge_ccu;

            if (u32 >= derivedConfigValues.minSocDeltaPerLoop_ccu)
            {
                if (u32 > derivedConfigValues.maxSocDeltaPerLoop_ccu)
                {
                    u32 = derivedConfigValues.maxSocDeltaPerLoop_ccu;
                }
                /* We can guarantee that maxSoCDeltaPerLoop + max possible charge will not cause overflow */
                stateVars.remainingCharge_ccu += u32;
                enforceRemainingChargeBounds();
                
#ifndef SOC_DISABLE_ACC_INTERFACE
                ACC_AddDeltaTo(ACC_IN, u32);
#endif
            }
            else
            {
                //Below threshold - too similar. Do nothing.
            }

        }
         else if (targetCharge_ccu < stateVars.remainingCharge_ccu) //We should *decrease* SoC
        {
            u32 = stateVars.remainingCharge_ccu - targetCharge_ccu;
            if (u32 >= derivedConfigValues.minSocDeltaPerLoop_ccu)
            {
                if (u32 > derivedConfigValues.maxSocDeltaPerLoop_ccu)
                {
                    u32 = derivedConfigValues.maxSocDeltaPerLoop_ccu;
                }

                if (u32 > stateVars.remainingCharge_ccu)
                {
                    stateVars.remainingCharge_ccu = 0;
                }
                else
                {
                    stateVars.remainingCharge_ccu -= u32;
                }
                
#ifndef SOC_DISABLE_ACC_INTERFACE
                ACC_AddDeltaTo(ACC_OUT, u32);
#endif
            }
            else
            {
                //Below threshold - too similar. Do nothing.
            }
        }
        else
        {
            //The SoC is exactly where is should be.
        }
    }
    
    skipLPF = 0; //Clear lingering flag, if it wasn't handled here (i.e. we snapped to 100% or 0%)
}
