/**
 * @file htr.c
 * @brief NOAA Heater controller logic.
 * @copyright 20146 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-07-01
 *
**/
//############################## INCLUDES ######################################

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include <bluflex_core.h>
#include <htr.h>

//<SRB>### MAY NOT BE NEEDED ####>  #include "noaa_v5_0.h"


//############################## DEFINES ######################################

typedef enum
{
    ST_PINIT = 0,
    ST_WAITING_FOR_SOC_CALCULATION,
    ST_FULL_SOC,
    ST_HIGH_SOC,
    ST_NORMAL_SOC,
    ST_LOW_SOC, 
    ST_DEAD_SOC,
    ST_MASTER_DISBALE,      
    ST_ERROR,
} T_State;

typedef enum
{
    ST_THRM_NOT_RUNNING = 0,
    ST_THRM_HEATERS_ON,
    ST_THRM_HEATERS_OFF,
} T_Thermostat_State;

//############################## TYPES/VARS ####################################

static const uint32_t configSignMask = 0b11111110000;
static struct
{
       uint32_t full_high_soc; ///<
       uint32_t high_normal_soc; ///
       uint32_t normal_low_soc;///
       uint32_t low_dead_soc;
       
       int32_t fullTarget_dC;
       int32_t highTarget_dC;
       int32_t continuousHeatTarget_dC; ///< This is target when we want to continuously heat, but dont' want to actually set thermostat to +Inf for safety reasons
       
       int32_t normal_battEnableTemp_dC;
       int32_t low_battEnableTemp_dC;
       
       int32_t tempTargetHystHi_dC;
       int32_t tempTargetHystLo_dC;
       
       uint32_t useFakeInputs;
               
} configValues = 
{
    .full_high_soc = 97,
    .high_normal_soc = 90,
    .normal_low_soc = 25,
    .low_dead_soc = 10,
    
    .fullTarget_dC = 10,
    .highTarget_dC = 5,
    .continuousHeatTarget_dC = 20,
    
    .normal_battEnableTemp_dC = 0,
    .low_battEnableTemp_dC = -5,
    
    .tempTargetHystHi_dC = 2,
    .tempTargetHystLo_dC = 0,
    
    .useFakeInputs = 0,
};

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "Full-High_%",
    "High-Normal_%",
    "Normal-Low_%",
    "Low-Dead_%",
    
    "FullTarget_dC",
    "HighTarget_dC",
    "ContHeatTarget_dC",
            
    "NormalSocBattEnable_dC",
    "LowSocBattEnable_dC",
        
    "TempTargetHystHi_dC",
    "TempTargetHystLo_dC",
    
    "UseFakeInputs?",
};

//######################### MODULE VARIABLES ###################################

static T_State currentState;
static T_HTR_Config htrConfig;
static T_CoreSignals coreSigs;
static T_Timer tmr;
static struct
{
    int8_t target_dC;
    bool isRunning;
    T_Thermostat_State currentState;
    int8_t upper_dC;
    int8_t lower_dC;
    bool heatNow;
} thermostat;

static struct
{
    uint8_t soc;
    int8_t minTemp;   
} fakeInputs;
//######################### MODULE FUNCTIONS ###################################
static void thrermostat_SetTarget(int8_t targetTemp_dC);
static void thermostat_StartRunning(void);
static void thermostat_StopRunning(void);
static void thermostat_Update(void);
static const char* thrmState2str(T_Thermostat_State s);

static void changeStateTo(T_State);
static const char* state2str(T_State);


static int8_t getMinTemp_dC(void);
static uint8_t getSoc(void);
static void updateHeaters(void);

//########################### PUBLIC CODE ######################################

void HTR_Init(T_HTR_Config* pConfig, T_CoreSignals* pCoreSigs)
{
  
    if (pConfig)
    {
        htrConfig = *pConfig;
    }
    
    if (pCoreSigs)
    {
        coreSigs = *pCoreSigs;
    }
    
    RLY_RequestAction(htrConfig.htrA_Relay, 0);
    RLY_RequestAction(htrConfig.htrB_Relay, 0);
    
    RLY_RequestAction(htrConfig.hbe_Relay, 0);
    
    currentState = ST_PINIT;  
    
    thermostat.target_dC = 0;
    thermostat.isRunning = 0;
    thermostat.currentState = ST_THRM_NOT_RUNNING;
    
    fakeInputs.minTemp = 20;
    fakeInputs.soc = 50;
}

static void routeState(uint8_t soc)
{
    if (soc >= configValues.full_high_soc)
    {
        changeStateTo(ST_FULL_SOC);
    }
    else if (soc >= configValues.high_normal_soc)
    {
        changeStateTo(ST_HIGH_SOC);
    }
    else if (soc >= configValues.normal_low_soc)
    {
        changeStateTo(ST_NORMAL_SOC);
    }
    else if (soc >= configValues.low_dead_soc)
    {
        changeStateTo(ST_LOW_SOC);
    }
    else
    {
        changeStateTo(ST_DEAD_SOC);
    }
      
}

bool HTR_Update(void)
{
    
    /*If we don't have Heater Hardware, just skip.*/
    if (!htrConfig.heaterHardwareExpected)
    {
        return true;
    }

    
    uint8_t soc = getSoc();
    int8_t minTemp = getMinTemp_dC();
    
    T_AlarmLevel mstrNow = SIG_GetAlarmNow(coreSigs.nRoot);
    if (mstrNow == ALARM_LVL_STOP)
    {
        if (currentState != ST_MASTER_DISBALE)
        {
            changeStateTo(ST_MASTER_DISBALE);
        }
    }
    
    switch (currentState)
    {
        case ST_PINIT: 
            if (SIG_IsReady())
            {
                TMR_StartOneshot(&tmr, 500);
                changeStateTo(ST_WAITING_FOR_SOC_CALCULATION);
            }
        break;
        
        case ST_WAITING_FOR_SOC_CALCULATION:
            if (TMR_HasTripped_ms(&tmr))
            {
                routeState(soc); //go to proper state depending on SoC/Temp
            }
        break;
        
        case ST_FULL_SOC:
            if (soc < configValues.full_high_soc)
            {
                changeStateTo(ST_HIGH_SOC);
            }
        break;
        
        case ST_HIGH_SOC:
            if (soc >= configValues.full_high_soc)
            {
                changeStateTo(ST_FULL_SOC);
            }
            else if (soc < configValues.high_normal_soc)
            {
                changeStateTo(ST_NORMAL_SOC);
            }
        break;
        
        case ST_NORMAL_SOC:
            if (soc >= configValues.high_normal_soc)
            {
                changeStateTo(ST_HIGH_SOC);
            }
            else if (soc < configValues.normal_low_soc)
            {
                changeStateTo(ST_LOW_SOC);
            }
            else   
            {
            	bool isChargeRelayClosed = RLY_IsClosed(htrConfig.chg_Relay);

                //We don't want to heat at all if charging is allowed!                
            	isChargeRelayClosed ? thermostat_StopRunning() : thermostat_StartRunning();
                 
				if (isChargeRelayClosed)
				{
					RLY_RequestAction(htrConfig.hbe_Relay, true);
				}
				else
				{
					// In this state, allow heating from battery iff T < 0.
					RLY_RequestAction(htrConfig.hbe_Relay, (minTemp < configValues.normal_battEnableTemp_dC));
				}
            }
        break;
        
        case ST_LOW_SOC:
            if (soc >= configValues.normal_low_soc)
            {
                changeStateTo(ST_NORMAL_SOC);
            }
            else if (soc < configValues.low_dead_soc)
            {
                changeStateTo(ST_DEAD_SOC);
            }
            else   
            {
            	bool isChargeRelayClosed = RLY_IsClosed(htrConfig.chg_Relay);

				//We don't want to heat at all if charging is allowed!
				isChargeRelayClosed ? thermostat_StopRunning() : thermostat_StartRunning();

				if (isChargeRelayClosed)
				{
					RLY_RequestAction(htrConfig.hbe_Relay, true);
				}
				else
				{
					// In this state, allow heating from battery iff T < -5.
					RLY_RequestAction(htrConfig.hbe_Relay, (minTemp < configValues.low_battEnableTemp_dC));
				}

            }
        break;
        
        case ST_DEAD_SOC:
            if (soc >= configValues.low_dead_soc)
            {
                changeStateTo(ST_LOW_SOC);
            }
            else   
            {
            	bool isChargeRelayClosed = RLY_IsClosed(htrConfig.chg_Relay);

				//We don't want to heat at all if charging is allowed!
				isChargeRelayClosed ? thermostat_StopRunning() : thermostat_StartRunning();

				if (isChargeRelayClosed)
				{
					RLY_RequestAction(htrConfig.hbe_Relay, true);
				}
				else
				{
					RLY_RequestAction(htrConfig.hbe_Relay, false);
				}
                 
                 //We never want to heat from battery in this state (HBE = 0).
            }
        break;
        
        case ST_MASTER_DISBALE:
            if (mstrNow != ALARM_LVL_STOP)
            {
                routeState(soc);
            }
        break;
        
        case ST_ERROR:
            //do nothing...
        break;
        
        default:
            printf("HTR: Unknown state %u\r\n", currentState);
            currentState = ST_ERROR;
        break;
    }
    
   
    thermostat_Update();

    return true; //Sleep always OK

  
}


void changeStateTo(T_State newState)
{
    //Entry event
    switch (newState)
    {
        case ST_PINIT: 
        
        break;   
        
        case ST_FULL_SOC:
            thrermostat_SetTarget(configValues.fullTarget_dC);
            RLY_RequestAction(htrConfig.hbe_Relay, true);
        break;
        
        case ST_HIGH_SOC:
            thrermostat_SetTarget(configValues.highTarget_dC);
            RLY_RequestAction(htrConfig.hbe_Relay, true);
        break;       
        
        case ST_NORMAL_SOC: 
            thrermostat_SetTarget(configValues.continuousHeatTarget_dC + 2); //make set temp slightly different to differentiate between states for debugging
            RLY_RequestAction(htrConfig.hbe_Relay, false);//this state may change once state runs
        break;
        
        case ST_LOW_SOC:
            thrermostat_SetTarget(configValues.continuousHeatTarget_dC + 1); //make set temp slightly different to differentiate between states for debugging
            RLY_RequestAction(htrConfig.hbe_Relay, false); //this state may change once state runs
        break;
        
        case ST_DEAD_SOC: 
            thrermostat_SetTarget(configValues.continuousHeatTarget_dC);
            RLY_RequestAction(htrConfig.hbe_Relay, false);  
        break;
 
        case ST_MASTER_DISBALE:
        case ST_ERROR:
            RLY_RequestAction(htrConfig.hbe_Relay, false); 
            RLY_RequestAction(htrConfig.htrA_Relay, false); 
            RLY_RequestAction(htrConfig.htrB_Relay, false);
            thermostat_StopRunning();
        break;
    }
    
    printf("HTR: \"%s\" --> \"%s\"\r\n", state2str(currentState), state2str(newState));
    currentState = newState;
}
  

static const char* state2str(T_State state)
{
    switch (state)
    {
        case ST_PINIT: return "Pinit";
        case ST_WAITING_FOR_SOC_CALCULATION: return "Wait4Soc";
        case ST_DEAD_SOC: return "Dead";
        case ST_LOW_SOC: return "Low";
        case ST_NORMAL_SOC: return "Normal";
        case ST_HIGH_SOC: return "High";
        case ST_FULL_SOC: return "Full";
        case ST_MASTER_DISBALE: return "Disbld";
        case ST_ERROR: return "Err";
        default: return "???";
    }
}


/*
 *  HBE | Charge | Htrs On? | Result
 * -----+--------+----------+-------
 *   X  |    X   |    0     |  No heating
 *   0  |    0   |    1     |  Heating only from solar
 *   1  |    0   |    1     |  Heat from any source, no charging
 *   X  |    1   |    1     |  Heat from any source, charging OK
 */

const char* getHwStateStr(void)
{
    bool heatersOn = RLY_IsClosed(htrConfig.htrA_Relay) || RLY_IsClosed(htrConfig.htrB_Relay);
    const char* toRet = "???";
    if (!heatersOn)
    {
        toRet =  "NoHeatng";
    }
    else if (RLY_IsClosed(htrConfig.chg_Relay))
    {
        toRet =  "AnySrc,ChrgOk";
    }
    else if (RLY_IsClosed(htrConfig.hbe_Relay))
    {
        toRet = "AnySrc,NoChrg";    
    }   
    else
    {
        toRet =  "OnlySolar";
    }
               
    return toRet;
            
}
//Handle command line inputs that had first token 'relay'
void HTR_ProcessInput(const char *tokens[], uint8_t nTokens)
{
    char c;
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("config\tView/adjust module configuration.");
        puts("info\tPrint state.");
        puts("fake-temp <dC>\tFake temperature input");
        puts("fake-soc <%>\tFake SoC input");
        
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
        CLI_ProcessSignedConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, configSignMask, tokens, nTokens);
    }
    else if (stricmp(tokens[0], "info") == 0)
    {
        printf("HW expected? %c\r\n", htrConfig.heaterHardwareExpected ? 'Y' : 'N');
        printf("State = \"%s\"\r\n", state2str(currentState));
        printf("Themrostat: %s (%d, %d, %d)dC\r\n", thrmState2str(thermostat.currentState), thermostat.target_dC, thermostat.upper_dC, thermostat.lower_dC);
        printf("HW: %s\r\n", getHwStateStr());
    }
    else if (stricmp(tokens[0], "fake-temp") == 0)
    {
        if (nTokens == 2)
        {
            int32_t i32 = atoi(tokens[1]);
            fakeInputs.minTemp = (int8_t) i32;
            printf("MinTemp <-- %dC\r\n", fakeInputs.minTemp);
        }
        else
        {
            puts("fake-temp <C>");
        }
    }
    else if (stricmp(tokens[0], "fake-soc") == 0)
    {
        if (nTokens == 2)
        {
            int32_t i32 = atoi(tokens[1]);
            fakeInputs.soc = (uint8_t) i32;
            printf("SoC <-- %u%%\r\n", fakeInputs.soc);
        }
        else
        {
            puts("fake-soc <%>");
        }
    }
    else
    {
        //Print error string.
        putchar('?');
    }
}


//###################### THERMOSTAT ############################################
// <editor-fold defaultstate="collapsed" desc="Thermostat">

static int8_t getMinTemp_dC()
{
    return (configValues.useFakeInputs ? fakeInputs.minTemp : SIG_GetData_I32(htrConfig.nCellTempRoot, SIG_RQST_MIN));
}

static uint8_t getSoc()
{
    return (configValues.useFakeInputs ? fakeInputs.soc : SIG_GetAvg_U32(coreSigs.nSoc));
}

static const char* thrmState2str(T_Thermostat_State s)
{
    switch (s)
    {
        case ST_THRM_NOT_RUNNING: return "NotRunning";
        case ST_THRM_HEATERS_ON: return "TooCold";
        case ST_THRM_HEATERS_OFF: return "WarmEnough";
    }
    
    return "???";
}

static void thermostat_StartRunning()
{
    thermostat.isRunning = 1;
}

static void thermostat_StopRunning()
{
    thermostat.isRunning = 0;
}

static void thrermostat_SetTarget(int8_t t_dC)
{
    thermostat.target_dC = t_dC;
    thermostat.isRunning = 1;
}


static void thermostat_Update(void)
{
    static T_Thermostat_State lastState = 0;
    
    int8_t temp_dC = getMinTemp_dC();
    
    
    thermostat.upper_dC = thermostat.target_dC  + (int8_t) configValues.tempTargetHystHi_dC;
    thermostat.lower_dC = thermostat.target_dC - (int8_t) configValues.tempTargetHystLo_dC;
    
    
    switch (thermostat.currentState)
    {
        case ST_THRM_NOT_RUNNING:
            if (thermostat.isRunning)
            {
                if (temp_dC < thermostat.target_dC)
                {
                    thermostat.currentState = ST_THRM_HEATERS_ON;
                    thermostat.heatNow = 1;
                }
                else
                {
                    thermostat.heatNow = 0;
                    thermostat.currentState = ST_THRM_HEATERS_OFF;
                }
            }
        break;
        
        case ST_THRM_HEATERS_ON:
            if (thermostat.isRunning == 0)
            {
                thermostat.heatNow = 0;
                thermostat.currentState = ST_THRM_NOT_RUNNING;
            }
            else if (temp_dC > thermostat.upper_dC )
            {
                //no more heating needed!
                thermostat.heatNow = 0;
                thermostat.currentState = ST_THRM_HEATERS_OFF;
            }
        break;
        
        case ST_THRM_HEATERS_OFF:
            if (thermostat.isRunning == 0)
            {
                thermostat.heatNow = 0;
                thermostat.currentState = ST_THRM_NOT_RUNNING;
            }
            else if (temp_dC < thermostat.lower_dC )
            {
                //heating needed!
                thermostat.heatNow = 1;
                thermostat.currentState = ST_THRM_HEATERS_ON;
            }
        break;
    }
    
    if (thermostat.currentState != lastState)
    {
        //printf("THRM: \"%s\" --> \"%s\"\r\n", thrmState2str(lastState), thrmState2str(thermostat.currentState));
        lastState = thermostat.currentState;
    } 
       
    updateHeaters();
       
}
// </editor-fold>

//####################### HEATER OUTPUT CONTROLLER #############################
// <editor-fold defaultstate="collapsed" desc="Heater Output Controller">

/**
 * This is the final stage of the heater controller. If ON, heater A and/or B
 * is turned on depending on battery voltage. If OFF, neither is on. The Temp
 * Controller own this function. 
 * @param s
 */
enum
{
    HTR_BOTH_OFF = 0,
    HTR_BOTH_ON,
    HTR_ONLY_A,
    HTR_ONLY_B
};




static void updateHeaters()
{
    uint32_t packVltg_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_SUM);
    
    static uint8_t lastHtrState = 0xFF;
    
    uint8_t thisHtrState = 0;    
    if (thermostat.heatNow)
    {
        if (packVltg_mV > 10e3)
        {
            thisHtrState = HTR_ONLY_A;
        }
        else if (packVltg_mV > 9e3)
        {
            thisHtrState = HTR_ONLY_B;
        }
        else
        {
            thisHtrState = HTR_BOTH_ON;
        }
    }
    else
    {
        thisHtrState = HTR_BOTH_OFF;
    }
           
        
    if (thisHtrState != lastHtrState)
    {
        switch (thisHtrState)
        {
            case HTR_BOTH_OFF:
                RLY_RequestAction(htrConfig.htrA_Relay, RLY_RQST_OPEN);
                RLY_RequestAction(htrConfig.htrB_Relay, RLY_RQST_OPEN);
            break;

            case HTR_BOTH_ON:
                RLY_RequestAction(htrConfig.htrA_Relay, RLY_RQST_CLOSE);
                RLY_RequestAction(htrConfig.htrB_Relay, RLY_RQST_CLOSE);
            break;

            case HTR_ONLY_A:
                RLY_RequestAction(htrConfig.htrA_Relay, RLY_RQST_CLOSE);
                RLY_RequestAction(htrConfig.htrB_Relay, RLY_RQST_OPEN);
            break;

            case HTR_ONLY_B:
                RLY_RequestAction(htrConfig.htrA_Relay, RLY_RQST_OPEN);
                RLY_RequestAction(htrConfig.htrB_Relay, RLY_RQST_CLOSE);
            break;
        }
        
        lastHtrState = thisHtrState;
    }
}

// </editor-fold>
//##################### DRAWING FUNCTIONS ######################################

void HTR_DrawInit()
{
    puts("\t\tHEATER DEBUG\n");
    
    VT100_PrintBold("STATS\t");
    puts("Temp\tSoC\tPack\n\n");

    VT100_PrintBold("RELAYS\t");
    puts("HBE\tChg\tHtrA\tHtrB\n\n");
    
    
    puts("HwState:\r\nHeater:\r\n");
    
    VT100_PrintBold("THERMOSTAT\r\n");
    puts("State:\r\nTarget:\r\nHeating?\r\n");

    
}

void HTR_DrawUpdate()
{
	char prtBfr[30] = {0};

	// STATS
    VT100_PrintClearLinePrint("\n\n\n\t", NULL);
//    printf("%dC\t%u%%\t", getMinTemp_dC(), getSoc());
    sprintf(prtBfr,"%dC\t%u%%\t", getMinTemp_dC(), getSoc());
    putstr(prtBfr);
    PrintAs_Millis_U32_f2(SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_SUM));
    
    // RELAYS
    putstr("V\r\n\n\n\t");
    VT100_CLEAR_LINE();
    sprintf(prtBfr,"%c\t%c\t%c\t%c\r\n",
           RLY_IsClosed(htrConfig.hbe_Relay)  ? 'X' : 'O',
           RLY_IsClosed(htrConfig.chg_Relay)  ? 'X' : 'O',
           RLY_IsClosed(htrConfig.htrA_Relay) ? 'X' : 'O',
           RLY_IsClosed(htrConfig.htrB_Relay) ? 'X' : 'O');
    putstr(prtBfr);
    
    VT100_PrintClearLinePrint("\n\t ", getHwStateStr());
    VT100_PrintClearLinePrint("\r\n\t ", state2str(currentState));
    VT100_PrintClearLinePrint("\r\n\n\n\t ", thrmState2str(thermostat.currentState));
    
    // THERMOSTAT
    VT100_PrintClearLinePrint("\r\n\t ", NULL);
    sprintf(prtBfr,"%dC\r\n\t ", thermostat.target_dC);
    putstr(prtBfr);
    
    VT100_PrintClearLinePrint(NULL, thermostat.heatNow ? "Yes" : "No");
    NEW_LINE();
}

void HTR_DrawInput(uint8_t c)
{
    switch (c)
    {
        case 'w':
            if (fakeInputs.soc < 100)
            {
                fakeInputs.soc++;
            }
        break;
        
        case 's':
            if (fakeInputs.soc > 0)
            {
                fakeInputs.soc--;
            }
        break;
        
        case 'i':
            fakeInputs.minTemp++;
        break;
        
        case 'k':
            fakeInputs.minTemp--;
        break;
        
        case 'c':
            RLY_RequestAction(htrConfig.chg_Relay, true);        
        break;
        
        case 'v':
            RLY_RequestAction(htrConfig.chg_Relay, false);        
        break;
            
        case '?':
            puts("[w] fakeSoc++\t[s] fakeSoc--");
            puts("[i] fakeTemp++\t[k] fakeTemp--");
            puts("[c] Close chg\t[v] Open chg");
        break;
        
        default:
        
        break;
    }
           
}
