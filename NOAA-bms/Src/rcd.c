/**
 * @file rcd.c
 * @brief Relay controller logic for Charge/Discharge relays.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-02
 *
**/
//############################## INCLUDES ######################################

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <ctype.h>
#include <bluflex_core.h>

#include <rcd.h>


//############################## DEFINES ######################################
#define MAX_NUM_CONTROLLERS (2)

typedef enum
{
    ST_OPEN,
    ST_CLOSED,
    ST_OPEN_COOLOFF,
    ST_OPEN_OVERRIDE,
    ST_CLOSED_OVERRIDE,
            
} T_State;


//############################## TYPES/VARS ####################################
enum
{
    OVR_RQST_NONE = 0,
    OVR_RQST_OPEN, 
    OVR_RQST_CLOSE,
    OVR_RQST_EXIT,
};



//Charge/Discharge
struct xRlyController
{
    const char* name;
    T_State currentState;
    T_Relay* pRelay;
    T_Timer tmr;
    
    T_fCheck fCheckStop;
    T_fCheck fCellVltgDisable;
    T_fCheck fCellVltgReenable;
    T_fCheck fTempDisable;
    T_fCheck fTempReenable;

    uint8_t ovrRequest;
};


static T_RCD_Controller controllerList[MAX_NUM_CONTROLLERS];
static uint8_t numControllers = 0;

static struct
{
       uint32_t minRelayOpenTime_ms; ///< Minimum time a relay must stay open after its been opened
       uint32_t overrideTimeout_ms; ///< How long a relay override request lasts for
} configValues = 
{
    .minRelayOpenTime_ms = 10000,
    .overrideTimeout_ms = 5000,
};

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "minRlyOpenTime_ms",
    "overrideTime_ms"
};

static void changeStateTo(T_State newState, T_RCD_Controller* obj);
static const char* state2str(T_State state);
//########################### CHARGE/DISCHARGE LOGIC ###########################


T_RCD_Controller* RCD_Create(const char* name, T_Relay* pRly, T_fCheck fStop, T_fCheck fCvDisable, T_fCheck fCvReenable, T_fCheck fTempDisable, T_fCheck fTempReenable)
{
    T_RCD_Controller* obj = NULL;
    
    if (numControllers < MAX_NUM_CONTROLLERS)
    {
        obj = &controllerList[numControllers];
        numControllers++;
        
        obj->name = name;
        obj->pRelay = pRly;
        obj->fCheckStop = fStop;
        obj->fCellVltgDisable = fCvDisable;
        obj->fCellVltgReenable = fCvReenable;
        obj->fTempDisable = fTempDisable;
        obj->fTempReenable = fTempReenable;
        
        obj->currentState = ST_OPEN;
        RLY_RequestAction(pRly, 0);
    }
    else
    {
        puts("RCD: Out of controller room!");
    }
    
    return obj;
}

void RCD_Update(T_RCD_Controller *obj)
{
    if (!obj)
    {
        puts("RCD update called on null obj!");
    }
    
    //Check for override events outside of normal SM.
    if (obj->ovrRequest == OVR_RQST_CLOSE)
    {
        changeStateTo(ST_CLOSED_OVERRIDE, obj);
        obj->ovrRequest = OVR_RQST_NONE;
    }
    else if (obj->ovrRequest == OVR_RQST_OPEN)
    {
        changeStateTo(ST_OPEN_OVERRIDE, obj);
        obj->ovrRequest = OVR_RQST_NONE;
    }
    
    
    
    switch (obj->currentState)
    {
        case ST_OPEN:
            if (obj->fCheckStop())
            {
                //printf("RCD: %s still has errors. Restarting timeout.\r\n", obj->name);
                changeStateTo(ST_OPEN_COOLOFF, obj);
            }
            else if (obj->fCellVltgReenable()  && obj->fTempReenable() )
            {
                printf("RCD: %s conditions ok - closing.\r\n", obj->name);
                changeStateTo(ST_CLOSED, obj);
            }
        break;
        
        case ST_CLOSED:
            if (obj->fCheckStop())
            {
                printf("RCD: %s has errors - opening.\r\n", obj->name);
                changeStateTo(ST_OPEN_COOLOFF, obj);
            }
            else if (obj->fCellVltgDisable())
            {
                printf("RCD: %s opening (cell voltage).\r\n", obj->name);
                changeStateTo(ST_OPEN_COOLOFF, obj);
            }
            else if (obj->fTempDisable())
            {
                printf("RCD: %s opening (temperature).\r\n", obj->name);
                changeStateTo(ST_OPEN_COOLOFF, obj);
            }
                   
        break;
        
        case ST_OPEN_COOLOFF:
            if (TMR_HasTripped_ms(&(obj->tmr)) || (obj->ovrRequest == OVR_RQST_EXIT))
            {
                obj->ovrRequest = OVR_RQST_NONE;
                changeStateTo(ST_OPEN, obj);
                //printf("RCD: %s cooloff done.\r\n", obj->name);
            }
        break;
        
        case ST_OPEN_OVERRIDE:
            if (TMR_HasTripped_ms(&(obj->tmr)) || (obj->ovrRequest == OVR_RQST_EXIT))
            {
                obj->ovrRequest = OVR_RQST_NONE;
                changeStateTo(ST_OPEN, obj);
                printf("RCD: %s open override expired.\r\n", obj->name);
            }
            
        break;
        
        case ST_CLOSED_OVERRIDE:
            if (TMR_HasTripped_ms(&(obj->tmr)))
            {
                changeStateTo(ST_CLOSED, obj);
                printf("RCD: %s close override expired.\r\n", obj->name);
            }
        break;
        
    }
    
}



static void changeStateTo(T_State newState, T_RCD_Controller* obj)
{
    switch (newState)
    {
        case ST_OPEN:
            RLY_RequestAction(obj->pRelay, false);
        break;
        
        case ST_CLOSED:
            RLY_RequestAction(obj->pRelay, true);
        break;
        
        case ST_OPEN_COOLOFF:
            RLY_RequestAction(obj->pRelay, false);
            TMR_StartOneshot(&(obj->tmr), configValues.minRelayOpenTime_ms);
        break;
        
        case ST_OPEN_OVERRIDE:
            RLY_RequestAction(obj->pRelay, false);
            TMR_StartOneshot(&(obj->tmr), configValues.overrideTimeout_ms);
        break;
        
        case ST_CLOSED_OVERRIDE:
            RLY_RequestAction(obj->pRelay, true);
            TMR_StartOneshot(&(obj->tmr), configValues.overrideTimeout_ms);
        break;
    }
    
    obj->currentState = newState;
}

static const char* state2str(T_State state)
{
    switch (state)
    {
        case ST_OPEN: return "Open";
        case ST_CLOSED: return "Closed";
        case ST_OPEN_COOLOFF:  return "Open (Cooloff)";
        case ST_OPEN_OVERRIDE: return "Open (OVR)";
        case ST_CLOSED_OVERRIDE: return "Closed (OVR)";
        
        default: return "???";
    }
}
const char* RCD_GetStateStr(const T_RCD_Controller* obj)
{
    return state2str(obj->currentState);
}


uint8_t RCD_ChgState()
{
    return 0;//(chgController.state);
}

uint8_t RCD_DisState()
{
    return 0;//(disController.state);
}


static T_RCD_Controller* lookupByFirstLetter(char c)
{
    uint8_t ii =0;
    for (ii = 0; ii < numControllers; ii++)
    {
        if (tolower(controllerList[ii].name[0]) == tolower(c))
        {
            return &(controllerList[ii]);
        }
    }
    
    return NULL;
}

//Handle command line inputs that had first token 'relay'
void RCD_ProcessInput(const char *tokens[], uint8_t nTokens)
{
    
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("config\tView/adjust module configuration.");
        puts("list\tPrint all relay controller states.");
        puts("open [name]\tForce specified relay open. First letter ok.");
        puts("close [name]\tForce specified relay closed. First letter ok.");
        puts("auto\tExit override and return control of relays to BMS.");
        
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
        CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, tokens, nTokens);
    }
    else if (stricmp(tokens[0], "list") == 0)
    {
        puts("Name\tState");
        uint8_t ii =0;
        for (ii = 0; ii < numControllers; ii++)
        {
            printf("[%d] %s\t%s\r\n", ii, controllerList[ii].name, state2str(controllerList[ii].currentState));
        }
      
    }
    else if (stricmp(tokens[0], STR_open) == 0)
    {
        if (nTokens == 2)
        {
            T_RCD_Controller* obj = lookupByFirstLetter(tokens[1][0]);
            if (obj)
            {
                obj->ovrRequest = OVR_RQST_OPEN;
            }
            else
            {
                puts("Unable to find controller.\r\n");
            }
            
        }
        else
        {
            puts("open [name]");
        }
    }
    else if (stricmp(tokens[0], STR_close) == 0)
    {
        if (nTokens == 2)
        {
            T_RCD_Controller* obj = lookupByFirstLetter(tokens[1][0]);
            if (obj)
            {
                obj->ovrRequest = OVR_RQST_CLOSE;
            }
            else
            {
                puts("Unable to find controller.\r\n");
            }
            
        }
        else
        {
            puts("close [name]");
        }
    }
    else if (stricmp(tokens[0], STR_auto) == 0)
    {
        uint8_t ii =0;
        for (ii = 0; ii < numControllers; ii++)
        {
            controllerList[ii].ovrRequest = OVR_RQST_EXIT;
        }
    }

    else
    {
        //Print error string.
        putchar('?');
    }
}
