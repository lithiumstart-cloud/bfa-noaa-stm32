/**
 * @file bms.c
 * @brief Implements "BMS Crunch" business logic.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-04-22
 *
**/

//############################# MODULE INCLUDES ##############################

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#include <bluflex_core.h>
#include <bms.h>
#include <noaa_bms.h>
#include <rcd.h>
//<SRB>### MAY NOT BE NEEDED ####>    #include "noaa_v5_0.h"

//############################ MODULE DEFINES ################################


//########################### MODULE VARIABLES ################################

static struct
{
    uint32_t bmsRunPeriod_ms; ///< Period between calls of crunch logic, ms
    uint32_t balanceMinDelta_mV; ///< Minimum difference between cells before balancing will take place. (mV)
    uint32_t balanceMin_mV; ///< Minimum cell voltage before balancing will take place. (mV)
    uint32_t stopChargingThresh_mV;
    uint32_t startChargingThresh_mV;
    uint32_t stopDischargingThresh_mV;
    uint32_t startDischargingThresh_mV;
    int32_t  minTempForCharge_dC;
    
} configValues = 
{
    .bmsRunPeriod_ms = 50, ///< Period between calls of crunch logic, ms
    .balanceMinDelta_mV = 50, ///< Minimum difference between cells before balancing will take place. (mV)
    .balanceMin_mV = 4000, ///< Minimum cell voltage before balancing will take place. (mV)
    .stopChargingThresh_mV = 4150, ///< When any cell voltage rises above limit, charging is disabled.
    .startChargingThresh_mV = 4125, ///< When all cell voltage drop below this limit, charging is renabled.
    .stopDischargingThresh_mV = 3200,///< When any cell voltage drops below this limit, discharging is disabled.
    .startDischargingThresh_mV  = 3250,///< When all cell voltages rises above this limit, discharging is renabled.
    .minTempForCharge_dC = 3,
    
};

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "bmsRunPeriod_ms",
    "balMinDelta_mV",
    "balMin_mV",
    "stopCharge_mV",
    "startCharge_mV",
    "stopDischg_mV",
    "startDischg_mV",
    "minTempForChg_dC",
};


static T_CoreSignals coreSigs;
static uint32_t packVoltage_mV;
static T_Timer tmr;

typedef enum
{
   ST_WAITING_FOR_DAQ_CALIBRATION,
   ST_MONITORING,
   ST_ERROR,

} T_State;

static T_State currentState;


//Drawing change variables
static bool isFirstDraw;

static uint8_t balancingMask;


struct
{
    T_SignalNode* pNode;
    uint8_t cellInx;
    uint32_t cellVltg_mV;
} maxCellInfo;

static T_BMS_Config bmsSigs ;


typedef enum
{
    ST_LP_PINIT = 0,
    ST_LP_IO_ON,
    ST_LP_IO_OFF
} T_LP_State;


static bool lpButtonPushed = false;
static T_LP_State lpIoState;
static T_Timer lpTmr;

static T_RCD_Controller* chgController;
static T_RCD_Controller* disController;
static T_LED* relayLed;
static T_LED* lowPowerLed;
static bool dischargeShortFlag = 0;
//############################## PROTOTYPES ####################################
//static T_LMode alarmLevel2LED(uint8_t worst);


static bool areThereChargeErrors();
static bool chgCvDisable(void);
static bool chgCvReenable(void);
static bool chgTempDisable(void);
static bool chgTempReenable(void);
static bool areThereDischargeErrors();
static bool disCvDisable(void);
static bool disCvReenable(void);
static bool disTempDisable(void);
static bool disTempReenable(void);
static void updateRelayLed(void);
static void updateLowPowerSm(void);
//############################ MODULE CODE ###################################


void BMS_Init(const T_CoreSignals* pCoreSigs,  T_BMS_Config* pBmsConfig)
{    
    balancingMask = 0x00;
    currentState = ST_WAITING_FOR_DAQ_CALIBRATION;
    packVoltage_mV = 0;
 
    lpIoState = ST_LP_PINIT;
    
    if (pCoreSigs && pBmsConfig)
    {
        coreSigs = *pCoreSigs;
        bmsSigs = *pBmsConfig;
    }
    else
    {
        puts("BMS: Core sigs null!");
    }

    chgController = RCD_Create("CHG", bmsSigs.pChg, areThereChargeErrors, chgCvDisable, chgCvReenable, chgTempDisable, chgTempReenable );
    disController = RCD_Create("DIS", bmsSigs.pDis, areThereDischargeErrors, disCvDisable, disCvReenable, disTempDisable, disTempReenable );
    
    
}

bool BMS_Update(void)
{ 
    updateRelayLed();
    updateLowPowerSm();
    
    switch (currentState)
    {
        case ST_WAITING_FOR_DAQ_CALIBRATION:
            if (SIG_IsReady())
            {
                currentState = ST_MONITORING;
                TMR_StartRepeating(&tmr, configValues.bmsRunPeriod_ms);
            }
        break;

        case ST_MONITORING:
            if (TMR_HasTripped_ms(&tmr))
            {
                if (!(coreSigs.nVltgRoot && coreSigs.nCurrRoot && coreSigs.nTempRoot))
                {
                    puts("Uninit nodes in BMS!");
                    //throw error about uninit'd nodes in BMS
                    return false;
                }


                /*
                 * Update pack voltage, temperatures, and current readings from
                 * signal tree.
                 */
                packVoltage_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_SUM);

                RCD_Update(chgController);
                RCD_Update(disController);

                

                //### Balancing ################################################
                /* We are only allowed to go in balancing mode if all
                 * temperatures are NOT "stop". If we do balance, it will only
                 * be 1 cell at at time. This avoids the "no two adjacent cells
                 * are allowed to be balancing" limitation of the TI AFE.
                 */
                if ( (SIG_GetAlarmNow(coreSigs.nTempRoot) != ALARM_LVL_STOP) )
                {

                    //Iterate over all cells, and find the one with the max
                    //voltage.

                    uint8_t inx = 0;


                    maxCellInfo.pNode = NULL;
                    maxCellInfo.cellInx = 0;
                    maxCellInfo.cellVltg_mV = 0;

                    T_SignalNode* thisChild = SIG_GetFirstChild(coreSigs.nVltgRoot);
                    while (thisChild)
                    {
                        uint32_t this_mV = SIG_GetAvg_U32(thisChild);

                        if ( this_mV > maxCellInfo.cellVltg_mV)
                        {
                            maxCellInfo.cellVltg_mV = this_mV;
                            maxCellInfo.cellInx = inx;
                            maxCellInfo.pNode = thisChild;
                        }

                        thisChild = SIG_GetNextSibling(thisChild);
                        inx++;
                    }

                    //Now, see if it qualifies for balancing.
                    bool cond1 = maxCellInfo.cellVltg_mV > (SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN) + configValues.balanceMinDelta_mV); //Balancing candidate must be higher than lowest cell by given delta
                    bool cond2 = (maxCellInfo.cellVltg_mV > configValues.balanceMin_mV); //This cell must be above threshold
                    if (cond1 && cond2)
                    {
                        //call afe to balance this cell
                    	AFE_SetBalanceMask(1 << maxCellInfo.cellInx);
                    }
                    else
                    {
                    	AFE_SetBalanceMask(0x00); //disable balancing
                    }
                }
    
            }//end event guard
        break;


        case ST_ERROR:
            //Just sit here until power cycle.
            break;

        default:
            LOG_Report(LOG_MOD_BMS, LOG_LVL_FATAL,  LOG_MSG_UNHANDLED_STATE, currentState);
            currentState = ST_ERROR;
            break;
    }

    

    return true; //Always ready to sleep

}

//### Charge Controller Trigger Functions

static bool areThereChargeErrors()
{
    if (SIG_GetAlarmNow(coreSigs.nTempRoot) == ALARM_LVL_STOP)
    {
        return true;
    }
    
    if (SIG_GetAlarmNow(coreSigs.nCurrRoot) == ALARM_LVL_STOP)
    {
        if (SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_AVG) > 0)
        {
            return true;
        }
    }
    return false;
}

static bool chgCvDisable(void)
{
    uint32_t max_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MAX);
    return (max_mV > configValues.stopChargingThresh_mV);
}

static bool chgCvReenable(void)
{
    uint32_t max_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MAX);
    return (max_mV < configValues.startChargingThresh_mV);
}

static bool chgTempDisable(void)
{
    int8_t minTemp_dC = SIG_GetData_I32(bmsSigs.nCellTemps, SIG_RQST_MIN);
    return (minTemp_dC < configValues.minTempForCharge_dC);
}

static bool chgTempReenable(void)
{
    int8_t minTemp_dC = SIG_GetData_I32(bmsSigs.nCellTemps, SIG_RQST_MIN);
    return (minTemp_dC > configValues.minTempForCharge_dC);
}
//### Discharge Controller Trigger Functions
static bool areThereDischargeErrors()
{
    if (dischargeShortFlag)
    {
        dischargeShortFlag = 0;
        return true;
    }
    
    
    if (SIG_GetAlarmNow(coreSigs.nTempRoot) == ALARM_LVL_STOP)
    {
        return true;
    }
    
    if (SIG_GetAlarmNow(coreSigs.nCurrRoot) == ALARM_LVL_STOP)
    {
        if (SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_AVG) < 0)
        {
            return true;
        }
    }
    
    
        
    
    return false;
}

static bool disCvDisable(void)
{
    uint32_t min_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN);
    return (min_mV < configValues.stopDischargingThresh_mV);
}

static bool disCvReenable(void)
{
    uint32_t min_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN);
    return (min_mV > configValues.startDischargingThresh_mV);
}

static bool disTempDisable(void)
{
    return false;
}

static bool disTempReenable(void)
{
    return true;
}

void BMS_SetDischargeShortCircuitFlag_fromISR(void)
{
    dischargeShortFlag = 1;
}

void BMS_SetRelayStatusLed(T_LED* _led)
{
    relayLed = _led;
}

void BMS_AssignLowPowerLed(T_LED* _led)
{
    lowPowerLed = _led;
}
static void updateRelayLed()
{
    static T_LMode lastLedState = LMODE_OFF;
    T_LMode thisLedState;
    if (!relayLed)
    {
        return;
    }
    
    
    bool chgClosed = RLY_IsClosed(bmsSigs.pChg);
    bool disClosed = RLY_IsClosed(bmsSigs.pDis);
    
    if (disClosed)
    {
        if (chgClosed)
        {
            thisLedState = LMODE_WINK;
        }
        else
        {
            thisLedState = LMODE_BLINK_DOUBLE;
        }
    }
    else if (chgClosed)
    {
        thisLedState = LMODE_BLIP;
    }
    else
    {
        thisLedState = LMODE_OFF;
    }   
    
    if (thisLedState != lastLedState)
    {
        LED_SetMode(relayLed, thisLedState);
        lastLedState = thisLedState;
    }
}

//Low Power LED Handler


void BMS_ProcessLowPowerButtonPush()
{
    lpButtonPushed = true;
}

#define TMR_1_MINUTE  (60*1000)
#define TMR_1_HOUR    (60*TMR_1_MINUTE)


void resetLowPowerSm(void)
{
	TMR_Stop(&lpTmr);
	lpIoState = ST_LP_PINIT;
}

static void updateLowPowerSm()
{
    switch (lpIoState)
    {
        
        case ST_LP_PINIT:
            TMR_StartOneshot(&lpTmr, TMR_1_HOUR);  // Set for 1 hour
            lpIoState = ST_LP_IO_ON;
        break;
             
        case ST_LP_IO_ON:
            if (TMR_HasTripped_ms(&lpTmr) || lpButtonPushed)
            {
                lpButtonPushed = 0;
                puts("Going into LED sleep mode!");
                lpIoState = ST_LP_IO_OFF;
                
                LED_SetMode(lowPowerLed, LMODE_BLIP_SLOW);
                LED_StashAndLockOthers(lowPowerLed);

//                DisableRS232();	 	// Turn off UARTS and XCVR
            }
        break;
        
        case ST_LP_IO_OFF:
            if (lpButtonPushed)
            {
                lpButtonPushed = 0;
                LED_SetMode(lowPowerLed, LMODE_BLINK_SLOW);
                LED_UnstashAndUnlockAll();
                lpIoState = ST_LP_IO_ON;

                TMR_StartOneshot(&lpTmr, TMR_1_HOUR);  // Set for 1 hour

//                EnableRS232();	 	// Turn on UARTS and XCVR

				puts("LEDs back on!");
            }
        break;
    }
}


//### Command Line Interface ###################################################
//Handle command line inputs that had first token 'bms'
void BMS_ProcessInput(const char *tokens[], uint8_t nTokens)
{   
    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("config <ind> <val>");
        puts("nquick");
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
      CLI_ProcessConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, tokens, nTokens);
    }
    else if (stricmp(tokens[0], "quick") == 0)
    {
        T_SignalNode* thisChild = SIG_GetFirstChild(coreSigs.nVltgRoot);
        while (thisChild)
        {
            printf("%lu ", SIG_GetAvg_U32(thisChild));
            thisChild = SIG_GetNextSibling(thisChild);
        }

         thisChild = SIG_GetFirstChild(coreSigs.nTempRoot);
        while (thisChild)
        {
            printf("%ld ", SIG_GetData_I32(thisChild,SIG_RQST_AVG));
            thisChild = SIG_GetNextSibling(thisChild);
        }

         printf("%ld", SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_AVG));
    }
    else
    {
        //Print error string.
        putchar('?');
    }
}




//############################## DRAW FUNCTIONS ##############################

void BMS_DrawInit()
{ 
    char 			prtBfr[30] = {0};

    MCU_HW_ClearWDT();

    //Print title
	VT100_CLEAR_SCREEN();
    VT100_MoveCursorTo(1,1);
    
    VT100_PrintUnderline("\t\tBMS DASHBOARD\n");

    // MASTER ALARM
    VT100_MoveCursorTo(2,1);
    VT100_PrintBold("MASTER");


    // CELLS
    VT100_MoveCursorTo(5,1);
    VT100_PrintBold("CELLS");
    
    uint8_t ii;
    for (ii = 0; ii < SIG_GetNumChildren(coreSigs.nVltgRoot); ii++)
    {
        sprintf(prtBfr,"\tCell%d", ii + 1);
        putstr(prtBfr);
    }

    VT100_MoveCursorTo(6,1);
    putstr("Volts");

    VT100_MoveCursorTo(7,1);
    putstr("Bal?");


    // TEMPS
    VT100_MoveCursorTo(9,1);
    VT100_PrintBold("TEMPS");
    T_SignalNode* child = SIG_GetFirstChild(coreSigs.nTempRoot);
    while (child)
    {
        sprintf(prtBfr,"\t%s", SIG_GetName(child));
        putstr(prtBfr);
        child = SIG_GetNextSibling(child);
    }

    VT100_MoveCursorTo(10,1);
    putstr(" dC");
    

    // PACK
    VT100_MoveCursorTo(13,1);
    VT100_PrintBold("PACK");

    VT100_MoveCursorTo(14,1);
    putstr("Amps");

    VT100_MoveCursorTo(15,1);
    putstr("Volts");


    // RELAYS
    VT100_MoveCursorTo(17,1);
    VT100_PrintBold("RELAYS");

    VT100_MoveCursorTo(18,1);
	VT100_CLEAR_LINE();
	RLY_PrintNames();

    // SOC
    VT100_MoveCursorTo(21,1);
    VT100_PrintBold(STR_soc);

    isFirstDraw = 1;
}




void BMS_DrawUpdate()
{
	T_AlarmBitmap	thisAlarm  = {0xFF};
    T_SignalNode*	child      = NULL;
    static uint8_t 	rly_old    = 0xFF;
	static uint8_t 	soc_old    = 0xFF;
    uint8_t 		rly_now    = 0;
    uint8_t 		soc_now    = 0;
    char 			prtBfr[30] = {0};

    // MASTER ALARM
	VT100_MoveCursorTo(3,1);
	thisAlarm = SIG_GetAlarm(coreSigs.nRoot);
	if (thisAlarm.hasChanged || isFirstDraw)
	{
		sprintf(prtBfr,">%s<",Alarm_Level2Str(thisAlarm.now));
		putstr(prtBfr);
	}


    // CELLS
	VT100_MoveCursorTo(6,9);
    VT100_CLEAR_LINE();

    child = SIG_GetFirstChild(coreSigs.nVltgRoot);
    while (child)
    {
        PrintAs_Millis_U16_f2(SIG_GetAvg_U32(child));
        child = SIG_GetNextSibling(child);
        SEND_TAB();
    }
    
	VT100_MoveCursorTo(7,9);
    VT100_CLEAR_LINE();

    uint8_t mask = AFE_GetBalanceMask();

    for (uint8_t ii =0; ii < 3; ii++)
    {
    	sprintf(prtBfr,"%c\t", mask & (1 << ii) ? 'X' : '.');
    	putstr(prtBfr);
    }


    //TEMPS
    VT100_MoveCursorTo(10,9);
	VT100_CLEAR_LINE();

    child = SIG_GetFirstChild(coreSigs.nTempRoot);
    while (child)
    {
    	sprintf(prtBfr,"%ld\t", SIG_GetData_I32(child, SIG_RQST_AVG));
    	putstr(prtBfr);
        child = SIG_GetNextSibling(child);
    }
    
    VT100_MoveCursorTo(11,9);
	VT100_CLEAR_LINE();

	child = SIG_GetFirstChild(coreSigs.nTempRoot);
    while (child)
    {
    	sprintf(prtBfr,"%s\t", Alarm_Level2Str(SIG_GetAlarmNow(child)));
    	putstr(prtBfr);
        child = SIG_GetNextSibling(child);
    }


    // PACK
    VT100_MoveCursorTo(14,9);
    VT100_CLEAR_LINE();

    PrintAs_Millis_I16_f2(SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_AVG));
	sprintf(prtBfr,"\t%s", Alarm_Level2Str(SIG_GetAlarmNow(coreSigs.nCurrRoot)));
	putstr(prtBfr);

    VT100_MoveCursorTo(15,9);
	VT100_CLEAR_LINE();

    PrintAs_Millis_U16_f2(packVoltage_mV);
	sprintf(prtBfr,"\t%s", Alarm_Level2Str(SIG_GetAlarmNow(coreSigs.nVltgRoot)));
	putstr(prtBfr);


    // RELAYS
    VT100_MoveCursorTo(19,1);
    rly_now = RLY_GetRelayMask();
    if (rly_now != rly_old || isFirstDraw)
    {
    	VT100_CLEAR_LINE();
        rly_old = rly_now;

        uint8_t ii;
        for (ii =0; ii<RLY_GetRelayCount(); ii++)
        {
        	sprintf(prtBfr,"%c\t", rly_now & (1 << ii) ? 'X' : 'O');
        	putstr(prtBfr);
        }
    }


    // SOC
    VT100_MoveCursorTo(21,9);
    soc_now = SIG_GetAvg_U32(coreSigs.nSoc);
    if (soc_now != soc_old || isFirstDraw)
    {
    	VT100_CLEAR_LINE();
        soc_old = soc_now;

        sprintf(prtBfr,"%u%%\t", soc_now);
    	putstr(prtBfr);

    	PrintAs_Scaled_U32(SOC_GetRemainingCharge_dAh(), 1);
        putstr("Ah");
    }

    // PROGRESS BAR
    VT100_MoveCursorTo(26,1);
    CLI_DrawProgressBar();

    isFirstDraw = 0;
}








































































//void BMS_DrawUpdate()
//{
//	VT100_MoveCursorTo(3,0);
//	VT100_CLEAR_LINE();
//
//
//	VT100_MoveCursorTo(6,8);
//    VT100_CLEAR_LINE();
//
//
//
//	VT100_MoveCursorTo(7,8);
//    VT100_CLEAR_LINE();
//
//
//    //Temps
//    VT100_MoveCursorTo(10,8);
//	  VT100_CLEAR_LINE();
//
//
//    NEW_LINE();
//
//
//    //Current
//    VT100_MoveCursorTo(15,8);
//	VT100_CLEAR_LINE();

//
//    //Relays
//    VT100_MoveCursorTo(18,8);
//	VT100_CLEAR_LINE();
//	putstr(RCD_GetStateStr(chgController));
//
//    VT100_MoveCursorTo(19,8);
//	VT100_CLEAR_LINE();
//
//
//    //SoC
//    VT100_MoveCursorTo(21,8);
//	VT100_CLEAR_LINE();
//
//
//    CLI_DrawProgressBar();
//
//
//    isFirstDraw = 0;
//}



//void BMS_DrawUpdate()
//{
//
//    putstr("\n\n\n\t");
//    printf(">%s<", Alarm_Level2Str(SIG_GetAlarmNow(coreSigs.nRoot)) );
//
//    VT100_PrintClearLinePrint("\r\n\t", "UNKNOWN");
//    VT100_PrintClearLinePrint("\r\n\t", "UNKNOWN");
//
//    uint32_t maxCell_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MAX);
//    uint32_t minCell_mV = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_MIN);
//    printf("\t(%d mV)", maxCell_mV - minCell_mV);
//
//    putstr("\r\n\n\n\t");
//
//
//    //Current
//
//    VT100_CLEAR_LINE();
//    PrintAs_Millis_I32_f2(SIG_GetData_I32(coreSigs.nCurrRoot, SIG_RQST_SUM));
//    SEND_TAB();
//    putstr(Alarm_Level2Str(SIG_GetAlarmNow(coreSigs.nCurrRoot)));
//
//    //Voltage
//    NEW_LINE_TAB();
//    VT100_CLEAR_LINE();
//    uint32_t sum = SIG_GetData_U32(coreSigs.nVltgRoot, SIG_RQST_SUM);
//    if (configValues.treatSubpacksAsStrings)
//    {
//        //Print pack voltage of average of strings
//        PrintAs_Millis_U32_f2(sum/SIG_GetNumChildren(coreSigs.nVltgRoot));
//    }
//    else
//    {
//        PrintAs_Millis_U32_f2(sum);
//    }
//    SEND_TAB();
//    putstr(Alarm_Level2Str(SIG_GetAlarmNow(coreSigs.nVltgRoot)));
//    SEND_TAB();
//
//
//    //Max Temp
//    NEW_LINE_TAB();
//    VT100_CLEAR_LINE();
//    PrintAs_Dec_I32(SIG_GetData_I32(coreSigs.nTempRoot, SIG_RQST_MAX), 0);
//    SEND_TAB();
//    putstr(Alarm_Level2Str(SIG_GetAlarmNow(coreSigs.nTempRoot)));
//
//    //Power
//    NEW_LINE_TAB();
//    VT100_CLEAR_LINE();
//    PrintAs_Millis_I64_f2(1234);
//    putstr("\r\n\t");
//
//
//    //SoC
//    VT100_CLEAR_LINE();
//    PrintAs_Dec_U32(SIG_GetAvg_U32(coreSigs.nSoc), 0);
//    NEW_LINE();
//
//
//
//    VT100_RelativeMoveCursor(VT100_CD__MOVE_DOWN, 3 + drawVars.subpackStartDrawInx);
//
//
//
//    //We start printing where we left the index last time
//    uint8_t ii;
//    for (ii = 0; ii < configValues.subpacksToDrawPerUpdate; ii++)
//    {
//        SEND_TAB();
//        VT100_CLEAR_LINE();
//        if (configValues.treatSubpacksAsStrings)
//        {
//            PrintAs_Millis_U32_f2(SIG_GetData_U32(drawVars.subpackCvs, SIG_RQST_SUM));
//            SEND_TAB();
//        }
//        PrintAs_Millis_U32_f2(SIG_GetData_U32(drawVars.subpackCvs, SIG_RQST_MIN));
//        SEND_TAB();
//        PrintAs_Millis_U32_f2(SIG_GetData_U32(drawVars.subpackCvs, SIG_RQST_AVG));
//        SEND_TAB();
//        PrintAs_Millis_U32_f2(SIG_GetData_U32(drawVars.subpackCvs, SIG_RQST_MAX));
//
//        if (drawVars.subpackBts)
//        {
//            SEND_TAB();
//            SIG_PrintData(drawVars.subpackBts, SIG_RQST_MIN);
//            SEND_TAB();
//            SIG_PrintData(drawVars.subpackBts, SIG_RQST_MAX);
//        }
//        else
//        {
//            putstr("\t--\t--");
//        }
//
//        if (drawVars.subpackCts)
//        {
//            SEND_TAB();
//            SIG_PrintData(drawVars.subpackCts, SIG_RQST_MIN);
//            SEND_TAB();
//            SIG_PrintData(drawVars.subpackCts, SIG_RQST_MAX);
//        }
//        else
//        {
//            putstr("\t--\t--");
//        }
//
//
//
//
//        NEW_LINE();
//
//
//        stepToNextSubpack();
//
//        if (drawVars.subpackCvs == NULL)
//        {
//            //we hit the end, no more to draw!
//            resetSubpackStepper();
//            break;
//        }
//    }
//
//
//    VT100_MoveCursorTo(drawVars.numSubpacks + 17 , 0);
//    CLI_DrawProgressBar();
//    NEW_LINE();
//
//
//    isFirstDraw = 0;
//
//}

