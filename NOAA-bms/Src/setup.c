/**
 * @file setup.c
 * @brief NOAA Setup options.
 * @copyright 20146 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-07-01
 *
**/
//############################## INCLUDES ######################################

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include <bluflex_core.h>

#include "setup.h"
//#include "noaa_v5_0.h"
//############################## DEFINES ######################################

//############################## TYPES/VARS ####################################

static const uint32_t configSignMask = 0;

static struct
{
       bool isHeaterHwExpected;

} configValues =
{
    .isHeaterHwExpected = true,
};

#define NUM_CONFIG_VALUES (sizeof(configValues)/sizeof(uint32_t))

static const char* configStrs[NUM_CONFIG_VALUES] =
{
    "HeaterHwExpected?",
};

//######################### MODULE VARIABLES ###################################

static T_MemAddr vAddrConfig;

//######################### MODULE FUNCTIONS ###################################

//########################### PUBLIC CODE ######################################

void SETUP_Init(void)
{

    vAddrConfig = MEM_AllocateData(NUM_CONFIG_VALUES);
    MEM_LoadDefaultData("Setup", vAddrConfig, (uint32_t*) &configValues, NUM_CONFIG_VALUES);
}


void SETUP_ProcessInput(const char *tokens[], uint8_t nTokens)
{

    if (nTokens == 0 || tokens[0][0] == '?')
    {
        puts("config\tView/adjust module configuration.");
        puts("save\tSave config table to NVRAM.");
    }
    else if (stricmp(tokens[0], "config") == 0)
    {
        CLI_ProcessSignedConfig(&configStrs[0],  (uint32_t *) &configValues, NUM_CONFIG_VALUES, configSignMask, tokens, nTokens);
    }
    else if (stricmp(tokens[0], "save") == 0)
    {
        if (MEM_WriteDataFromRamToNvm(vAddrConfig, (uint32_t*) &configValues, NUM_CONFIG_VALUES))
        {
            puts("Table successfully saved to NVRAM. Reboot necessary for changes to take effect.");
        }
        else
        {
            puts("Failed to save table to NVRAM!");
        }
    }
    else
    {
        //Print error string.
        putchar('?');
    }
}

bool SETUP_IsHeaterHwExpected(void)
{
    return configValues.isHeaterHwExpected;
}
