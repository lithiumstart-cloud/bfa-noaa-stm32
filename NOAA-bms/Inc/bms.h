/**
 * @file bms.h
 * @brief Implements "BMS Crunch" business logic.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-04-22
 *
**/

#ifndef BMS_H
#define BMS_H


#include <stdint.h>
#include <stdbool.h>

#include <bluflex_core.h>



typedef struct
{
    T_SignalNode* nCellTemps;
    T_Relay* pChg;
    T_Relay* pDis;
} T_BMS_Config;

/**
 * Intialization function to NOAA BMS.j
 * @param  coreSigs
 * @param bmsConfig
 */
void BMS_Init(const T_CoreSignals*,  T_BMS_Config*);


/**
 * Process system events to step the BMS state machine forward. Call as fast
 * as possible.
 * @return Did the state machine do something?
 */
bool BMS_Update(void);


/**
 * Reset the IO Low Power state machine and restaart the timer
 * @return nothing
 */
void resetLowPowerSm(void);

/**
 * Determine whether or not it is OK to measure cells right now.
 * @return Boolean indicating whether or not it is OK to measure cells.
 */
bool BMS_IsMeasureCellsOK(void);

void BMS_SetRelayStatusLed(T_LED* _led);
void BMS_AssignLowPowerLed(T_LED* _led);
void BMS_SetDischargeShortCircuitFlag_fromISR(void);
void BMS_ProcessLowPowerButtonPush(void);
/**
 * Initialize the fullscreen draw mode for the BMS.
 */
void BMS_DrawInit(void);

/**
 * Print the latest information to the debug screen for fullscreen BMS mode.
 */
void BMS_DrawUpdate(void);


/**
 * Process command line inputs for this module.
 * @param buf Two-dimensional array of CL inputs.
 * @param nArgs The number of tokens in the input.
 */
void BMS_ProcessInput(const char *tokens[], uint8_t nTokens);


#endif

