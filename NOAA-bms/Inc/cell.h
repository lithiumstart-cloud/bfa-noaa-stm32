/*
 * File:   cell.h
 * Author: akessler
 *
 * Created on May 9, 2016, 2:56 PM
 */

#ifndef CELL_H
#define	CELL_H

#include <bluflex_core.h>




typedef enum __attribute__((packed))
{
    CELL_TYPE_PANASONIC_NCR18650 = 0,
    CELL_TYPE_SAMSUNG_INR18650_30Q,
    CELL_TYPE_A123_26650B,
    CELL_TYPE_A123_26700M1,
    CELL_TYPE_LG_18650_HG2,
    
    CELL_TYPE_24V_LEAD_ACID,
    
            
    CELL_TYPE_NUM_CELL_TYPES

}  T_Cell_Type;

typedef enum __attribute__((packed))
{
    CELL_USAGE_BASIC = 0,
    CELL_USAGE_WIDE,
            
    CELL_USAGE_NUM_USAGE_MODES
    
} T_Cell_UsageMode;


typedef struct
{
    uint16_t cclMaxAllowableCellVoltage_mV;
    uint16_t cclTaperTriggerCellVoltage_mV;
    
    uint16_t dclMinAllowableCellVoltage_mV;
    uint16_t dclTaperTriggerCellVoltage_mV;
        
    uint16_t cvlNominalCellVoltage_mV;
    uint16_t dvlNominalCellVoltage_mV;

} T_CELL_LIM_Settings;


typedef struct
{
    uint16_t full_mV; ///< When min cell voltages exceeds this, SoC is 100%
    uint16_t empty_mV; ///< When max cell voltage drops below this, SoC is 0%
} __attribute__((packed)) T_CELL_SnapPoints;

const char* CELL_Type2Str(T_Cell_Type c);
const char* CELL_UsageMode2Str(T_Cell_UsageMode c);

bool CELL_SelectProfile(T_Cell_Type, T_Cell_UsageMode);

/**
 * Request what the SoC of a cell with given voltage on dishcarge cycle is.
 * @param cellVltg_mV
 */
uint32_t CELL_LookupDischargeSocFromVltg_Prct_x10(uint32_t cellVltg_mV);

/**
 * * Request what the SoC of a cell with given voltage on charge cycle is.
 * @param cellVltg_mV
 */
uint32_t CELL_LookupChargeSocFromVltg_Prct_x10(uint32_t cellVltg_mV);




void CELL_ProcessInput(const char* tokens[], uint8_t nTokens);

const T_AlarmLimits* CELL_GetCellVoltageAlarmLimits(void);
const T_AlarmLimits* CELL_GetCellTemperatureAlarmLimits(void);
uint16_t CELL_GetReccomendedMinBalancingVoltage_mV(void);
T_CELL_SnapPoints CELL_GetReccomendedSocSnapVoltages(void);
T_CELL_LIM_Settings CELL_GetReccomendedLimSettings(void);

#endif

