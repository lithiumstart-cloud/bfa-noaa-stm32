/**
 * @file setup.h
 * @brief NOAA Setup Manager.
 * @copyright 20146 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-07-01
 *
**/

#ifndef SETUP_H
#define SETUp_H

#include <stdint.h>
#include <stdbool.h>

#include <bluflex_core.h>


void SETUP_Init(void);

void SETUP_ProcessInput(const char *tokens[], uint8_t nTokens);

bool SETUP_IsHeaterHwExpected(void);

#endif
