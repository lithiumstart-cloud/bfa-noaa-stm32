/**
 * @file smbus.h
 * @brief SMBus driver for STM32L4xx
 * @copyright 2017 EP Lithiumstart Division
 * @author A. Kessler
 * @date   2017-08-01
 *
**/

#ifndef SMBUS_H
#define	SMBUS_H

#include <stdint.h>
#include <stdbool.h>
#include <bluflex_core.h>

#define SMBUS_MAX_BLOCK_SIZE (32)
#define SMBUS_HOST_ADDRESS    (0x10)


//Supported SMBus Protocols
typedef enum
{
    SMBUS_TYPE__WRITE_FROM_HOST,	// The host is writing
    SMBUS_TYPE__READ_FROM_HOST,		// The host is reading
} T_SMBUS_Type;


//A structure to hold a single message, with a unique ID for future reference.
typedef struct
{
    uint8_t addr;
    uint8_t cmd;

    uint8_t numBytes;

    uint8_t data[SMBUS_MAX_BLOCK_SIZE+1];  // Max data xfer is 32 bytes plus 1 for PEC byte

    uint8_t pid;

    T_SMBUS_Type cmdType;

    uint8_t crc; //running value

} T_SMBUS_Packet;

/**
 * @brief Module initialization function.
 */
void SMBUS_Init(void);

/**
 * @brief Module update function. Returns TRUE if ok to sleep.
 */
bool SMBUS_Update(void);

/**
 * @brief Command line interface callback.
 */
void SMBUS_ProcessInput(const char* tokens[], uint8_t nArgs);

/**
 * @brief Assign activity LED
 */
void SMB_AssignLED(T_LED* _pLed);


#endif

