/**
 * @file soc.h
 * @brief State of Charge (SoC) tracker
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-02
 *
**/


#ifndef SOC_H
#define SOC_H

#include <stdint.h>
#include <stdbool.h>

#include <bluflex_core.h>

enum
{
    SOC_MAP_CHG_CYCLE=0,
    SOC_MAP_DIS_CYCLE,
    SOC_MAP_NUM_CYCLE_TYPES,
};


typedef struct
{
    uint32_t runPeriod_ms;
    uint32_t designCapacity_mAh; ///< Theoretical capcity of a new pack, in mAh

    int32_t ccCurrThreshP_mA; ///< Minimum current threshold (+) to qualify for CCing
    int32_t ccCurrThreshN_mA;  ///< Minimum current threshold (-) to qualify for CCing
    int32_t absSocLookupCurr_mA; ///< Max magnitude of current for voltage lookup

    uint32_t loSocSnapThresh_mV; ///< Cell voltage belwo which we assume SoC is 0
    uint32_t hiSocSnapThresh_mV; ///< Cell voltage above which we assume SoC is 100%

    uint32_t minTimeForActiveCcOut_ms;
    uint32_t minTimeForActiveCcIn_ms;
    uint32_t minTimeForActiveLkp_ms;

    uint32_t maxSocDeltaPerMin_p01pct; ///< Maximum amount SoC can change per sec, in units of .01% of full charge
    uint32_t minSocDeltaPerLoop_p01pct; ///< Minimum difference between looked up SoC and current SoC to trigger change, in units of .01% of full charge
    
    uint32_t cycleUpper_pct_SoC; ///< Defines the top of a cycle in units of % SoC
    uint32_t cycleLower_pct_SoC;///< Defines the bottom of a cycle in units of % SoC
    
} T_SOC_Config;
/**
 * Initalize the SoC module.
 */
void SOC_Init(T_CoreSignals*, T_SOC_Config*);

/**
 * Update the SoC module with system events. Calls as fast as possible.
 * @return OK to sleep?
 */
bool SOC_Update(void);

/**
 * Get the mode of the SoC module as a string.
 * @return Null-terminated string representing state.
 */
const char* SOC_GetModeStr(void);


//These are not implemented.
uint16_t SOC_GetInsTimeToFull_m(void);
uint16_t SOC_GetInsTimeToEmpty_m(void);
uint16_t SOC_GetAveTimeToFull_m(void);
uint16_t SOC_GetAveTimeToEmpty_m(void);
uint16_t SOC_GetTimeToEmptyAtRate_m(int16_t atRate);
uint16_t SOC_GetTimeToFullAtRate_m(int16_t atRate);


/**
 * Process command line inputs for this module.
 * @param buf Two-dimensional array of CL inputs.
 * @param nArgs The number of tokens in the input.
 */
void SOC_ProcessInput(const char* tokens[], uint8_t nTokens);


/**
 * Initalize fullscreen mode for this module.
 */
void SOC_DrawInit(void);

/**
 * Update fullscreen mode for this module. 
 */
void SOC_DrawUpdate(void);


/**
 * Get the current SoC in dAh (deci-amp-hours).
 * So e.g:  120.0  Ah = 1200 dAh
 *           14.5 Ah =  145 dAh
 * I promise, this is actually useful. 
 * @return SoC in dAh (scaled by 10)
 */
uint32_t SOC_GetRemainingCharge_dAh(void);

/**
 * Get the best estimate of actual full charge capacity of pack.
 * @return Estimated full charge capacit of pack, in dAh
 */
uint32_t SOC_GetFullChargeCapacity_dAh(void);

/**
 * Get the value of constant CCU per Ah.
 * @return CCUs per 1 Ah
 */
uint32_t SOC_GetCcuPerAh(void);

/**
 * Adjust the full charge capacity estimate by the given amount.
 * @param delta_dAh Amount by which to change capacity. Positive increases. 
 */
void SOC_AdjustFullChargeCapacity_dAh(int32_t delta_dAh);

/**
 * Calculate and return ROUND(FullChargeCapacity/DesignCapacity * 100%)
 * @return [0,100] %
 */
uint8_t SOC_GetRelativeCapacity_Prct(void);

/**
 * Save SoC and Capacity to NVRAM.
 * @return was successful
 */
bool SOC_PrepareForShutdown(void);

/**
 * Force outward facing SoC to match what is indicated by voltage lookup, 
 * skipping any rate limiting, etc.
 */
void SOC_SnapToVoltageLookup(void);

/**
 * Manually set the relative SoC based on user input- only use for error recovery
 * and other special conditions. No protections on enforced here besides making
 * sure input is valid percentage.
 * 
 * WARNING! Do not call unless you know what you're doing. 
 * @param soc_prct Relative SoC to set module to. 
 */
void SOC_OverwriteRelativeStateOfCharge(uint8_t soc_prct);

#endif
