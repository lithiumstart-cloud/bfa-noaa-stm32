/**
 * @file htr.h
 * @brief NOAA Heater controller logic.
 * @copyright 20146 by Lithiumstart Inc
 * @author A. Kessler
 * @date   2016-07-01
 *
**/

#ifndef HTR_H
#define HTR_H

#include <stdint.h>
#include <stdbool.h>

#include <bluflex_core.h>

typedef struct
{
    bool heaterHardwareExpected;
    T_Relay* htrA_Relay; ///< Heater A Relay Object
    T_Relay* htrB_Relay; ///< Heater B Relay Object
    T_Relay* chg_Relay; ///< Charge Relay Object
    T_Relay* hbe_Relay; ///< Heater From Battery Enable (HBE) Relay Object
    T_SignalNode* nCellTempRoot;
} T_HTR_Config;

void HTR_Init(T_HTR_Config* pConfig,  T_CoreSignals* pCoreSigs);
bool HTR_Update(void);
void HTR_ProcessInput(const char *tokens[], uint8_t nTokens);

void HTR_DrawInit(void);
void HTR_DrawUpdate(void);
void HTR_DrawInput(uint8_t c);

#endif