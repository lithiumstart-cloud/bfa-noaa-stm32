/**
 * @file rcd.h
 * @brief Relay controller logic.
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-05-02
 *
**/

#ifndef RCD_H
#define RCD_H

#include <stdint.h>
#include <stdbool.h>
#include <bluflex_core.h>

struct xRlyController;
typedef struct xRlyController T_RCD_Controller;

typedef bool (*T_fCheck) (void);

/**
 * 
 * @param pRly
 * @param fStop
 * @param fCvDisable
 * @param fCvReenable
 * @param fTempDisable
 * @param fTempReenable
 * @return 
 */
T_RCD_Controller* RCD_Create( const char* name,
                                T_Relay* pRly, 
                                T_fCheck fStop, 
                                T_fCheck fCvDisable, 
                                T_fCheck fCvReenable,
                                T_fCheck fTempDisable,
                                T_fCheck fTempReenable
                             );



void RCD_Update(T_RCD_Controller *obj);

const char* RCD_GetStateStr(const T_RCD_Controller* obj);

uint8_t RCD_ChgState(void);
uint8_t RCD_DisState(void);


//Handle command line inputs that had first token 'relay'
void RCD_ProcessInput(const char *tokens[], uint8_t nTokens);


#endif