/**
 * @file acc.h
 * @brief Lifetime charge accumulator
 * @copyright 2014 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-08-18
 *
**/


#ifndef ACC_H
#define ACC_H

#include <stdint.h>
#include <stdbool.h>


typedef enum
{
    ACC_IN = 0,
    ACC_OUT,
    ACC_NUM_ACCUMULATORS
} T_AccId;

/**
 * Initalize the Accumulator module.
 */
void ACC_Init(void);

bool ACC_Update(void);

/**
 * Add an amount of charge to the given accumualtor.
 * @param id The ID of the desired accumualtor
 * @param dCCU The amount to change by
 */
void ACC_AddDeltaTo(T_AccId id, uint32_t dCCU);

/**
 * Process command line inputs for this module.
 * @param buf Two-dimensional array of CL inputs.
 * @param nArgs The number of tokens in the input.
 */
void ACC_ProcessInput(const char* tokens[], uint8_t nTokens);

/**
 * Get the number of cycles the battery has completed since Power on
 * @return Number of cycles, integer
 */
uint16_t ACC_GetNumberCycles(void);

#endif
