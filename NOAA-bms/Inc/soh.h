/**
 * @file soh.h
 * @brief State of Health tracker
 * @copyright 2012 by Lithiumstart LLC
 * @author A. Kessler
 * @date   2014-07-16
 *
**/


#ifndef SOH_H
#define SOH_H

#include <stdint.h>
#include <stdbool.h>

#include <bluflex_core.h>


typedef struct
{

  uint32_t runPeriod_ms; //How often SOH state machine update functions are called
  uint32_t numCellsInParallel; //How many cells are in parallel in a single group?
  
  /* SoH Power Parameters */
  int32_t  sohP_currStart_mA; //Current must be more negative than this for SoH_P to kick in
  int32_t  sohP_currStop_mA; //If current must be more negative than this, SoH_P will stop. Should be less than currStart in magnitude
  
  uint32_t sohP_dResMin_uOhm; //Difference in resistance between AVG and MIN cell where tapering of SoH_P starts
  uint32_t sohP_dResMax_uOhm; //Difference in resistance between AVG and MIN cell where SoH_P hits 0%
  
  uint32_t sohP_UpperSoc_Prct; //Top of allowable range for SoH_P monitoring
  uint32_t sohP_LowerSoc_Prct; //Bottom of allowable range for SoH_P monitoring
  
  uint32_t sohP_RisingTau_sec; //Time Constant when SohP is increasing, in seconds
  uint32_t sohP_FallingTau_sec;  //Time Constant when SohP is falling, in seconds
  
  /* SoH Capacity / Single Cell Failure Parameters */
  int32_t  scf_MaxAllowedTemp_dC; //Maximum temp allowabled for a cycle to be eligble to SCF calculation
  uint32_t scf_NeededSoc_Prct; //The SoC must be at or  below this threhold at end of discharge to trigger SCF calculation
  
  uint32_t sohC_NeededSoc_Prct; //The SoC must be at or below this threhold at end of discharge to trigger SOH C calculation. Should be larger than SCF threshold
  uint32_t sohC_maxImbalance_mV; //Max difference between max cell voltage and min cell voltage for pack to be considered "balanced"
  uint32_t sohC_OcvRecoveryTime_ms; //How long we need to wait to get OCV 
  int32_t  sohC_absNoCurrLimit_mA; //what defines "no current", absolute value
  
} T_SOH_Config;

typedef struct
{
  T_SignalNode* soh_capacity;
  T_SignalNode* soh_power;
  T_SignalNode* soh_selfDischarge;
  
  T_SignalNode* nCellTemps; 
  
} T_SOH_Signals;

/**
 * Initalize the SoH module.
 */
void SOH_Init(T_CoreSignals*,T_SOH_Signals*,  T_SOH_Config* );

/**
 * Update the SoH module with system events. Calls as fast as possible.
 * @return OK to sleep?
 */
bool SOH_Update(void);

/**
 * 
 */
void SOH_PrepareForShutdown(void);

void SOH_FillNumFailedCells(uint8_t* dst, uint8_t nBytes);
/**
 * Process command line inputs for this module.
 * @param buf Two-dimensional array of CL inputs.
 * @param nArgs The number of tokens in the input.
 */
void SOH_ProcessInput(const char* tokens[], uint8_t nTokens);

void SOH_DrawInit(void);
void SOH_DrawUpdate(void);
void SOH_DrawInput(uint8_t cin);

#endif
